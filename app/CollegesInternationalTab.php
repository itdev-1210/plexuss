<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ViewDataController;

class CollegesInternationalTab extends Model
{
   	protected $table = 'colleges_international_tabs';

 	protected $fillable = array('college_id',
 								'define_program',
 								'undergrad_total_yearly_cost',
								'undergrad_tuition',
								'undergrad_room_board',
								'undergrad_book_supplies',
								'grad_total_yearly_cost',
								'grad_tuition',
								'grad_room_board',
								'grad_book_supplies',
								'epp_total_yearly_cost',
								'epp_tuition',
								'epp_room_board',
								'epp_book_supplies',
								'undergrad_application_deadline',
								'undergrad_admissions_available',
								'undergrad_application_fee',
								'undergrad_num_of_applicants',
								'undergrad_num_of_admitted',
								'undergrad_num_of_admitted_enrolled',
								'undergrad_scholarship_link',
								'grad_application_deadline',
								'grad_admissions_available',
								'grad_application_fee',
								'grad_num_of_applicants',
								'grad_num_of_admitted',
								'grad_num_of_admitted_enrolled',
								'epp_admissions_available',
								'epp_application_deadline',
								'epp_application_fee',
								'epp_num_of_applicants',
								'epp_num_of_admitted',
								'epp_num_of_admitted_enrolled',
								'undergrad_scholarship_available',
								'undergrad_scholarship_student_received_aid',
								'undergrad_scholarship_avg_financial_aid_given',
								'undergrad_scholarship_requirments',
								'undergrad_scholarship_gpa',
								'undergrad_scholarship_link',
								'grad_scholarship_available',
								'grad_scholarship_student_received_aid',
								'grad_scholarship_avg_financial_aid_given',
								'grad_scholarship_requirments',
								'grad_scholarship_gpa',
								'grad_scholarship_link',
								'epp_scholarship_student_received_aid',
								'epp_scholarship_available',
								'epp_scholarship_avg_financial_aid_given',
								'epp_scholarship_requirments',
								'epp_scholarship_gpa',
								'epp_scholarship_link',
								'undergrad_grade_gpa_min',
								'undergrad_grade_gpa_avg',
								'undergrad_grade_toefl_min',
								'undergrad_grade_toefl_avg',
								'undergrad_grade_ielts_min',
								'undergrad_grade_ielts_avg',
								'undergrad_grade_act_composite_min',
								'undergrad_grade_act_composite_avg',
								'undergrad_grade_act_english_min',
								'undergrad_grade_act_english_avg',
								'undergrad_grade_act_math_min',
								'undergrad_grade_act_math_avg',
								'grad_grade_gpa_min',
								'grad_grade_gpa_avg',
								'grad_grade_toefl_min',
								'grad_grade_toefl_avg',
								'grad_grade_ielts_min',
								'grad_grade_ielts_avg',
								'grad_grade_act_composite_min',
								'grad_grade_act_composite_avg',
								'grad_grade_act_english_min',
								'grad_grade_act_english_avg',
								'grad_grade_act_math_min',
								'grad_grade_act_math_avg',
								'undergrad_grade_gre_writing_min',
								'undergrad_grade_gre_writing_avg',
								'undergrad_grade_gre_verbal_min',
								'undergrad_grade_gre_verbal_avg',
								'undergrad_grade_gre_quant_min',
								'undergrad_grade_gre_quant_avg',
								'undergrad_grade_sat_math_min',
								'undergrad_grade_sat_math_avg',
								'undergrad_grade_sat_reading_min',
								'undergrad_grade_sat_reading_avg',
								'grad_grade_sat_math_min',
								'grad_grade_sat_math_avg',
								'grad_grade_sat_reading_min',
								'grad_grade_sat_reading_avg',
								'grad_grade_gre_writing_min',
								'grad_grade_gre_writing_avg',
								'grad_grade_gre_verbal_min',
								'grad_grade_gre_verbal_avg',
								'grad_grade_gre_quant_min',
								'grad_grade_gre_quant_avg',
								'undergrad_grade_gmat_quant_min',
								'undergrad_grade_gmat_quant_avg',
								'undergrad_grade_gmat_verbal_min',
								'undergrad_grade_gmat_verbal_avg',
								'grad_grade_gmat_quant_min',
								'grad_grade_gmat_quant_avg',
								'grad_grade_gmat_verbal_min',
								'grad_grade_gmat_verbal_avg',
								'undergrad_grade_psat_math_min',
								'undergrad_grade_psat_math_avg',
								'undergrad_grade_psat_reading_min',
								'undergrad_grade_psat_reading_avg',
								'grad_grade_psat_math_min',
								'grad_grade_psat_math_avg',
								'grad_grade_psat_reading_min',
								'grad_grade_psat_reading_avg',
								'epp_grade_act_composite_avg',
								'epp_grade_act_composite_min',
								'epp_grade_act_english_avg',
								'epp_grade_act_english_min',
								'epp_grade_act_math_avg',
								'epp_grade_act_math_min',
								'epp_grade_gmat_quant_avg',
								'epp_grade_gmat_quant_min',
								'epp_grade_gmat_verbal_avg',
								'epp_grade_gmat_verbal_min',
								'epp_grade_gpa_avg',
								'epp_grade_gpa_min',
								'epp_grade_gre_quant_avg',
								'epp_grade_gre_quant_min',
								'epp_grade_gre_verbal_avg',
								'epp_grade_gre_verbal_min',
								'epp_grade_gre_writing_avg',
								'epp_grade_gre_writing_min',
								'epp_grade_ielts_avg',
								'epp_grade_ielts_min',
								'epp_grade_psat_math_avg',
								'epp_grade_psat_math_min',
								'epp_grade_psat_reading_avg',
								'epp_grade_psat_reading_min',
								'epp_grade_sat_math_avg',
								'epp_grade_sat_math_min',
								'epp_grade_sat_reading_avg',
								'epp_grade_sat_reading_min',
								'epp_grade_toefl_avg',
								'epp_grade_toefl_min');

	public function getCollegeInternationalTab($college_id){
		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData(true);

		$cit = CollegesInternationalTab::on('rds1')->where('college_id', $college_id)->first();

		// dd($cit);

		$ret = array();

		$ret['route'] = array();
		$ret['route']['undergrad'] = '/college/'. $data['school_slug'] .'/undergrad';
		$ret['route']['grad']	   = '/college/'. $data['school_slug'] .'/grad';
		$ret['route']['epp'] = '/college/'. $data['school_slug'] .'/epp';

		if (!isset($cit)) {
			return $ret;
		}

		// $ret['define_program'] = isset($cit->define_program) ? $cit->define_program : 'all';
		
		$ret['define_program'] = array();
		$ret['define_program']['epp'] = false;
		$ret['define_program']['undergrad'] = false;
		$ret['define_program']['grad'] = false;

		if (isset($cit->define_program) && ($cit->define_program == 'all' || $cit->define_program == 'grad_epp' || 
				  $cit->define_program == 'undergrad_epp' || $cit->define_program == 'epp' )) {
			$ret['define_program']['epp'] = true;
		}

		if (isset($cit->define_program) && ($cit->define_program == 'all' || $cit->define_program == 'both' || 
				  $cit->define_program == 'undergrad_epp' || $cit->define_program == 'undergrad' )) {
			$ret['define_program']['undergrad'] = true;
		}

		if (isset($cit->define_program) && ($cit->define_program == 'all' || $cit->define_program == 'both' || 
				  $cit->define_program == 'grad_epp' || $cit->define_program == 'grad' )) {
			$ret['define_program']['grad'] = true;
		}
		
		// Header Info tab
		$ret['header_info']['undergrad'] = array();
		$ret['header_info']['grad'] 	 = array(); 
		$ret['header_info']['epp'] 	 = array(); 

		$tmp = array();
		$tmp['undergrad_total_yearly_cost'] = isset($cit->undergrad_total_yearly_cost) ? (int)$cit->undergrad_total_yearly_cost : null;
		$tmp['undergrad_tuition']			= isset($cit->undergrad_tuition) 		   ? (int)$cit->undergrad_tuition : null;
		$tmp['undergrad_room_board']		= isset($cit->undergrad_room_board) 	   ? (int)$cit->undergrad_room_board  : null;
		$tmp['undergrad_book_supplies']		= isset($cit->undergrad_book_supplies)     ? (int)$cit->undergrad_book_supplies : null;

		$ret['header_info']['undergrad'] = $tmp;

		$tmp = array();
		$tmp['grad_total_yearly_cost']  = isset($cit->grad_total_yearly_cost) ? (int)$cit->grad_total_yearly_cost : null;
		$tmp['grad_tuition']			= isset($cit->grad_tuition) 	      ? (int)$cit->grad_tuition : null;
		$tmp['grad_room_board']			= isset($cit->grad_room_board) 		  ? (int)$cit->grad_room_board : null;
		$tmp['grad_book_supplies']		= isset($cit->grad_book_supplies) 	  ? (int)$cit->grad_book_supplies : null;

		$ret['header_info']['grad'] = $tmp;

		$tmp = array();
		$tmp['epp_total_yearly_cost']   = isset($cit->epp_total_yearly_cost) ? (int)$cit->epp_total_yearly_cost : null;
		$tmp['epp_tuition']				= isset($cit->epp_tuition) 			 ? (int)$cit->epp_tuition : null;
		$tmp['epp_room_board']			= isset($cit->epp_room_board) 		 ? (int)$cit->epp_room_board : null;
		$tmp['epp_book_supplies']		= isset($cit->epp_book_supplies) 	 ? (int)$cit->epp_book_supplies : null;

		$ret['header_info']['epp'] = $tmp;


		// Video Testimonials.
		$videos = CollegesInternationalTestimonial::on('rds1')
												  ->where('cit_id', $cit->id)
												  ->get();
		if (isset($videos)) {
			$tmp = array();

			foreach ($videos as $key) {
				$tmp = array();

				$tmp['id']    = (int)$key->id;
				$tmp['title'] = $key->title;
				$tmp['url']   = $key->url;
				$tmp['source']= $key->source;

				if ($tmp['source'] == "youtube") {
					$tmp['embed'] = '<iframe width="100%" height="315" src="https://www.youtube.com/embed/'.$tmp['url'].'" frameborder="0" allowfullscreen></iframe>';
				}elseif ($tmp['source'] == "vimeo") {
					$tmp['embed'] = '<iframe src="https://player.vimeo.com/video/'.$tmp['url'].'" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				}
				$ret['video_testimonials'][] = $tmp;
			}
		}
		
		// Addmission Info
		$ret['admission_info']['undergrad'] = array();
		$ret['admission_info']['grad'] 	 	= array(); 

		if((isset($cit->undergrad_admissions_available) && $cit->undergrad_admissions_available == 1)){
			$undergrad_admissions_available = 'yes';
		}elseif ((isset($cit->undergrad_admissions_available) && $cit->undergrad_admissions_available == -1)) {
			$undergrad_admissions_available = 'no';
		}else{
			$undergrad_admissions_available = 'unknown';
		}

		$tmp = array();
		$tmp['undergrad_application_deadline'] 	  = $cit->undergrad_application_deadline;
		$tmp['undergrad_admissions_available'] 	  = $undergrad_admissions_available;
		$tmp['undergrad_application_fee'] 	   	  = isset($cit->undergrad_application_fee) ? (int)$cit->undergrad_application_fee : null;
		$tmp['undergrad_num_of_applicants']    	  = isset($cit->undergrad_num_of_applicants) ? (int)$cit->undergrad_num_of_applicants : null;
		$tmp['undergrad_num_of_admitted'] 		  = isset($cit->undergrad_num_of_admitted) ? (int)$cit->undergrad_num_of_admitted : null;
		$tmp['undergrad_num_of_admitted_enrolled'] = isset($cit->undergrad_num_of_admitted_enrolled) ? (int)$cit->undergrad_num_of_admitted_enrolled : null;

		$ret['admission_info']['undergrad'] = $tmp;

		$tmp = array();
		if((isset($cit->grad_admissions_available) && $cit->grad_admissions_available == 1)){
			$grad_admissions_available = 'yes';
		}elseif ((isset($cit->grad_admissions_available) && $cit->grad_admissions_available == -1)) {
			$grad_admissions_available = 'no';
		}else{
			$grad_admissions_available = 'unknown';
		}

		$tmp = array();
		$tmp['grad_application_deadline'] 	  = $cit->grad_application_deadline;
		$tmp['grad_admissions_available'] 	  = $grad_admissions_available;
		$tmp['grad_application_fee'] 	   	  = isset($cit->grad_application_fee) ? (int)$cit->grad_application_fee : null;
		$tmp['grad_num_of_applicants']    	  = isset($cit->grad_num_of_applicants) ? (int)$cit->grad_num_of_applicants : null;
		$tmp['grad_num_of_admitted'] 		  = isset($cit->grad_num_of_admitted) ? (int)$cit->grad_num_of_admitted : null;
		$tmp['grad_num_of_admitted_enrolled'] = isset($cit->grad_num_of_admitted_enrolled) ? (int)$cit->grad_num_of_admitted_enrolled : null;

		$ret['admission_info']['grad'] = $tmp;

		$tmp = array();
		if((isset($cit->grad_admissions_available) && $cit->grad_admissions_available == 1)){
			$epp_admissions_available = 'yes';
		}elseif ((isset($cit->grad_admissions_available) && $cit->grad_admissions_available == -1)) {
			$epp_admissions_available = 'no';
		}else{
			$epp_admissions_available = 'unknown';
		}

		$tmp = array();
		$tmp['epp_application_deadline'] 	  = $cit->epp_application_deadline;
		$tmp['epp_admissions_available'] 	  = $epp_admissions_available;
		$tmp['epp_application_fee'] 	   	  = isset($cit->epp_application_fee) 		  ? (int)$cit->epp_application_fee : null;
		$tmp['epp_num_of_applicants']    	  = isset($cit->epp_num_of_applicants) 		  ? (int)$cit->epp_num_of_applicants : null;
		$tmp['epp_num_of_admitted'] 		  = isset($cit->epp_num_of_admitted) 		  ? (int)$cit->epp_num_of_admitted : null;
		$tmp['epp_num_of_admitted_enrolled']  = isset($cit->epp_num_of_admitted_enrolled) ? (int)$cit->epp_num_of_admitted_enrolled : null;

		$ret['admission_info']['epp'] = $tmp;

		// Scholarship Info
		$ret['scholarship_info']['undergrad']   = array();
		$ret['scholarship_info']['grad'] 	 	= array(); 

		$tmp = array();
		if((isset($cit->undergrad_scholarship_available) && $cit->undergrad_scholarship_available == 1)){
			$undergrad_scholarship_available = 'yes';
		}elseif ((isset($cit->undergrad_scholarship_available) && $cit->undergrad_scholarship_available == -1)) {
			$undergrad_scholarship_available = 'no';
		}else{
			$undergrad_scholarship_available = 'unknown';
		}
		$tmp['undergrad_scholarship_available'] 	  		  = $undergrad_scholarship_available;
		$tmp['undergrad_scholarship_student_received_aid'] 	  = $cit->undergrad_scholarship_student_received_aid;
		$tmp['undergrad_scholarship_avg_financial_aid_given'] = (int)$cit->undergrad_scholarship_avg_financial_aid_given;
		$tmp['undergrad_scholarship_requirments']    	  	  = $cit->undergrad_scholarship_requirments;
		$tmp['undergrad_scholarship_gpa'] 		  			  = $cit->undergrad_scholarship_gpa;
		$tmp['undergrad_scholarship_link'] 					  = $cit->undergrad_scholarship_link;

		$ret['scholarship_info']['undergrad'] = $tmp;

		$tmp = array();
		if((isset($cit->grad_scholarship_available) && $cit->grad_scholarship_available == 1)){
			$grad_scholarship_available = 'yes';
		}elseif ((isset($cit->grad_scholarship_available) && $cit->grad_scholarship_available == -1)) {
			$grad_scholarship_available = 'no';
		}else{
			$grad_scholarship_available = 'unknown';
		}
		$tmp['grad_scholarship_available'] 	  		  	  = $grad_scholarship_available;
		$tmp['grad_scholarship_student_received_aid'] 	  = $cit->grad_scholarship_student_received_aid;
		$tmp['grad_scholarship_avg_financial_aid_given']  = (int)$cit->grad_scholarship_avg_financial_aid_given;
		$tmp['grad_scholarship_requirments']    	  	  = $cit->grad_scholarship_requirments;
		$tmp['grad_scholarship_gpa'] 		  			  = $cit->grad_scholarship_gpa;
		$tmp['grad_scholarship_link'] 					  = $cit->grad_scholarship_link;

		$ret['scholarship_info']['grad'] = $tmp;

		$tmp = array();
		if((isset($cit->epp_scholarship_available) && $cit->epp_scholarship_available == 1)){
			$epp_scholarship_available = 'yes';
		}elseif ((isset($cit->epp_scholarship_available) && $cit->epp_scholarship_available == -1)) {
			$epp_scholarship_available = 'no';
		}else{
			$epp_scholarship_available = 'unknown';
		}
		$tmp['epp_scholarship_available'] 	  		  	  = $epp_scholarship_available;
		$tmp['epp_scholarship_student_received_aid'] 	  = $cit->epp_scholarship_student_received_aid;
		$tmp['epp_scholarship_avg_financial_aid_given']   = (int)$cit->epp_scholarship_avg_financial_aid_given;
		$tmp['epp_scholarship_requirments']    	  	  	  = $cit->epp_scholarship_requirments;
		$tmp['epp_scholarship_gpa'] 		  			  = $cit->epp_scholarship_gpa;
		$tmp['epp_scholarship_link'] 					  = $cit->epp_scholarship_link;

		$ret['scholarship_info']['epp'] = $tmp;

		// Additional Notes
		$additional_notes  = CollegesInternationalAdditionalNote::on('rds1')
															    ->where('cit_id', $cit->id)
												  				->get();
		if (isset($additional_notes)) {
			$tmp = array();

			foreach ($additional_notes as $key) {
				$tmp = array();

				$tmp['id']             = (int)$key->id;
				$tmp['title'] 		   = $key->title;
				$tmp['description']    = $key->description;
				$tmp['attachment_url'] = $key->attachment_url;
				$tmp['content']		   = $key->content;

				$ret['additional_notes'][$key->view_type][] = $tmp;
			}
		}

		// Grade and Exams
		$ret['grade_exams']['undergrad'] = array();
		$ret['grade_exams']['grad'] 	 = array(); 
		$ret['grade_exams']['epp'] 	 	 = array(); 

		$tmp = array();
		$tmp['undergrad_grade_gpa_min'] 	      = $cit->undergrad_grade_gpa_min;
		$tmp['undergrad_grade_gpa_avg'] 	      = $cit->undergrad_grade_gpa_avg;
		$tmp['undergrad_grade_toefl_min'] 	   	  = $cit->undergrad_grade_toefl_min;
		$tmp['undergrad_grade_toefl_avg'] 	   	  = $cit->undergrad_grade_toefl_avg;
		$tmp['undergrad_grade_ielts_min']    	  = $cit->undergrad_grade_ielts_min;
		$tmp['undergrad_grade_ielts_avg'] 		  = $cit->undergrad_grade_ielts_avg;
		$tmp['undergrad_grade_act_composite_min'] = $cit->undergrad_grade_act_composite_min;
		$tmp['undergrad_grade_act_composite_avg'] = $cit->undergrad_grade_act_composite_avg;
		$tmp['undergrad_grade_act_english_min']   = $cit->undergrad_grade_act_english_min;
		$tmp['undergrad_grade_act_english_avg']   = $cit->undergrad_grade_act_english_avg;
		$tmp['undergrad_grade_act_math_min']      = $cit->undergrad_grade_act_math_min;
		$tmp['undergrad_grade_act_math_avg'] 	  = $cit->undergrad_grade_act_math_avg;
		$tmp['undergrad_grade_gre_writing_min']   = $cit->undergrad_grade_gre_writing_min;
		$tmp['undergrad_grade_gre_writing_avg']   = $cit->undergrad_grade_gre_writing_avg;
		$tmp['undergrad_grade_gre_quant_min'] 	  = $cit->undergrad_grade_gre_quant_min;
		$tmp['undergrad_grade_gre_quant_avg'] 	  = $cit->undergrad_grade_gre_quant_avg;
		$tmp['undergrad_grade_sat_math_min'] 	  = $cit->undergrad_grade_sat_math_min;
		$tmp['undergrad_grade_sat_math_avg'] 	  = $cit->undergrad_grade_sat_math_avg;
		$tmp['undergrad_grade_sat_reading_min']   = $cit->undergrad_grade_sat_reading_min;
		$tmp['undergrad_grade_sat_reading_avg']   = $cit->undergrad_grade_sat_reading_avg;
		$tmp['undergrad_grade_gmat_quant_min'] 	  = $cit->undergrad_grade_gmat_quant_min;
		$tmp['undergrad_grade_gmat_quant_avg'] 	  = $cit->undergrad_grade_gmat_quant_avg;
		$tmp['undergrad_grade_gmat_verbal_min']   = $cit->undergrad_grade_gmat_verbal_min;
		$tmp['undergrad_grade_gmat_verbal_avg']   = $cit->undergrad_grade_gmat_verbal_avg;
		$tmp['undergrad_grade_psat_math_min'] 	  = $cit->undergrad_grade_psat_math_min;
		$tmp['undergrad_grade_psat_math_avg'] 	  = $cit->undergrad_grade_psat_math_avg;
		$tmp['undergrad_grade_psat_reading_min']  = $cit->undergrad_grade_psat_reading_min;
		$tmp['undergrad_grade_psat_reading_avg']  = $cit->undergrad_grade_psat_reading_avg;


		$ret['grade_exams']['undergrad'] = $tmp;

		$tmp = array();
		$tmp['grad_grade_gpa_min'] 	         = $cit->grad_grade_gpa_min;
		$tmp['grad_grade_gpa_avg'] 	         = $cit->grad_grade_gpa_avg;
		$tmp['grad_grade_toefl_min'] 	   	 = $cit->grad_grade_toefl_min;
		$tmp['grad_grade_toefl_avg'] 	   	 = $cit->grad_grade_toefl_avg;
		$tmp['grad_grade_ielts_min']    	 = $cit->grad_grade_ielts_min;
		$tmp['grad_grade_ielts_avg'] 		 = $cit->grad_grade_ielts_avg;
		$tmp['grad_grade_act_composite_min'] = $cit->grad_grade_act_composite_min;
		$tmp['grad_grade_act_composite_avg'] = $cit->grad_grade_act_composite_avg;
		$tmp['grad_grade_act_english_min']   = $cit->grad_grade_act_english_min;
		$tmp['grad_grade_act_english_avg']   = $cit->grad_grade_act_english_avg;
		$tmp['grad_grade_act_math_min']      = $cit->grad_grade_act_math_min;
		$tmp['grad_grade_act_math_avg'] 	 = $cit->grad_grade_act_math_avg;
		$tmp['grad_grade_gre_writing_min']   = $cit->grad_grade_gre_writing_min;
		$tmp['grad_grade_gre_writing_avg']   = $cit->grad_grade_gre_writing_avg;
		$tmp['grad_grade_gre_quant_min'] 	 = $cit->grad_grade_gre_quant_min;
		$tmp['grad_grade_gre_quant_avg'] 	 = $cit->grad_grade_gre_quant_avg;
		$tmp['grad_grade_sat_math_min'] 	 = $cit->grad_grade_sat_math_min;
		$tmp['grad_grade_sat_math_avg'] 	 = $cit->grad_grade_sat_math_avg;
		$tmp['grad_grade_sat_reading_min']   = $cit->grad_grade_sat_reading_min;
		$tmp['grad_grade_sat_reading_avg']   = $cit->grad_grade_sat_reading_avg;
		$tmp['grad_grade_gmat_quant_min'] 	 = $cit->grad_grade_gmat_quant_min;
		$tmp['grad_grade_gmat_quant_avg'] 	 = $cit->grad_grade_gmat_quant_avg;
		$tmp['grad_grade_gmat_verbal_min']   = $cit->grad_grade_gmat_verbal_min;
		$tmp['grad_grade_gmat_verbal_avg']   = $cit->grad_grade_gmat_verbal_avg;
		$tmp['grad_grade_psat_math_min'] 	 = $cit->grad_grade_psat_math_min;
		$tmp['grad_grade_psat_math_avg'] 	 = $cit->grad_grade_psat_math_avg;
		$tmp['grad_grade_psat_reading_min']  = $cit->grad_grade_psat_reading_min;
		$tmp['grad_grade_psat_reading_avg']  = $cit->grad_grade_psat_reading_avg;

		$ret['grade_exams']['grad'] = $tmp;

		$tmp = array();
		$tmp['epp_grade_gpa_min'] 	         = $cit->epp_grade_gpa_min;
		$tmp['epp_grade_gpa_avg'] 	         = $cit->epp_grade_gpa_avg;
		$tmp['epp_grade_toefl_min'] 	   	 = $cit->epp_grade_toefl_min;
		$tmp['epp_grade_toefl_avg'] 	   	 = $cit->epp_grade_toefl_avg;
		$tmp['epp_grade_ielts_min']    	 	 = $cit->epp_grade_ielts_min;
		$tmp['epp_grade_ielts_avg'] 		 = $cit->epp_grade_ielts_avg;
		$tmp['epp_grade_act_composite_min']  = $cit->epp_grade_act_composite_min;
		$tmp['epp_grade_act_composite_avg']  = $cit->epp_grade_act_composite_avg;
		$tmp['epp_grade_act_english_min']    = $cit->epp_grade_act_english_min;
		$tmp['epp_grade_act_english_avg']    = $cit->epp_grade_act_english_avg;
		$tmp['epp_grade_act_math_min']       = $cit->epp_grade_act_math_min;
		$tmp['epp_grade_act_math_avg'] 	 	 = $cit->epp_grade_act_math_avg;
		$tmp['epp_grade_gre_writing_min']    = $cit->epp_grade_gre_writing_min;
		$tmp['epp_grade_gre_writing_avg']    = $cit->epp_grade_gre_writing_avg;
		$tmp['epp_grade_gre_quant_min'] 	 = $cit->epp_grade_gre_quant_min;
		$tmp['epp_grade_gre_quant_avg'] 	 = $cit->epp_grade_gre_quant_avg;
		$tmp['epp_grade_sat_math_min'] 	 	 = $cit->epp_grade_sat_math_min;
		$tmp['epp_grade_sat_math_avg'] 		 = $cit->epp_grade_sat_math_avg;
		$tmp['epp_grade_sat_reading_min']    = $cit->epp_grade_sat_reading_min;
		$tmp['epp_grade_sat_reading_avg']    = $cit->epp_grade_sat_reading_avg;
		$tmp['epp_grade_gmat_quant_min'] 	 = $cit->epp_grade_gmat_quant_min;
		$tmp['epp_grade_gmat_quant_avg'] 	 = $cit->epp_grade_gmat_quant_avg;
		$tmp['epp_grade_gmat_verbal_min']    = $cit->epp_grade_gmat_verbal_min;
		$tmp['epp_grade_gmat_verbal_avg']    = $cit->epp_grade_gmat_verbal_avg;
		$tmp['epp_grade_psat_math_min'] 	 = $cit->epp_grade_psat_math_min;
		$tmp['epp_grade_psat_math_avg'] 	 = $cit->epp_grade_psat_math_avg;
		$tmp['epp_grade_psat_reading_min']   = $cit->epp_grade_psat_reading_min;
		$tmp['epp_grade_psat_reading_avg']   = $cit->epp_grade_psat_reading_avg;
		$tmp['epp_grade_gre_verbal_min']	 = $cit->epp_grade_gre_verbal_min;
		$tmp['epp_grade_gre_verbal_avg']	 = $cit->epp_grade_gre_verbal_avg;

		$ret['grade_exams']['epp'] = $tmp;

		// Requirements
		$requirements = CollegesInternationalRequirment::on('rds1')
													 ->where('cit_id', $cit->id)
													 ->orderBy('type')
										  			 ->get();
        
        $ret['requirements'] = array();
		if (isset($requirements)) {
        	foreach ($requirements as $key) {
        		$tmp = array();

        		$tmp['id']             = (int)$key->id;
        		$tmp['attachment_url'] = $key->attachment_url;
        		$tmp['title']		   = $key->title;
        		$tmp['description']	   = $key->description;

        		$ret['requirements'][$key->view_type][$key->type][] = $tmp; 
        	}
        }

		// Majors and Degrees
		$majors_degrees  = CollegesInternationalMajor::on('rds1')
													 ->where('cit_id', $cit->id)
										  			 ->get();
		$ret['majors_degrees'] = array();
		if (isset($majors_degrees)) {
			$tmp = array();

			$degree_id_arr =array();
			$major_id_arr =array();
			$dep_id_arr =array();

			foreach ($majors_degrees as $key) {
				$val = explode(',', $key->val);

				// check if the degree already exists in degree_id_arr to save a call!
				if (isset($degree_id_arr[$val[2]])) {
					$degree = $degree_id_arr[$val[2]];
				}else{
					$degree = Degree::on('rds1')->find($val[2]);
					$degree = $degree->display_name;

					$degree_id_arr[$val[2]] = $degree;
				}

				//check if the major exists if not insert the department instead.
				if (isset($val[1]) && !empty($val[1])) {

					if (isset($major_id_arr[$val[1]])) {
						$major = $major_id_arr[$val[1]];
					}else{
						$major = Major::on('rds1')->find($val[1]);

						$major = $major->name;

						$major_id_arr[$val[1]] = $major;
					}

				}else{

					if (isset($dep_id_arr[$val[0]])) {
						$major = $dep_id_arr[$val[0]];
					}else{
						$major = Department::on('rds1')->find($val[0]);
						$major = $major->name;

						$dep_id_arr[$val[0]] = $major;
					}
				}

				if ($degree == "Master's Degree" || $degree == "PHD / Doctorate" || $degree == 'Certificate Programs') {
                    $ret['majors_degrees']['grad'][$degree][] = $major;
                }

                if ($degree == "Bachelor's Degree" || $degree == "Associate's Degree" || $degree == 'Certificate Programs'){
                	$ret['majors_degrees']['undergrad'][$degree][] = $major;
                }		
			}

			if( isset($ret['majors_degrees']['grad']) ){
				foreach ($ret['majors_degrees']['grad'] as $key => $val) {
					sort($val);
					$ret['majors_degrees']['grad'][$key] = $val;
				}
			}
			
			if( isset($ret['majors_degrees']['undergrad']) ){
				foreach ($ret['majors_degrees']['undergrad'] as $key => $val) {
					sort($val);
					$ret['majors_degrees']['undergrad'][$key] = $val;
				}
			}

		}

		// International Alums
		$alums = CollegesInternationalAlum::on('rds1')
										  ->leftjoin('colleges as c', 'c.id', '=', 'colleges_international_alums.college_id')
										  ->leftjoin('department as d', 'd.id', '=', 'colleges_international_alums.dep_id')
										  ->select('colleges_international_alums.*', 'c.school_name')
										  ->where('cit_id', $cit->id)
										  ->select('colleges_international_alums.id', 'colleges_international_alums.alumni_name',
										  		   'c.school_name', 'colleges_international_alums.grad_year', 'colleges_international_alums.location',
										  		   'colleges_international_alums.linkedin', 'colleges_international_alums.photo_url',
										  		   'd.name as dep_name', 'd.id as dep_id')
										  ->get();
        
        $ret['alums'] = array();
        if (isset($alums)) {
        	foreach ($alums as $key) {
        		$tmp = array();

        		$tmp['id']				= (int)$key->id;
        		$tmp['alumni_name']     = $key->alumni_name;
        		$tmp['school_name'] 	= $key->school_name;
        		$tmp['grad_year_abbr']	= substr($key->grad_year, -2);
        		$tmp['grad_year']		= $key->grad_year;
        		$tmp['location']		= $key->location;
        		$tmp['linkedin']		= $key->linkedin;
        		$tmp['photo_url']		= 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/alumni/'.$key->photo_url;
        		$tmp['department']		= $key->dep_name;
        		$tmp['dep_id']			= (int)$key->dep_id;

        		$ret['alums'][] = $tmp; 
        	}
        }

		return $ret;
	}
}
