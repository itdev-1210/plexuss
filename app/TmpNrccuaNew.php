<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpNrccuaNew extends Model
{
    protected $table = 'tmp_nrccua_new';

 	protected $fillable = array( 'user_id', 'dc_id', 'ro_id', 'manual', 'sent' );
}
