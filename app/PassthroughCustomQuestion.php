<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassthroughCustomQuestion extends Model
{
    //
    protected $table = 'passthrough_custom_questions';
    protected $fillable = ['ad_redirect_campaign_id', 'question_name'];
}
