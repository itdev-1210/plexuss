<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HonorAward extends Model {

	protected $table = 'honor_award';

 	protected $fillable = array('user_id', 'whocanseethis', 'title', 'issuer', 'month_received', 'year_received', 'honor_description', 'bullet_points');
}
