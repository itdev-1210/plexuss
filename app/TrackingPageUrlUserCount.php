<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrlUserCount extends Model
{
    protected $table = 'tracking_page_url_user_counts';

    protected $fillable = array( 'user_id', 'date', 'count', 'tpu_id', 'device_id', 'browser_id', 'platform_id', 'college_id');
}
