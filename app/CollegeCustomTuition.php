<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeCustomTuition extends Model
{
    protected $table = 'colleges_custom_tuitions';
}
