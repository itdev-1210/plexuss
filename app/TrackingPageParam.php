<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageParam extends Model
{
    protected $table = 'tracking_page_url_params';
    
    protected $fillable = array( 'tpu_id', 'date', 'params');

}
