<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class PublicProfileSettings extends Model
{
    protected $table = 'sn_users_public_profile_settings';

    protected $fillable = ['user_id', 'basic_info', 'claim_to_fame', 'objective', 'skills_endorsements', 'projects_publications', 'liked_colleges', 'created_at', 'updated_at'];

    /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getPublicProfileSettings($user_id) {
        $profile_settings = DB::connection('rds1')->table('sn_users_public_profile_settings as snpps')
                                                    ->where('snpps.user_id', $user_id)
                                                    ->select('snpps.user_id', 'snpps.basic_info', 'snpps.claim_to_fame', 'snpps.objective', 'snpps.skills_endorsements', 'snpps.projects_publications', 'snpps.liked_colleges')
                                                    ->first();

        return $profile_settings;
    }

    public static function insertOrUpdate($data) {
        if (!isset($data['user_id'])) {
            return 'Missing user_id';
        }

        $attributes = [
            'user_id' => $data['user_id'],
        ];

        $values = [
            'user_id' => $data['user_id'],
            'basic_info' => $data['basic_info'],
            'claim_to_fame' => $data['claim_to_fame'],
            'objective' => $data['objective'],
            'skills_endorsements' => $data['skills_endorsements'],
            'projects_publications' => $data['projects_publications'],
            'liked_colleges' => $data['liked_colleges'],
        ];

        PublicProfileSettings::updateOrCreate($attributes, $values);

        return 'success';
    }
}
