<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorLog extends Model {


	/**
	* The database table used by the model.
	*
	* @var string
	*/

	protected $connection = 'log';
	
	protected $table = 'error_logs';


 	protected $fillable = array('user_id', 'exception', 'code', 'ip', 'url', 'device', 'browser', 'platform', 'params' );


 	public function insertError($user_id=null,$exception=null, $code=null, $ip=null, $url=null,$device=null,$browser=null, $platform=null, $params =null){

 		$er = new ErrorLog;

 		$er->user_id = $user_id;
 		$er->exception = $exception;
 		$er->code = $code;
 		$er->ip = $ip;
 		$er->url = $url;
 		$er->device = $device;
 		$er->browser = $browser;
 		$er->platform = $platform;
 		$er->params = $params;

 		$er->save();
 	}
}


