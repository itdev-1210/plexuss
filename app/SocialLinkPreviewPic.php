<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLinkPreviewPic extends Model
{
    protected $table = 'sn_link_preview_pics';

 	protected $fillable = array('path', 'sn_link_preview_id');

 	public function link()
    {
      return $this->belongsTo('App\SocialLinkPreview');
    }
}
