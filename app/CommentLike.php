<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentLike extends Model
{
    protected $table = 'comment_likes';

	private $comment_id;		// comment id to get count for
	private $count;				// number of likes for a given comment_id

	public function setCommentId( $id ){
		$this->comment_id = $id;
	}

	public function getCount(){
		return $this->count;
	}

	public function count(){
		$comment_id = $this->comment_id;
		$count = CommentLike::where( 'comment_id', $comment_id )->count();

		$this->count = $count;
	}
}
