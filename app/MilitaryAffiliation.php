<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MilitaryAffiliation extends Model {

	protected $table = 'military_affiliation';
	
	protected $fillable = array('name');
	
	public function user()
    {
        return $this->belongsTo('User');
    }

    public function getAll(){
		$ma = MilitaryAffiliation::all();
		$temp = array();
		$temp[''] = 'Select...';
		foreach ($ma as $key) {
			$temp[$key->name] = $key->name;
		}
		return $temp;
	}

}