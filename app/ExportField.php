<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ExportField extends Model {


	protected $table = 'export_fields';

 	protected $fillable = array( 'name', 'select_field_name' );
}