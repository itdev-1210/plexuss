<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\EmailLogicHelper;
USE DB;

class UsersCompletionTimestamp extends Model
{
    protected $table = 'users_completion_timestamps';

 	protected $fillable = array('user_id', 'profile_completion_timestamp', 'selected_1_4_timestamp', 
 								'selected_5_more_timestamp');


 	public function addToProfileCompletionTimestamp($user_id){

 		$qry = UsersCompletionTimestamp::on('rds1')
 									   ->where('user_id', $user_id)
 									   ->select('profile_completion_timestamp')
 									   ->first();
 		$check = false;
 		if (!isset($qry)) {
 			$check = $this->checkIfProfileComplete($user_id);
 		}elseif (isset($qry) && !isset($qry->profile_completion_timestamp)) {
 			$check = $this->checkIfProfileComplete($user_id);
 		}

 		if ($check) {
 			$now = Carbon::now();

 			$attr = array('user_id' => $user_id);
 			$val  = array('user_id' => $user_id, 'profile_completion_timestamp' => $now);

 			UsersCompletionTimestamp::updateOrCreate($attr, $val);

 			$attr = array('user_id' => $user_id);
 			$val  = array('user_id' => $user_id, 'is_complete_profile' => 1);

 			$user = User::on('bk')
 						->where('id', $user_id)
 						->select('created_at')
 						->first();


 			if (isset($user)) {
				$created_at = Carbon::parse($user->created_at);
				$created_at = $created_at->toDateString();

				$now = Carbon::now()->toDateString();

				if ($now == $created_at) {
					$val['is_complete_profile_initial_visit'] = 1;
				}
			}	

			EmailLogicHelper::updateOrCreate($attr, $val);		

 		}
 	}

 	private function checkIfProfileComplete($user_id){

 		$qry = DB::connection('rds1')->table('users as u')
							 		->join('scores as s', 'u.id', '=', 's.user_id')
							 		
									->where('is_ldy', 0)
									->where('email', 'REGEXP', '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$')
									->where('email', 'NOT LIKE', '%test%')
									->where('fname', 'NOT LIKE', '%test%')
									->where('email', 'NOT LIKE', '%nrccua%')
									
									->whereNotNull('u.address')
									->where(DB::raw('length(u.address)'), '>=', 3)
									->whereNotNull('u.zip')
									->whereIn('u.gender', array('m', 'f'))
									->whereRaw('coalesce(s.hs_gpa, s.overall_gpa, s.weighted_gpa) is not null')
								 	
								 	->where('u.id', $user_id)
								 	->select('u.id')
								 	->first();

		if (isset($qry)) {
			return true;
		}

		return false;
 	}
}
