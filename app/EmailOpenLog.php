<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailOpenLog extends Model
{
    protected $table = 'email_open_logs';

 	protected $fillable = array('email', 'user_id', 'template_id', 'geo_city', 'geo_country', 'geo_latitude', 'geo_longitude', 'open_date', 'user_agent', 'response', 'subject', 'ip_address', 'sending_ip');
}
