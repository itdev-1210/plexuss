<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaCronSchedule extends Model
{
    protected $table = 'nrccua_cron_schedules';
}
