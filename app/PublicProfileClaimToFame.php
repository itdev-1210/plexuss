<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicProfileClaimToFame extends Model
{
    protected $table = 'public_profile_claim_to_fame';

    protected $fillable = ['user_id', 'description', 'youtube_url', 'vimeo_url', 'updated_at', 'created_at'];

    public static function insertOrUpdate($data) {
        if (!isset($data['description']) || !isset($data['user_id'])) {
            return 'Missing description or user_id';
        }

        $attributes = [
            'user_id' => $data['user_id'],
        ];

        $values = [
            'description' => $data['description'],
            'user_id' => $data['user_id'],
        ];

        if (!empty($data['youtube_url'])) {
            $values['youtube_url'] = $data['youtube_url'];
        } else {
            $values['youtube_url'] = null;
        }

        if (!empty($data['vimeo_url'])) {
            $values['vimeo_url'] = $data['vimeo_url'];
        } else {
            $values['vimeo_url'] = null;
        }

        PublicProfileClaimToFame::updateOrCreate($attributes, $values);

        return 'success';
    }
}
