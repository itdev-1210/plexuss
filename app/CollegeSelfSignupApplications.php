<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NewCollegesFromSelfSignup;

class CollegeSelfSignupApplications extends Model
{
    protected $table = 'college_self_signup_applications';

    protected $fillable = array( 'college_id', 'user_id', 'skype_id', 'agreement_name', 'agreement_email', 'agreement_date', 'blurb', 'title', 'working_since_date', 'show_on_frontpage_check', 'show_on_collegepage_check');

    public $timestamps = true;

    public static function insertOrUpdate($user_id, $input) {
        if (!isset($user_id) || !isset($input['agreement_name']) || !isset($input['agreement_email']) || !isset($input['agreement_date']) || !isset($input['title']) || !isset($input['working_since_date']) || !isset($input['college_selected']))
                return 'fail';

        $potential_keys = ['blurb', 'show_on_frontpage_check', 'show_on_collegepage_check', 'skype_id'];

        $has_new_college = !isset($input['college_id']);

        $attributes = ['user_id' => $user_id];

        $values = [
            'user_id' => $user_id, 
            'agreement_name' => $input['agreement_name'],
            'agreement_email' => $input['agreement_email'],
            'agreement_date' => $input['agreement_date'],
            'title' => $input['title'],
            'working_since_date' => $input['working_since_date'],
        ];

        foreach ($potential_keys as $key) {
            if (isset($input[$key]))
                $values[$key] = $input[$key];
        }

        if ($has_new_college) {
            $query = CollegeSelfSignupApplications::updateOrCreate($attributes, $values);
            
            $college_selected = json_decode($input['college_selected'], true);

            return NewCollegesFromSelfSignup::insertOrUpdate($query->id, $college_selected);
        }
        
        $values['college_id'] = $input['college_id'];
        
        CollegeSelfSignupApplications::updateOrCreate($attributes, $values);

        return 'success';
    }
}
