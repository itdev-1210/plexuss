<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAddtlInfo extends Model
{
    protected $table = 'users_addtl_info';
	protected $fillable = array('user_id', 'whatsapp', 'is_incognito', 'get_started_percent', 'google_id', 'linkedin_id');
}
