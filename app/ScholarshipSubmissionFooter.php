<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use App\ScholarshipSubmission;

class ScholarshipSubmissionFooter extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	public function update_scholarship_footer($data='',$user_id){
 		if (!isset($data['interested_service'])) {
 			return;
 		}

 		$scholarship = ScholarshipSubmission::where('user_id', $user_id)
            ->orderBy('created_at', 'desc')
            ->first();

        $scholarship->interested_service = $data['interested_service'];
        $scholarship->save();
 	}
}
