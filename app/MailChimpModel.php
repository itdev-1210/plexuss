<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailChimpModel extends Model {

	private $mc;
	private $list_id;


	public function __construct($list_name)  {
        $this->mc = new Mailchimp('43ed420bc050cd68e940047f8827e2ba-us9');
        $this->setListId($list_name);
    }
	
    public function subscribe($email, $params = null){

    	$this->mc->lists->subscribe($this->list_id, $email, $params,  'html', 'false');
    }

    public function members($opt = array()){
        return $this->mc->lists->members($this->list_id,'subscribed', $opt);
    }

    private function setListId($list_name){
    	switch ($list_name) {
    		case 'webinar':
    			
    			$this->list_id = "6ae573ab49";
    			break;
    		case 'users_invites':
    			
    			$this->list_id = "e3987cd3a7";
    			break;

            case 'users_received_msg_from_college':
                
                $this->list_id = "f4642b3337";
                break;
            
            case 'welcome_email':
                
                $this->list_id = "6f7a8f486c";
                break;

            case 'college_recommendations':
                
                $this->list_id = "807b754e65";
                break;
    		default:
    			# code...
    			break;
    	}

    }
}