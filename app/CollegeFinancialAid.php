<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeFinancialAid extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'colleges_financial_aid';


 	protected $fillable = array( 'college_id', 'page_title', 'meta_keywords', 'meta_description' );
}
