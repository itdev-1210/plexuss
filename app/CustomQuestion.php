<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomQuestion extends Model
{
    protected $table = 'custom_questions';

 	protected $fillable = array('predefined', 'type', 'question', 'field_name', 'table_name', 'title');

}
