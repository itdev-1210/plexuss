<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;

Class Transcript extends Model{

	protected $table = 'transcript';
	protected $fillable = array('user_id', 'transcript_name', 'transcript_path', 'school_type', 'doc_type', 'label');

	public function getUsersTranscript($user_id = null){

		if (!isset($user_id)) {
			return;
		}

		$trc = Transcript::where('user_id', $user_id)->get();

		return $trc;
	}

	public function getUsersTranscriptFormatted($user_id){
		$trc = Transcript::where('user_id', $user_id)->get();

		$ret = array();
		foreach ($trc as $key) {
			$tmp = array();

			$tmp['id'] = $key->id;
			$tmp['doc_type'] = $key->doc_type;
			$tmp['path'] = $key->transcript_path.$key->transcript_name;
			$tmp['file_name'] = $key->transcript_name;
			$tmp['date'] = date('m/d/Y', strtotime($key->created_at));
			
			$ret[] = $tmp;
		}

		return $ret;
	}

	public function removeAttachment($id, $user_id){
		$t = Transcript::where('id', $id)
					   ->where('user_id', $user_id)
					   ->first();

		$bucket_url = 'asset.plexuss.com/users/transcripts';
		$keyname    = $t->transcript_name;

		$bc = new Controller;
		$bc->generalDeleteFile($bucket_url, $keyname);
		

		$t->delete();

		return "success";
	}
	
	/*********************************************
	*  updates document type for upload
	*  returns number of columns effected
	***********************************************/
	public function changeUploadType($id, $type){

		$t = Transcript::where('id', $id)
					->update(array('doc_type' => $type));

		return $t;
	}

}
