<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\CollegesInternationalMajor;

class Priority extends Model {


    protected $table = 'priority';

    protected $fillable = array('college_id', 'aor_id', 'type', 'contract', 'tier', 'org_tier',
                                'promote', 'goal', 'start_goal', 'end_goal', 'financial_filter', 'financial_filter_order');

    public function getPrioritySchools(){

        $result = DB::connection('rds1')->table('priority as p')
                    ->leftJoin('contract_types as ct', 'ct.id', '=', 'p.contract')
                    ->leftJoin('client_types as clt', 'clt.id', '=', 'p.type')
                    ->leftJoin('colleges as c', 'c.id', '=', 'p.college_id')
                    ->leftJoin('organization_branches as ob', 'ob.school_id', '=', 'p.college_id')
                    ->leftJoin('aor as a', 'a.id', '=', 'p.aor_id')
                    ->select('c.id as college_id', 'c.school_name as school_name', 'c.slug',
                             'p.id as priority_id', 'p.promote as promoted', 'p.goal', 'p.start_goal',
                             'p.end_goal', 'p.aor_id', 'p.tier', 'p.org_tier',
                             'ct.id as contract_id', 'ct.name as contract', 'clt.name as aor', 'ob.balance',
                             DB::raw('IF(p.aor_id != NULL, a.balance, ob.balance) as balance'),
                             DB::raw('IF(p.aor_id != NULL, a.cost_per_handshake, ob.cost_per_handshake) as price')
                             )
                    ->where('p.active', 1)
                    ->orderBy('p.promote', 'desc')
                    ->orderBy(DB::raw('ISNULL(p.id)'))
                    ->orderBy('p.contract', 'DESC')
                    ->orderBy('p.type', 'ASC')
                    ->groupBy('c.id') 
                    ->get();

        return $result;
    }

    public function getPrioritySchoolsById($id){

        $result = DB::connection('rds1')->table('priority as p')
            ->leftJoin('contract_types as ct', 'ct.id', '=', 'p.contract')
            ->leftJoin('client_types as clt', 'clt.id', '=', 'p.type')
            ->leftJoin('colleges as c', 'c.id', '=', 'p.college_id')
            ->leftJoin('organization_branches as ob', 'ob.school_id', '=', 'p.college_id')
            ->leftJoin('aor as a', 'a.id', '=', 'p.aor_id')
            ->select('c.id as college_id', 'c.school_name as school_name', 'c.slug',
                'p.id as priority_id', 'p.promote as promoted', 'p.goal', 'p.start_goal',
                'p.end_goal', 'p.aor_id', 'p.tier', 'p.org_tier',
                'ct.id as contract_id', 'ct.name as contract', 'clt.name as aor', 'ob.balance',
                DB::raw('IF(p.aor_id != NULL, a.balance, ob.balance) as balance'),
                DB::raw('IF(p.aor_id != NULL, a.cost_per_handshake, ob.cost_per_handshake) as price')
            )
            ->where('p.id', $id)
            ->orderBy('p.promote', 'desc')
            ->orderBy('p.contract', 'DESC')
            ->orderBy('p.type', 'ASC')
            ->groupBy('c.id')
            ->first();

        return $result;
    }

    public function getApplicationCollege(){

        $result = DB::connection('rds1')->table('priority as p')
                    ->leftJoin('contract_types as ct', 'ct.id', '=', 'p.contract')
                    ->leftJoin('client_types as clt', 'clt.id', '=', 'p.type')
                    ->leftJoin('colleges as c', 'c.id', '=', 'p.college_id')
                    ->leftJoin('organization_branches as ob', 'ob.school_id', '=', 'p.college_id')
                    ->leftJoin('aor as a', 'a.id', '=', 'p.aor_id')
                    ->select('c.id as college_id', 'c.school_name as school_name', 'c.slug',
                             'p.id as id', 'p.promote as promoted', 'p.goal', 'p.start_goal',
                             'p.end_goal', 'p.aor_id', 'p.tier', 'p.org_tier', 'p.financial_filter_order',
                             'ct.id as contract_id', 'ct.name as contract', 'clt.name as aor', 'ob.balance',
                             DB::raw('IF(p.aor_id != NULL, a.balance, ob.balance) as balance'),
                             DB::raw('IF(p.aor_id != NULL, a.cost_per_handshake, ob.cost_per_handshake) as price'),
                             'a.name as aor_name'
                             )
                    ->where('p.active', 1)
                    ->orderBy(DB::raw('ISNULL(p.financial_filter_order), p.financial_filter_order'), 'ASC')
                    ->orderBy('p.promote', 'desc')
                    ->orderBy(DB::raw('ISNULL(p.id)'))
                    ->orderBy('p.contract', 'DESC')
                    ->orderBy('p.type', 'ASC')
                    ->groupBy('c.id') 
                    ->get();

        return $result;
    }

    public function getCollegeClientType($college_id, $data){
        $qry = DB::connection('rds1')->table('priority as p')
                                     ->join('contract_types as ct', 'p.contract', '=', 'ct.id')
                                     ->join('client_types as cs', 'p.type', '=', 'cs.id')
                                     ->join('client_status as css', 'p.status', '=', 'css.id')
                                     ->where('p.college_id', $college_id)
                                     ->select('css.name as contractName');

        if (isset($data['aor_id']) && !empty($data['aor_id'])) {
            $qry = $qry->where('p.aor_id', $data['aor_id']);
        }else{
            $qry = $qry->whereNull('p.aor_id');
        }                   

        $qry = $qry->first();
        
        return $qry;
    }

    public function getPrioritySchoolsForIntlStudents($user_id = NULL, $aor_id = NULL, $type = NULL, $offset = NULL, $search_term = NULL){
        $qry = DB::connection('rds1')->table('priority as p')
                                     ->join('colleges as c', 'c.id', '=', 'p.college_id')
                                     ->leftjoin('colleges_international_tuition_costs as citc', 'citc.college_id', '=', 'c.id')
                                     ->leftjoin('colleges_ranking as cr', 'cr.college_id', '=', 'c.id')
                                     ->leftjoin('countries as co', 'co.id', '=', 'c.country_id')
                                     ->leftjoin('revenue_organizations as ro', function($q){
                                                $q->on('ro.id', '=', 'p.ro_id');
                                                $q->on('ro.active', '=', DB::raw(1));
                                     })
                                     ->where('p.active', 1)
                                     ->groupBy('p.college_id');

        if (isset($user_id)) {
            $qry = $qry->leftjoin('users_applied_colleges as upc', function($q) use ($user_id) {
                                  $q->on('upc.college_id', '=', 'c.id');
                                  $q->on('upc.user_id',    '=', DB::raw($user_id));
                                  $q->on('upc.submitted',  '=', DB::raw(1));
                        })
                       ->leftJoin('portal_notifications as pn', function($q) use ($user_id){
                            $q->on('pn.school_id', '=', 'p.college_id');
                            $q->on('pn.user_id', '=', DB::raw($user_id));
                       })
                       ->select('upc.id as userHasApplied', 
                                   'pn.is_higher_rank_recommend', 'pn.is_major_recommend', 'pn.is_department_recommend',
                                   'pn.is_lower_tuition_recommend', 'pn.is_top_75_percentile_recommend');

            $user = User::on('rds1')->find($user_id);

            // if (isset($user) && !empty($user->financial_firstyr_affordibility)) {
            //     if ($user->financial_firstyr_affordibility == '0' || $user->financial_firstyr_affordibility == '0.00' || 
            //         $user->financial_firstyr_affordibility == '0 - 5,000' || $user->financial_firstyr_affordibility == '5,000 - 10,000' ||
            //         $user->financial_firstyr_affordibility == '10,000 - 20,000') {
            //         $qry = $qry->where('financial_filter', 'under 20K');
            //     }else{
            //         $qry = $qry->where('financial_filter', 'over 20K');
            //     }
            // }
            $qry = $qry->orderByRaw('`p`.`financial_filter_order` is null, `p`.`financial_filter_order` asc');
        }else{
            $qry = $qry->orderBy('p.promote', 'desc')
                       ->orderBy(DB::raw('ISNULL(p.id)'))
                       ->orderBy('p.contract', 'DESC')
                       ->orderBy('p.type', 'ASC');
        }

        if (isset($offset)) {
            $qry = $qry->take(10)
                       ->skip($offset);
        }

        if (isset($search_term)) {
          $qry = $qry->where(function($q) use($search_term){
                    $q->orWhere('c.school_name', 'LIKE', '%'. $search_term . '%')
                      ->orWhere('c.alias', 'LIKE', '%'. $search_term . '%');
                    });
        }

        isset($aor_id)? $qry = $qry->where('p.aor_id', $aor_id) : NULL;

        if (isset($type)) {
            switch ($type) {
                case 'cc':
                    $qry = $qry->where('c.school_sector', 'Public, 2-year');
                    break;
                
                case 'uni':
                    $qry = $qry->where('c.school_sector', '!=', 'Public, 2-year');
                    break;

                default:
                    # code...
                    break;
            }
        }

        $ret = array();
        $ret['undergrad'] = array();

        $cim = new CollegesInternationalMajor;

        $modal_num = NULL;

        if (isset($user_id)) {
            $user_obj = Objective::on('rds1')->where('user_id', $user_id)->first();
            $score    = Score::on('rds1')->where('user_id', $user_id)->first();

            $taken_ept = false;

            if (isset($score->toefl_total) || isset($score->toefl_reading) || isset($score->toefl_listening) || isset($score->toefl_speaking) || 
                isset($score->toefl_writing) || 
                
                isset($score->toefl_ibt_total) || isset($score->toefl_ibt_reading) || isset($score->toefl_ibt_listening) || 
                isset($score->toefl_ibt_speaking) || isset($score->toefl_ibt_writing) || 

                isset($score->toefl_pbt_total) || isset($score->toefl_pbt_reading) ||isset($score->toefl_pbt_listening) || 
                isset($score->toefl_pbt_written) || 

                isset($score->ielts_total) || isset($score->ielts_reading) || isset($score->ielts_listening) || isset($score->ielts_speaking) || 
                isset($score->ielts_writing) ||

                isset($score->pte_total) || isset($score->english_institute_name)) {

                $taken_ept = true;
            }
            if (isset($user->country_id) && $user->country_id == 1) {
                $taken_ept = true;
            }
            if (isset($user->country_id) && $user->country_id != 1) {
                if ($user->financial_firstyr_affordibility == "0" || $user->financial_firstyr_affordibility == "0.00" || 
                    $user->financial_firstyr_affordibility == "0 - 5,000" || $user->financial_firstyr_affordibility == NULL ||
                    !isset($user->financial_firstyr_affordibility)) {
                    $ret['modal_num'] = 1;
                    // If 5,000 or less budget, do not run execute query.
                    return $ret;
                }elseif ($user->financial_firstyr_affordibility == '5,000 - 10,000' && $taken_ept) {
                    $modal_num = 2;
                    // ELS only
                    $qry = $qry->where('p.aor_id', 5);
                }elseif (($user->financial_firstyr_affordibility == '5,000 - 10,000' || $user->financial_firstyr_affordibility == '10,000 - 20,000') 
                        && !$taken_ept) {
                    $modal_num = 3;
                    // ELS only
                    $qry = $qry->where('p.aor_id', 5);
                }elseif ($user->financial_firstyr_affordibility == '10,000 - 20,000' && $taken_ept) { 
                    // Between $10,000 to $20,000 and has taken EPT Just show colleges  Community colleges
                    // Otero 663, Peralta 498, De Anza 320, Foothill 349,  
                    $qry = $qry->where(function($q){
                                        $q->orWhereIn('p.aor_id', array(10))
                                          ->orWhereIn('c.id', array(663,498,320,349, 1129, 1348, 1877, 3705, 3826, 4071, 4083));
                    });

                }elseif ($user->financial_firstyr_affordibility == '20,000 - 30,000' || $user->financial_firstyr_affordibility == '20,000 - 30,000' ||
                         $user->financial_firstyr_affordibility == '30,000 - 50,000' || $user->financial_firstyr_affordibility == '50,000') {
                    
                    // Shorelight, Devry, and all the other oneapp schools (Liberty, UIC, MSOE, TTU, Arkansas and international universiteis)
                    $qry = $qry->where(function($q){
                                        $q->orWhereIn('p.aor_id', array(2,3, 10))
                                          ->orWhereIn('c.id', array(4124,1128,4346,3813,148,199688,228792,199687,3604,3598,199688,228792,199687,4124, 1129, 1348, 1877, 3705, 3826, 4071, 4083));
                    });
                }
            }
            // }else{
                
            //     // for United States students only show them Liberty and the four international universities for now
            //     $qry = $qry->whereIn('c.id', array(199688,228792,199687,4124));

            // }
        }

        $qry = $qry->where('ro.type', '!=', 'post')
                   ->orWhereNull('ro.type');

        $colleges_qry = $qry;
        $colleges_qry = $colleges_qry->addSelect('c.id as college_id')->get();

        $college_id_arr = array();

        foreach ($colleges_qry as $key) {
            if (isset($key->college_id)) {
                $college_id_arr[] = $key->college_id;
            } 
        }
        
        if (isset($user_id)) {

            $query = DB::connection('rds1')->table('colleges_application_allowed_sections')
                                           ->whereIn('college_id', $college_id_arr)
                                           ->select('college_id', 'page', 'sub_section', 'required')
                                           ->groupBy('page', 'sub_section');


            if (isset($user_obj)) {
                if (isset($user_obj->degree_type)) {
                    if ($user_obj->degree_type == 1 || $user_obj->degree_type == 2 || $user_obj->degree_type == 3 ||
                        $user_obj->degree_type == 6 || $user_obj->degree_type == 7 || $user_obj->degree_type == 8) {
                        
                        $query = $query->where(function($q){
                                                $q->orWhere('define_program', '=', 'undergrad')
                                                  ->orWhere('define_program', '=', 'epp');
                        });
                    }else{
                        $query = $query->where(function($q){
                                                $q->orWhere('define_program', '=', 'grad')
                                                  ->orWhere('define_program', '=', 'epp');
                        });
                    }
                }
            }
                                           
            $query = $query->get();

            if (isset($user_obj)) {
                $cap = CollegesApplicationDeclaration::on("rds1")->whereIn('college_id', $college_id_arr);
                // Undergraduate declarations
                if ($user_obj->degree_type == 1 || $user_obj->degree_type == 2 || $user_obj->degree_type == 3 || 
                    $user_obj->degree_type == 6 || $user_obj->degree_type == 7 || $user_obj->degree_type == 8) {
                    
                    $cap = $cap->where(function($q){
                                       $q->orWhere('type', '=', DB::raw("'undergrad'"));
                                       $q->orWhere('type', '=', DB::raw("'both'"));
                    });
                }
                // Graduate declarations
                else{
                    $cap = $cap->where(function($q){
                                       $q->orWhere('type', '=', DB::raw("'grad'"));
                                       $q->orWhere('type', '=', DB::raw("'both'"));
                    });
                }

                $cap = $cap->get();
            }
        }
        
        // dd($colleges_qry);
        $qry = $qry->addSelect('c.school_name', 'c.city', 'c.long_state', 'c.logo_url', 'c.id as cid', 'c.slug',
                                              'c.paid_app_url', 'c.application_url', 'cr.plexuss as rank',
                                              'co.country_code', 'co.country_name',
                                              'citc.*',
                                              'p.aor_id',
                                               'ro.id as ro_id',
                                                'ro.type as ro_type');
        
        $qry = $qry->get();
        // dd($qry);
        foreach ($qry as $key) {

            $undergrad = array();
            $undergrad['ro_id']       = $key->ro_id;
            $undergrad['ro_type']     = $key->ro_type;
            $undergrad['aor_id']      = $key->aor_id;
            $undergrad['college_id']  = (int)$key->cid;
            $undergrad['school_name'] = $key->school_name;
            $undergrad['slug']        = '/college/'.$key->slug;
            $undergrad['city']        = $key->city;
            $undergrad['state']       = $key->long_state;
            $undergrad['logo_url']    = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/'.$key->logo_url;
            $undergrad['rank']        = isset($key->rank) ? (int)$key->rank : 0;
            $undergrad['application_fee'] = isset($key->undergrad_application_fee) ? (double)$key->undergrad_application_fee : 0;
            $undergrad['avg_tuition']     = isset($key->undergrad_avg_tuition) ? (double)$key->undergrad_avg_tuition : 0;
            $undergrad['other_cost']      = isset($key->undergrad_other_cost) ? (double)$key->undergrad_other_cost : 0;
            $undergrad['avg_scholarship'] = isset($key->undergrad_avg_scholarship) ? (double)$key->undergrad_avg_scholarship : 0;
            $undergrad['avg_work_study']  = isset($key->undergrad_avg_work_study) ? (double)$key->undergrad_avg_work_study : 0;
            $undergrad['other_financial'] = isset($key->undergrad_other_financial) ? (double)$key->undergrad_other_financial : 0;
            $undergrad['app_url']         = isset($key->paid_app_url) ? $key->paid_app_url : $key->application_url;
            $undergrad['country_name']    = $key->country_name;
            $undergrad['country_code']    = strtolower($key->country_code);
            $undergrad['quick_tip']       = isset($key->quick_tip) ? $key->quick_tip : '';
            $undergrad['company_logo']    = isset($key->company_logo_file) ? $key->company_logo_file : '';

            $undergrad['why_recommended'] = array();

            if( isset($key->is_higher_rank_recommend) && $key->is_higher_rank_recommend == 1 ){
                $undergrad['why_recommended'][] = 'A higher rank';
            }
            if( isset($key->is_major_recommend) && $key->is_major_recommend == 1 ){
                $undergrad['why_recommended'][] = 'They offer your degree and major';
            }
            if( isset($key->is_department_recommend) && $key->is_department_recommend == 1 ){
                $undergrad['why_recommended'][] = 'They offer your degree and majors in the same department';
            }
            if( isset($key->is_lower_tuition_recommend) && $key->is_lower_tuition_recommend == 1 ){
                $undergrad['why_recommended'][] = 'Lower Tuition';
            }
            if( isset($key->is_top_75_percentile_recommend) && $key->is_top_75_percentile_recommend == 1 ){
                $undergrad['why_recommended'][] = "Your score put you in the top 75% percentile of their past year’s enrollment class";
            }

            if( strpos($undergrad['app_url'], 'http') === false ){
                $undergrad['app_url'] = 'http://'.$undergrad['app_url'];
            }

            // // getting estimated cost
            // $undergrad['est_cost'] = $undergrad['avg_tuition'] + $undergrad['other_cost'];
            // // getting estimated assisted cost
            // $undergrad['assist_cost'] = $undergrad['avg_scholarship'] + $undergrad['avg_work_study'] + $undergrad['other_financial'];
            // // gettign estimated total annual cost
            // $undergrad['annual_cost'] = $undergrad['est_cost'] - $undergrad['assist_cost'];

            // TEMPORARY CHANGE , DO NOT TAKE UNDERGRAD AND GRAD VALUES FOR ELS
            if ($key->aor_id == 5) {
                
                $undergrad['undergrad_column_cost'] = NULL;
                $undergrad['grad_column_cost']      = NULL;

            }else{
                $undergrad['undergrad_column_cost'] = ((double)$key->undergrad_avg_tuition + (double)$key->undergrad_other_cost) != 0 ? 
                                                  (double)$key->undergrad_avg_tuition + (double)$key->undergrad_other_cost : 0;

                $undergrad['grad_column_cost']      = ((double)$key->grad_avg_tuition + (double)$key->grad_other_cost) != 0 ? 
                                                      (double)$key->grad_avg_tuition + (double)$key->grad_other_cost : 0;
            }
                                                  
            
            $undergrad['epp_column_cost']       = ((double)$key->epp_avg_tuition + (double)$key->epp_other_cost) != 0 ? 
                                                  (double)$key->epp_avg_tuition + (double)$key->epp_other_cost : 0;   

            $undergrad['majors'] = $cim->getMajors($key->cid);

            if (isset($user_id)) {
                $undergrad['userHasApplied'] = $key->userHasApplied;
            }

            if (isset($user_id)) {

                foreach ($query as $k) {
                    if ($k->college_id == $undergrad['college_id']) {
                        if ($k->page == 'uploads') {
                            if (isset($undergrad['allowed_uploads'])) {
                                $undergrad['allowed_uploads'][] = $k->sub_section;
                            }else{
                                $undergrad['allowed_uploads'] = array();
                                $undergrad['allowed_uploads'][] = $k->sub_section;

                                $undergrad['allowed_sections'][] = $k->page;
                            }
                        }elseif ($k->page == 'custom' || $k->page == 'additional') {
                           if (isset($undergrad['custom_questions'])) {
                                $undergrad['custom_questions'][$k->sub_section] = (isset($k->required) && $k->required == 1) ? true : false;
                           }else{
                                $undergrad['custom_questions']   = array();
                                $undergrad['custom_questions'][$k->sub_section] = (isset($k->required) && $k->required == 1) ? true : false;
                           }
                        }else{
                            $undergrad['allowed_sections'][] = $k->page;
                        }
                    }
                }

                if (isset($user_obj)) {
                    
                    $undergrad['declarations'] = array();

                    foreach ($cap as $k) {
                        if ($k->college_id == $undergrad['college_id']) {
                            $tmp = array();

                            $tmp['id']       = $k->id;
                            $tmp['language'] = $k->language;

                            $undergrad['declarations'][] = $tmp;
                        }
                        
                    }
                }
            }

            // $undergrad['modal_num'] = $modal_num;

            $ret['undergrad'][] = $undergrad; 
        }
        
        $ret['modal_num'] = $modal_num;

        return $ret;
    }
}
