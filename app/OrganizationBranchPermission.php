<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Crypt;

class OrganizationBranchPermission extends Model {

	protected $table = 'organization_branch_permissions';
	protected $fillable = array( 'organization_branch_id','user_id' ,'export_file_cnt', 'show_on_college_page',
								 'show_on_front_page', 'title', 'description', 'member_since', 'super_admin', 
								 'department', 'created_at', 'updated_at');

	public function orgs()
    {
        return $this->belongsTo('OrganizationBranch');
    }

    public function isOrgsFirstUser( $ob_id = null ){
        if( !isset($ob_id) ){
            return 'must have a user id';
        }

        $qry = DB::connection('rds1')->table('organization_branch_permissions as obp')
                                     ->where('organization_branch_id', $ob_id)
                                     ->where('super_admin', 1)
                                     ->count();

        return $qry == 1;
    }

    public function getAllAdminUserAllInfo($user_id, $organization_branch_id){

    	$qry = DB::connection('rds1')->table('organization_branches as ob')
    								 ->join('organization_branch_permissions as obp', 'ob.id', '=', 'obp.organization_branch_id')
    								 ->leftjoin('organization_portals as op', function($join){
    								 		$join->on('op.org_branch_id', '=', 'ob.id');
									    	$join->where('op.active', '=', 1);
    								 })
    								 ->join('organization_portal_users as opu', function($q){
                                            $q->on('op.id', '=', 'opu.org_portal_id');
                                            $q->on('opu.user_id', '=', 'obp.user_id');
                                     })
    								 ->leftjoin('users as u', function($q){
                                            $q->on('u.id', '=', 'obp.user_id');
                                            $q->on('opu.user_id', '=', 'u.id');
                                     })
                                     ->join('colleges as c', 'c.id', '=', 'ob.school_id')
                                     ->leftjoin('college_overview_images as coi', function($query){
                                        $query->on('coi.college_id', '=', 'c.id')
                                              ->where('coi.is_video', '=', 0)
                                              ->where('coi.is_tour', '=', 0);
                                     })
    								 ->where('ob.id', $organization_branch_id)
    								 ->select('u.id as user_id', 'u.fname', 'u.lname', 'u.email', 'u.profile_img_loc',
    								 		  'ob.id as org_branch_id',
    								 		  'obp.super_admin', 'obp.show_on_college_page', 'obp.show_on_front_page',
    								 		  'obp.title', 'obp.department', 'obp.description', 'obp.member_since', 
    								 		  'op.id as portal_id', 'op.name as portal_name', 'op.num_of_applications',
    								 		  'op.num_of_enrollments', 'op.premier_trial_begin_date', 'op.premier_trial_end_date',
    								 		  'op.num_of_filtered_rec','op.active',
    								 		  'opu.is_default',
                                              'c.logo_url',
                                              'coi.url as image_url')
                                     ->where('obp.user_id', '!=', $user_id)
                                     ->where('u.is_organization', '=', 1)
                                     ->whereNotNull('u.id')
    								 ->groupBy('u.id', 'op.id', 'coi.college_id')
    								 ->get();
    	$ret = array();
    	$tmp_user_id = '';

    	foreach ($qry as $key) {
    		if ($tmp_user_id == '' || $tmp_user_id != $key->user_id) {
    			if (isset($tmp)) {
    				$ret[] = $tmp;
    			}
    			$tmp_user_id = $key->user_id; 
    			$tmp = array();
	    		$tmp['id'] 			         = (int)$key->user_id;
	    		$tmp['hasheduserid']	     = Crypt::encrypt($key->user_id);
	    		$tmp['fname']				 = $key->fname;
	    		$tmp['lname']				 = $key->lname;
	    		$tmp['email']				 = $key->email;
                $tmp['profile_pic']          = $key->profile_img_loc ? 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/users/images/'.$key->profile_img_loc : 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/frontpage/default.png';
	    		$tmp['org_branch_id']		 = (int)$key->org_branch_id;
	    		$tmp['super_admin']			 = (int)$key->super_admin;
                $tmp['role']                 = $key->super_admin == 1 ? 'Admin' : 'User';
	    		$tmp['show_on_college_page'] = (int)$key->show_on_college_page;
	    		$tmp['show_on_front_page']   = (int)$key->show_on_front_page;
	    		$tmp['title']				 = $key->title;
	    		$tmp['department']			 = $key->department;
	    		$tmp['blurb']			     = $key->description;
	    		$tmp['working_since']		 = $key->member_since ? explode('-', $key->member_since)[0] : null;
                $tmp['school_logo']          = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/'.$key->logo_url;
                $tmp['school_background']    = isset($key->image_url) ? 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/overview_images/'.$key->image_url : '';

	    		$tmp['portal_info'] 		 = array();
                $arr         = array();
                $arr['portal_id'] 				 = (int)$key->portal_id;
                $arr['hashedid'] 				 = Crypt::encrypt($key->portal_id);
                $arr['portal_name'] 			 = $key->portal_name;
                $arr['num_of_applications'] 	 = (int)$key->num_of_applications;
                $arr['num_of_enrollments'] 		 = (int)$key->num_of_enrollments;
                $arr['premier_trial_begin_date'] = $key->premier_trial_begin_date;
                $arr['premier_trial_end_date']   = $key->premier_trial_end_date;
                $arr['num_of_filtered_rec']      = (int)$key->num_of_filtered_rec;
                $arr['active'] 					 = (int)$key->active;
                $arr['is_default'] 				 = (int)$key->is_default;

                $tmp['portal_info'][] = $arr;
    		}else{
    			$tmp_user_id = $key->user_id; 
    			$arr 		 = array();
	    		$arr['portal_id'] 				 = (int)$key->portal_id;
                $arr['hashedid'] 				 = Crypt::encrypt($key->portal_id);
                $arr['portal_name'] 			 = $key->portal_name;
                $arr['num_of_applications'] 	 = (int)$key->num_of_applications;
                $arr['num_of_enrollments'] 		 = (int)$key->num_of_enrollments;
                $arr['premier_trial_begin_date'] = $key->premier_trial_begin_date;
                $arr['premier_trial_end_date']   = $key->premier_trial_end_date;
                $arr['num_of_filtered_rec']      = (int)$key->num_of_filtered_rec;
                $arr['active'] 					 = (int)$key->active;
                $arr['is_default'] 				 = (int)$key->is_default;

	    		$tmp['portal_info'][] = $arr;
    		}
    	}

        if( isset($tmp) ){
            $ret[] = $tmp;
        }
  
    	return $ret;
    }
}
