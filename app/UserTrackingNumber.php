<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrackingNumber extends Model
{
    protected $table = 'user_tracking_numbers';

    protected $fillable = array('date', 'type', 'num_of_us_students', 'num_of_intl_students', 'num_of_us_students_com', 'num_of_intl_students_com', 'monetized_us_students_selected_1_5', 'monetized_intl_students_selected_1_5', 'monetized_us_students_selected_over_5', 'monetized_intl_students_selected_over_5', 'allschools_us_students_selected_1_5', 'allschools_intl_students_selected_1_5', 'allschools_us_students_selected_over_5', 'allschools_intl_students_selected_over_5', 'num_of_us_students_premium', 'num_of_intl_students_premium',
		'num_of_us_students_com_premium', 'num_of_intl_students_com_premium');
}