<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Organization extends Model
{
    protected $table = 'organizations';
	protected $fillable = array( 'name','address');

	public function branches(){
        return $this->hasMany('OrganizationBranch');
    }

    public function getOrgsAdminInfo( $onDate = null, $term = null, $aor_id = NULL, $takeSkip = NULL ){

        //$tmp = array(984, 54, 4346);
        //$tmp = array(54);
        $orgs = DB::connection('rds1')->table('organization_branch_permissions as obp')
                    ->join('organization_branches as ob', 'ob.id', '=', 'obp.organization_branch_id')
                    ->join('organizations as o', 'o.id', '=', 'organization_id')
                    ->join('colleges as c', 'c.id', '=', 'ob.school_id')
                    ->join('users as u', 'u.id', '=', 'obp.user_id')
                    ->select('o.name as org_name', 
                             'ob.id as org_branch_id', 'ob.bachelor_plan',
                             'obp.export_file_cnt',
                             'u.id as user_id', 'u.fname', 'u.lname', 'u.created_at as date_joined', 'u.email',
                             'c.id as college_id','c.school_name');
                    //->whereIn('c.id', $tmp)
        if(isset($term)) {
            $viewDataController = new ViewDataController();
            $data = $viewDataController->buildData();
            $orgs = $orgs->where('u.email', 'like', '%'.$term.'%')
                         ->where('ob.id', $data['org_branch_id']);
        }else{
            $orgs = $orgs->where('u.is_plexuss', 0);
        }

        // if( isset($onDate) ){
        //     $orgs = $orgs->where('u.created_at', '>', $onDate);
        // }
        // AOR starts here
        if (isset($aor_id)) {
            $orgs = $orgs->join('aor_permissions as aorp','u.id','=','aorp.user_id')
                         ->where('aorp.aor_id','=',$aor_id)
                         ->orderBy('o.id');
        }
        // AOR ends here
        else{
            $orgs = $orgs->orderBy('c.id');
        }
        
        if (isset($takeSkip)) {
            $orgs = $orgs->take($takeSkip['take'])->skip($takeSkip['skip']);
        }

        $orgs = $orgs->get();
        return $orgs;
    }


    public function getThisOrgInfo($org_branch_id, $is_aor = null){
        $ob = DB::connection('rds1')->table('organization_branches as ob')
                                    ->join('organization_branch_permissions as obp', 'ob.id', '=', 'obp.organization_branch_id')
                                    ->join('users as u', 'u.id', '=', 'obp.user_id')
                                    ->where('ob.id', $org_branch_id)
                                    ->select('u.id as user_id', 'u.fname', 'u.lname', 'u.profile_img_loc',
                                             'obp.super_admin');
                                    
        // Do not include AOR users, We use this in SettingController@getManageUserss
        if (!$is_aor) {
            $ob = $ob->where('u.is_aor', 0);
        }

        $ob = $ob->get();

        return $ob;
    }

    /**
     * getFrontPageReps
     * this method returns the super admins for each college along with information 
     * about them college rep and the college itself
     *
     * @return object
    */
    public function getFrontPageReps($school_id = null, $limit = null, $skip = null){

        $orgs = DB::connection('rds1')->table('organization_branch_permissions as obp')
                    ->join('organization_branches as ob', 'ob.id', '=', 'obp.organization_branch_id')
                    ->join('organizations as o', 'o.id', '=', 'organization_id')
                    ->join('colleges as c', 'c.id', '=', 'ob.school_id')
                    ->join('users as u', 'u.id', '=', 'obp.user_id')
                    ->leftjoin('college_overview_images as coi', function($query){
                        $query = $query->on('coi.college_id', '=', 'c.id');
                        $query = $query->where('coi.is_video', '=', 0)
                                       ->where('coi.url', '!=', DB::raw("''"));
                    })
                    ->leftjoin('colleges_ranking as cr', 'cr.college_id', '=', 'c.id')
                    ->where('obp.super_admin', 1)
                    ->where('obp.show_on_front_page', 1)
                    ->where('u.is_plexuss', 0)
                    ->where('c.in_our_network', 1)
                    ->select('u.id as user_id', 'u.fname', 'u.lname', 'u.profile_img_loc',
                             'obp.title', 'obp.description', 'obp.member_since', 'obp.id', 
                             'c.id as college_id', 'c.school_name', 'c.logo_url', 'c.slug',
                             'coi.url as school_bk_img')
                    ->groupBy('ob.id')
                    ->orderby(DB::raw('`u`.`profile_img_loc` IS NULL,`u`.`profile_img_loc`'));
                    

        if( isset($school_id) ){
            $orgs = $orgs->where('c.id', $school_id);
        }

        if( isset($skip) && !isset($school_id)){
            $orgs = $orgs->skip($skip);
        }

        if( isset($limit) && !isset($school_id) ){
            $orgs = $orgs->limit($limit);
        }

        $orgs = $orgs->get();
        // $orgs = (array) $orgs;
        
        // // Randomize the first results 
        if( !isset($skip) && !isset($school_id)){
            $orgs = $orgs->shuffle();
        }

        return $orgs;
    }


    /**
     * getCollegeRepInfo
     * this method returns the info of a college rep 
     * We use this method in BaseController@getUserMessages
     *
     * @return object
    */
    public function getCollegeRepInfo($user_id, $org_branch_id){

        $orgs = DB::connection('rds1')->table('organization_branch_permissions as obp')
                    ->join('organization_branches as ob', 'ob.id', '=', 'obp.organization_branch_id')
                    ->join('organizations as o', 'o.id', '=', 'ob.organization_id')
                    ->join('colleges as c', 'c.id', '=', 'ob.school_id')
                    ->join('users as u', 'u.id', '=', 'obp.user_id')
                    ->leftjoin('college_overview_images as coi', function($query){
                        $query = $query->on('coi.college_id', '=', 'c.id');
                        $query = $query->where('coi.is_video', '=', 0)
                                       ->where('coi.url', '!=', '');
                    })
                    ->leftjoin('colleges_ranking as cr', 'cr.college_id', '=', 'c.id')
                    ->select('u.id as user_id', 'u.fname', 'u.lname', 'u.profile_img_loc',
                             'obp.title', 'obp.description', 'obp.member_since', 'obp.id as obp_id', 
                             'c.id as college_id', 'c.school_name', 'c.logo_url', 'c.slug', 'c.address', 'c.city', 'c.state', 'c.zip',
                             'coi.url as school_bk_img', 
                             'cr.plexuss as rank')
                    ->groupBy('ob.id')
                    ->where('u.id', $user_id)
                    ->where('ob.id', $org_branch_id)
                    ->first();
        
        return $orgs;
    }
}
