<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevenueNrccuaStudent extends Model
{
	protected $table = 'revenue_nrccua_students';
	
    protected $fillable = array('ip_zip', 'is_dup', 'ip', 'ip_city', 'ip_state', 'checked_for_zip');
}
