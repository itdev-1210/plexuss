<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpNrccua extends Model
{
    protected $table = 'tmp_nrccua';

 	protected $fillable = array('globalContactOptIn', 'email', 'firstName', 'lastName', 'birthday', 'addressLine1', 'city', 'state', 'zip', 'country', 'gender', 'userType', 'utmCampaign', 'highSchoolGpa', 'satEnglish', 'satWriting', 'satMath', 'satReadingWriting', 'partnerInquiryCaptureDate', 'highSchoolGradYear', 'cellPhone', 'isInterestedInOnlineEducation', 'currentSchoolIpedId', 'interestedSchoolIpedId', 'attendedHighSchoolCeebCodes', 'nq_id', 'rec_id', 'user_id', 'utmSource');
}