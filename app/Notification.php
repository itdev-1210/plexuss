<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notification extends Model{
	
	//public $timestamps = false;
	protected $table = 'notifications';	
	
	// manage school tab menu list
	public function manageSchoolData($userid=false,$type=false)
	{
		
		 $query = 'notifications.id as list_id,colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,colleges.long_state,colleges.zip,colleges.logo_url,colleges.chief_title,colleges.chief_name,colleges.logo_url,colleges_ranking.plexuss,"notifications" as tablename';
		
		$result=DB::connection('rds1')->table('notifications')
		->join('colleges', 'colleges.id', '=', 'notifications.school_id')
		->leftJoin('colleges_ranking', 'colleges_ranking.ipeds_id', '=', 'colleges.ipeds_id')		
		->leftJoin('colleges_tuition', 'colleges_tuition.ipeds_id', '=', 'colleges.ipeds_id')	
		->leftJoin('colleges_athletics', 'colleges_athletics.ipeds_id', '=', 'colleges.ipeds_id')	
		->leftJoin('colleges_admissions', 'colleges_admissions.ipeds_id', '=', 'colleges.ipeds_id')
		->select( DB::raw($query));
		
		
		if(isset($userid) && $userid!='')
		{				
			$result = $result->where('notifications.user_id', '=',$userid);
			$result = $result->where('notifications.type', '=',$type);
			$result = $result->whereNull('notifications.trash');			
		}
		
		$result = $result->orderBy('colleges.school_name', 'asc');	
		$result = $result->groupBy('notifications.school_id');	
		
		//$result = $result->toSql();	
		$result = $result->get();
		return $result;
	}
	
	// manage scholarship tab menu list
	public function scholarshipData($userid=false,$type=false)
	{
		 $query = 'notifications.id as list_id,colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,colleges.long_state,colleges.zip,colleges.logo_url,colleges.chief_title,colleges.chief_name,colleges.logo_url,colleges.alias,colleges_ranking.plexuss,"notifications" as tablename';
		
		$result=DB::connection('rds1')->table('notifications')
		->join('colleges', 'colleges.id', '=', 'notifications.scholarship_id')		
		->leftJoin('colleges_ranking', 'colleges_ranking.ipeds_id', '=', 'colleges.ipeds_id')
		->select( DB::raw($query));		
		
		if(isset($userid) && $userid!='')
		{				
			$result = $result->where('notifications.user_id', '=',$userid);		
			if($type=='trash')
			{
				$result = $result->whereNotNull('notifications.scholarship_id');
				$result = $result->where('notifications.trash','=','1');
			}
			else
			{				
				$result = $result->where('notifications.type', '=',$type);
				$result = $result->whereNull('notifications.trash');
			}
		}
		$result = $result->orderBy('colleges.school_name', 'asc');	
		$result = $result->groupBy('notifications.scholarship_id');	
		
		//$result = $result->toSql();	
		$result = $result->get();
		return $result;
	}
	
	
	// quick fact data
	public function GetListRecord($record_id=false,$tab=false)
	{
		
		$join_table_field='';
		if($tab=='managescholarships')
		{$join_table_field='scholarship_id';}
		else
		{$join_table_field='school_id';}
		
		$result=DB::connection('rds1')->table('notifications')
		->join('colleges', 'colleges.id', '=', 'notifications.'.$join_table_field.'')
		->leftJoin('colleges_ranking', 'colleges_ranking.ipeds_id', '=', 'colleges.ipeds_id')		
		->leftJoin('colleges_tuition', 'colleges_tuition.ipeds_id', '=', 'colleges.ipeds_id')	
		->leftJoin('colleges_athletics', 'colleges_athletics.ipeds_id', '=', 'colleges.ipeds_id')	
		->leftJoin('colleges_admissions', 'colleges_admissions.ipeds_id', '=', 'colleges.ipeds_id')
	
		
		->select(array('notifications.id as list_id','colleges.id', 'colleges.slug' ,'colleges.ipeds_id','colleges.school_name','colleges.address',																				                       'colleges.city','colleges.state','colleges.long_state','colleges.zip','colleges.logo_url','colleges.chief_title','colleges.chief_name','colleges.logo_url',                       'colleges.alias','colleges.student_faculty_ratio','colleges.student_body_total','colleges.admissions_total','colleges.applicants_total',
					   'colleges_ranking.plexuss','colleges_tuition.tuition_avg_in_state_ftug',																																												'colleges_tuition.tuition_avg_out_state_ftug','colleges_athletics.class_name','colleges_admissions.sat_read_75','colleges_admissions.sat_write_75','colleges_admissions.sat_math_75'
		,'colleges_admissions.deadline','colleges_admissions.act_composite_75'
		));		
	
		$result = $result->where('notifications.id', '=',$record_id);
				
		$result = $result->orderBy('colleges.school_name', 'asc');	
		
		if($tab=='managescholarships')
		{
			$result = $result->groupBy('notifications.scholarship_id');	
		}
		else
		{
			$result = $result->groupBy('notifications.school_id');	
		}
		
		
		//$result = $result->toSql();	
		$result = $result->first();
		return $result;
	}
	
	public function getTrashRecord($userid=false)
	{
		 
		  $query1 = 'college_list.id as list_id,colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,colleges.long_state,colleges.zip,colleges.logo_url,colleges_ranking.plexuss,"college_list" as tablename';
		 
		 
		 $college_list_data = DB::connection('rds1')->table('college_list')	
		->join('colleges', 'colleges.id', '=', 'college_list.school_id')		
		->leftJoin('colleges_ranking', 'colleges_ranking.ipeds_id', '=', 'colleges.ipeds_id')	
		->select( DB::raw( $query1 ) )
		->where('college_list.user_id', '=',$userid)
		->where('college_list.trash','=','1')
		->whereNull('college_list.deleted')
		->orderby( 'colleges.school_name', 'asc' )	
		->groupBy('college_list.school_id')	
		->get();	
		
		
		//for yourlist menu of manage school tab
		 $query2 = 'notifications.id as list_id,colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,colleges.long_state,colleges.zip,colleges.logo_url,colleges_ranking.plexuss,"notifications" as tablename';
		 
		 $notifications_data = DB::connection('rds1')->table( 'notifications' )	
		->join('colleges', 'colleges.id', '=', 'notifications.school_id')		
		->leftJoin('colleges_ranking', 'colleges_ranking.ipeds_id', '=', 'colleges.ipeds_id')	
		
		->select( DB::raw( $query2 ) )
		->where('notifications.user_id', '=',$userid)
		->where('notifications.trash','=','1')
		->whereNull('notifications.deleted')
		->orderby( 'colleges.school_name', 'asc' )	
		->groupBy('notifications.school_id')	
		->get();
		
				
		$result = array_merge($college_list_data,$notifications_data);
		
		return $result;
	}
	
}

