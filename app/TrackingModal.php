<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingModal extends Model
{
	protected $connection = 'log';
    protected $table = 'tracking_modals';
 	protected $fillable = array('user_id', 'page', 'device_id', 'browser_id', 
 								'platform_id', 'modal_name', 'trigged_action');
}
