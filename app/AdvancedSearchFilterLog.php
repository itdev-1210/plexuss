<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvancedSearchFilterLog extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'advanced_search_filter_logs';


 	protected $fillable = array( 'as_filter_id', 'val' );

}
