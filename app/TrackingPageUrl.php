<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrl extends Model
{
    protected $table = 'tracking_page_urls';

    protected $fillable = array('url');
}
