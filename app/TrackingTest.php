<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingTest extends Model {

	protected $connection = 'ldy';
	protected $table = 'tracking_test';	

	protected $fillable = array( 'plexuss_inserted', 'flag', 'tginfo', 'ad_num');

}