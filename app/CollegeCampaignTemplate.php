<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeCampaignTemplate extends Model
{
    protected $table = 'college_campaign_templates';

 	protected $fillable = array( 'name', 'subject', 'type', 'content' );
}
