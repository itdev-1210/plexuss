<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalCollegeContactPlexussEmail extends Model
{
    //
    protected $table = "internal_college_contact_plexuss_emails";
}
