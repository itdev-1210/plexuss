<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlexussBannerCopy extends Model
{
    protected $table = 'plexuss_banner_copies';

 	protected $fillable = array('end_point', 'active', 'url');

 	public function getAdCopies(){

 		$qry = PlexussBannerCopy::on('rds1')->where('active', 1);

 		$qry = $qry->orderByRaw("RAND()")->first();

 		$ret = array();

 		if (!isset($qry)) {
 			return $ret;
 		}
 		$ret['company'] = 'Plexuss';
 		$ret['end_point'] = env('CURRENT_URL').$qry->end_point;
 		if (strpos($qry->url, 'http') !== FALSE){
 			$ret['url'] = $qry->url;
 		}else{
 			$ret['url'] = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/ad_copies/'.$qry->url;
 		}
 		
 		$ret['ad_copy_id'] = $qry->id;

 		return $ret;

 	}
}
