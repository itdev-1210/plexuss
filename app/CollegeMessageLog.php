<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\CollegeMessageThreadMembers;
use DB;

class CollegeMessageLog extends Model
{
    protected $table = 'college_message_logs';

	protected $fillable = ['user_id', 'thread_id', 'msg', 'is_read', 'attachment_url', 'is_deleted', 
						   'is_text', 'post_id', 'share_article_id'];

	private $org_branch_id;

	public function getNumOfDailyChat($org_branch_id = null){

		if ($org_branch_id == null) {
			return 0;
		}

		$cnt = DB::connection('bk')->table('college_message_threads as cmt')
					->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id' )
					->join('college_message_logs as cml', 'cmt.id', '=', 'cml.thread_id')
					->join('users as u', 'u.id', '=', 'cml.user_id')
					->where('cmt.is_chat', 1)
					//->whereNotIn('cml.user_id', DB::raw('SELECT DISTINCT user_id FROM organization_branch_permissions WHERE organization_branch_id='. $org_branch_id))
					->where(function ($query) use($org_branch_id) {
						foreach ($org_branch_id as $k) {
							$query->orwhere('cmtm.org_branch_id', $k);
						}
					    
					})
					
					->where('u.is_alumni', 0)
					->where('u.is_parent', 0)
					->where('u.is_counselor', 0)
					->where('u.is_organization', 0)
					->where('u.is_agency', 0)
					->where('u.is_plexuss', 0)
					->where('u.is_university_rep', 0)
					->select(DB::raw('count(DISTINCT u.id) as cnt , cmtm.org_branch_id'))
					->where('cml.created_at', '>=', Carbon::now()->today())
					->groupBy('cmtm.org_branch_id')
					->get();

		if (!isset($cnt)) {
			return 0;
		}
		return $cnt;
	}

	public function getNumOfDaysChatted($org_branch_id = null, $onDate = null, $endDate = null, $college_id = null){

		if ($org_branch_id == null && $college_id == null) {
			return 0;
		}

		$cnt = DB::connection('rds1')->table('college_message_threads as cmt')
					->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id' )
					->join('college_message_logs as cml', 'cmt.id', '=', 'cml.thread_id')
					->where('cmt.is_chat', 1);

		if (isset($org_branch_id)) {
			$cnt = $cnt->whereIn('cmtm.org_branch_id', $org_branch_id)
					   ->select(DB::raw('COUNT(DISTINCT DATE(cml.created_at)) as cnt , cmtm.org_branch_id'))
					   ->groupBy('cmtm.org_branch_id');
		}

		if (isset($college_id)) {
			$cnt = $cnt->join('organization_branches as ob', 'ob.id', '=', 'cmtm.org_branch_id')
					   ->whereIn('ob.school_id', $college_id)
					   ->select(DB::raw('COUNT(DISTINCT DATE(cml.created_at)) as cnt , ob.school_id as college_id'))
					   ->groupBy('ob.school_id');
		}

		if( isset($onDate) && isset($endDate) ){
 			$cnt = $cnt->whereBetween('cml.created_at', array($onDate, $endDate));
		}elseif( isset($onDate) ){
			$cnt = $cnt->where('cml.created_at', '>', $onDate);
		}

		$cnt = $cnt->get();

		if (!isset($cnt)) {
			return 0;
		}

		return $cnt;
	}


	public function getNumOfMsgSentOfChatAndMessages($user_id_arr = null, $onDate = null, $endDate = null){

		if ($user_id_arr == null) {
			return 0;
		}

		$cnt = DB::connection('rds1')->table('college_message_threads as cmt')
					->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id' )
					->join('users as u', 'u.id', '=', 'cmtm.user_id')
					//->whereNotIn('cml.user_id', DB::raw('SELECT DISTINCT user_id FROM organization_branch_permissions WHERE organization_branch_id='. $user_id_arr))
					->where(function ($query) use($user_id_arr) {
						foreach ($user_id_arr as $k) {
							$query->orwhere('u.id', $k);
						}
					    
					})

					->select(DB::raw('SUM(cmtm.num_of_sent_msg) as cnt, cmtm.org_branch_id, u.is_organization, cmt.is_chat, u.id as user_id'))
					->groupBy('cmt.is_chat', 'u.id')
					->orderBy('cmtm.org_branch_id');
					

		if( isset($onDate) && isset($endDate) ){
 			$cnt = $cnt->whereBetween('cmt.created_at', array($onDate, $endDate));
		}elseif( isset($onDate) ){
			$cnt = $cnt->where('cmt.created_at', '>', $onDate);
		}

		$cnt = $cnt->get();

		if (!isset($cnt)) {
			return null;
		}
		return $cnt;
	}

	public function getNumOfMsgSentReceivedOfChatAndMessagesByDate($user_id_arr = null, $onDate = null, $endDate = null, $sentReceived = null){

		if ($user_id_arr == null) {
			return 0;
		}

		$cnt = DB::connection('rds1')->table('college_message_threads as cmt')
					->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id' )
					->join('college_message_logs as cml', 'cml.thread_id', '=', 'cmt.id')
					->join('users as u', 'u.id', '=', 'cmtm.user_id')
					//->whereNotIn('cml.user_id', DB::raw('SELECT DISTINCT user_id FROM organization_branch_permissions WHERE organization_branch_id='. $user_id_arr))
					->where(function ($query) use($user_id_arr, $sentReceived) {
						foreach ($user_id_arr as $k) {
							$query->where('cmtm.user_id', $k)
								  ->orwhere('cml.user_id', $sentReceived, $k);
						}
					    
					})

					->select(DB::raw('COUNT(cml.id) as cnt,cmt.is_chat'))
					->groupBy('u.id', 'cmt.is_chat');
					

		if( isset($onDate) && isset($endDate) ){
 			$cnt = $cnt->whereBetween('cml.created_at', array($onDate, $endDate));
		}elseif( isset($onDate) ){
			$cnt = $cnt->where('cml.created_at', '>', $onDate);
		}

		$cnt = $cnt->get();

		if (!isset($cnt)) {
			return null;
		}
		return $cnt;
	}

	public function getNumOfMsgReceivedOfChatAndMessages($user_id = null){

		if ($user_id == null) {
			return 0;
		}

		$thread_id_arr = CollegeMessageThreadMembers::where('user_id', $user_id)
											->select('thread_id')
											->distinct()
											->get()->toArray();
		if (empty($thread_id_arr)) {
			return null;
		}
		//dd($thread_id_arr);
		$cnt = DB::connection('bk')->table('college_message_threads as cmt')
					->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id' )
					->join('users as u', 'u.id', '=', 'cmtm.user_id')
					//->whereNotIn('cml.user_id', DB::raw('SELECT DISTINCT user_id FROM organization_branch_permissions WHERE organization_branch_id='. $user_id_arr))
					->whereIn('cmt.id', $thread_id_arr)
					->where('u.is_organization', 0)

					->select(DB::raw('SUM(cmtm.num_of_sent_msg) as cnt, cmtm.org_branch_id, cmt.is_chat'))
					->groupBy('cmt.is_chat')
					->orderBy('cmtm.org_branch_id')
					->get();

		if (!isset($cnt)) {
			return null;
		}
		return $cnt;
	}

	public function numOfMsgSent($user_id = null, $org_branch_id = null){

		$this->org_branch_id = $org_branch_id;

		if (!isset($user_id) || !isset($org_branch_id)) {
			return 0;
		}

		$cnt = DB::connection('bk')->table('college_message_threads as cmt')
					->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id' )
					->join('college_message_logs as cml', 'cmt.id', '=', 'cml.thread_id')

					->where('cmtm.org_branch_id', $org_branch_id)
					->where('cml.user_id', $user_id)
					->select('cml.id')
					->groupBy('cml.id')
					->get();


		if (!isset($cnt)) {
			return 0;
		}
		$cnt = count($cnt);

		return $cnt;
	}

	public function getNumOfIdleMsgs($user_id_arr = null){

		if ($user_id_arr == null) {
			return 0;
		}

		$cnt = CollegeMessageThreadMembers::whereIn('user_id', $user_id_arr)
											->select(DB::raw('SUM(num_unread_msg) as cnt, user_id'))
											->groupBy('user_id')
											->get();

		if (!isset($cnt)) {
			return null;
		}
		return $cnt;
	}



	/********************************************
	*  get last twenty messages with param threadId 
	* 
	*********************************************/
	public function getAllMessages($threadId){

		if(empty($threadId)){
			return null;
		}


		$results = DB::connection('bk')
					->table('college_message_logs as cml')
					->where('cml.thread_id', '=', $threadId)
					->join('users as u', function($join){

						$join->on('u.id', '=', 'cml.user_id');
					})
					->orderBy('cml.created_at', 'asc')
					->select('cml.msg', 'cml.created_at', 'u.profile_img_loc', 'u.id')
					->take(20)
					->get();

		return $results;			

	}
}
