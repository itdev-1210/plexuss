<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasedPhone extends Model {


	protected $table = 'purchased_phones';

 	protected $fillable = array( 'sid', 'org_branch_id', 'phone', 'purchased_by_user_id', 'expires_at' );


}