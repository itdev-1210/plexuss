<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalCollegeContactTemplate extends Model
{
    //
    protected $table = 'internal_college_contact_templates';
}
