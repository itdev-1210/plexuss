<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Aws;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\MandrillLog, App\RoleBaseEmail;
use App\Http\Controllers\Controller;

class SesModel extends Model
{
    private $md;
    private $template_name;
    private $sparky;


    public function __construct($list_name)  {
        $this->setTemplateName($list_name);
    }

    public function sendTemplate($email_arr, $params, $reply_email, $attachments = null, $bcc_address = null){

    	$ses = \AWS::createClient('ses');

        // Check for role base email
        $rbe = new RoleBaseEmail;
        if ($rbe->isRoleBase($email_arr['email'])) {
            return 'failed';
        }

        $template_name = $this->template_name;
        $template_content = array();
        
        $message = array();

        $message['Source']       = 'anthony.shayesteh@plexuss.com';
        $message['Template'] 	 = $template_name;
        
        $message['Destination']  = array();
        $message['Destination']['ToAddresses'] 	 = array();
        $message['Destination']['ToAddresses'][] = 'linkinster@gmail.com';
        $message['TemplateData'] = json_encode($params);

        $c = new Controller();
       	$response = $ses->SendTemplatedEmail($message);

       	$c->customdd($response);
       	exit();
        // dd($message);
        try {
            // Build your email and send it!      
        	

            $ml = new MandrillLog();
            $ml->template_name = $this->template_name;
            $ml->response = $response;
            $ml->email = $email_arr['email'];
            unset($message['attachments']);
            $ml->params = json_encode($message);

            $ml->save();
            $this->template_name = '';

        } catch (\Exception $str) {

            $ml = new MandrillLog();
            $ml->template_name = $this->template_name;
            $ml->response = $str;
            $ml->email = $email_arr['email'];
            unset($message['attachments']);
            $ml->params = json_encode($message);

            $ml->save();

            $this->template_name = '';
        }
        
        return 'success';
    }

    private function setTemplateName($list_name){
        switch ($list_name) {
            case 'college_send_message_for_users':
                
                $this->template_name = "users-a-school-sent-you-a-message";
                break;
            case 'college_recommendation_for_users':
                
                $this->template_name = "users-college-recommendations";
                break;

            case 'colleges_viewed_your_profile':
                
                $this->template_name = "users-schools-viewed-your-profile";
                break;
            
            case 'college_wants_to_recruit_you':
                
                $this->template_name = "users-a-school-wants-to-recruit-you-new";
                break;

            case 'college_recommendations':
                
                $this->template_name = "your-student-recommendations-final";
                break;

            case 'potential_college_recommendations':
                
                $this->template_name = "your-student-recommendations-shadow";
                break;
                
            case 'user_want_to_get_recruited_for_colleges':
                
                $this->template_name = "colleges-users-want-to-get-recruited";
                break;

            case 'user_accepts_request_to_be_recruited_for_colleges':
                
                $this->template_name = "colleges-users-accepts-request-to-be-recruited";
                break;

             case 'user_send_message_for_colleges':
                
                $this->template_name = "colleges-user-sends-your-college-a-message";
                break;

            case 'college_agreed_to_recruit_you':
                
                $this->template_name = "users-a-school-agreed-to-recruit-you";
                break;

            case 'new_user_confirmation_email':
                
                $this->template_name = "users-confirm-email";
                break;    

            case 'paid_member_request_to_sales':
                
                $this->template_name = "paid-member-request-to-sales";
                break; 

            case 'agency_recruiting_to_students':
                
                $this->template_name = "agency-recruiting-to-students";
                break;

            case 'college_prep_recruiting_in_area':
                
                $this->template_name = "college-prep-recruiting-in-area "; // extra space found here, bug?
                break;

            case 'agency_recruiting_visa_help':
                 $this->template_name = "agency-recruiting-visa-help";
                 break; 

            case 'agencies_agency_sign_up_request':
                $this->template_name = "agencies-agency-sign-up-request";
                break;

            case 'english_recruiting_in_area':
                 $this->template_name = "english-recruiting-in-area";
                 break; 

            case 'in_network_comparison_college_users_email':
                $this->template_name = "users-rec-in-network-college-score-comparison";
                break;

            case 'ranking_update_college_users_email':
                $this->template_name = "users-college-ranking-update-school-on-list";
                break;

            case 'near_you_college_users_email':
                $this->template_name = "users-rec-in-network-college-near-you";
                break;

            case 'chat_session_college_users_email':
                $this->template_name = "users-chat-session-invite";
                break;
            
            case 'school_you_liked_college_users_email':
                $this->template_name = "users-in-network-school-you-liked";
                break;
            
            case 'users_in_network_school_you_messaged_college_users_email':
                $this->template_name = 'users-in-network-school-you-messaged';
                break;

            case 'users_in_network_school_u_wanted_to_get_recruited_college_users_email':
                $this->template_name = 'users-in-network-school-u-wanted-to-get-recruited';
                break;

            case 'college_prep_recruiting_in_area_agency_users_email':
                $this->template_name = 'users-college-prep-recruiting-in-area';
                break;

            case 'agency_recruiting_visa_help_agency_users_email':
                $this->template_name = 'users-agency-recruiting-visa-help';
                break;

            case 'english_recruiting_in_area_agency_users_email':
                $this->template_name = 'users-english-recruiting-in-area';
                break;

            case 'college_prep_user_interested_in_your_services':
                $this->template_name = 'college-prep-user-interested-in-your-services';
                break;

            case 'agencies_user_interested_in_your_services':
                $this->template_name = 'agencies-user-interested-in-your-services';
                break;
            
            case 'english_program_user_interested_in_your_services':
                $this->template_name = 'users-english-program-handshake';
                break;

            case 'infilaw_survey_completion_notification_and_data':
                $this->template_name = 'infilaw-survey-completion-notification-and-data';
                break;

            case 'csl_amazon_code':
                $this->template_name = 'csl-amazon-code';
                break;

            case 'fcsl_amazon_code':
                $this->template_name = 'fcsl-amazon-code';
                break;
            
            case 'welcome_email':
                $this->template_name = 'users-engagement-email-1-what-is-plexuss-mdl-1';
                break;

            case 'how_recruitment_work_after_one_week':
                $this->template_name = 'users-engagement-email-2-recruitment-mdl';
                break;
            
            case 'how_recruitment_work_after_two_week':
                $this->template_name = 'users-engagement-email-3-indicators-mdl';
                break;

            case 'how_recruitment_work_after_three_week':
                $this->template_name = 'users-engagement-email-4-less-than-30-mdl';
                break;

            case 'birthday_email':
                $this->template_name = 'users-happy-birthday';
                break;

            case 'users_friend_invite':
                $this->template_name = 'users-friend-invite-mdl';
                break;

            case 'new_college_ranking_for_colleges':
                $this->template_name = 'colleges-new-ranking';
                break;
            
            case 'test_message':
                $this->template_name = 'colleges-user-sends-your-college-a-message-test';
                break;

            case 'users_friend_invite_2_mdl':
                $this->template_name = 'users-friend-invite-2-mdl';
                break;

            case 'users_friend_invite_3_mdl':
                $this->template_name = 'users-friend-invite-3-mdl';
                break;

            case 'colleges_weekly_plexuss_update_mdl':
                $this->template_name = 'colleges-weekly-plexuss-update-mdl';
                break;

            case 'internal_triggers':
                $this->template_name = 'internal-triggers';
                break;

            case 'march_madness_invite_2016':
                $this->template_name = 'march-madness-invite-2016';
                break;

            case 'internal_college_upgrade_to_premiere_request':
                $this->template_name = 'internal-college-upgrade-to-premiere-request';
                break;

            case 'colleges_manage_portal_new_user':
                $this->template_name = 'colleges-manage-portal-new-user';
                break;

            case 'colleges_manage_portal_existing_account':
                $this->template_name = 'colleges-manage-portal-existing-account';
                break;

            case 'colleges_welcome_to_the_plexuss_premier_program':
                $this->template_name = 'colleges-welcome-to-the-plexuss-premier-program';
                break;

            case 'colleges_sample_premier_contract':
                $this->template_name = 'colleges-sample-premier-contract';
                break;

            case 'colleges_college_self_sign_up_request':
                $this->template_name = 'colleges-college-self-sign-up-request';
                break;
            
            case 'colleges_college_interested_in_premium_service':
                $this->template_name = 'colleges-college-interested-in-premium-service';
                break;

            case 'internal_remove_plexuss_account':
                $this->template_name = 'internal-remove-plexuss-account';
                break;
            
            case 'internal_manually_add_to_suppression':
                $this->template_name = 'internal-manually-add-to-suppression';
                break;

            case 'internal_why_unsbuscribed':
                $this->template_name = 'internal-why-unsbuscribed';
                break;

            case 'users_premium_order_confirmation':
                $this->template_name = 'users-premium-order-confirmation';
                break;

            case 'college_export_file':
                $this->template_name = 'college-export-file';
                break;

            case 'webinar_10_4_confirmation':
                $this->template_name = 'users-webinar-10-4-confirmation';
                break;

            case 'users_webinar_10_4_otero_initial_invite':
                $this->template_name = 'users-webinar-10-4-otero-initial-invite';
                break;

            case 'users_webinar_10_4_otero_15_minute_reminder':
                $this->template_name = 'users-webinar-10-4-otero-15-minute-reminder';
                break;
                
            case 'users_college_needs_more_info':
                $this->template_name = 'users-a-college-needs-more-info';
                break;

            case 'users_handshake_next_steps':
                $this->template_name = 'users-handshake-next-steps';
                break;

            case 'users_financial_documents':
                $this->template_name = 'users-financial-documents';
                break;

            case 'users_webinar_11_4_uic_initial_invite':
                $this->template_name = 'users-webinar-11-4-uic-initial-invite';
                break;

            case 'colleges_daily_recs_and_inquiries':
                $this->template_name = 'colleges-daily-recs-and-inquiries';
                break;

            case 'users_webinar_11_4_uic_15_minute_reminder':
                $this->template_name = 'users-webinar-11-4-uic-15-minute-reminder';
                break;

            case 'users_webinar_12_1_ttu_confirmation':
                $this->template_name = 'users-webinar-12-1-ttu-confirmation';
                break;

            case 'users_webinar_12_1_15_minute_reminder':
                $this->template_name = 'users-webinar-12-1-15-minute-reminder';
                break;

            case 'users_webinar_12_1_ttu_initial_invite':
                $this->template_name = 'users-webinar-12-1-ttu-initial-invite';
                break;

            case 'users_webinar_3_1_liberty_confirmation':
                $this->template_name = 'users-webinar-3-1-liberty-confirmation';
                break;

            case 'users_webinar_3_1_liberty_24_hour_reminder':
                $this->template_name = 'users-webinar-3-1-liberty-24-hour-reminder';
                break;

            case 'users_webinar_3_1_liberty_15_minute_reminder':
                $this->template_name = 'users-webinar-3-1-liberty-15-minute-reminder';
                break;

            case 'users_webinar_3_1_liberty_initial_invite':
                $this->template_name = 'users-webinar-3-1-liberty-initial-invite';
                break;
                
            case 'internal_urgent_email_from_college_rep':
                $this->template_name = 'internal-urgent-email-from-college-rep';
                break;

            case 'internal_urgent_email_from_agency_rep':
                $this->template_name = 'internal-urgent-email-from-agency-rep';
                break;
            
            case 'users_oneapp_invite_day_one':
                $this->template_name = 'users-oneapp-invite-day-one';
                break;

            case 'users_oneapp_invite_daily_followup':
                $this->template_name = 'users-oneapp-invite-daily-followup';
                break;

            case 'users_oneapp_daily_application_status':
                $this->template_name = 'users-oneapp-daily-application-status';
                break;

            case 'users_oneapp_invite_daily_followup_finished_app':
                $this->template_name = 'users-oneapp-invite-daily-followup-finished-app';
                break;

            case 'users_oneapp_invite_weekly_followup':
                $this->template_name = 'users-oneapp-invite-weekly-followup';
                break;

            case 'users_oneapp_invite_coveted_wkly_followup_post_10':
                $this->template_name = 'users-oneapp-invite-coveted-wkly-followup-post-10';
                break;
                
            case 'users_admitsee_email_major':
                $this->template_name = 'users-admitsee-email-major';
                break;

            case 'users_admitsee_email_school':
                $this->template_name = 'users-admitsee-email-school';
                break;
            
            case 'users_admitsee_email_country':
                $this->template_name = 'users-admitsee-email-country';
                break;

            case 'b2b_plexuss_weekly_digest_1':
                $this->template_name = 'b2b-plexuss-weekly-digest-1';
                break;

            case 'users_els_additional_info':
                $this->template_name = 'users-els-additional-info';
                break;

            case 'other_intern_survey':
                $this->template_name = 'other-intern-survey';
                break;

            case 'other_hiring_survey':
                $this->template_name = 'other-hiring-survey';
                break;

            case 'oneapp_internal_finished_app':
                $this->template_name = 'oneapp-internal-finished-app';
                break;
            
            case 'users_additional_uploads_required':
                $this->template_name = 'users-additional-uploads-required';
                break;
                
            case 'users_oneapp_demoted':
                $this->template_name = 'users-oneapp-demoted';
                break;

            case 'users_oneapp_promoted':
                $this->template_name = 'users-oneapp-promoted';
                break;

            case 'users_edx_college_credit_while_in_highschool':
                $this->template_name = 'users-edx-college-credit-while-in-highschool';
                break;

            case 'users_edx_free_courses_top_universities':
                $this->template_name = 'users-edx-free-courses-top-universities';
                break;
            
            case 'users_edx_ielts_and_toefl':
                $this->template_name = 'users-edx-ielts-and-toefl';
                break;

            case 'users_oneapp_scholarship_email':
                $this->template_name = 'users-oneapp-scholarship-email';
                break;
            
            case 'users_plexuss_mobile_app_annoucement':
                $this->template_name = 'users-plexuss-mobile-app-annoucement';
                break;

            case 'agencies_one_lead_a_day_non_registered':
                $this->template_name = 'agencies-one-lead-a-day-non-registered';
                break;
            
            case 'agencies_add_description':
                $this->template_name = 'agencies-add-description';
                break;

            case 'users_edx_free_courses_top_universities_nr':
                $this->template_name = 'users-edx-free-courses-top-universities-nr';
                break;
            
            case 'users_edx_free_courses_top_universities_events':
                $this->template_name = 'users-edx-free-courses-top-universities-events';
                break;

            case 'colleges_student_organically_inquired':
                $this->template_name = 'colleges-student-organically-inquired';
                break;
            
            case 'users_recommended_by_plexuss':
                $this->template_name = 'users-recommended-by-plexuss';
                break;

            case 'users_a_college_viewed_your_profile':
                $this->template_name = 'users-a-college-viewed-your-profile';
                break;

            case 'users_a_college_wants_to_recuit_you':
                $this->template_name = 'users-a-college-wants-to-recuit-you';
                break;

            case 'users_a_college_wants_to_recuit_you_2':
                $this->template_name = 'users-a-college-wants-to-recuit-you-2';
                break;

            case 'truscribe':
                $this->template_name = 'truscribe';
                break;

            case 'topuniversities_grad_school':
                $this->template_name = 'topuniversities-grad-school';
                break;

            case 'topuniversities_mba':
                $this->template_name = 'topuniversities-mba';
                break;

            default:
                # code...
                break;
        }
    }
}
