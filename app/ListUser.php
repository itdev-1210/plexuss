<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListUser extends Model {

	protected $table = 'list_users';

 	protected $fillable = array('fname', 'lname', 'org_branch_id', 'phone', 'created_at', 'updated_at');
}