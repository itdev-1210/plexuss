<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Agency extends Model
{
    protected $table = 'agency';

 	protected $fillable = array( 'type', 'name', 'web_url', 'logo_url', 'college_counseling',
 								 'tutoring_center', 'test_preparation', 'international_student_assistance',
 								 'detail', 'phone', 'city', 'state', 'country', 'active', 'balance', 'cost_per_approved',
 								 'num_of_filtered_rec', 'plexuss_note', 'skype_id', 'whatsapp_id' );


 	public function getAgencyProfile($user_id){

 		$agency = DB::table('agency as a')
 					->join('agency_permissions as ap', 'a.id', '=', 'ap.agency_id')
 					->where('ap.user_id', $user_id)
 					->first();

 		return $agency;
 	}

 	public function getAgencyUsers(){
 		$agency = DB::table('agency as a')
 					->join('agency_permissions as ap', 'a.id', '=', 'ap.agency_id')
 					->get();

 		return $agency;

 	}

 	// Returns an array with all agency countries where agency is active
 	public static function getAllAgencyCountries() {
 		$query = Agency::on('rds1')
 					   ->distinct()
 					   ->whereNotNull('country')
 					   ->where('active', 1)
 					   ->pluck('country');

 		if (isset($query)) {
 			return $query->toArray();
 		}

 		return [];
 	}
}
