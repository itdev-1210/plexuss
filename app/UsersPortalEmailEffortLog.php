<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPortalEmailEffortLog extends Model
{
    protected $table = 'users_portal_email_effort_logs';

 	protected $fillable = array( 'user_id', 'template_name', 'ro_id', 'company', 'params' );

 	public function saveLog($attr){

 		$x = $attr;
 		$v = $attr;

 		UsersPortalEmailEffortLog::create($x, $v);
 	}
}