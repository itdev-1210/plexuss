<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrlDictionary extends Model
{
    protected $table = 'tracking_page_url_dictionary';

    protected $fillable = array( 'url', 'title', 'sub_title', 'manual' );

}
