<?php
// use Cviebrock\EloquentSluggable\SluggableInterface;
// use Cviebrock\EloquentSluggable\SluggableTrait;

namespace App;

use Illuminate\Database\Eloquent\Model;


class NewsSubcategory extends Model
{

	// use SluggableTrait;

	// protected $sluggable = array(
 //        'build_from' => 'name',
 //        'save_to'    => 'slug',
 //    );

	protected $table = 'news_subcategories';

	protected $fillable = array('news_categories_id', 'name');

	public function category(){
		return $this->hasOne('NewsCategory');
	}

	public function newsarticles(){
		return $this->hasMany('NewsArticle');
	}
 
}
