<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ScholarshipProvider extends Model
{
    protected $table = "scholarship_providers";

    protected $fillable = array('id', 'company_name', 'contact_fname', 'contact_lname', 'phone', 'email', 'address', 'city', 'state', 'zip', 'country_id');

    /*************************************
    *	gets a provider given an id
    **************************************/
    public function getProvider($id){
    	if(!isset($id)) return null;

    	$res = DB::table('scholarship_providers as sp')->find($id);

    	return $res;
    }
}
