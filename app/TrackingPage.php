<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

use Request;

class TrackingPage extends Model
{
    //
    protected $table = 'tracking_pages';


	protected $fillable = array( 'ip', 'pixel', 'specid', 'camid', 'url', 'ref_url',
	 'slug', 'device', 'browser', 'user_id' , 'params', 'requestFormat', 'isAjax', 'created_at', 'updated_at');

	public function getNumCollegeView($user_id =null, $slug = null){

		$count = TrackingPage::on('rds1-log')
					->where('slug', $slug)
					->where('user_id', $user_id)
					->count();
		return $count;
	}

	public function getLastLoggedInDate($user_id = null){

		$tp = TrackingPage::on('rds1-log')->where('user_id', $user_id)
							->orderBy('id', 'DESC')
							->select('created_at')
							->first();
		if (!isset($tp)) {
			return  "N/A";
		}
		return $tp->created_at;
	}

	public function getLastLogForThisUserId($user_id = null){

		$tp = TrackingPage::on('rds1-log')->where('user_id', $user_id)
							->orderBy('id', 'DESC')
							->first();
		return $tp;
	}


	public function getUserActivity($user_id = null){

		if ($user_id == null) {
			return 0;
		}

		// $tp = TrackingPage::on('rds1-log')
		// 					->join('plexuss.users as u', 'u.id', '=', 'tracking_pages.user_id')
		// 					->where('u.id', $user_id)
		// 					->count('tracking_pages.user_id');

		$tp = TrackingPage::on('rds1-log')
							->whereIn('user_id', $user_id)
							->select(DB::raw('count(DISTINCT id) as cnt, user_id'))
							->groupBy('user_id')
							->get();

		if (!isset($tp)) {
			return 0;
		}
		
		return $tp;
	}

	public function getCollegePageViews($slug){
		
		if (Cache::has(env('ENVIRONMENT') .'_getCollegePageViews_'. $slug)) {
			$count = Cache::get(env('ENVIRONMENT') .'_getCollegePageViews_'. $slug);
		}
		$url = Request::url();
		$pos = strpos($url, '/', strpos($url, '/') + 2);

		$url = substr($url, 0, $pos);
		$url = $url ."/college/".$slug."%";

		$count = TrackingPage::on('rds1-log')
					->where('url', 'LIKE', $url)
					->count();
		Cache::put(env('ENVIRONMENT').'_getCollegePageViews_', $slug, 460);
		
		return $count;
	}
}
