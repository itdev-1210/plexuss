<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ScholarshipsUserApplied extends Model
{
    protected $table = 'scholarships_user_applied';

    protected $fillable = array('id', 'user_id', 'scholarship_id', 'status', 'last_status', 'email_sent');

    /*************************************
    * gets scholarships for portal  -- gets all that a user has interacted with minus removed
    ***************************************/
 	public function getUsersScholarships($uid){

 		$res = DB::table('scholarships_user_applied as sua')
 						->join('scholarship_verified as sv', 'sv.id', '=', 'sua.scholarship_id')
 						->join('scholarship_providers', 'scholarship_providers.id', '=', 'sv.provider_id')
 						->where('sua.user_id', '=', $uid)
 						->where('sua.status', '!=', 'removed')
 						->get();
                        
 		return $res;	
 	}


 	/***********************************
 	* removing scholarships 
 	*******************************************/
 	public function trashScholarships($uid, $arr){

 		foreach ($arr as $key => $value) {
 			$sua = ScholarshipsUserApplied::where('user_id', $uid)
 										  ->where('scholarship_id', $value)
 										  ->first();

			if (isset($sua)) {
			  	
			  	$sua->last_status = $sua->status;
			  	$sua->status      = 'removed';

			  	$sua->save();
			} 										  
 		}

	  	return "success";
 	}

 	/*******************************/
 	public function getScholarshipsForPortal($uid, $status){

 		if(!isset($uid)) return 'error';

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->leftJoin('scholarships_user_applied as sua', function($qry) use ($uid){
 										 	   $qry->on('sua.scholarship_id', '=', 'sv.id')
 										 	   ->where('sua.user_id', '=', $uid);
 										 })
 										 	
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
 										 		  'sv.website',
 										 		  'sv.num_of_awards as numberof',
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description',
 										 		  'sua.status')
 										 ->where('sua.status' , $status)
 										 ->paginate(10);

 		return $results;
 	}
}
