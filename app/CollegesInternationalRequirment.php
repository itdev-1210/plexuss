<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;

class CollegesInternationalRequirment extends Model
{
    protected $table = 'colleges_international_requirements';
	protected $fillable = array('cit_id', 'college_id', 'type', 'attachment_url', 'title', 'description', 'view_type');

	public function removeInternationalRequirment($id){
		$cit = CollegesInternationalRequirment::find($id);
		$cit->delete();

		return "success";
	}

	public function removeInternationalAttachment($id){
		$cit = CollegesInternationalRequirment::find($id);

		if (strpos($cit->attachment_url, 's3-us-west-2.amazonaws.com') !== FALSE){
			$url = $cit->attachment_url;
			$bucket_url = 'asset.plexuss.com/admin/internationalTools';
			$keyname    = substr($url, strrpos($url, '/') + 1);

			$bc = new Controller;
			$bc->generalDeleteFile($bucket_url, $keyname);
		}
		$cit->attachment_url = null;

		$cit->save();

		return "success";
	}
}
