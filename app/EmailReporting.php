<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailReporting extends Model {

	protected $table = 'email_template_sender_providers';
	protected $fillable = array( 'template_name', 'sparkpost_key', 'sendgrid_key', 'provider','client_name','category_id');

}