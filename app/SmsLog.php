<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsLog extends Model {


	protected $table = 'sms_log';
	protected $fillable = array( 'campaign_id', 'sender_user_id', 'receiver_user_id', 'is_list_user', 'sid', 'from', 'to',
								 'smsBy', 'status', 'error_url', 'body', 'price', 'price_unit', 'error_code', 'error_message',
								 'num_media', 'num_segments', 'thread_id', 'college_id');

}
