<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicProfileSkillsEndorsements extends Model
{
    protected $table = 'public_profile_skills_endorsements';

    protected $fillable = ['public_profile_skills_id', 'endorser_user_id', 'updated_at', 'created_at'];
}
