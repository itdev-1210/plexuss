<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageCronTracker extends Model
{
    protected $table = 'tracking_page_cron_trackers';

	protected $fillable = array( 'from_date', 'to_date', 'num_ran', 'total' );

}
