<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
  protected $table = 'sn_article_tags';
  protected $fillable = array('tag_number', 'social_article_id');

  public function articles()
  {
      return $this->hasMany('App\SocialArticle');
  }
}
