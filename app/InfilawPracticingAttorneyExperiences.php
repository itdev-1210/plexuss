<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfilawPracticingAttorneyExperiences extends Model {


	protected $table = 'infilaw_practicing_attorney_experiences';

 	protected $fillable = array( 'name');
}