<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class CollegeRecommendationFilters extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'college_recommendation_filters';



 	protected $fillable = array('agency_id', 'college_id', 'org_branch_id', 'type', 'category', 'name', 'org_portal_id', 'aor_id','aor_portal_id','scholarship_id', 'post_id', 'share_article_id');

 	public function clearFilter($agency_id , $category = null, $college_id = null, $org_branch_id = null, $org_portal_id = NULL, $aor_id = NULL, $post_id = NULL, $share_article_id = NULL){
 		
 		if ($category == null && $college_id == null && $org_branch_id == null && $agency_id == null && 
 			$post_id == NULL && $share_article_id == NULL) {
 			return;
 		}

 		$filter_log = CollegeRecommendationFilters::where('category', $category);
								 					
 		
 		if (isset($agency_id)) {
 			
 			$filter_log = $filter_log->where('agency_id', $agency_id);

 		}else{
 			$filter_log = $filter_log->where('college_id', $college_id)
								 	 ->where('org_branch_id', $org_branch_id);
 		}

 		if (isset($org_portal_id)) {
 			$filter_log = $filter_log->where('org_portal_id', $org_portal_id);
 		}else{
 			$filter_log = $filter_log->whereNull('org_portal_id');
 		}

 		if (isset($aor_id)) {
 			$filter_log = $filter_log->where('aor_id', $aor_id);
 		}else{
 			$filter_log = $filter_log->whereNull('aor_id');
 		}

 		if (isset($post_id)) {
 			$filter_log = $filter_log->where('post_id', $post_id);
 		}else{
 			$filter_log = $filter_log->whereNull('post_id');
 		}

 		if (isset($share_article_id)) {
 			$filter_log = $filter_log->where('share_article_id', $share_article_id);
 		}else{
 			$filter_log = $filter_log->whereNull('share_article_id');
 		}

 		$filter_log->delete();
 	}
	
	public function clearFilter_scholarship($category = null,$scholarship_id){
 		if ($category == null) {
 			return;
 		}
		$filter_log = CollegeRecommendationFilters::where('category', $category);
		$filter_log = $filter_log->where('scholarship_id', $scholarship_id);							 					
 		$filter_log->delete();
 	}

 	public function saveFilter($agency_id, $college_id, $org_branch_id, $type, $category, $name, $org_portal_id, $aor_id=null, 
 							   $post_id = NULL, $share_article_id = NULL){

 		$attr = array('agency_id'=> $agency_id, 'college_id'=> $college_id, 'org_branch_id'=> $org_branch_id, 
 					  'type'=> $type, 'category'=> $category, 'name'=> $name, 'org_portal_id' => $org_portal_id, 
 					  'aor_id' => $aor_id, 'post_id' => $post_id, 'share_article_id' => $share_article_id);
 		$val =  array('agency_id'=> $agency_id, 'college_id'=> $college_id, 'org_branch_id'=> $org_branch_id, 
 					  'type'=> $type, 'category'=> $category, 'name'=> $name, 'org_portal_id' => $org_portal_id, 
 					  'aor_id' => $aor_id, 'post_id' => $post_id, 'share_article_id' => $share_article_id);


 		$qry = CollegeRecommendationFilters::updateOrCreate($attr, $val);

 		return $qry;

 		
 	}
	
	public function saveFilter_scholarship($agency_id=null, $college_id=null, $org_branch_id=null, $type, $category, $name, $org_portal_id=null, $aor_id=null,$scholarship_id=null){

 	$attr = array('agency_id'=> $agency_id, 'college_id'=> $college_id, 'org_branch_id'=> $org_branch_id, 'type'=> $type, 'category'=> $category, 'name'=> $name, 'org_portal_id' => $org_portal_id, 'aor_id' => $aor_id, 'scholarship_id' => $scholarship_id);
 	$val =  array('agency_id'=> $agency_id, 'college_id'=> $college_id, 'org_branch_id'=> $org_branch_id, 'type'=> $type, 'category'=> $category, 'name'=> $name, 'org_portal_id' => $org_portal_id, 'aor_id' => $aor_id, 'scholarship_id' => $scholarship_id);


 	$qry = CollegeRecommendationFilters::updateOrCreate($attr, $val);

 	return $qry;

 		
 	}

 	public function getFiltersAndLogs($agency_id, $college_id, $org_branch_id, $category=null, $org_portal_id=null, $aor_id=null,$aor_portal_id=null, $department_query=null, $inside=null, $scholarship_id = NULL, $post_id = NULL, $share_article_id = NULL){

 		$qry = DB::connection('rds1')->table('college_recommendation_filters as crf')
 					->leftjoin('college_recommendation_filter_logs as crfl', 'crfl.rec_filter_id', '=', 'crf.id');

 		if(!isset($inside)){
			$qry = $qry->select('crf.type', 'crf.category', 'crf.name', 'crfl.val');
		}
		else{
			$qry = $qry->select('crf.type', 'crf.category', 'crf.name',
 							 DB::raw("concat(substring_index(val,',',1),',,',substring_index(val,',',-1)) as val"))
 				->where('category','=','majorDeptDegree')
 				->groupBy('val');
		}

 		if (!isset($agency_id) && isset($college_id)) {
 			$qry = $qry->where('college_id', $college_id);
 		}elseif(isset($agency_id)){
 			$qry = $qry->where('agency_id', $agency_id);
 		}

 		if (isset($post_id)) {
 			$qry = $qry->where('post_id', $post_id); 		
 		}

 		if (isset($share_article_id)) {
 			$qry = $qry->where('share_article_id', $share_article_id); 		
 		}

 		if (isset($scholarship_id)) {
 			$qry = $qry->where('scholarship_id', $scholarship_id); 		
 		}

 		if (isset($category)) {
 			$qry = $qry->where('category', $category);
 		}

 		if (isset($org_branch_id) && !isset($agency_id)) {
 			$qry = $qry->where('org_branch_id', $org_branch_id);
 		}

 		if (isset($org_portal_id)) {
 			$qry = $qry->where('org_portal_id', $org_portal_id);
 		}else{
 			$qry = $qry->whereNull('org_portal_id');
 		}

 		if (isset($aor_id)) {
 			$qry = $qry->where('aor_id', $aor_id);
 			if (isset($aor_portal_id)){
 				$qry = $qry->where('aor_portal_id', $aor_portal_id);
 			}
 		}else{
 			$qry = $qry->whereNull('aor_id')
 					   ->whereNull('aor_portal_id');
 		}

		if (isset($department_query)){
			$qry = $qry->where('category','!=','majorDeptDegree');
			$cue = $this->getFiltersAndLogs($agency_id, $college_id, $org_branch_id, null, $org_portal_id, $aor_id, $aor_portal_id, null, true);
			$qry = $qry->union($cue);
		}
		if(!isset($inside) && !isset($department_query)){
			$departments_as_majors = clone $qry;
			$departments_as_majors = $departments_as_majors
				->where('category', "majorDeptDegree")
				->where('crfl.val', 'NOT LIKE', "%,,%")
				->join('department as d', 'd.id', '=', DB::raw("substring_index(val,',',1)"))
				->join('majors as m', 'm.name', '=', 'd.name')
				->select('crf.type', 'crf.category', 'crf.name',
						 DB::raw("concat(d.id, ',', m.id, ',', substring_index(val,',',-1)) as val"));
			$qry = $qry->union($departments_as_majors);
		}
		if(!isset($inside)){
			$qry = $qry->orderBy('category')
					   ->get();
		}

		return $qry;
	}
	
	public function getFiltersAndLogs_scholarship($scholarship_id = NULL){

 		$qry = DB::connection('rds1')->table('college_recommendation_filters as crf')
 					->leftjoin('college_recommendation_filter_logs as crfl', 'crfl.rec_filter_id', '=', 'crf.id');

 		if(!isset($inside)){
 			$qry = $qry->select('crf.type', 'crf.category', 'crf.name', 'crfl.val');
		}
		else{
			
			$qry = $qry->select('crf.type', 'crf.category', 'crf.name',
 							 DB::raw("concat(substring_index(val,',',1),',,',substring_index(val,',',-1)) as val"))
 				->where('category','=','majorDeptDegree')
 				->groupBy('val');
		}

 		
 	   $qry = $qry->where('scholarship_id', $scholarship_id); 		
 	

		
		if(!isset($inside) && !isset($department_query)){
			$departments_as_majors = clone $qry;
			$departments_as_majors = $departments_as_majors
				->where('category', "majorDeptDegree")
				->where('crfl.val', 'NOT LIKE', "%,,%")
				->join('department as d', 'd.id', '=', DB::raw("substring_index(val,',',1)"))
				->join('majors as m', 'm.name', '=', 'd.name')
				->select('crf.type', 'crf.category', 'crf.name',
						 DB::raw("concat(d.id, ',', m.id, ',', substring_index(val,',',-1)) as val"));
			$qry = $qry->union($departments_as_majors);
		}
		if(!isset($inside)){
			$qry = $qry->orderBy('category')
					   ->get();
		}

		return $qry;
	}

	public function generateFilterQry($data, $do_not_check = NULL){

		if (isset($data['agency_collection']->id)) {
			$agency_id = $data['agency_collection']->id;
		}else{
			$agency_id = null;
		}

		$org_portal_id = NULL;
		if (isset($data['default_organization_portal'])) {
			$org_portal_id = $data['default_organization_portal']->id;
		}

		$aor_id = NULL;
		if (isset($data['aor_id'])) {
			$aor_id = $data['aor_id'];
		}

		$aor_portal_id = NULL;
		if (isset($data['aor_portal_id'])) {
			$aor_portal_id = $data['aor_portal_id'];
		}

		$scholarship_id = NULL;
		if (isset($data['scholarship_id'])) {
			$scholarship_id = $data['scholarship_id'];
		}

		$post_id = NULL;
		if (isset($data['post_id'])) {
			$post_id = $data['post_id'];
		}

		$share_article_id = NULL;
		if (isset($data['share_article_id'])) {
			$share_article_id = $data['share_article_id'];
		}

		$org_school_id = NULL;
		if (isset($data['org_school_id'])) {
			$org_school_id = $data['org_school_id'];
		}

		$org_branch_id = NULL;
		if (isset($data['org_branch_id'])) {
			$org_branch_id = $data['org_branch_id'];
		}
 		
		if(isset($data['department_query'])){
			$filters = $filters = $this->getFiltersAndLogs($agency_id, $org_school_id, $org_branch_id, null, $org_portal_id, $aor_id, $aor_portal_id, true, null, $scholarship_id, $post_id, $share_article_id);
		}
		else{
			$filters = $this->getFiltersAndLogs($agency_id, $org_school_id, $org_branch_id, null, $org_portal_id, $aor_id, $aor_portal_id, null, null, $scholarship_id, $post_id, $share_article_id);
		}
		//dd($data['org_school_id'] . ' '. $data['org_branch_id']);
		if (empty($filters)) {
			return null;
		}
		$ret = array();
		$name = '';
		if (isset($filters)) {
			$temp = array();

			foreach ($filters as $key) {
				if ($name == '') {
					
					$name = $key->name;

					$temp = array();
					$temp['type'] = $key->type;
					$temp['category'] = $key->category;
					$temp['filter'] = $key->name;
					if (isset($key->val)) {
						$temp[$key->name] = array();
						$temp[$key->name][] = $key->val;
					}
				}elseif($name == $key->name){

					if (isset($key->val)) {
						$temp[$key->name][] = $key->val;
					}
					$name = $key->name;
				}else{
					$ret[] = $temp;

					$name = $key->name;

					$temp = array();
					$temp['type'] = $key->type;
					$temp['category'] = $key->category;
					$temp['filter'] = $key->name;
					if (isset($key->val)) {
						$temp[$key->name] = array();
						$temp[$key->name][] = $key->val;
					}

				}
			}

			$ret[] = $temp;
		}

		$ret =json_decode(json_encode($ret));
		$qry = $this->globalMethodGenerateFilterQry($ret, null, $do_not_check);
		
		return $qry;
 	}


 	public function globalMethodGenerateFilterQry($ret, $is_advanced_search, $do_not_check = NULL){
 		// echo "<pre>";
 		// print_r($ret);
 		// echo "</pre>";
 		// exit();
 		$db_host = (isset($is_advanced_search) && $is_advanced_search == true) ? $db_host = "rds1" : $db_host = "bk";
 		$db_host = 'rds1';

 		$qry = DB::connection($db_host)->table('plexuss.users as userFilter')
 								   ->leftjoin('plexuss.nrccua_users as nu', 'userFilter.id', '=', 'nu.user_id');

		$city_table 					   = false;
		$state_table 					   = false;
		$scores_table 					   = false;
		$objectives_table 				   = false;
		$major_table 					   = false;
		$department_table 				   = false;
		$ethnicity_table 				   = false;
		$religion_table 				   = false;
		$degree_table 					   = false;
		$intl_filter 					   = false;
		$country_table 					   = false;
		$financial_filter 				   = false;
		$military_affiliation_table 	   = false;
		$plexuss_users_verifications_table = false;
		$users_applied_colleges_table	   = false;
		$users_custom_questions_table	   = false;

		// $profile_completion_table = false; 

		if (isset($ret)) {
			foreach ($ret as $key) {
				if (!isset($key->category)) {
					continue;
				}
				$cat = $key->category;
				switch ($cat) {
					case 'hasApplied':
						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (isset($key->hasApplied)) {
							if (!$users_applied_colleges_table) {
								$users_applied_colleges_table = true;
								$qry = $qry->join('plexuss.users_applied_colleges as upc', 'upc.user_id', '=', 'userFilter.id');
							}

							foreach ($key->hasApplied as $k => $v) {
								$qry = $qry->where('upc.college_id', $v);
							}
						}
						break;
					case 'application':
						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (isset($key->application)) {

							foreach ($key->application as $k => $v) {
								if ($v == "less-than-30") {
									$qry = $qry->where('userFilter.profile_percent', '<', 30);
								}elseif ($v == "30-or-more") {
									$qry = $qry->where('userFilter.profile_percent', '>=', 30);
								}elseif ($v == "has-selected-schools") {
									if (!$users_applied_colleges_table) {
										$users_applied_colleges_table = true;
										$qry = $qry->join('plexuss.users_applied_colleges as upc', 'upc.user_id', '=', 'userFilter.id');
									}
								}elseif ($v == "app-completed") {
									if (!$users_custom_questions_table) {
										$qry = $qry->join('plexuss.users_custom_questions as ucq', function($q){
													       $q->on('ucq.user_id', '=', 'userFilter.id');
													       $q->on('ucq.application_submitted', '=', DB::raw(1));
										});
									}
								}else{
									$qry = $qry->join('plexuss.users_custom_questions as ucq', function($q) use ($v){
													       $q->on('ucq.user_id', '=', 'userFilter.id');
													       $q->on('ucq.application_state', '=', DB::raw("'".$v."'"));
										});
								}
							}
						}
						break;
					case 'interview':
						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (isset($key->interview)) {

							if (!$plexuss_users_verifications_table) {
								$plexuss_users_verifications_table = true;
								$qry = $qry->join('plexuss.plexuss_users_verifications as puv', 'puv.user_id', '=', 'userFilter.id');
							}

							$plexuss_users_verifications_arr = $key->interview;

							$qry = $qry->where(function($q) use($plexuss_users_verifications_arr){
								foreach ($plexuss_users_verifications_arr as $k => $v) {
									if ($v == "notHappened") {
										$str = "Interview did not Happen";
									}elseif ($v == "scheduled") {
										$str = "Scheduled Interview";
									}elseif ($v == "notScheduled") {
										$str = "Not Scheduled Interview";
									}else{
										$str = $v;
									}
									$q->orWhere('puv.status', '=', $str);
								}
							});
						}
						break;
					case 'contact':
						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (isset($key->contact)) {

							$contact_arr = $key->contact;

							foreach ($contact_arr as $k => $v) {
								
								if ($v == 'verifiedText') {
									$qry = $qry->where('userFilter.verified_phone', '=', 1);
								}
							}
						}
						break;
					case 'location':
						if ($key->filter == 'intl_filter' && $key->type == 'exclude') {
							$qry = $qry->where(function ($query) {
								    $query->orWhere('userFilter.country_id', 1)
										   ->orwhereRaw('`userFilter`.`country_id` IS NULL');
								});
							$intl_filter = true;
						
						}elseif ($key->filter == 'us_filter' && $key->type == 'exclude') {
							$qry = $qry->where(function ($query) {
								    $query->orWhere('userFilter.country_id', '!=' ,1)
										   ->orwhereRaw('`userFilter`.`country_id` IS NULL');
								});
							$intl_filter = true;
						
						}elseif ($key->filter == 'state') {
							if (!$state_table) {
								$state_table = true;
								// $qry = $qry->leftjoin('plexuss.states as st', 'st.state_abbr', '=', 'userFilter.state');

								// The reason for 1=1 is to make it work in laravel, I could'nt left join RAW
								$qry = $qry->leftjoin('plexuss.states as st', DB::raw("Case
														When `userFilter`.`address` is not null
														and length(`userFilter`.`state`) = 2
														and `userFilter`.`country_id` = 1 Then
															`st`.`state_abbr` = `userFilter`.`state`
														When `userFilter`.`address` is not null
														and length(`userFilter`.`state`) > 2
														and `userFilter`.`country_id` = 1 Then
															`st`.`state_name` = `userFilter`.`state`
														When `userFilter`.`address` is null Then
														`st`.`state_abbr` = `nu`.`state` end and 1
															"), "=", DB::raw("1"));
							}

							if ($key->type == 'include') {
								
								$includeExclude = '=';
							}else{
								$includeExclude = '!=';
							}

							$qry = $qry->where(function($q) use ($key, $includeExclude) {
									foreach ($key->state as $k => $v) {
										$q = $q->orWhere('st.state_name', $includeExclude, $v);
									}
							});

						}elseif ($key->filter == 'city') {
							if (!$city_table) {
								$city_table = true;
							}

							if ($key->type == 'include') {
								
								$includeExclude = '=';
							}else{
								$includeExclude = '!=';
							}

							$qry = $qry->where(function($q) use ($key, $includeExclude) {
									foreach ($key->city as $k => $v) {
										$q = $q->orWhere('userFilter.city', $includeExclude, $v);
									}
							});
							
						}elseif ($key->filter == 'zipcode') {
							if (isset($do_not_check['zipcode'])) {
								continue;
							}
							if (!$city_table) {
								$city_table = true;
							}

							if ($key->type == 'include') {
								
								$includeExclude = '=';
							}else{
								$includeExclude = '!=';
							}
							$qry = $qry->where(function($q) use ($key) {
								foreach ($key->zipcode as $k => $v) {
									$arr = explode(",", $v);
									$q = $q->whereIn('userFilter.zip', $arr);					
								}
							});
							
						}elseif ($key->filter == 'country') {

							if (!$country_table) {
								$country_table = true;
								$qry = $qry->leftjoin('plexuss.countries as ct', 'ct.id', '=', 'userFilter.country_id');
							}

							if ($key->type == 'include') {
								
								$includeExclude = '=';
							}else{
								$includeExclude = '!=';
							}

							if ($key->type == 'include') {

								$country_arr = $key->country;
								$qry = $qry->where(function($q) use($country_arr, $includeExclude){
									foreach ($country_arr as $k => $v) {
										$q->orWhere('ct.country_name', $includeExclude, $v);
									}
								});
								
							}else{
								foreach ($key->country as $k => $v) {
									$qry = $qry->where('ct.country_name', $includeExclude, $v);
								}
							}
							
						}
						break;				
					case 'majorDeptDegree':
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (!$objectives_table) {
							$objectives_table = true;
							$qry = $qry->join('plexuss.objectives as o', 'o.user_id', '=', 'userFilter.id');
						}

						if (!$major_table) {
							$major_table = true;
							$qry = $qry->join('plexuss.majors as m', 'm.id', '=', 'o.major_id');
						}

						if (!$department_table) {
							$department_table = true;
							$qry = $qry->join('plexuss.department as d', 'd.id', '=', 'm.department_id');
						}

						if (isset($key->majorDeptDegree)) {
							$major_arr = $key->majorDeptDegree;

							if ($key->type == 'include') {
								$qry = $qry->where(function($q) use($major_arr, $includeExclude){
									foreach ($major_arr as $k) {
										$val = explode(",", $k);

										$q->orWhere(function($q2) use ($val, $includeExclude){

											if (isset($val[0]) && !empty($val[0])) {
												$q2 = $q2->where('d.id', $includeExclude, $val[0]);
											}

											if (isset($val[1]) && !empty($val[1])) {
												$q2 = $q2->where('o.major_id', $includeExclude, $val[1]);
											}

											if (isset($val[2]) && !empty($val[2])) {
												$q2 = $q2->where('o.degree_type', $includeExclude, $val[2]);
											}
											
										});
									}
								});
							}else{
								foreach ($major_arr as $k) {
									$val = explode(",", $k);

									if (isset($val[0]) && !empty($val[0]) && isset($val[1]) && !empty($val[1]) && isset($val[2]) && !empty($val[2])) {
										$qry = $qry->whereRaw('(`d`.`id`,`o`.`major_id`,`o`.`degree_type`)'.$includeExclude.'('.$k.')');
									}

									else{
										$qry = $qry->whereRaw('(`d`.`id`,`o`.`degree_type`)'.$includeExclude.'('.$val[0].','.$val[2].')');
									}		
								}
							}			
						}
										
						break;
					case 'major':

						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (!$objectives_table) {
							$objectives_table = true;
							$qry = $qry->join('plexuss.objectives as o', 'o.user_id', '=', 'userFilter.id');
						}

						if (!$major_table) {
							$major_table = true;
							$qry = $qry->join('plexuss.majors as m', 'm.id', '=', 'o.major_id');
						}

						if (!$department_table) {
							$department_table = true;
							$qry = $qry->join('plexuss.department as d', 'd.id', '=', 'm.department_id');
						}
						
						if(isset($key->department)){
							if ($key->type == 'include') {
								
								$department_arr = $key->department;
								$qry = $qry->where(function($q) use($department_arr, $includeExclude){
									foreach ($department_arr as $k => $v) {
										$q->orWhere('d.name', $includeExclude, $v);
									}
								});
							}else{
								foreach ($key->department as $k => $v) {
									$qry = $qry->where('d.name', $includeExclude, $v);
								}
							}
						}else{
							if ($key->type == 'include') {

								$major_arr = $key->major;
								$qry = $qry->where(function($q) use($major_arr, $includeExclude){
									foreach ($major_arr as $k => $v) {
										$q->orWhere('m.name', $includeExclude, $v);
									}
								});
							}else{
								foreach ($key->major as $k => $v) {
									$qry = $qry->where('m.name', $includeExclude, $v);
								}
							}

						}					
						break;
					case 'scores':

						if (!$scores_table) {
							$scores_table = true;
							$qry = $qry->join('plexuss.scores as s', 's.user_id', '=', 'userFilter.id');
						}
						if ($key->filter == 'gpa_filter' && isset($key->gpa_filter[0])) {
							$key->gpa_filter = explode(",", $key->gpa_filter[0]);
							if (count($key->gpa_filter) == 2) {
								$arr = $key->gpa_filter;
								$qry = $qry->where(function($q) use($arr){
									$q->whereBetween(DB::raw("coalesce(s.hs_gpa, s.overall_gpa, if(weighted_gpa is null, null, least(weighted_gpa, 4)), nu.gpa)"), $arr);		
								});
							}

						} elseif ($key->filter == 'weighted_gpa_filter' && isset($key->weighted_gpa_filter)) {

							if (count($key->weighted_gpa_filter) == 2) {
								$arr = $key->weighted_gpa_filter;
								$qry = $qry->where(function($q) use($arr){
									$q->whereBetween('s.weighted_gpa', $arr);			
								});
							}

						} elseif ($key->filter == 'college_gpa_filter' && isset($key->college_gpa_filter)) {

							if (count($key->college_gpa_filter) == 2) {
								$arr = $key->college_gpa_filter;
								$qry = $qry->where(function($q) use($arr){
									$q->whereBetween('s.overall_gpa', $arr);			
								});
							}

						} elseif ($key->filter == 'sat_act') {
							if (count($key->sat_act) == 2){
								$qry = $qry->where(function($qry) use($key){
									foreach ($key->sat_act as $k => $v) {
										$arr = explode(',', $v);
										if ($arr[0] == 'sat'){
											$que = $qry->orWhereBetween('s.sat_total', array_slice($arr,1));
										}else{
											$que = $qry->orWhereBetween('s.act_composite', array_slice($arr,1));
										}
									}
								});
							}else{
								foreach ($key->sat_act as $k => $v) {
									$arr = explode(',', $v);
									if ($arr[0] == 'sat'){
										$qry = $qry->whereBetween('s.sat_total', array_slice($arr,1));
									}else{
										$qry = $qry->whereBetween('s.act_composite', array_slice($arr,1));
									}
								}
							}
						}elseif ($key->filter == 'ielts_toefl') {
							$qry = $qry->where(function($qry) use($key){
								
								foreach ($key->ielts_toefl as $k => $v) {
									$arr = explode(',', $v);
									if ($arr[0] == 'ielts'){
										$qry = $qry->orWhereBetween('s.ielts_total', array_slice($arr,1));
									}else{
										$qry = $qry->orWhereBetween('s.toefl_total', array_slice($arr,1));
									}
								}
							});
						}elseif ($key->filter == 'sat_filter') {

								$arr = $key->sat_filter;
								$qry = $qry->whereBetween('s.sat_total', $arr);
						}elseif ($key->filter == 'act_filter') {

								$arr = $key->act_filter;
								$qry = $qry->whereBetween('s.act_composite', $arr);
						}
						break;					
					case 'uploads':
						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						$qry = $qry->join('plexuss.transcript as tt', 'tt.user_id', '=', 'userFilter.id');

						if ($key->type == 'exclude') {
							foreach ($key->uploads as $k => $v) {
								$val = '';
								if ($v == 'transcript_filter') {
									$val = 'transcript';
								}elseif ($v == 'toefl_filter') {
									$val = 'toefl';
								}elseif ($v == 'resume_filter') {
									$val = 'resume';
								}elseif ($v == 'financialInfo_filter') {
									$val = 'financial';
								}elseif ($v == 'ielts_fitler') {
									$val = 'ielts';
								}elseif ($v == 'passport_filter') {
									$val = 'passport';
								}elseif ($v == 'essay_filter') {
									$val = 'essay';
								}elseif ($v == 'prescreen_interview_filter') {
									$val = 'prescreen_interview';
								}elseif ($v == 'other_filter') {
									$val = 'other';
								}

								if ($val != '') {
									$qry = $qry->where('tt.doc_type', $includeExclude, $val);
								}
								
							}
						}else{

							$uploads_local = $key->uploads;
							$qry = $qry->where(function($q) use($uploads_local, $includeExclude){
								foreach ($uploads_local as $k => $v) {

									$val = '';
									if ($v == 'transcript_filter') {
										$val = 'transcript';
									}elseif ($v == 'toefl_filter') {
										$val = 'toefl';
									}elseif ($v == 'resume_filter') {
										$val = 'resume';
									}elseif ($v == 'financialInfo_filter') {
										$val = 'financial';
									}elseif ($v == 'ielts_fitler') {
										$val = 'ielts';
									}elseif ($v == 'passport_filter') {
										$val = 'passport';
									}elseif ($v == 'essay_filter') {
										$val = 'essay';
									}elseif ($v == 'prescreen_interview_filter') {
										$val = 'prescreen_interview';
									}elseif ($v == 'other_filter') {
										$val = 'other';
									}

									if ($val != '') {
										$q->orWhere('tt.doc_type', $includeExclude, $val);
									}
								}
							});


						}
						break;					
					case 'demographic':

						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if ($key->filter == 'ageMin_filter') {
							foreach ($key->ageMin_filter as $k => $v) {

								$qry = $qry->where(DB::raw('date(userFilter.birth_date)'), '<=', substr(Carbon::now()->subYears($v), 0, 10));
							}
						}elseif ($key->filter == 'ageMax_filter') {
							foreach ($key->ageMax_filter as $k => $v) {

								$qry = $qry->where(DB::raw('date(userFilter.birth_date)'), '>=', substr(Carbon::now()->subYears($v), 0, 10));
							}
						}elseif ($key->filter == 'gender') {
							foreach ($key->gender as $k => $v) {
								if ($v == "female") {
									$v = 'f';
								}else{
									$v = 'm';
								}
								$qry = $qry->where('userFilter.gender', $includeExclude, $v);
							}
						}elseif ($key->filter == 'ethnicity' || $key->filter == 'include_eth_filter') {

							if (!$ethnicity_table) {
								$ethnicity_table = true;
								$qry = $qry->leftjoin('plexuss.ethnicities as eth', 'eth.id', '=', 'userFilter.ethnicity');
							}

							if (isset($key->ethnicity)) {
								$ethnicity_arr = $key->ethnicity;
							}else{
								$ethnicity_arr = $key->include_eth_filter;
							}

							if ($key->type == 'include') {
								
								$qry = $qry->where(function($q) use($ethnicity_arr, $includeExclude){
											foreach ($ethnicity_arr as $k => $v) {
												$q->orWhere('eth.ethnicity', $includeExclude, $v);
											}
										});
							}else{
								foreach ($ethnicity_arr as $k => $v) {
									$qry = $qry->where('eth.ethnicity', $includeExclude, $v);
								}
							}
						}elseif ($key->filter == 'religion' || $key->filter == 'include_rgs_filter') {

							if (!$religion_table) {
								$religion_table = true;
								$qry = $qry->leftjoin('plexuss.religions as rgs', 'rgs.id', '=', 'userFilter.religion');
							}

							if (isset($key->religion)) {
								$religion_arr = $key->religion;
							}else{
								$religion_arr = $key->include_rgs_filter;
							}
							
							if ($key->type == 'include') {

								$qry = $qry->where(function($q) use($religion_arr, $includeExclude){
											foreach ($religion_arr as $k => $v) {
												$q->orWhere('rgs.religion', $includeExclude, $v);
											}
										});
							}else{
								foreach ($religion_arr as $k => $v) {
									$qry = $qry->where('rgs.religion', $includeExclude, $v);
								}
							}
						}
						
						break;						
					case 'educationLevel':

						foreach ($key->educationLevel as $k => $v) {
							if ($v == 'hsUsers_filter') {
								$v = 0;
							}else{
								$v = 1;
							}
							$qry = $qry->where('userFilter.in_college', $v);
						}
						break;				
					case 'desiredDegree':

						if ($key->type == 'include') {
								
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (!$objectives_table) {
							$objectives_table = true;
							$qry = $qry->join('plexuss.objectives as o', 'o.user_id', '=', 'userFilter.id');
						}
						if (!$degree_table) {
							$degree_table = true;
							$qry = $qry->join('plexuss.degree_type as dt', 'o.degree_type', '=', 'dt.id');
						}

						if ($key->type == 'include') {

							$desiredDegree_local = $key->desiredDegree;
							$qry = $qry->where(function($q) use($desiredDegree_local, $includeExclude){
								foreach ($desiredDegree_local as $k => $v) {
									$q->orWhere('dt.display_name', $includeExclude, $v);
								}
							});

						}else{
							foreach ($key->desiredDegree as $k => $v) {
								$qry = $qry->where('dt.display_name', $includeExclude, $v);
							}
						}
						

						break;
					case 'militaryAffiliation':
						
						if (isset($key->inMilitary)) {
							foreach ($key->inMilitary as $k => $v) {
								if ($v == 'Yes') {
									$v = 1;
								}elseif($v == 'No'){
									$v = 0;
								}
								$qry = $qry->where('userFilter.is_military', '=', $v);
							}
						}

						if (isset($key->militaryAffiliation)) {

							if (!$military_affiliation_table) {
								$military_affiliation_table = true;
								$qry = $qry->leftjoin('plexuss.military_affiliation as ma', 'ma.id', '=', 'userFilter.military_affiliation');
							}

							$militaryAffiliation_arr = $key->militaryAffiliation;

							$qry = $qry->where(function($q) use($militaryAffiliation_arr){
								foreach ($militaryAffiliation_arr as $k => $v) {
									$q->orWhere('ma.name', '=', $v);
								}
							});
						}
						break;
					case 'profileCompletion' :
						if(isset($key->profileCompletion)) {
							foreach ($key->profileCompletion as $k => $v) {
								$qry = $qry->where('userFilter.profile_percent', '>=', $v);
							}
						}
						break;
					case 'name':
						if ($key->type == 'include') {
							
							$includeExclude = 'LIKE';
						}else{
							$includeExclude = 'NOT LIKE';
						}

						if ($key->type == 'include') {

							$name_arr = $key->name;
							$qry = $qry->where(function($q) use($name_arr, $includeExclude){
								foreach ($name_arr as $k => $v) {

									if (strpos($v, '@') !== false){
										$q->orWhere('userFilter.email', $includeExclude, $v);
									}else{
										$q->orWhere(DB::raw("concat_ws(' ', userFilter.fname, userFilter.lname) "), $includeExclude, '%'.$v.'%');
									}
								}
							});
							
						}else{
							foreach ($key->name as $k => $v) {
								if (strpos($v, '@') !== false){
									$q->orWhere('userFilter.email', $includeExclude, $v);
								}else{
									$qry = $qry->where(DB::raw("concat_ws(' ', userFilter.fname, userFilter.lname) "), $includeExclude, '%'.$v.'%');
								}
							}
						}
						break;
					case 'startyr':
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if ($key->type == 'include') {

							$startyr_arr = $key->startyr;
							$qry = $qry->where(function($q) use($startyr_arr, $includeExclude){
								foreach ($startyr_arr as $k => $v) {
									$q->orWhere('userFilter.planned_start_yr', $includeExclude, $v);
								}
							});
							$qry = $qry->whereNotNull('userFilter.planned_start_yr');
						}else{
							foreach ($key->startyr as $k => $v) {
								$qry = $qry->where('userFilter.planned_start_yr', $includeExclude, $v);
							}
							$qry = $qry->whereNotNull('userFilter.planned_start_yr');
						}
						break;
					case 'startterm':
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if ($key->type == 'include') {

							$startterm_arr = $key->startterm;
							$qry = $qry->where(function($q) use($startterm_arr, $includeExclude){
								foreach ($startterm_arr as $k => $v) {
									$q->orWhere('userFilter.planned_start_term', $includeExclude, $v);
								}
							});
							$qry = $qry->whereNotNull('userFilter.planned_start_term');
						}else{
							foreach ($key->startterm as $k => $v) {
								$qry = $qry->where('userFilter.planned_start_term', $includeExclude, $v);
							}
							$qry = $qry->whereNotNull('userFilter.planned_start_term');
						}
						break;
					case 'startDateTerm':
					
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						


						//$startDateTerm_arr = $key->startDateTerm;


						$startDateTerm_arr = isset($key->startDateTerm) ? $key->startDateTerm : null;
      					if( !isset($startDateTerm_arr) ) break;

						if ($key->type == 'include') {

							$qry = $qry->where(function($q) use($startDateTerm_arr, $includeExclude){
								foreach ($startDateTerm_arr as $k => $v) {
									$arr = array();
									$arr = explode(" ", $v);
									$q->orWhere(function($query) use($includeExclude, $arr) {
										$query->where('userFilter.planned_start_yr', $includeExclude, trim($arr[1]))
									  	  	  ->Where('userFilter.planned_start_term', $includeExclude, trim($arr[0]));
									});
								}
							});

							$qry = $qry->whereNotNull('userFilter.planned_start_yr')
									   ->whereNotNull('userFilter.planned_start_term');
						}else{
							foreach ($key->startDateTerm as $k => $v) {
								$arr = array();
								$arr = explode(" ", $v);
								
								$qry = $qry->where('userFilter.planned_start_yr', $includeExclude, trim($arr[1]))
										   ->where('userFilter.planned_start_term', $includeExclude, trim($arr[0]));
							}

							$qry = $qry->whereNotNull('userFilter.planned_start_yr')
									   ->whereNotNull('userFilter.planned_start_term');
						}
						break;
					case 'financial':

						if (!$financial_filter) {
							$financial_filter = true;
						}
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}
						if (isset($key->financial)) {
							if ($key->type == 'include') {



								$financial_arr = $key->financial;


								$qry = $qry->where(function($q) use($financial_arr, $includeExclude){
									foreach ($financial_arr as $k => $v) {
										$q->orWhere('userFilter.financial_firstyr_affordibility', $includeExclude, $v);
									}
								});
								$qry = $qry->whereNotNull('userFilter.financial_firstyr_affordibility');
							}else{
								foreach ($key->financial as $k => $v) {
									$qry = $qry->where('userFilter.financial_firstyr_affordibility', $includeExclude, $v);
								}
								$qry = $qry->whereNotNull('userFilter.financial_firstyr_affordibility');
							}
						}

						// We don't need to filter based on interested in aid for now -Anthony
						
						// if (isset($key->interested_in_aid) && $key->interested_in_aid == 0) {

						// 	$qry = $qry->where('userFilter.interested_in_aid', '=', 0);
						// }
						
						break;
					case 'schooltype':
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if ($key->type == 'include') {

							$schooltype_arr = $key->schooltype;
							$qry = $qry->where(function($q) use($schooltype_arr, $includeExclude){

								foreach ($schooltype_arr as $k => $v) {
									if( !is_numeric($v) ){
										switch ($v) {
											case 'campus_only': $v = 0; break;
											case 'online_only': $v = 1; break;	
											default: $v = 2; break;
										}
									}

									// if they have selected both, then disregard, and continue
									// because logically both covers all three options which are 
									// online only, on campus only and both
									if ($v == 2) {
										break;
									}
									$q->orWhere('userFilter.interested_school_type', $includeExclude, $v);
								}
							});
							$qry = $qry->whereNotNull('userFilter.interested_school_type');
						}else{
							foreach ($key->schooltype as $k => $v) {
								if( !is_numeric($v) ){
									switch ($v) {
										case 'Campus Only': $v = 0; break;
										case 'Online Only': $v = 1; break;	
										default: $v = 2; break;
									}
								}
								
								// if they have selected both, then disregard, and continue
								// because logically both covers all three options which are 
								// online only, on campus only and both
								if ($v == 2) {
									break;
								}
								$qry = $qry->where('userFilter.interested_school_type', $includeExclude, $v);
							}
							$qry = $qry->whereNotNull('userFilter.interested_school_type');
						}
						break;
					case 'typeofschool':
						$v = $key->interested_school_type[0];
						switch ($v) {
							case '0': $v = Array(0,2); break;
							case '1': $v = Array(1,2); break;
							case 'campus_only': $v = Array(0,2); break;
							case 'online_only': $v = Array(1,2); break;
							default: $v = Array(0,1,2); break;
						}

						// if they have selected both, then disregard, and continue
						// because logically both covers all three options which are 
						// online only, on campus only and both

						$qry = $qry->whereIn('userFilter.interested_school_type', $v);
						break;
					case 'gradYear':
						if ($key->type == 'include') {
							
							$includeExclude = '=';
						}else{
							$includeExclude = '!=';
						}

						if (isset($key->gradYear)) {
							$gradYear_arr = $key->gradYear;

							if ($key->type == 'include') {
								$qry = $qry->where(function($q) use($gradYear_arr, $includeExclude){
									foreach ($gradYear_arr as $k) {
										$val = explode(",", $k);

										foreach ($val as $x => $y) {
											$q->orWhere(DB::raw("IF(`userFilter`.`in_college` = 0, `hs_grad_year`, `college_grad_year`)"), '=', $y);
										}
									}
								});
							}
						}
						break;
					default:
						break;
				}
			}
		}

		// if (($city_table || $state_table) && !$intl_filter && !$country_table) {
		// 	$qry = $qry->orWhere('userFilter.country_id', '!=', 1);	
		// }

		if (isset($is_advanced_search)) {
			if ($objectives_table == false) {
				$qry = $qry->leftjoin('plexuss.objectives as o', 'o.user_id', '=', 'userFilter.id');
			}
			
			$qry = $qry->leftjoin('plexuss.colleges as c', 'c.id','=', 'userFilter.current_school_id')
			->leftjoin('plexuss.high_schools as h', 'h.id', '=', 'userFilter.current_school_id')	
			->leftjoin('plexuss.professions as p', 'p.id', '=', 'o.profession_id');

			if ($scores_table == false) {
				$qry = $qry->leftjoin('plexuss.scores as s', 's.user_id', '=','userFilter.id');
			}
			if ($degree_table == false) {
				$qry = $qry->leftjoin('plexuss.degree_type as dt', 'dt.id', '=', 'o.degree_type');
			}
			if ($major_table == false) {
				$qry = $qry->leftjoin('plexuss.majors as m', 'm.id', '=', 'o.major_id');
			}

			if ($country_table == false) {
				$qry = $qry->leftjoin('plexuss.countries as ct', 'ct.id', '=', 'userFilter.country_id');
			}
		}
		$tmp = $qry->toSql();
		if ($tmp == 'select * from `plexuss`.`users` as `userFilter`') {
			return null;
		}
		return $qry;

 	}
	
	public function getFiltersAndLogs_scholarshipadmin($category = NULL, $scholarship_id){

 		$qry = DB::connection('rds1')->table('college_recommendation_filters as crf')
 					->leftjoin('college_recommendation_filter_logs as crfl', 'crfl.rec_filter_id', '=', 'crf.id');

 		if(!isset($inside)){
			$qry = $qry->select('crf.type', 'crf.category', 'crf.name', 'crfl.val');
		}
		else{
			$qry = $qry->select('crf.type', 'crf.category', 'crf.name',
 							 DB::raw("concat(substring_index(val,',',1),',,',substring_index(val,',',-1)) as val"))
 				->where('category','=','majorDeptDegree')
 				->groupBy('val');
		}

 		
 		$qry = $qry->where('scholarship_id', $scholarship_id); 		
 		

 		if (isset($category)) {
			$qry = $qry->where('category', $category);
 		}

 		
		if (isset($department_query)){
			$qry = $qry->where('category','!=','majorDeptDegree');
			$cue = $this->getFiltersAndLogs($agency_id, $college_id, $org_branch_id, null, $org_portal_id, $aor_id, $aor_portal_id, null, true);
			$qry = $qry->union($cue);
		}
		if(!isset($inside) && !isset($department_query)){
			$departments_as_majors = clone $qry;
			$departments_as_majors = $departments_as_majors
				->where('category', "majorDeptDegree")
				->where('crfl.val', 'NOT LIKE', "%,,%")
				->join('department as d', 'd.id', '=', DB::raw("substring_index(val,',',1)"))
				->join('majors as m', 'm.name', '=', 'd.name')
				->select('crf.type', 'crf.category', 'crf.name',
						 DB::raw("concat(d.id, ',', m.id, ',', substring_index(val,',',-1)) as val"));
			$qry = $qry->union($departments_as_majors);
		}
		if(!isset($inside)){
			$qry = $qry->orderBy('category')
					   ->get();
		}

		return $qry;
	}
	
}
