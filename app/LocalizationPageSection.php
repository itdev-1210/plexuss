<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalizationPageSection extends Model {

	protected $table = 'localization_page_sections';

 	protected $fillable = array('page_id', 'section_name', 'english_content');
}