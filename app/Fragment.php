<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fragment extends Model
{
    protected $table = 'fragments';

 	protected $fillable = array('name');
}
