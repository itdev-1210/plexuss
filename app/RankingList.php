<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RankingList extends Model {

	protected $table = 'lists';
	
	protected $fillable = array('custom_college', 'is_plexuss');
	
	public function getNumOfUploadedRankingForColleges($college_id = null, $onDate = null){
        if ($college_id == null) {
 			return 0;
 		}

 		$rec= RankingList::on('bk')->whereIn('custom_college', $college_id)
 							->where('is_plexuss', 0)
 							->select(DB::raw('count(DISTINCT id) as cnt, custom_college'))
 							->groupBy('custom_college');

 		if( isset($onDate) ){
 			$rec->where('created_at', '>', $onDate);
 		}

 		$rec->get();

 		return $rec;
    }

}