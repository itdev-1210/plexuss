<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPackageMat extends Model {
	
	protected $table = 'product_package_mats';

 	protected $fillable = array( 'ccp_id', 'goods_id', 'user_id');
}