<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClubOrg extends Model
{
    protected $table = 'club_org';

 	protected $fillable = array('user_id', 'whocanseethis', 'club_name', 'position', 'location', 'month_from', 'year_from', 'month_to',
 								'year_to', 'currentlyworkhere', 'club_description', 'bullet_points');
}
