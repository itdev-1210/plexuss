<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPortalEmailEffortCouldNotSendLog extends Model
{
    protected $table = 'users_portal_email_effort_could_not_send_logs';

 	protected $fillable = array( 'user_id', 'template_name', 'ro_id', 'company', 'params', 'test_num', 'upeel_id' );

}
