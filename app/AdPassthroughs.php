<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPassthroughs extends Model
{
    protected $table = 'ad_passthroughs';

    protected $fillable = ['section', 'user_id', 'user_invite_id', 'utm_campaign', 'utm_source', 'url', 'cid', 'company', 'ip', 'device', 'browser', 'countryName', 'stateName', 'cityName', 'zip', 'updated_at', 'created_at'];
}
