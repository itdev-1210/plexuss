<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionClient extends Model
{
    protected $table = 'distribution_clients';

 	protected $fillable = array('ro_id', 'org_branch_id', 'college_id', 'school_name', 'delivery_url', 'delivery_type', 'response_type', 'success_tag', 'success_string', 'failed_tag');
}
