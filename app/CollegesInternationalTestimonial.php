<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesInternationalTestimonial extends Model
{
    protected $table = 'colleges_international_video_testimonials';
	protected $fillable = array('cit_id', 'college_id', 'title', 'url', 'source');

	public function removeVideoTestimonial($id){
		$cit = CollegesInternationalTestimonial::find($id);
		$cit->delete();

		return "success";
	}
}
