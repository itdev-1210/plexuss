<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyBucketNames extends Model {
    protected $table = 'agency_bucket_names';

    protected $fillable = array('name', 'display_name', 'updated_at', 'created_at');

	public $timestamps = true;

	public static function getAllBucketNames() {
		return AgencyBucketNames::on('rds1')
								->select('id', 'name', 'display_name')
								->get()
								->toArray();
	}
}
