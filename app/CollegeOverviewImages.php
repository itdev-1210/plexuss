<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class CollegeOverviewImages extends Model
{
    protected $table = 'college_overview_images';
	protected $fillable = array('college_id', 'url', 'title', 'is_video', 'section',
							    'video_id', 'is_tour', 'tour_id', 'is_youtube');

	public function college(){
		return $this->belongsTo('College');
	}

	public function getImagesByCollegeId($college_id){
		$qry = CollegeOverviewImages::on('rds1')->where('is_video', 0)
												->where('is_tour', 0)
												->whereNotNull('url')
												->where('url', '!=', DB::raw("''"))
												->where('college_id', $college_id)
												->selectRaw('CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/overview_images/", url) as url')
												->get();
	
		return $qry;
	}
}
