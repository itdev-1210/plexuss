<?php

namespace App;

use SendGrid;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

use App\MandrillLog;
use App\Http\Controllers\Controller;

class SendGridModel extends Model
{
    private $md;
    private $template_name;
    private $sg;


    public function __construct($list_name)  {
        $this->setTemplateName($list_name);

        $apiKey = env('SENDGRID_KEY');
        $this->sg = new \SendGrid($apiKey);
    }

    public function sendTemplate($email_arr, $params, $reply_email, $attachments = null, $bcc_address = null, $fromName = null){
        $template_name = $this->template_name;
        $template_content = array();

        $from = new SendGrid\Email($fromName, $reply_email);
		$subject = ""; // Subject will be predefined in the template.
		$to = new SendGrid\Email(null, $email_arr['email']);
		$content = new SendGrid\Content("text/html", "Set to HTML");
		$mail = new SendGrid\Mail($from, $subject, $to, $content);

        foreach ($params as $key => $value) {
            $mail->personalization[0]->addSubstitution('{{'.$key.'}}', (string) $value);
        }

		$mail->setTemplateId($this->template_name);
		
        try {
            // Build your email and send it!      
            $response = $this->sg->client->mail()->send()->post($mail);

            $ml = new MandrillLog();
            $ml->template_name = $this->template_name;
            $ml->response = json_encode($response->headers());
            $ml->email = $email_arr['email'];
            $ml->params = json_encode($params);

            $ml->save();
            $this->template_name = '';

        } catch (\Exception $str) {

            $ml = new MandrillLog();
            $ml->template_name = $this->template_name;
            $ml->response = $str;
            $ml->email = $email_arr['email'];
            $ml->params = json_encode($params);

            $ml->save();

            $this->template_name = '';
        }
        
        return 'success';
    }

    private function setTemplateName($list_name){
        switch ($list_name) {
            case 'college_send_message_for_users':
                
                $this->template_name = "e32b29c6-8079-4ca7-b78c-4501d811e03c";
                break;
            case 'college_recommendation_for_users':
                
                $this->template_name = "5c6c8f6d-e0d3-4d3a-9f50-fb7412335327";
                break;

            case 'colleges_viewed_your_profile':
                
                $this->template_name = "c23a15ac-ec88-49c2-8abf-cb8a86bc2475";
                break;
            
            case 'college_wants_to_recruit_you':
                
                $this->template_name = "a214fdac-7961-44cf-b0c4-96f79eab74df";
                break;

            case 'college_recommendations':
                
                $this->template_name = "2010e87d-a7f8-4d4b-ae41-fdeb03e7b0bb";
                break;

            case 'potential_college_recommendations':
                
                $this->template_name = "8048f3fa-74e0-4fb0-8a43-3c14e6b66a78";
                break;
                
            case 'user_want_to_get_recruited_for_colleges':
                
                $this->template_name = "aa75c6f1-2ada-476e-b157-4ee9fc9d866a";
                break;

            case 'user_accepts_request_to_be_recruited_for_colleges':
                
                $this->template_name = "ca2b69ab-33e6-4ad8-9aa0-f236b56ad5cb";
                break;

             case 'user_send_message_for_colleges':
                
                $this->template_name = "0d4f80c9-35e0-4195-b2d4-8eaccc697875";
                break;

            case 'college_agreed_to_recruit_you':
                
                $this->template_name = "7d1621b7-fdfc-4e3f-9fd1-d98f59c0d74e";
                break;

            case 'new_user_confirmation_email':
                
                $this->template_name = "fd9893f0-fadc-4649-85db-1523678592cd";
                break;    

            case 'paid_member_request_to_sales':
                
                $this->template_name = "f68c7200-ed76-4505-972e-4bdc7d6ff275";
                break; 

            case 'agency_recruiting_to_students':
                
                $this->template_name = "0c266c4b-7f13-4762-8a23-323a9459869a";
                break;

            case 'college_prep_recruiting_in_area':
                
                $this->template_name = "1ffb7b66-06e0-4d38-ba17-8764aedca54c";
                break;

            case 'agency_recruiting_visa_help':
                 $this->template_name = "bed93c55-b91d-45bf-887b-859b22e11470";
                 break; 

            case 'agencies_agency_sign_up_request':
                $this->template_name = "18e41c17-0aac-49d6-aafd-f6da4759a434";
                break;

            case 'english_recruiting_in_area':
                 $this->template_name = "fe8b1a21-a60b-4e9e-b162-129afa407f46";
                 break; 

            case 'in_network_comparison_college_users_email':
                $this->template_name = "4f5fceca-958d-47c0-bb16-b30590cd1cc6";
                break;

            case 'ranking_update_college_users_email':
                $this->template_name = "20e02d03-79ca-419a-a9f2-776a633e4b80";
                break;

            case 'near_you_college_users_email':
                $this->template_name = "593a627f-82e4-4ee0-a6ec-1dad72ebb606";
                break;

            case 'chat_session_college_users_email':
                $this->template_name = "d22e70c7-8695-4798-83ac-da41c2c5c90f";
                break;
            
            case 'school_you_liked_college_users_email':
                $this->template_name = "c46902b6-0909-44b8-a7bf-ae01e64f7843";
                break;
            
            case 'users_in_network_school_you_messaged_college_users_email':
                $this->template_name = '07c33832-21ef-44d9-96bb-28c871dca34b';
                break;

            case 'users_in_network_school_u_wanted_to_get_recruited_college_users_email':
                $this->template_name = '619ce82a-c412-42e3-ac05-4d37e8ccfd73';
                break;

            case 'college_prep_recruiting_in_area_agency_users_email':
                $this->template_name = '4b020580-70e5-4314-8e87-4638e175971a';
                break;

            case 'agency_recruiting_visa_help_agency_users_email':
                $this->template_name = '0caba06d-d644-4d5c-9019-5204176566db';
                break;

            case 'english_recruiting_in_area_agency_users_email':
                $this->template_name = 'ac2920c3-6a0c-4447-b1ba-f356fafdb811';
                break;

            case 'college_prep_user_interested_in_your_services':
                $this->template_name = '808db644-2f82-4344-a772-235009a36234';
                break;

            case 'agencies_user_interested_in_your_services':
                $this->template_name = '6a9e5b0b-a452-4b05-b55c-41c11985b880';
                break;
            
            case 'english_program_user_interested_in_your_services':
                $this->template_name = '7de5f696-3942-4d3e-bb4d-e7cd8bcbf9b9';
                break;

            case 'infilaw_survey_completion_notification_and_data':
                $this->template_name = 'infilaw-survey-completion-notification-and-data';
                break;

            case 'csl_amazon_code':
                $this->template_name = 'csl-amazon-code';
                break;

            case 'fcsl_amazon_code':
                $this->template_name = 'fcsl-amazon-code';
                break;
            
            case 'welcome_email':
                $this->template_name = 'cb710cd8-3321-411c-8e6e-8c4cbc228307';
                break;

            case 'how_recruitment_work_after_one_week':
                $this->template_name = '5c10afeb-69ba-47c9-a669-e361514ab6cf';
                break;
            
            case 'how_recruitment_work_after_two_week':
                $this->template_name = '219a3bd1-1823-41ee-a002-038a64c48171';
                break;

            case 'how_recruitment_work_after_three_week':
                $this->template_name = 'ddea3d2f-7f39-4143-a42d-cc67d1abe826';
                break;

            case 'birthday_email':
                $this->template_name = '8285399f-4d08-4355-95c0-544c3759d181';
                break;

            case 'users_friend_invite':
                $this->template_name = '10d6172b-76f2-4c03-aae5-83464d47ae25';
                break;

            case 'new_college_ranking_for_colleges':
                $this->template_name = '58a2cf00-b372-4310-8908-81bdd4a3a87d';
                break;
            
            case 'test_message':
                $this->template_name = 'colleges-user-sends-your-college-a-message-test';
                break;

            case 'users_friend_invite_2_mdl':
                $this->template_name = '6a81ee3a-2804-4f08-9a25-bbc769ba0d03';
                break;

            case 'users_friend_invite_3_mdl':
                $this->template_name = 'd8b6afd4-5fa3-4d1d-8842-2f16b64150ba';
                break;

            case 'colleges_weekly_plexuss_update_mdl':
                $this->template_name = 'e1a07ed2-45fd-485b-b92e-a591ab7df9f0';
                break;

            case 'internal_triggers':
                $this->template_name = 'eead8379-bed3-4b43-bce9-823dc14b3525';
                break;

            case 'march_madness_invite_2016':
                $this->template_name = 'march-madness-invite-2016';
                break;

            case 'internal_college_upgrade_to_premiere_request':
                $this->template_name = '0311bbf0-f6c8-4b42-b9f5-124100e80c87';
                break;

            case 'colleges_manage_portal_new_user':
                $this->template_name = 'ed171b83-7ed3-425c-bb92-e23c238f48df';
                break;

            case 'colleges_manage_portal_existing_account':
                $this->template_name = '231f4c3a-0ab9-43e7-871a-9a0f58d0c3f2';
                break;

            case 'colleges_welcome_to_the_plexuss_premier_program':
                $this->template_name = 'df21be32-f118-4d92-bd34-4aa8189e6427';
                break;

            case 'colleges_sample_premier_contract':
                $this->template_name = '0da8e355-476f-4eeb-9d78-36bd203062b3';
                break;

            case 'colleges_college_self_sign_up_request':
                $this->template_name = 'ffb9bcc0-5e67-40cc-9530-464aad25e55b';
                break;
            
            case 'colleges_college_interested_in_premium_service':
                $this->template_name = '6622d361-91f2-4905-8d45-5afd027f4c83';
                break;

            case 'internal_remove_plexuss_account':
                $this->template_name = '2cfb8369-fcb7-42ac-b4e1-e766f63fefa0';
                break;
            
            case 'internal_manually_add_to_suppression':
                $this->template_name = '1b83f7d2-0dc3-4c6c-b626-d1b3aa7fc940';
                break;

            case 'internal_why_unsbuscribed':
                $this->template_name = '3c48ead1-9c99-437f-ae28-ac0eab291dfe';
                break;

            case 'users_premium_order_confirmation':
                $this->template_name = '9bf4bb02-f7b7-48df-a166-aaf29dd716d0';
                break;

            // case 'college_export_file':
            //     $this->template_name = 'college-export-file';
            //     break;

            case 'webinar_10_4_confirmation':
                $this->template_name = 'users-webinar-10-4-confirmation';
                break;

            case 'users_webinar_10_4_otero_initial_invite':
                $this->template_name = 'users-webinar-10-4-otero-initial-invite';
                break;

            case 'users_webinar_10_4_otero_15_minute_reminder':
                $this->template_name = 'users-webinar-10-4-otero-15-minute-reminder';
                break;
                
            case 'users_college_needs_more_info':
                $this->template_name = 'ac12a3f5-5c9a-4766-a805-d192e3fb5e5e';
                break;

            case 'users_handshake_next_steps':
                $this->template_name = '9cf17eb5-174d-427c-952d-aea08830e2e0';
                break;

            case 'users_financial_documents':
                $this->template_name = '72a06215-71b4-46fe-b014-8cd90c7e295f';
                break;

            case 'users_webinar_11_4_uic_initial_invite':
                $this->template_name = 'users-webinar-11-4-uic-initial-invite';
                break;

            case 'colleges_daily_recs_and_inquiries':
                $this->template_name = '478fcdbc-888b-45d7-8c8d-f6328be872a9';
                break;

            case 'users_webinar_11_4_uic_15_minute_reminder':
                $this->template_name = 'users-webinar-11-4-uic-15-minute-reminder';
                break;

            case 'users_webinar_12_1_ttu_confirmation':
                $this->template_name = 'users-webinar-12-1-ttu-confirmation';
                break;

            case 'users_webinar_12_1_15_minute_reminder':
                $this->template_name = 'users-webinar-12-1-15-minute-reminder';
                break;

            case 'users_webinar_12_1_ttu_initial_invite':
                $this->template_name = 'users-webinar-12-1-ttu-initial-invite';
                break;

            case 'users_webinar_3_1_liberty_confirmation':
                $this->template_name = 'users-webinar-3-1-liberty-confirmation';
                break;

            case 'users_webinar_3_1_liberty_24_hour_reminder':
                $this->template_name = 'users-webinar-3-1-liberty-24-hour-reminder';
                break;

            case 'users_webinar_3_1_liberty_15_minute_reminder':
                $this->template_name = 'users-webinar-3-1-liberty-15-minute-reminder';
                break;

            case 'users_webinar_3_1_liberty_initial_invite':
                $this->template_name = 'users-webinar-3-1-liberty-initial-invite';
                break;
                
            case 'internal_urgent_email_from_college_rep':
                $this->template_name = 'd6c7352d-93ed-4725-9bad-b8b6293eba84';
                break;

            case 'internal_urgent_email_from_agency_rep':
                $this->template_name = '75e577ba-5462-4342-b925-a239dcfa4471';
                break;
            
            case 'users_oneapp_invite_day_one':
                $this->template_name = '32c75479-b7b6-4740-8d98-1729788a03b9';
                break;

            case 'users_oneapp_invite_daily_followup':
                $this->template_name = '303acc81-bc9e-4eaa-895a-cdd57ea9c2bf';
                break;

            case 'users_oneapp_daily_application_status':
                $this->template_name = '35e2a615-3465-4d55-b459-a5b12d3668d6';
                break;

            case 'users_oneapp_invite_daily_followup_finished_app':
                $this->template_name = 'a5e2d77d-bb30-48ef-90a4-a0c7da1b9173';
                break;

            case 'users_oneapp_invite_weekly_followup':
                $this->template_name = '41cb5f3f-a96f-45fa-811a-df13de5b4850';
                break;

            case 'users_oneapp_invite_coveted_wkly_followup_post_10':
                $this->template_name = '27aae1d0-3b4b-4e23-85aa-c5b28752dbbb';
                break;
                
            case 'users_admitsee_email_major':
                $this->template_name = 'users-admitsee-email-major';
                break;

            case 'users_admitsee_email_school':
                $this->template_name = '69ba8fa3-a753-4107-8945-618c51503ee1';
                break;
            
            case 'users_admitsee_email_country':
                $this->template_name = '1f79bd62-1c2e-4c63-acb7-9da9f338ab57';
                break;

            case 'b2b_plexuss_weekly_digest_1':
                $this->template_name = 'b2b-plexuss-weekly-digest-1';
                break;

            case 'users_els_additional_info':
                $this->template_name = 'e0fc18b7-2c2d-4b0c-8640-1e97f6631bb6';
                break;

            case 'other_intern_survey':
                $this->template_name = '4510d0eb-8b6a-4aab-b67a-63fb4b4efcf9';
                break;

            case 'other_hiring_survey':
                $this->template_name = 'ed11f77a-6c29-4cab-acf5-b417e1f690f1';
                break;

            case 'oneapp_internal_finished_app':
                $this->template_name = '5451b751-21e6-42ba-a331-bce34eb2811f';
                break;
            
            case 'users_additional_uploads_required':
                $this->template_name = 'b78ffd7e-6902-401a-8d0d-83c41c09247b';
                break;
                
            case 'users_oneapp_demoted':
                $this->template_name = '5c1abc78-b4c6-450f-ac69-48176e720d7f';
                break;

            case 'users_oneapp_promoted':
                $this->template_name = 'daa89acd-6f7e-4395-9a14-5e8c8ffcb11c';
                break;

            case 'users_edx_college_credit_while_in_highschool':
                $this->template_name = '47f27062-bd3d-4497-9821-50ed53e15a87';
                break;

            case 'users_edx_free_courses_top_universities':
                $this->template_name = '79b5a6ee-1cd6-421f-8595-23595d714888';
                break;
            
            case 'users_edx_ielts_and_toefl':
                $this->template_name = '259bd3bd-6e34-4233-bcd0-393c50bcb043';
                break;

            case 'users_oneapp_scholarship_email':
                $this->template_name = '730c8fa6-cf8b-4470-8b84-d7bd431265cb';
                break;
            
            case 'users_plexuss_mobile_app_annoucement':
                $this->template_name = '11801067-f22b-45fb-8f43-cf51cf5f87b9';
                break;

            case 'agencies_one_lead_a_day_non_registered':
                $this->template_name = '5bb5cc40-841c-4b70-b479-6ec8fa9d30c4';
                break;
            
            case 'agencies_add_description':
                $this->template_name = 'agencies-add-description';
                break;

            case 'users_edx_free_courses_top_universities_nr':
                $this->template_name = '177eb543-d343-4d5a-a897-3faab97a90b8';
                break;
            
            case 'users_edx_free_courses_top_universities_events':
                $this->template_name = '7241d487-b566-4d9b-bfe2-a1fb82893afb';
                break;

            case 'colleges_student_organically_inquired':
                $this->template_name = '922bc122-77ad-4585-9098-2c5eb77da6fe';
                break;
            
            case 'users_recommended_by_plexuss':
                $this->template_name = '124f74e9-7344-44f8-b7e6-8d775cd42d58';
                break;

            case 'users_a_college_viewed_your_profile':
                $this->template_name = '9d56297e-8107-468b-90fa-35e7b38bc511';
                break;

            case 'users_a_college_wants_to_recuit_you':
                $this->template_name = '905397b2-3a4b-47a5-a7ec-5d3912fcf19a';
                break;

            case 'users_a_college_wants_to_recuit_you_2':
                $this->template_name = 'bfdf66af-6f33-4db2-a3a1-6156b51613a9';
                break;

            case 'truscribe':
                $this->template_name = '0afeb898-93d9-4964-b15d-8f7631a4c0a1';
                break;

            case 'topuniversities_grad_school':
                $this->template_name = '770c79d4-6d99-423f-b722-3d69a4a066d4';
                break;

            case 'topuniversities_mba':
                $this->template_name = '61d1c22e-b8b2-41a1-ad63-611c917a4802';
                break;

            case 'colleges_weekly_crm_report':
                $this->template_name = 'c65a8476-fae0-4f17-8a70-e6c730baf38d';
                break;

            case 'colleges_daily_crm_report':
                $this->template_name = '57fc5213-612d-46a7-93d9-f8269d8781d4';
                break;

            case 'topuniversities_grad_school_ver_b':
                $this->template_name = 'edc2b80a-a320-447e-9cdb-044aae515c33';
                break;

            case 'topuniversities_mba_ver_b':
                $this->template_name = 'bf579046-fc69-47d4-a04a-64ce4689647b';
                break;

            case 'springboard_first':
                $this->template_name = '6d6c0fcb-e160-45e9-a091-3faabc776730';
                break;

            case 'music_institute_v1':
                $this->template_name = '16cd1677-4c5a-4621-9cee-1bd2c1c0342f';
                break;

            case 'users_invite_springboard_first':
                $this->template_name = '4106969f-3697-49e0-a639-7abf884d8c05';
                break;

            case 'users_invite_topuniversities_mba_ver_b':
                $this->template_name = '447d00c7-e78a-4b06-a389-1a367c65cb05';
                break;

            case 'users_invite_music_institute_v1':
                $this->template_name = '3b6d0327-1adb-4c20-ba21-5159f5980624';
                break;

            case 'users_invite_edx_free_courses_top_universities_nr':
                $this->template_name = '6512ab20-b679-470b-b3cf-da1b3953fa68';
                break;

            case 'users_invite_gcu_eddy_click':
                $this->template_name = 'c6ba0aa0-0548-4766-aa25-3dcae660c26d';
                break;

            case 'users_invite_calu_eddy_click':
                $this->template_name = '3050d399-21ae-44ef-a77f-f1dcf1f7ac1d';
                break;

            case 'gcu_eddy_click':
                $this->template_name = 'aef881c9-1edb-46bc-93ce-b2f9e4d3a8ad';
                break;

            case 'calu_eddy_click':
                $this->template_name = '68829a01-d6af-4951-987e-a84b04aa6ede';
                break;

            case 'exampal_v1':
                $this->template_name = "4840c4b4-efe5-46f6-b906-02f90fb1a3b2";
                break;

            case 'exampal_v2':
                $this->template_name = "exampal-v2";
                break;

            case 'daily_client_report':
                $this->template_name = 'cb95228f-89ed-417f-bdf0-91693a6d3af0';
                break;

            case 'cornell_college_v1':
                $this->template_name = '74eda9f7-1e13-416e-808b-d0c79615b689';
                break;

            case 'qs_scholarships_template1_copy_04':
                $this->template_name = "6965e8b5-3c1c-44c0-80a5-88e7a10541a2";
                break;

            case 'nafsa_2018_first_email':
                $this->template_name = "23f4e3ce-3f90-4e47-8bda-efb7d5987c49";
                break;
            
            case 'nafsa_2018_second_email':
                $this->template_name = "96936a29-049c-4c99-9737-6201f2916a61";
                break;

            default:
                # code...
                break;
        }
    }

    public function importSendGridSuppression(){

        $arr = array('blocks', 'bounces', 'invalid_emails', 'spam_reports');
        $yesterday = strtotime(Carbon::now()->subHour(13));
        $tomorrow  = strtotime(Carbon::tomorrow());
        $cnt = 0;

        foreach ($arr as $k => $v) {
        
            $url = "https://api.sendgrid.com/v3/suppression/".$v;
            $client = new Client(['base_uri' => 'http://httpbin.org']);
            // $esl    = new EmailSuppressionList;
            $source = $v;

            try {
                $response = $client->request('GET', $url, [
                            'headers' => [
                                'Authorization' => 'Bearer '.env('SENDGRID_KEY'),
                                'Accept'        => 'application/json'
                                ],
                            'query' => [
                                'start_time' => $yesterday,
                                'end_time'   => $tomorrow]
                            ]);
                $result = json_decode($response->getBody()->getContents(), true);

                foreach ($result as $key) {
                    $uid = null;
                    $uiid = null;
                    
                    $user = User::on('bk')
                                ->where('email', $key['email'])
                                ->select('id')
                                ->first();

                    if (isset($user)) {
                        $uid = $user->id;
                    }

                    if (!isset($uid)) {
                        $user = UsersInvite::on('bk')
                                           ->where('invite_email', $key['email'])
                                           ->select('id')
                                           ->first();
                        $uiid = $user->id;
                    }

                    (!isset($key['reason']) && isset($key['ip'])) ? $key['reason'] = $key['ip'] : null;
                    (!isset($key['reason'])) ? $key['reason'] = $source : null;

                    $attr = array('recipient' => $key['email']);
                    $val  = array('recipient' => $key['email'], 'description' => $key['reason'], 'source' => $source,
                                  'type' => 'transactional', 'non_transactional' => 1);

                    isset($uid) ? $val['uid'] = $uid : null;
                    isset($uiid) ? $val['uiid'] = $uiid : null;
                    isset($uid) ? $attr['uid'] = $uid : null;
                    isset($uiid) ? $attr['uiid'] = $uiid : null;
                    $esl = EmailSuppressionList::updateOrCreate($attr, $val);

                    if (isset($uid) || isset($uiid)) {
                        $qry = EmailSuppressionList::where('recipient', $key['email'])
                                                   ->whereNull('uid')
                                                   ->whereNull('uiid')
                                                   ->delete();
                    }
                    $cnt++;
                }
            } catch (\Exception $e) {
                // return false;
                // return "something bad happened";
            }
        }

        return $cnt;
    }
}
