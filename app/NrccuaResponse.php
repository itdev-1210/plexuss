<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaResponse extends Model
{
    protected $table = 'nrccua_responses';

	protected $fillable = array( 'url', 'params', 'response', 'user_id', 'success', 'error_msg' );
}
