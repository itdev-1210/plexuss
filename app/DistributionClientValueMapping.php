<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionClientValueMapping extends Model {

	protected $table = 'distribution_client_value_mappings';

 	protected $fillable = array('is_default', 'plexuss_value', 'client_value', 'client_value_name', 'dc_id', 'dcfm_id');
}
