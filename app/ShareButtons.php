<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareButtons extends Model
{
    private $platforms;			// Array to decide which platform params arrays to build
	private $share_title;		// Title of the page
	private $share_image;		// image name
	private $share_image_path;	// Path to image
	private $share_slug;		// Slug name
	private $share_slug_path;	// Path to slug
	private $params;			// array of platforms, which are arrays of parameters

	public function setPlatforms( $platforms ){
		$this->platforms = $platforms;
	}

	public function setTitle( $title ){
		$this->share_title = $title;
	}

	public function setImage( $image ){
		$this->share_image = $image;
	}
	
	public function setImagePath( $path ){
		$this->share_image_path = $path;
	}

	public function setSlug( $slug ){
		$this->share_slug = $slug;
	}

	public function setSlugPath( $path ){
		$this->share_slug_path = $path;
	}


	/* Takes platforms, title, image, image path and makes an array
	 */
	public function makeParams(){
		$params = array();
		foreach( $this->platforms as $platform ){
			switch( $platform ){
				case 'facebook':
					$params[ $platform ] = array(
						'platform' => $platform,
						'name' => $this->share_title,
						'picture' => $this->share_image_path . $this->share_image,
						'href' => $this->share_slug_path . $this->share_slug
					);
					break;
				case 'twitter':
					$params[ $platform ] = array(
						'platform' => $platform,
						'text' => $this->share_title,
						'href' => $this->share_slug_path . $this->share_slug
					);
					break;
				case 'pinterest':
					$params[ $platform ] = array(
						'platform' => $platform,
						'description' => $this->share_title,
						'picture' => $this->share_image_path . $this->share_image,
						'href' => $this->share_slug_path . $this->share_slug
					);
					break;
				case 'linkedin':
					$params[ $platform ] = array(
						'platform' => $platform,
						'title' => $this->share_title,
						'picture' => $this->share_image_path . $this->share_image,
						// below for testing only! change back!
						//'href' => 'http://plexuss.local/linkedin'
						'href' => $this->share_slug_path . $this->share_slug
					);
					break;
			}
		}
		$this->params = $params;
	}

	public function getParams(){
		return $this->params;
	}
}
