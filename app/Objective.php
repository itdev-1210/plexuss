<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Objective extends Model {

	protected $table = 'objectives';
	
	protected $fillable = array( 'user_id', 'degree_type', 'major_id','profession_id', 'university_location');
	
	public function user()
    {
        return $this->belongsTo('User');
    }


    /*********************************
    *	public function get major a user is interested in
    *
    **********************************/
    public function getUserMajors($uid){

    	if($uid == null)
    		return null;


		// SELECT m.name
		// FROM plexuss.objectives o
		// JOIN majors m on m.id = o.major_id
		// WHERE o.user_id = $uid;
    	$results = DB::table('objectives as o')
    			   ->join('majors as m', 'm.id' , '=', 'o.major_id')
    			   ->select('m.name', 'm.id')
    			   ->where('o.user_id', '=', $uid)
    			   ->get();

    	return $results; 
    }

}