<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PotentialClient extends Model {

	protected $table = 'potential_clients';

	protected $fillable = array('college_id', 'fname', 'lname', 'email');

}