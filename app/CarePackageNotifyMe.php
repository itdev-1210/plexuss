<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarePackageNotifyMe extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'carepackage_notfiyme';


 	protected $fillable = array('email', 'ip', 'user_id');

}
