<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageType extends Model
{
    protected $table = 'tracking_page_logs';

	protected $fillable = array( 'type' );
}
