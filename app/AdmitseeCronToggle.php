<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmitseeCronToggle extends Model
{
    protected $table = 'admitsee_cron_toggles';

 	protected $fillable = array('name', 'active');
}
