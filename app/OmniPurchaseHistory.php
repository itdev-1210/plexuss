<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OmniPurchaseHistory extends Model {

	protected $table = 'omni_purchase_history';
	
	protected $fillable = array( 'sale_id', 'balance_transaction_id', 'amount', 'currency', 'receipt_json');

}
