<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

  protected $table = 'sn_likes';
  protected $fillable = array('user_id', 'post_id', 'social_article_id', 'post_comment_id');

  /*
  * Model Relationships
  */

  public function post()
  {
    return $this->belongsTo('App\Post');
  }

  public function article()
  {
    return $this->belongsTo('App\SocialArticle');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function comment()
  {
    return $this->belongsTo('App\PostComment');
  }
}
