<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScholarshipSubmission extends Model
{
    //
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'scholarship_submission';


 	protected $fillable = array( 'scholarship_title', 'contact', 'email', 'phone', 'fax', 'deadline', 'number_of_awards', 'max_amount' , 'website', 'address' ,'Address2' , 'city', 'state', 'zip', 'scholarship_description','random_token','user_id', 'interested_service');
}
