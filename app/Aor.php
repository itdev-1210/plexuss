<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aor extends Model
{
    protected $table = 'aor';

 	protected $fillable = array('name', 'college_ids', 'distinction', 'overlap_allowed','num_of_filtered_rec', 'show_on_frontpage', 'contract', 'cost_per_handshake', 'balance');
}
