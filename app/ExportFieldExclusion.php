<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ExportFieldExclusion extends Model {


	protected $table = 'export_field_exclusions';

 	protected $fillable = array( 'type', 'type_id', 'export_field_id' );
}