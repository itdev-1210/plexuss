<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevenueEabStudent extends Model
{
    protected $table = 'revenue_eab_students';

 	protected $fillable = array('zip', 'is_dup', 'ip', 'city', 'state', 'checked_for_zip');

}
