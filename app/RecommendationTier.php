<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecommendationTier extends Model {


	protected $table = 'recommendation_tiers';

 	protected $fillable = array( 'name', 'country_ids' );

}