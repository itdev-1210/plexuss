<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailResubscribeList extends Model
{
    protected $table = 'email_resubscribe_lists';

    protected $fillable = array('user_id', 'sparkpost_response', 'sparkpost_direct_response', 'sendgrid_response', 'created_at', 'updated_at');
}