<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Highschool extends Model {

    protected $fillable = ['school_name', 'verified', 'user_id_submitted'];
    
	protected $table = 'high_schools';

	public function findHighschools($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('high_schools as hs')
					->select('hs.id', DB::raw('CONCAT(proper(hs.school_name),", ", hs.state) as school_name'))
					// ->select('hs.id', DB::raw('CONCAT(proper(hs.school_name),", ", proper(hs.city),", ", hs.state) as school_name'))
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('hs.school_name', 'like', '%'.$input.'%');
					})
                    ->where('verified', 1)
					->limit(10)
					->get();

		return $result;
	}

	public function getHsName( $school_id = null ){
		if( !isset($school_id) ){
			return null;
		}

		$result = Highschool::on('rds1')
							->where('id', '=', $school_id)
							->select('school_name')
							->first();

		return $result;
	}

	public function getHighSchoolsAttended($id = null){
		if( !isset($id) ){
			return 'error - no id passed';
		}

		$schools_attended = DB::table( 'educations' )
								->select( 
									'educations.user_id',
									'educations.school_id as id',
									'educations.school_type',
									'high_schools.school_name as name',
									'high_schools.slug',
									'high_schools.city',
									'high_schools.state' 
								)
								->join( 'high_schools', 'educations.school_id', '=', 'high_schools.id' )
								->where( 'educations.school_type', 'highschool' )
								->where( 'user_id', $id )
								->limit(10)
								->get();

		return $schools_attended;
	}

	public function findHighschoolsCustom($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('high_schools as hs')
					->select('hs.id', 'hs.school_name as name')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('hs.school_name', 'like', '%'.$input.'%');
					})
					->limit(10)
					->get();

		foreach ($result as $key) {
			$key->school_type = 'highschool';
		}

		return $result;
	}
}