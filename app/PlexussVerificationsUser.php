<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlexussVerificationsUser extends Model {

	protected $table = 'plexuss_users_verifications';

 	protected $fillable = array('user_id', 'status', 'verified_skype', 'phonecall_verified');
}
