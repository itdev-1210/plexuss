<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeSubmission extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'college_submission';


 	protected $fillable = array( 'company', 'type', 'contact' , 'title', 'email', 'phone', 'notes', 'utm_source',
 								 'utm_medium', 'utm_content', 'utm_campaign', 'utm_term', 'newsletter', 'analyticsletter', 'first_name', 'last_name', 'client_type');

  public function collegeSubmissionClientType(){
    return $this->hasOne('App\CollegeSubmissionClientType');
  }
}
