<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaMajors extends Model
{
    protected $table = 'nrccua_majors';

    protected $fillable = array( 'key', 'display_name', 'created_at', 'updated_at' );

    public $timestamps = true;
}
