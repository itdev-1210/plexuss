<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SplitTest extends Model {

	protected $table = 'split_tests';

 	protected $fillable = array('name', 'user_id', 'split');
}
