<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfilawAmazonCode extends Model {


	protected $table = 'infilaw_amazon_codes';

 	protected $fillable = array( 'awarded_email', 'emailed', 'isu_id');
}


