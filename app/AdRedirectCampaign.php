<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdRedirectCampaign extends Model
{
    protected $table = 'ad_redirect_campaigns';

 	protected $fillable = array('company', 'url');
}
