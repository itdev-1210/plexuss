<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryConflict extends Model
{
    protected $table = 'country_conflicts';

 	protected $fillable = array('user_id');
}
