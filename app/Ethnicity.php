<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ethnicity extends Model {


	protected $table = 'ethnicities';

	public function getAllUsEthnicities(){
		$eth = Ethnicity::all();
		$temp = array();
		$temp[''] = 'Select...';
		foreach ($eth as $key) {
			$temp[$key->ethnicity] = $key->ethnicity;
		}
		return $temp;
	}

	public function getAllEthnicities(){
		$result = DB::connection('rds1')
					->table('ethnicities as e')
					->get();

		$all = array();

		foreach ($result as $key) {
			$tmp = array();

			$tmp['id'] = $key->id;
			$tmp['name'] = $key->ethnicity;
			$all[] = $tmp;
		}

		return $all;
	}

}