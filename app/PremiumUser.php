<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumUser extends Model {


	protected $table = 'premium_users';

 	protected $fillable = array( 'type', 'user_id', 'omni_purchase_history_id', 'level', 'recurring', 'expires_at' );


}