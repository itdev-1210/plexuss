<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingNotificationLogHistory extends Model
{
    protected $table = 'setting_notification_log_histories';

 	protected $fillable = array('user_id', 'snn_id', 'type', 'removed_date');
}
