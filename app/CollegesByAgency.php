<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesByAgency extends Model
{
    protected $table = 'colleges_by_agencies';

 	protected $fillable = array( 'agency_id', 'college_id' );

}
