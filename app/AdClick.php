<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdClick extends Model
{
    protected $table = 'ad_clicks';

 	protected $fillable = array('company', 'ip', 'ad_passthrough_id', 'user_id', 'user_invite_id', 'slug', 'device', 'browser', 'platform', 'utm_source', 'passthru_status',
 								'countryName', 'stateName', 'cityName', 'pixel_tracked', 'ad_copy_id', 'paid_client', 'college_id');
}
