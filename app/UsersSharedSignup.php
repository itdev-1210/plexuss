<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSharedSignup extends Model {
    protected $table = 'users_shared_signup';

    protected $fillable = array('user_id', 'utm_term', 'utm_content');
    
    public $timestamps = true;

    public static function insertShare($user_id, $utm_term, $utm_content) {
        if (!isset($user_id) || !isset($utm_term)) {
            return 'fail';
        }

        $attributes = ['user_id' => $user_id, 'utm_term' => $utm_term];

        $values = ['user_id' => $user_id, 'utm_term' => $utm_term, 'utm_content' => $utm_content];

        UsersSharedSignup::updateOrCreate($attributes, $values);

        return 'success';
    }
}
