<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recruitment;
use App\Http\Controllers\ViewDataController;
use App\Http\Controllers\Controller;
use DB;

class LikesTally extends Model
{
    	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'likes_tally';


 	protected $fillable = array( 'ip', 'user_id', 'type', 'type_col', 'type_val' );

 	/**
	* get the tally number of likes
	*
	* @return integer
	*/
	public function getLikesTally($arr){

		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData(true);

		$tmp = LikesTally::on('bk')->where('type', $arr['type'])
						->where('type_col', $arr['type_col'])
						->where('type_val', $arr['type_val']);

		if($data['signed_in'] == 1){
			$tmp = $tmp->select(DB::raw("count(*) as cnt, IF(user_id = ".$data['user_id'].", true, false) as isLiked"))
						->first();
		}else{
			$tmp = $tmp->select(DB::raw("count(*) as cnt, IF(ip = '".$data['ip']."', true, false) as isLiked"))
						->first();
		}

		$ret = (object) array();

		$ret->isLiked = $tmp->isLiked;
		$ret->cnt     = $tmp->cnt;

		$rec = Recruitment::on('bk')->where('user_recruit', 1)
									->where('college_id', $arr['type_val'])
									->count();
		$ret->cnt     = $ret->cnt + $rec;
		
		return $ret;
	}

	/**
	* get the tally number of likes
	*
	* @return integer
	*/
	public function getLikesTallyMultipleCollegeIds($arr){

		$tmp = LikesTally::on('bk')->where('type', 'college')
						->where('type_col', 'id')
						->whereIn('type_val', $arr)
						->select(DB::raw("count(*) as cnt, type_val"))
						->groupBy('type_val')
						->get();

		return $tmp;
	}

	public function getLikesTallyForAPI($offset){

		$cr = new Controller;
		$locationArr = $cr->iplookup();

		if (!isset($locationArr['latitude']) || empty($locationArr['latitude'])) {
			$locationArr['latitude'] = '37.910078';
			$locationArr['longitude'] = '-122.065182';
		}

		$distance = '( 3959 * acos( cos( radians('.$locationArr['latitude'].') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$locationArr['longitude'].') ) + sin( radians('.$locationArr['latitude'].') ) * sin(radians(latitude)) ) ) AS distance ';

		$qry = DB::connection('rds1')->table('recruitment as r')
									 ->join('colleges as c', 'r.college_id', '=', 'c.id')
									 ->leftjoin('colleges_ranking as cr', 'cr.college_id', '=', 'c.id')
									 ->leftjoin('college_overview_images as coi', 'coi.college_id', '=', 'c.id')
									 ->where('r.college_id', '!=', 7916)
									 ->where(function($qry){
									 		$qry->orWhere('r.user_recruit', '=', 1)
									 			->orWhere('r.user_recruit', '=', 11);
									 })
									 ->groupBy('c.id')
									 ->orderBy('cnt', 'DESC')
									 ->select('c.id as college_id', 'c.city', 'c.state', 'cr.plexuss as rank', 'c.slug', 'c.school_name',									 		  DB::raw("CASE WHEN `c`.`logo_url` IS NULL THEN 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/default-missing-college-logo.png' ELSE CONCAT('https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/', c.logo_url)  END AS logo_url"),
									 		  DB::raw('CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/overview_images/", `coi`.url) as url'),
									 		  DB::raw($distance), DB::raw("COUNT(r.college_id) as cnt"))
									 ->take(10)
									 ->skip($offset)
									 ->get();

		foreach ($qry as $key) {
	
			if (isset($key->cnt)) {
				
				$count = LikesTally::on('rds1')->where('type', "college")
											   ->where('type_col', "id")
											   ->where('type_val', $key->college_id)
											   ->count();
				$key->cnt += $count;
			}
		}

		return $qry;
	}
}
