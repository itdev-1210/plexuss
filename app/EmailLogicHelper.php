<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailLogicHelper extends Model
{
    protected $table = 'email_logic_helper';

 	protected $fillable = array( 'user_id', 'is_duplicate', 'is_unsubscribed', 'is_inactive', 'is_complete_profile', 
 								 'last_time_sent', 'last_open', 'last_click', 'profile_flow_last_template', 
 								 'profile_flow_last_time_sent', 'profile_flow_assignment', 'timezone_adjustment',
 								 'is_complete_profile_initial_visit', 'choose_last_time_sent', 'choose_last_template',
 								 'is_international' );

}
