<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use DB;
use App\Http\Controllers\ViewDataController, App\Http\Controllers\Controller;
use App\CollegeRecommendationFilters;

class Recruitment extends Model{


	protected $table = 'recruitment';

 	protected $fillable = array( 'user_id', 'college_id', 'user_recruit', 'college_recruit','reputation', 'location', 
 								 'tuition', 'program_offered', 'athletic', 'religion', 'onlineCourse', 'campus_life', 
 								 'other', 'note', 'status', 'type', 'email_sent', 'applied', 'enrolled', 'updated_at', 
 								 'applied_at', 'enrolled_at', 'aor_id', 'user_applied', 'user_applied_at', 'ro_id', 
 								 'is_seen', 'is_seen_recruit' );
 	/**
	 * getNumOfInquiryForColleges
	 *
	 * Total number of inquiries for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfInquiryForColleges($college_id = null, $type = null, $onDate = null, $endDate = null, $aor_id = null){

 		if ($college_id == null || $type == null) {
 			return 0;
 		}
 		
 		$rec= Recruitment::on('bk')->whereIn('recruitment.college_id', $college_id)
 							->where('recruitment.type', $type)
 							->select(DB::raw('count(DISTINCT recruitment.user_id) as cnt, recruitment.college_id'))
 							->groupBy('recruitment.college_id');

 		if( isset($onDate) && isset($endDate) ){
 			$rec = $rec->whereBetween('recruitment.created_at', array($onDate, $endDate));
 		}elseif( isset($onDate) ){
 			$rec = $rec->where('recruitment.created_at', '>', $onDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR
 		
 		$rec = $rec->get();

 		return $rec;
 	}

 	/**
	 * getNumOfInquiryRejectedForColleges
	 *
	 * Total number of rejected inquiries for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfInquiryRejectedForColleges($college_id = null, $type = null, $onDate = null, $endDate = null, $aor_id = null){

 		if ($college_id == null || $type == null) {
 			return 0;
 		}

		$rec= Recruitment::on('bk')->whereIn('college_id', $college_id)
					->where('type', $type)
					->where('user_recruit', 1)
					->where('college_recruit', -1)
					->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
					->groupBy('college_id');

 		if( isset($onDate) && isset($endDate) ){
 			$rec = $rec->whereBetween('created_at', array($onDate, $endDate));
 		}elseif( isset($onDate) ){
 			$rec = $rec->where('created_at', '>', $onDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id)
				->join('users as u','u.id','=','recruitment.user_id');
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		$rec = $rec->get();
 		

 		return $rec;
 	}

 	/**
	 * getNumOfInquiryAcceptedForColleges
	 *
	 * Total number of accepted inquiries for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfInquiryAcceptedForColleges($college_id = null, $type = null, $onDate = null, $endDate = null, $raw_filter_qry = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}

		$rec= Recruitment::on('bk')->whereIn('college_id', $college_id)
						->where('user_recruit', 1)
						->where('college_recruit', 1)
						->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
						->groupBy('college_id');

		if (isset($type)) {
			$rec = $rec->where('type', $type);
		}

 		if( isset($onDate) && isset($endDate) ){
 			$rec = $rec->whereBetween('created_at', array($onDate, $endDate));
 		}elseif( isset($onDate) ){
 			$rec = $rec->where('created_at', '>', $onDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id)
				->join('users as u','u.id','=','recruitment.user_id');
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		if (isset($raw_filter_qry)) {
 			$rec = $rec->join(DB::raw('('.$raw_filter_qry.')  as t2'), 't2.filterUserId' , '=', 'recruitment.user_id');
 		}

 		$rec = $rec->get();
	 		
 		return $rec;
 	}

 	/**
	 * getNumOfInquiryIdleForColleges
	 *
	 * Total number of idle inquiries for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfInquiryIdleForColleges($college_id = null, $type = null, $onDate = null,
 												   $aor_id = null, $fromDate = NULL, $toDate = NULL, $only_targetted = NULL){

 		if ($college_id == null) {
 			return 0;
 		}

 		$rec= DB::connection('bk')->table('recruitment as r')
 								  ->where('r.user_recruit', 1)
		 						  ->where('college_recruit', 0)
		 						  ->select(DB::raw('count(DISTINCT r.user_id) as cnt, r.college_id'))
		 						  ->groupBy('r.college_id')
		 						  ->where('r.status', 1);

		if (is_array($college_id)) {
			$rec = $rec->where(function($q) use ($college_id){
				foreach ($college_id as $key => $value) {
					$q = $q->orWhere('r.college_id', '=', $value);
				}
			});
		}
 							
 		if (isset($type)) {
 			$rec = $rec->where('r.type', 'LIKE', "%".$type."%");
 		}

 		if( isset($onDate) ){
 			$rec = $rec->where('r.updated_at', '>', $onDate);
 		}

 		if( isset($fromDate) ){
 			$rec = $rec->where('r.updated_at', '>=', $fromDate);
 		}

 		if( isset($toDate) ){
 			$rec = $rec->where('r.updated_at', '<=', $toDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('r.aor_id', $aor_id)
				->join('users as u','u.id','=','r.user_id');
		}else{
			$rec = $rec->whereNull('r.aor_id');
		}
		// End of AOR

		if (isset($only_targetted)) {
			$rec = $rec->leftjoin('recruitment_tags as rt', function($q) use($aor_id){
								  $q->on('rt.user_id', '=', 'r.user_id');
								  $q->on('rt.college_id', '=', 'r.college_id');
								  if (isset($aor_id)) {
								  	$q->on('rt.aor_id', '=', 'r.aor_id');
								  }else{
								  	$q->whereNull('rt.aor_id');
								  }
								})
						->whereNotNull('rt.id');
			if (isset($aor_id)) {
				$rec = $rec->where('rt.aor_id', $aor_id);
			}else{
				$rec = $rec->whereNotNull('rt.org_portal_id')
						   ->where('rt.org_portal_id', '!=', -1);
			}
		}

 		$rec = $rec->get();

 		return $rec;
 	}

 	/**
	 * getNumOfTotalPendingFromAllSourcesForColleges
	 *
	 * Total number of pending from all sources for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfTotalPendingFromAllSourcesForColleges($college_id = null, $aor_id = null){
 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('recruitment.college_id', $college_id)
 							->where('recruitment.type', '!=', 'inquiry')
 							->select(DB::raw('count(DISTINCT recruitment.user_id) as cnt, recruitment.college_id'))
 							->groupBy('recruitment.college_id');

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id)
				->join('users as u','u.id','=','recruitment.user_id');
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		$rec = $rec->get();
 		
 		return $rec;
 	}

 	/**
	 * getNumOfTotalPendingApprovedForColleges
	 *
	 * Total number of pending from all sources for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfTotalPendingApprovedForColleges($college_id = null, $aor_id = null){
 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('recruitment.college_id', $college_id)
 							->where('recruitment.user_recruit', 1)
 							->where('recruitment.college_recruit', 1)
 							->where('recruitment.type', '!=', 'inquiry')
 							->select(DB::raw('count(DISTINCT user_id) as cnt, recruitment.college_id'))
 							->groupBy('recruitment.college_id');

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id)
				->join('users as u','u.id','=','recruitment.user_id');
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		$rec = $rec->get();

 		return $rec;
 	}

 	/**
	 * getNumOfTotalApprovedForColleges
	 *
	 * Total number of approved
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfTotalApprovedForColleges($college_id = null, $fromDate = null, $toDate = null, $raw_filter_qry = null,
 													 $aor_id = null, $only_targetted = NULL, $type = NULL){
 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= DB::connection('bk')->table('recruitment as r')
 								  ->where('r.user_recruit', 1)
		 						  ->where('r.college_recruit', 1)
		 						  ->select(DB::raw('count(DISTINCT r.user_id) as cnt, r.college_id'))
		 						  ->where('r.status',1)
		 						  ->groupBy('r.college_id');

		if (is_array($college_id)) {
			$rec = $rec->where(function($q) use ($college_id){
				foreach ($college_id as $key => $value) {
					$q = $q->orWhere('r.college_id', '=', $value);
				}
			});
		}

		if (isset($type)) {
			$rec = $rec->where('r.type', 'LIKE', "%".$type."%");
		}
		
 		if( isset($fromDate) ){
 			$rec = $rec->where('r.updated_at', '>=', $fromDate);
 		}

 		if( isset($toDate) ){
 			$rec = $rec->where('r.updated_at', '<=', $toDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('r.aor_id', $aor_id)
				->join('users as u','u.id','=','r.user_id');
		}else{
			$rec = $rec->whereNull('r.aor_id');
		}
		// End of AOR

 		if (isset($raw_filter_qry)) {
 			$rec = $rec->join(DB::raw('('.$raw_filter_qry.')  as t2'), 't2.filterUserId' , '=', 'r.user_id');
 		}

 		if (isset($only_targetted)) {
			$rec = $rec->leftjoin('recruitment_tags as rt', function($q) use($aor_id){
								  $q->on('rt.user_id', '=', 'r.user_id');
								  $q->on('rt.college_id', '=', 'r.college_id');
								  if (isset($aor_id)) {
								  	$q->on('rt.aor_id', '=', 'r.aor_id');
								  }else{
								  	$q->whereNull('rt.aor_id');
								  }
								})
						->whereNotNull('rt.id');
			if (isset($aor_id)) {
				$rec = $rec->where('rt.aor_id', $aor_id);
			}else{
				$rec = $rec->whereNotNull('rt.org_portal_id')
						   ->where('rt.org_portal_id', '!=', -1)
						   ->leftjoin('organization_portals as op', 'op.id', '=', 'rt.org_portal_id')
						   ->where('op.active', 1);
			}
		}

 		$rec = $rec->get();

 		return $rec;
 	}

 	/**
	 * getNumOfPendingForColleges
	 *
	 * Total number of pending for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfPendingForColleges($college_id = null, $aor_id = null){

 		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData(true);

 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('college_id', $college_id)
 							->where('status', 1)
 							->where('user_recruit', 0)
 							->where('college_recruit', 1)
 							->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
 							->join('users as u', 'u.id', '=', 'recruitment.user_id')
 							->groupBy('college_id');

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		// If user has a department set, show results based on the filters they have set
		$crf = new CollegeRecommendationFilters;
		$filter_qry = $crf->generateFilterQry($data);

		if (isset($filter_qry) && isset($data['default_organization_portal'])) {
			$filter_qry = $filter_qry->select('userFilter.id as filterUserId');
			$bc = new Controller;
			$tmp_qry = $bc->getRawSqlWithBindings($filter_qry);
			$rec = $rec->join(DB::raw('('.$tmp_qry.')  as t2'), 't2.filterUserId' , '=', 'recruitment.user_id');
		}
		// End of department query set

		$rec = $rec->get();

 		return $rec;
 	}
 	/**
	 * getNumOfApprovedForColleges
	 *
	 * Total number of approved for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfApprovedForColleges($college_id = null, $onDate = null, $endDate = null, $aor_id = null){

 		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData(true);

 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('recruitment.college_id', $college_id)
 							->where('recruitment.user_recruit', 1)
 							->where('recruitment.college_recruit', 1)
 							->where('recruitment.status', 1)
 							->select(DB::raw('count(DISTINCT recruitment.user_id) as cnt, recruitment.college_id'))
 							->join('users as u', 'u.id', '=', 'recruitment.user_id')
 							->groupBy('recruitment.college_id');

 		if( isset($onDate) && isset($endDate) ){
 			$rec = $rec->whereBetween('recruitment.created_at', array($onDate, $endDate));
 		}elseif( isset($onDate) ){
 			$rec = $rec->where('recruitment.created_at', '>', $onDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		// If user has a department set, show results based on the filters they have set
		$crf = new CollegeRecommendationFilters;
		$filter_qry = $crf->generateFilterQry($data);

		if (isset($filter_qry) && isset($data['default_organization_portal'])) {
			$filter_qry = $filter_qry->select('userFilter.id as filterUserId');

			$bc = new Controller;
			$tmp_qry = $bc->getRawSqlWithBindings($filter_qry);
			$rec = $rec->join(DB::raw('('.$tmp_qry.')  as t2'), 't2.filterUserId' , '=', 'recruitment.user_id');

		}

 		$rec = $rec->get();
 		
 		return $rec;
 	}
 	/**
	 * getNumOfRejectedUsersForColleges
	 *
	 * Total number of rejected users for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfRejectedUsersForColleges($college_id = null, $aor_id = null){
 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('college_id', $college_id)
 							->where('status', '!=', 1)
 							->where('college_recruit', 1)
 							->where('user_recruit', 0)
 							->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
 							->groupBy('college_id')
 							->get();


 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR		

 		return $rec;
 	}

 	/**
	 * getCountOfRecruitment
	 *
	 * Total number of college recruits
	 *
	 * @return (int) (count)
	 */
 	public function getCountOfRecruitment(){

 		if (Cache::has(env('ENVIRONMENT') .'_'.'getCountOfRecruitment')) {
 			$rec = Cache::get(env('ENVIRONMENT') .'_'.'getCountOfRecruitment');
 		}else{
 			$cnt = 272984;
	 		$rec= Recruitment::on('rds1')
	 							->where('college_recruit', 1)
	 							->where('user_recruit', 1)
	 							->where('status', 1)
	 							->count();
	 		$rec = $rec + $cnt;
	 		Cache::add(env('ENVIRONMENT') .'_'.'getCountOfRecruitment', $rec, 5);
 		}
 		
 		return $rec;
 	}

 	/**
	 * getNumOfEnrolled
	 *
	 * Total number of enrolled in each college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfEnrolled($college_id = null, $fromDate = null, $toDate = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('college_id', $college_id)
 							->where('enrolled', 1)
 							->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
 							->groupBy('college_id');

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id',$aor_id);
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR

 		if( isset($fromDate) ){
 			$rec = $rec->where('created_at', '>=', $fromDate);
 		}

 		if( isset($toDate) ){
 			$rec = $rec->where('created_at', '<=', $toDate);
 		}
 		
 		$rec = $rec->get();
 		return $rec;

 	}

 	/**
	 * getNumOfApplied
	 *
	 * Total number of applied in each college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfApplied($college_id = null, $fromDate = null, $toDate = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}
 		$rec= Recruitment::on('bk')->whereIn('college_id', $college_id)
 							->where('applied', 1)
 							->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
 							->groupBy('college_id');

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('recruitment.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('recruitment.aor_id');
		}
		// End of AOR	

 		if( isset($fromDate) ){
 			$rec = $rec->where('created_at', '>=', $fromDate);
 		}

 		if( isset($toDate) ){
 			$rec = $rec->where('created_at', '<=', $toDate);
 		}
 		
 		$rec = $rec->get();
 		
 		return $rec;
 	}

 	public function getRecruitmentDistinctUsers($type, $suppression_user_ids = null, $data = null, $txtmsg_page = NULL){

 		if (!isset($data)) {
 			$viewDataController = new ViewDataController();
			$data = $viewDataController->buildData(true);
 		}

		DB::connection('rds1')->statement('SET SESSION group_concat_max_len = 1000000;');

		if(isset($data['agency_collection']->agency_id)){
			$rec = AgencyRecruitment::on('rds1')
									->where('agency_id', $data['agency_collection']->agency_id)
									->join('users as u', 'u.id', '=', 'agency_recruitment.user_id');
			if(isset($data['aor_id'])) {
				$rec = $rec->where('agency_recruitment.aor_id', $data['aor_id']);
			} else {
				$rec = $rec->whereNull('agency_recruitment.aor_id');
			}

		}else{
			$rec = Recruitment::on('rds1')
							  ->where('college_id', $data['org_school_id'])
							  ->join('users as u', 'u.id', '=', 'recruitment.user_id');
							  
			if(isset($data['aor_id'])) {
				$rec = $rec->where('recruitment.aor_id', $data['aor_id']);
			} else {
				$rec = $rec->whereNull('recruitment.aor_id');
			}
		}

		if ($type == 'pending') {
			$rec = $rec->where('college_recruit', 1)
					   ->where('user_recruit', 0);
		}else{
			$rec = $rec->where('college_recruit', 1)
					   ->where('user_recruit', 1);
		}

		if (isset($txtmsg_page)) {
			$rec = $rec->where('u.txt_opt_in', 1);
		}
		
 		$rec = $rec->select(DB::raw('GROUP_CONCAT(DISTINCT user_id SEPARATOR ",") as users'))
				   ->where('status', 1);

		if (isset($suppression_user_ids) && !is_array($suppression_user_ids)) {
			$suppression_user_ids = explode(",", $suppression_user_ids);
		}

		if (isset($suppression_user_ids)) {
			$rec = $rec->whereNotIn('user_id', $suppression_user_ids);
		}

 		// If user has a department set, show results based on the filters they have set
		$crf = new CollegeRecommendationFilters;
		$filter_qry = $crf->generateFilterQry($data);

		if (isset($filter_qry) && isset($data['default_organization_portal'])) {
			$filter_qry = $filter_qry->select('userFilter.id as filterUserId');
			$bc = new Controller;
			$tmp_qry = $bc->getRawSqlWithBindings($filter_qry);
			$rec = $rec->join(DB::raw('('.$tmp_qry.')  as t2'), 't2.filterUserId' , '=', 'recruitment.user_id');
		}
		// End of department query set

		$rec = $rec->distinct('recruitment.user_id');
		$rec = $rec->first();

		return $rec->users;
 	}

 	/**
	 * getAvgAgeOfStudents
	 *
	 * Average age of students.
	 *
	 * @param (numeric) (college_id) college id 
	 * @param (data)
	 * @return (int) 
	 */
 	public function getAvgAgeOfStudents($college_id, $data, $raw_filter_qry){

 		$avg = DB::connection('rds1')->table('recruitment as r')
 									 ->join('users as u', 'u.id', '=', 'r.user_id')
 									 ->where('r.college_id', $college_id)
 									 ->whereNotNull(DB::raw('year(u.birth_date)'))
 									 ->where(DB::raw('year(u.birth_date)'), '!=', 0)
 									 ->select(DB::raw('ROUND(avg(year(u.birth_date))) as birth_date'));

 		if (isset($data['aor_id']) && !empty($data['aor_id'])) {
 			$avg = $avg->where('r.aor_id', $data['aor_id']);
 		}else{
 			$avg = $avg->whereNull('r.aor_id');
 		}

 		if (isset($raw_filter_qry)) {
 			$avg = $avg->join(DB::raw('('.$raw_filter_qry.')  as t2'), 't2.filterUserId' , '=', 'r.user_id');
 		}

 		$avg = $avg->first();

 		$birth_date = ceil($avg->birth_date);
 		$this_year = Carbon::now();
 		$this_year = $this_year->year;

 		return $this_year - $birth_date;
 	}

 	/**
	 * getPercentageOfGender
	 *
	 * Average age of students.
	 *
	 * @param (numeric) (college_id) college id 
	 * @param (data)
	 * @return (obj) 
	 */
 	public function getPercentageOfGender($college_id, $data, $raw_filter_qry){

 		$qry = DB::connection('rds1')->table('recruitment as r')
 									 ->join('users as u', 'u.id', '=', 'r.user_id')
 									 ->where('r.college_id', $college_id)
 									 ->select(DB::raw("concat(round(sum(IF(u.gender = 'm', 1, 0)) / count(*) * 100, 2)) as males,
													   concat(round(sum(IF(u.gender = 'f', 1, 0)) / count(*) * 100, 2)) as females"))
 									 ->whereNotNull('u.gender');

 		if (isset($data['aor_id']) && !empty($data['aor_id'])) {
 			$qry = $qry->where('r.aor_id', $data['aor_id']);
 		}else{
 			$qry = $qry->whereNull('r.aor_id');
 		}

 		if (isset($raw_filter_qry)) {
 			$qry = $qry->join(DB::raw('('.$raw_filter_qry.')  as t2'), 't2.filterUserId' , '=', 'r.user_id');
 		}

 		$qry = $qry->first();

 		return $qry;
 	}

	/**
		 * getUserFeedbackColleges
		 *
		 * Get the list of colleges 
		 *
		 * @param (numeric) (user_id) user id 
		 * @return (obj) 
		 */
 	public function getUserFeedbackColleges($user_id){ 
 		if (!isset($user_id)) {
 			return NULL;
 		}
 		$two_weeks_ago = Carbon::now()->subWeeks(2);

 		$rec = DB::connection('bk')->table('colleges as c')
 								   ->leftjoin('recruitment as r', function($join1) use($user_id, $two_weeks_ago) {
 								   			$join1->on('c.id', '=', 'r.college_id');
 								   			$join1->on('r.user_id', '=', DB::raw($user_id));
 								   			$join1->on('r.status', '=', DB::raw(1));
 								   			$join1->on('r.user_applied', '=', DB::raw(0));
 								   			$join1->on('r.updated_at', '<', DB::raw("'".$two_weeks_ago."'"));
 									})
 								   ->leftjoin('prescreened_users as pr', function($join) use($user_id, $two_weeks_ago){
 								   			$join->on('c.id', '=', 'pr.college_id');
 								   			$join->on('pr.user_id', '=', DB::raw($user_id));
 								   			$join->on('pr.active', '=', DB::raw(1));
 								   			$join->on('pr.user_applied', '=', DB::raw(0));
 								   			$join->on('pr.updated_at', '<', DB::raw("'".$two_weeks_ago."'"));
 								   })
 								   ->leftjoin('college_overview_images as coi', function($join)
											{
											    $join->on('c.id', '=', 'coi.college_id');
											    $join->on('coi.url', '!=', DB::raw('""'));
											    $join->on('coi.is_video', '=', DB::raw(0));
											    $join->on('coi.is_tour', '=', DB::raw(0));
											})
 								   ->where('c.verified', 1)
 								   ->where(function($qry){
 								   		$qry->where(function($q){
		 								   		$q->orWhereRaw('(`r`.`user_recruit`,`r`.`college_recruit`)'.'='.'(1,1)')
		 								   		  ->orWhereRaw('(`r`.`user_recruit`,`r`.`college_recruit`)'.'='.'(0,1)')
		 								   		  ->orWhereRaw('(`r`.`user_recruit`,`r`.`college_recruit`)'.'='.'(1,0)')
		 								   		  ->orWhereRaw('(`r`.`user_recruit`,`r`.`college_recruit`)'.'='.'(1,-1)');
		 								    })
 								   			->orWhereNotNull('pr.id');
 								   })
 								   
 								   ->select('c.id as college_id', 'c.logo_url', 'c.school_name', 'c.slug',
 								   			'coi.url as img_url',
 								   			'r.id as rec_id',
 								   			'pr.id as pr_id',
 								   			DB::raw('IF(c.paid_app_url != "", c.paid_app_url, c.application_url) as app_url'))
 								   ->orderByRaw(' CASE
												WHEN r.user_applied = -1 THEN
												 8
												WHEN pr.id is not NULL THEN
												 1
												WHEN r.user_recruit = 1
												AND r.college_recruit = 1 THEN
												 2
												WHEN r.user_recruit = 0
												AND r.college_recruit = 1 THEN
												 3
												WHEN r.user_recruit = 1
												AND r.college_recruit = 0 THEN
												 4
												WHEN r.user_recruit = 1
												AND r.college_recruit = - 1 THEN
												 5
												WHEN c.in_our_network = 1 THEN
												 6
												ELSE
												 7
												END ASC, 
												r.user_applied DESC')
 								   ->groupBy('c.id')
 								   ->first();

 		return $rec;
 	}

 	/**
	 * getNumberOfPicksBasedOnType
	 *
	 * Get number of recruitments based on type name no matter if they are handshake, inquiries, etc.
	 *
	 * @param (numeric) (user_id) user id 
	 * @return (obj) 
	 */
 	public function getNumberOfPicksBasedOnType($college_id = null, $type = null, $onDate = null,
 												   $aor_id = null, $fromDate = NULL, $toDate = NULL, $only_targetted = NULL){

 		if ($college_id == null) {
 			return 0;
 		}

 		$rec= DB::connection('bk')->table('recruitment as r')
		 						  ->select(DB::raw('count(DISTINCT r.user_id) as cnt, r.college_id'))
		 						  ->groupBy('r.college_id')
		 						  ->where('r.status', 1);

		if (is_array($college_id)) {
			$rec = $rec->where(function($q) use ($college_id){
				foreach ($college_id as $key => $value) {
					$q = $q->orWhere('r.college_id', '=', $value);
				}
			});
		}
 							
 		if (isset($type)) {
 			$rec = $rec->where('r.type', 'LIKE', "%".$type."%");
 		}

 		if( isset($onDate) ){
 			$rec = $rec->where('r.updated_at', '>', $onDate);
 		}

 		if( isset($fromDate) ){
 			$rec = $rec->where('r.updated_at', '>=', $fromDate);
 		}

 		if( isset($toDate) ){
 			$rec = $rec->where('r.updated_at', '<=', $toDate);
 		}

 		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('r.aor_id', $aor_id)
				->join('users as u','u.id','=','r.user_id');
		}else{
			$rec = $rec->whereNull('r.aor_id');
		}
		// End of AOR

		if (isset($only_targetted)) {
			$rec = $rec->leftjoin('recruitment_tags as rt', function($q) use($aor_id){
								  $q->on('rt.user_id', '=', 'r.user_id');
								  $q->on('rt.college_id', '=', 'r.college_id');
								  if (isset($aor_id)) {
								  	$q->on('rt.aor_id', '=', 'r.aor_id');
								  }else{
								  	$q->whereNull('rt.aor_id');
								  }
								})
						->whereNotNull('rt.id');
			if (isset($aor_id)) {
				$rec = $rec->where('rt.aor_id', $aor_id);
			}else{
				$rec = $rec->whereNotNull('rt.org_portal_id')
						   ->where('rt.org_portal_id', '!=', -1)
						   ->leftjoin('organization_portals as op', 'op.id', '=', 'rt.org_portal_id')
						   ->where('op.active', 1);
			}
		}

 		$rec = $rec->get();

 		return $rec;
 	}
}