<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

use App\MandrillLog, App\RoleBaseEmail, App\User, App\EmailLogicHelper;
use App\Http\Controllers\Controller;

class SparkpostModel extends \Eloquent {

    private $md;
    private $template_name;
    private $sparky;


    public function __construct($list_name, $secondary_key = null)  {
        $this->setTemplateName($list_name);

        $httpClient = new GuzzleAdapter(new Client());

        (isset($secondary_key)) ? $key = env('SPARKPOST_DIRECT_KEY') : $key = env('SPARKPOST_KEY');
        
        $this->sparky  = new SparkPost($httpClient, ['key' => $key]);
        
    }

    public function sendTemplate($email_arr, $params, $reply_email, $attachments = null, $bcc_address = null, $subject = null, $ab_test_id = null){

        // Check for role base email
        $rbe = new RoleBaseEmail;
        if ($rbe->isRoleBase($email_arr['email'])) {
            return 'failed';
        }

        $template_name = $this->template_name;
        $template_content = array();
        
        $message = array();

        $message['substitution_data'] = $params;

        $message['content']          = array();
      
        $message['content']['from']          = array();

        $message['content']['from']['name']    = 'Plexuss';
        $message['content']['from']['email']   = $reply_email;

        $message['replyTo']          = $reply_email; 

        $message['recipients']       = array();

        $tmp = array();
        $tmp['address']['name'] = $email_arr['name'];
        $tmp['address']['email'] = $email_arr['email'];
        $message['recipients'][] = $tmp;
      
        if (isset($bcc_address)) {
            $message['bcc']       = array();

            $tmp = array();
            $tmp['address']['email'] = $bcc_address;
            $tmp['address']['name']  = '';
            $message['bcc'][] = $tmp;
        }

        // Note: Currently, sparkpost does not allow attachments with templates in the same transaction,
        // To make templates work, first we request the HTML from the sparkpost template and 
        // manually create a email with attachment(s).
        if (isset($attachments) && !empty($attachments)) {
            $message['content']['attachments'] = $attachments;

            $get_promise = $this->sparky->request('GET', 'templates/' . $template_name);

            try {
                $get_response = $get_promise->wait();
                $get_response_body = $get_response->getBody();

                $message['content']['html'] = $get_response_body['results']['content']['html'];
                $message['content']['subject'] = $get_response_body['results']['content']['subject'];

            } catch (\Exception $str) {
                $ml = new MandrillLog();
                $ml->template_name = $this->template_name;
                $ml->response = $str;
                $ml->email = $email_arr['email'];
                unset($message['attachments']);
                $ml->params = json_encode($message);

                $ml->save();
                
                return 'failed fetching template';
            }

        }elseif (isset($ab_test_id)) {
            $message['content']['ab_test_id'] = $ab_test_id;
        
        }else { // If no attachment, just use template id.
            $message['content']['template_id'] = $template_name;
        }

        if (isset($subject)) {
            $message['content']['subject'] = $subject;
        }
        
        try {
            // Build your email and send it!      
            $promise = $this->sparky->transmissions->post($message);
            $response = $promise->wait();

            $ml = new MandrillLog();
            $ml->template_name = $this->template_name;
            $ml->response = json_encode($response->getBody());
            $ml->email = $email_arr['email'];
            unset($message['attachments']);
            $ml->params = json_encode($message);

            $ml->save();
            $this->template_name = '';

            $this->updateEmailLogicHelper($email_arr['email']);

        } catch (\Exception $str) {

            $ml = new MandrillLog();
            $ml->template_name = $this->template_name;
            $ml->response = $str;
            $ml->email = $email_arr['email'];
            unset($message['attachments']);
            $ml->params = json_encode($message);

            $ml->save();

            $this->template_name = '';
        }
        
        return 'success';
    }

    private function updateEmailLogicHelper($email){

        $user = User::on('bk')->where('email', $email)
                              ->select('id')
                              ->first();

        if (!isset($user)) {
            return "failed";
        }

        $attr = array();
        $val  = array();

        $now = Carbon::now();
        $attr['user_id'] = $user->id;
        $val['user_id']  = $user->id;
        $val['last_time_sent'] = $now;

        EmailLogicHelper::updateOrCreate($attr, $val);

        return "success";
    }

    private function setTemplateName($list_name){
        switch ($list_name) {
            
            case 'college_send_message_for_users':
                
                $this->template_name = "users-a-school-sent-you-a-message";
                break;
            case 'college_recommendation_for_users':
                
                $this->template_name = "users-college-recommendations";
                break;

            case 'colleges_viewed_your_profile':
                
                $this->template_name = "users-schools-viewed-your-profile";
                break;
            
            case 'college_wants_to_recruit_you':
                
                $this->template_name = "users-a-school-wants-to-recruit-you-new";
                break;

            case 'college_recommendations':
                
                $this->template_name = "your-student-recommendations-final";
                break;

            case 'potential_college_recommendations':
                
                $this->template_name = "your-student-recommendations-shadow";
                break;
                
            case 'user_want_to_get_recruited_for_colleges':
                
                $this->template_name = "colleges-users-want-to-get-recruited";
                break;

            case 'user_accepts_request_to_be_recruited_for_colleges':
                
                $this->template_name = "colleges-users-accepts-request-to-be-recruited";
                break;

             case 'user_send_message_for_colleges':
                
                $this->template_name = "colleges-user-sends-your-college-a-message";
                break;

            case 'college_agreed_to_recruit_you':
                
                $this->template_name = "users-a-school-agreed-to-recruit-you";
                break;

            case 'new_user_confirmation_email':
                
                $this->template_name = "users-confirm-email";
                break;    

            case 'paid_member_request_to_sales':
                
                $this->template_name = "paid-member-request-to-sales";
                break; 

            case 'agency_recruiting_to_students':
                
                $this->template_name = "agency-recruiting-to-students";
                break;

            case 'college_prep_recruiting_in_area':
                
                $this->template_name = "college-prep-recruiting-in-area "; // extra space found here, bug?
                break;

            case 'agency_recruiting_visa_help':
                 $this->template_name = "agency-recruiting-visa-help";
                 break; 

            case 'agencies_agency_sign_up_request':
                $this->template_name = "agencies-agency-sign-up-request";
                break;

            case 'english_recruiting_in_area':
                 $this->template_name = "english-recruiting-in-area";
                 break; 

            case 'in_network_comparison_college_users_email':
                $this->template_name = "users-rec-in-network-college-score-comparison";
                break;

            case 'ranking_update_college_users_email':
                $this->template_name = "users-college-ranking-update-school-on-list";
                break;

            case 'near_you_college_users_email':
                $this->template_name = "users-rec-in-network-college-near-you";
                break;

            case 'chat_session_college_users_email':
                $this->template_name = "users-chat-session-invite";
                break;
            
            case 'school_you_liked_college_users_email':
                $this->template_name = "users-in-network-school-you-liked";
                break;
            
            case 'users_in_network_school_you_messaged_college_users_email':
                $this->template_name = 'users-in-network-school-you-messaged';
                break;

            case 'users_in_network_school_u_wanted_to_get_recruited_college_users_email':
                $this->template_name = 'users-in-network-school-u-wanted-to-get-recruited';
                break;

            case 'college_prep_recruiting_in_area_agency_users_email':
                $this->template_name = 'users-college-prep-recruiting-in-area';
                break;

            case 'agency_recruiting_visa_help_agency_users_email':
                $this->template_name = 'users-agency-recruiting-visa-help';
                break;

            case 'english_recruiting_in_area_agency_users_email':
                $this->template_name = 'users-english-recruiting-in-area';
                break;

            case 'college_prep_user_interested_in_your_services':
                $this->template_name = 'college-prep-user-interested-in-your-services';
                break;

            case 'agencies_user_interested_in_your_services':
                $this->template_name = 'agencies-user-interested-in-your-services';
                break;
            
            case 'english_program_user_interested_in_your_services':
                $this->template_name = 'users-english-program-handshake';
                break;

            case 'infilaw_survey_completion_notification_and_data':
                $this->template_name = 'infilaw-survey-completion-notification-and-data';
                break;

            case 'csl_amazon_code':
                $this->template_name = 'csl-amazon-code';
                break;

            case 'fcsl_amazon_code':
                $this->template_name = 'fcsl-amazon-code';
                break;
            
            case 'welcome_email':
                $this->template_name = 'users-engagement-email-1-what-is-plexuss-mdl-1';
                break;

            case 'how_recruitment_work_after_one_week':
                $this->template_name = 'users-engagement-email-2-recruitment-mdl';
                break;
            
            case 'how_recruitment_work_after_two_week':
                $this->template_name = 'users-engagement-email-3-indicators-mdl';
                break;

            case 'how_recruitment_work_after_three_week':
                $this->template_name = 'users-engagement-email-4-less-than-30-mdl';
                break;

            case 'birthday_email':
                $this->template_name = 'users-happy-birthday';
                break;

            case 'users_friend_invite':
                $this->template_name = 'users-friend-invite-mdl';
                break;

            case 'new_college_ranking_for_colleges':
                $this->template_name = 'colleges-new-ranking';
                break;
            
            case 'test_message':
                $this->template_name = 'colleges-user-sends-your-college-a-message-test';
                break;

            case 'users_friend_invite_2_mdl':
                $this->template_name = 'users-friend-invite-2-mdl';
                break;

            case 'users_friend_invite_3_mdl':
                $this->template_name = 'users-friend-invite-3-mdl';
                break;

            case 'colleges_weekly_plexuss_update_mdl':
                $this->template_name = 'colleges-weekly-plexuss-update-mdl';
                break;

            case 'internal_triggers':
                $this->template_name = 'internal-triggers';
                break;

            case 'march_madness_invite_2016':
                $this->template_name = 'march-madness-invite-2016';
                break;

            case 'internal_college_upgrade_to_premiere_request':
                $this->template_name = 'internal-college-upgrade-to-premiere-request';
                break;

            case 'colleges_manage_portal_new_user':
                $this->template_name = 'colleges-manage-portal-new-user';
                break;

            case 'colleges_manage_portal_existing_account':
                $this->template_name = 'colleges-manage-portal-existing-account';
                break;

            case 'colleges_welcome_to_the_plexuss_premier_program':
                $this->template_name = 'colleges-welcome-to-the-plexuss-premier-program';
                break;

            case 'colleges_sample_premier_contract':
                $this->template_name = 'colleges-sample-premier-contract';
                break;

            case 'colleges_college_self_sign_up_request':
                $this->template_name = 'colleges-college-self-sign-up-request';
                break;
            
            case 'colleges_college_interested_in_premium_service':
                $this->template_name = 'colleges-college-interested-in-premium-service';
                break;

            case 'internal_remove_plexuss_account':
                $this->template_name = 'internal-remove-plexuss-account';
                break;
            
            case 'internal_manually_add_to_suppression':
                $this->template_name = 'internal-manually-add-to-suppression';
                break;

            case 'internal_why_unsbuscribed':
                $this->template_name = 'internal-why-unsbuscribed';
                break;

            case 'users_premium_order_confirmation':
                $this->template_name = 'users-premium-order-confirmation';
                break;

            case 'college_export_file':
                $this->template_name = 'college-export-file';
                break;

            case 'webinar_10_4_confirmation':
                $this->template_name = 'users-webinar-10-4-confirmation';
                break;

            case 'users_webinar_10_4_otero_initial_invite':
                $this->template_name = 'users-webinar-10-4-otero-initial-invite';
                break;

            case 'users_webinar_10_4_otero_15_minute_reminder':
                $this->template_name = 'users-webinar-10-4-otero-15-minute-reminder';
                break;
                
            case 'users_college_needs_more_info':
                $this->template_name = 'users-a-college-needs-more-info';
                break;

            case 'users_handshake_next_steps':
                $this->template_name = 'users-handshake-next-steps';
                break;

            case 'users_financial_documents':
                $this->template_name = 'users-financial-documents';
                break;

            case 'users_webinar_11_4_uic_initial_invite':
                $this->template_name = 'users-webinar-11-4-uic-initial-invite';
                break;

            case 'colleges_daily_recs_and_inquiries':
                $this->template_name = 'colleges-daily-recs-and-inquiries';
                break;

            case 'users_webinar_11_4_uic_15_minute_reminder':
                $this->template_name = 'users-webinar-11-4-uic-15-minute-reminder';
                break;

            case 'users_webinar_12_1_ttu_confirmation':
                $this->template_name = 'users-webinar-12-1-ttu-confirmation';
                break;

            case 'users_webinar_12_1_15_minute_reminder':
                $this->template_name = 'users-webinar-12-1-15-minute-reminder';
                break;

            case 'users_webinar_12_1_ttu_initial_invite':
                $this->template_name = 'users-webinar-12-1-ttu-initial-invite';
                break;

            case 'users_webinar_3_1_liberty_confirmation':
                $this->template_name = 'users-webinar-3-1-liberty-confirmation';
                break;

            case 'users_webinar_3_1_liberty_24_hour_reminder':
                $this->template_name = 'users-webinar-3-1-liberty-24-hour-reminder';
                break;

            case 'users_webinar_3_1_liberty_15_minute_reminder':
                $this->template_name = 'users-webinar-3-1-liberty-15-minute-reminder';
                break;

            case 'users_webinar_3_1_liberty_initial_invite':
                $this->template_name = 'users-webinar-3-1-liberty-initial-invite';
                break;
                
            case 'internal_urgent_email_from_college_rep':
                $this->template_name = 'internal-urgent-email-from-college-rep';
                break;

            case 'internal_urgent_email_from_agency_rep':
                $this->template_name = 'internal-urgent-email-from-agency-rep';
                break;
            
            case 'users_oneapp_invite_day_one':
                $this->template_name = 'users-oneapp-invite-day-one';
                break;

            case 'users_oneapp_invite_daily_followup':
                $this->template_name = 'users-oneapp-invite-daily-followup';
                break;

            case 'users_oneapp_daily_application_status':
                $this->template_name = 'users-oneapp-daily-application-status';
                break;

            case 'users_oneapp_invite_daily_followup_finished_app':
                $this->template_name = 'users-oneapp-invite-daily-followup-finished-app';
                break;

            case 'users_oneapp_invite_weekly_followup':
                $this->template_name = 'users-oneapp-invite-weekly-followup';
                break;

            case 'users_oneapp_invite_coveted_wkly_followup_post_10':
                $this->template_name = 'users-oneapp-invite-coveted-wkly-followup-post-10';
                break;
                
            case 'users_admitsee_email_major':
                $this->template_name = 'users-admitsee-email-major';
                break;

            case 'users_admitsee_email_school':
                $this->template_name = 'users-admitsee-email-school';
                break;
            
            case 'users_admitsee_email_country':
                $this->template_name = 'users-admitsee-email-country';
                break;

            case 'b2b_plexuss_weekly_digest_1':
                $this->template_name = 'b2b-plexuss-weekly-digest-1';
                break;

            case 'users_els_additional_info':
                $this->template_name = 'users-els-additional-info';
                break;

            case 'other_intern_survey':
                $this->template_name = 'other-intern-survey';
                break;

            case 'other_hiring_survey':
                $this->template_name = 'other-hiring-survey';
                break;

            case 'oneapp_internal_finished_app':
                $this->template_name = 'oneapp-internal-finished-app';
                break;
            
            case 'users_additional_uploads_required':
                $this->template_name = 'users-additional-uploads-required';
                break;
                
            case 'users_oneapp_demoted':
                $this->template_name = 'users-oneapp-demoted';
                break;

            case 'users_oneapp_promoted':
                $this->template_name = 'users-oneapp-promoted';
                break;

            case 'users_edx_college_credit_while_in_highschool':
                $this->template_name = 'users-edx-college-credit-while-in-highschool';
                break;

            case 'users_edx_free_courses_top_universities':
                $this->template_name = 'users-edx-free-courses-top-universities';
                break;
            
            case 'users_edx_ielts_and_toefl':
                $this->template_name = 'users-edx-ielts-and-toefl';
                break;

            case 'users_oneapp_scholarship_email':
                $this->template_name = 'users-oneapp-scholarship-email';
                break;
            
            case 'users_plexuss_mobile_app_annoucement':
                $this->template_name = 'users-plexuss-mobile-app-annoucement';
                break;

            case 'agencies_one_lead_a_day_non_registered':
                $this->template_name = 'agencies-one-lead-a-day-non-registered';
                break;
            
            case 'agencies_add_description':
                $this->template_name = 'agencies-add-description';
                break;

            case 'users_edx_free_courses_top_universities_nr':
                $this->template_name = 'users-edx-free-courses-top-universities-nr';
                break;
            
            case 'users_edx_free_courses_top_universities_events':
                $this->template_name = 'users-edx-free-courses-top-universities-events';
                break;

            case 'colleges_student_organically_inquired':
                $this->template_name = 'colleges-student-organically-inquired';
                break;
            
            case 'users_recommended_by_plexuss':
                $this->template_name = 'users-recommended-by-plexuss';
                break;

            case 'users_a_college_viewed_your_profile':
                $this->template_name = 'users-a-college-viewed-your-profile';
                break;

            case 'users_a_college_wants_to_recuit_you':
                $this->template_name = 'users-a-college-wants-to-recuit-you';
                break;

            case 'users_a_college_wants_to_recuit_you_2':
                $this->template_name = 'users-a-college-wants-to-recuit-you-2';
                break;

            case 'truscribe':
                $this->template_name = 'truscribe';
                break;

            case 'topuniversities_grad_school':
                $this->template_name = 'topuniversities-grad-school';
                break;

            case 'topuniversities_mba':
                $this->template_name = 'topuniversities-mba';
                break;

            case 'colleges_plexuss_email_for_analytics':
                $this->template_name = 'colleges-plexuss-email-for-analytics';
                break;
            case 'colleges_plexuss_email_for_analytics_101_to_500':
                $this->template_name = 'colleges-plexuss-email-for-analytics-101-to-500';
                break;
            case 'colleges_plexuss_email_for_analytics_500_plus':
                $this->template_name = 'colleges-plexuss-email-for-analytics-500-plus';
                break;    
            case 'colleges_weekly_crm_report':
                $this->template_name = 'colleges-weekly-crm-report';
                break;

            case 'colleges_daily_crm_report':
                $this->template_name = 'colleges-daily-crm-report';
                break;

            case 'topuniversities_grad_school_ver_b':
                $this->template_name = 'topuniversities-grad-school-ver-b';
                break;

            case 'topuniversities_mba_ver_b':
                $this->template_name = 'topuniversities-mba-ver-b';
                break;

            case 'springboard_first':
                $this->template_name = 'springboard-first';
                break;

            case 'music_institute_v1':
                $this->template_name = 'music-institute-v1';
                break;

            case 'users_invite_springboard_first':
                $this->template_name = 'users-invite-springboard-first';
                break;

            case 'users_invite_topuniversities_mba_ver_b':
                $this->template_name = 'users-invite-topuniversities-mba-ver-b';
                break;

            case 'users_invite_music_institute_v1':
                $this->template_name = 'users-invite-music-institute-v1';
                break;

            case 'users_invite_edx_free_courses_top_universities_nr':
                $this->template_name = 'users-invite-edx-free-courses-top-universities-nr';
                break;

            case 'users_invite_gcu_eddy_click':
                $this->template_name = 'users-invite-gcu-eddy-click';
                break;

            case 'users_invite_calu_eddy_click':
                $this->template_name = 'users-invite-calu-eddy-click';
                break;

            case 'gcu_eddy_click':
                $this->template_name = 'gcu-eddy-click';
                break;

            case 'calu_eddy_click':
                $this->template_name = 'calu-eddy-click';
                break;

            case 'exampal_v1':
                $this->template_name = "exampal-v1";
                break;

            case 'exampal_v2':
                $this->template_name = "exampal-v2";

            case 'daily_client_report':
                $this->template_name = 'daily-client-report';
                break;

            case 'cornell_college_v1':
                $this->template_name = 'cornell-college-v1';
                break;

            case 'plexuss_pixel_request':
                $this->template_name = 'plexuss-pixel-request';
                break;

            case 'b2b_plexuss_resources':
                $this->template_name = 'b2b-plexuss-resources';
                break;
            
            case 'plexuss_survey_email_r1':
                $this->template_name = "plexuss-survey-email-r1";
                break;

            case 'qs_scholarships_template1_copy_04':
                $this->template_name = "qs-scholarships-template1-copy-04";
                break;

            case 'internal_scholarship_submission':
                $this->template_name = "internal-scholarship-submission";
                break;

            case 'passthrough_second_email':
                $this->template_name = 'passthrough-second-email';
                break;
                
            case 'magoosh_email_r4':
                $this->template_name = 'magoosh-email-r4';
                break;

            case 'sdsu_email_r2':
                $this->template_name = 'sdsu-email-r2';
                break;

            case 'usf_email_updated_copy_r1':
                $this->template_name = 'usf-email-updated-copy-r1';
                break;

            case 'studyportals_email_r2':
                $this->template_name = 'studyportals-email-r2';
                break;
            
            case 'usf_dynamic_program_email':
                $this->template_name = 'usf-dynamic-program-email';
                break;

            case 'plexuss_email_reminder_send_numbers_r2':
                $this->template_name = 'plexuss-email-reminder-send-numbers-r2';
                break;

            case 'test_template':
                $this->template_name = 'test-template';
                break;
            
            case 'plexuss_scholarship_email_user_thankyou':
                $this->template_name = 'plexuss-scholarship-email-user-thankyou';
                break;

            case 'plexuss_hult_email_blackhtml':
                $this->template_name = "plexuss-hult-email-blackhtml";
                break;

            case 'onboarding_signup':
                $this->template_name = 'onboarding-signup';
                break;

            case 'edx_certificate':
                $this->template_name = 'edx-certificate';
                break;

            case 'osu_email_r1':
                $this->template_name = 'osu-email-r1';
                break;

            case 'alliant_email_r1':
                $this->template_name = 'alliant-email-r1';
                break;

            case 'nafsa_2018_first_email':
                $this->template_name = "nafsa-2018-first-email";
                break;
            
            case 'nafsa_2018_second_email':
                $this->template_name = "nafsa-2018-second-email";
                break;
            
            case 'cappex_scholarship_email_r1':
                $this->template_name = "cappex-scholarship-email-r1";
                break;

            case 'premium_emails_light_dark_r1':
                $this->template_name = 'premium-emails-light-dark-r1';
                break;

            case 'plexuss_premium_save_r2':
                $this->template_name = 'plexuss-premium-save-r2';
                break;

            case 'into_uk_email':
                $this->template_name = 'into-uk-email';
                break;

            case 'daily_college_inquiries_report':
                $this->template_name = 'daily-college-inquiries-report';
                break;

            case 'nafsa_2018_third_email':
                $this->template_name = 'nafsa-2018-third-email';
                break;

            case 'owl_scholarship_email_r1':
                $this->template_name = 'owl-scholarship-email-r1';
                break;

            case 'plexuss_scholarship_email_user_deadline':
                $this->template_name = 'plexuss-scholarship-email-user-deadline';
                break;

            case 'plexuss_scholarship_email_user_provider_r2_one':
                $this->template_name = 'plexuss-scholarship-email-user-provider-r2-one';
                break;

            case 'plexuss_scholarship_email_user_provider_r2_four':
                $this->template_name = 'plexuss-scholarship-email-user-provider-r2-four';
                break;

            case 'edx_emails_howto':
                $this->template_name = 'edx-emails-howto';
                break;

            case 'openclassromms_email_r2':
                $this->template_name = 'openclassromms-email-r2';
                break;

            case 'plexuss_qs_email_template_update_r1':
                $this->template_name = 'plexuss-qs-email-template-update-r1';
                break;

            case 'plexuss_qs_email_template_update_india':
                $this->template_name = 'plexuss-qs-email-template-update-india';
                break;

            case 'plexuss_qs_email_template_update_europe':
                $this->template_name = 'plexuss-qs-email-template-update-europe';
                break;

            case 'plexuss_qs_email_template_update_america':
                $this->template_name = 'plexuss-qs-email-template-update-america';
                break;

            case 'edx_email_certificate':
                $this->template_name = 'edx-email-certificate';
                break;

            case 'studyportal_email3':
                $this->template_name = 'studyportal-email3';
                break;

            case 'into_study_uk_2':
                $this->template_name = 'into-study-uk-2';
                break;

            case 'plexuss_more_info_emails_r1':
                $this->template_name = 'plexuss-more-info-emails-r1';
                break;

            case 'studygroup_email_study_in_uk':
                $this->template_name = 'studygroup-email-study-in-uk';
                break;  

            case 'plexuss_cappex_scholarship_update_r1':
                $this->template_name = 'plexuss-cappex-scholarship-update-r1';
                break;

            case 'plexuss_owl_scholarship_update_r2':
                $this->template_name = 'plexuss-owl-scholarship-update-r2';
                break;

            case 'plexuss_recommended_mobile_desktop_r3':
                $this->template_name = 'plexuss-recommended-mobile-desktop-r3';

            case 'welcome_mail':
                $this->template_name = 'welcome-mail';
                break;
            /////////////////////////////
            case 'users_choose5_countries_seeking_you':
                $this->template_name = 'users-choose5-countries-seeking-you';
                break;

            case 'users_choose5_financial_requirements':
                $this->template_name = 'users-choose5-financial-requirements';
                break;

            case 'users_choose5_programs_interested_in':
                $this->template_name = 'users-choose5-programs-interested-in';
                break;

            case 'users_choose5_majors_viewed':
                $this->template_name = 'users-choose5-majors-viewed';
                break;

            case 'users_choose5_online_only_degrees':
                $this->template_name = 'users-choose5-online-only-degrees';
                break;

            case 'users_choose5_gpa_recommended':
                $this->template_name = 'users-choose5-gpa-recommended';
                break;

            case 'users_choose1_certificate_programs_seeking':
                $this->template_name = 'users-choose1-certificate-programs-seeking';
                break;

            case 'users_choose1_compare_3_colleges':
                $this->template_name = 'users-choose1-compare-3-colleges';
                break;

            case 'users_choose1_countries_seeking_you':
                $this->template_name = 'users-choose1-countries-seeking-you';
                break;

            case 'users_choose1_gpa_recommended':
                $this->template_name = 'users-choose1-gpa-recommended';
                break;

            case 'users_choose1_majors_viewed':
                $this->template_name = 'users-choose1-majors-viewed';
                break;

            case 'users_choose1_best_schools_within_your_state':
                $this->template_name = 'users-choose1-best-schools-within-your-state';
                break;

            case 'users_choose1_best_schools_within_your_state_2':
                $this->template_name = 'users-choose1-best-schools-within-your-state-2';
                break;

            case '15_how_can_i_apply_to_colleges_through_plexuss_version_1':
                $this->template_name = '15-how-can-i-apply-to-colleges-through-plexuss-version-1';
                break;

            case 'user_info_template_2':
                $this->template_name = 'user-info-template-2';
                break;

            case 'user_recruit_you':
                $this->template_name = 'user-recruit-you';
                break;

            case 'verify_email_template':
                $this->template_name = 'verify-email-template';
                break;

            case 'update_info_template_1':
                $this->template_name = 'update-info-template-1';
                break;

            case 'user_resources':
                $this->template_name = 'user-resources';
                break;

            case 'plexuss_scholorship':
                $this->template_name = 'plexuss-scholorship';
                break;

            case 'majors_email_template':
                $this->template_name = 'majors-email-template';
                break;

            case 'compare_college':
                $this->template_name = 'compare-college';
                break;

            case 'college_want_to_recruit':
                $this->template_name = 'college-want-to-recruit';
                break;

            case 'plexuss_portal_student':
                $this->template_name = 'plexuss-portal-student';
                break;

            case 'user_schlorship':
                $this->template_name = 'user-schlorship';
                break;

            case 'users_international_visa_email':
                $this->template_name = 'users-international-visa-email';
                break;

            case 'plexuss_international_resources':
                $this->template_name = 'plexuss-international-resources';
                break;

            case '15_how_can_i_apply_to_colleges_through_plexuss_version_2draft':
                $this->template_name = '15-how-can-i-apply-to-colleges-through-plexuss-version-2draft';
                break;

            case '11___primer_colleges_seeking_':
                $this->template_name = '11---primer-colleges-seeking-';
                break;

            case 'how_to_search_college':
                $this->template_name = 'how-to-search-college';
                break;

            case 'users_choose1_online_only_degrees':
                $this->template_name = 'users-choose1-online-only-degrees';
                break;

            case 'users_choose1_meet_your_financial_requirements':
                $this->template_name = 'users-choose1-meet-your-financial-requirements';
                break;  
            
            case 'users_choose1_scholarships_from_seeking_you':
                $this->template_name = 'users-choose1-scholarships-from-seeking-you';
                break;

            case 'users_choose5_compare_3_colleges':
                $this->template_name = 'users-choose5-compare-3-colleges';
                break;

            case 'users_choose5_scholarships_from_seeking_you':
                $this->template_name = 'users-choose5-scholarships-from-seeking-you';
                break;

            case 'verify_email_template_int':
                $this->template_name = 'verify-email-template-int'; 
                break;

            case 'welcome_mail_int':
                $this->template_name = 'welcome-mail-int'; 
                break;
                
            case 'update_info_template_1_int':
                $this->template_name = 'update-info-template-1-int'; 
                break;
                
            case 'user_resources_int':
                $this->template_name = 'user-resources-int'; 
                break;
                
            case 'how_to_search_college_int':
                $this->template_name = 'how-to-search-college-int'; 
                break;
                
            case 'plexuss_scholorship_int':
                $this->template_name = 'plexuss-scholorship-int'; 
                break;
                
            case 'majors_email_template_int':
                $this->template_name = 'majors-email-template-int'; 
                break;
                
            case 'compare_college_int':
                $this->template_name = 'compare-college-int'; 
                break;
                
            case 'college_want_to_recruit_int':
                $this->template_name = 'college-want-to-recruit-int'; 
                break;
                
            case 'plexuss_portal_student_int':
                $this->template_name = 'plexuss-portal-student-int'; 
                break;
                
            case '11___primer_colleges_seeking_int':
                $this->template_name = '11---primer-colleges-seeking-int'; 
                break;
                
            case 'user_schlorship_int':
                $this->template_name = 'user-schlorship-int'; 
                break;
                
            case 'users_international_visa_email_int':
                $this->template_name = 'users-international-visa-email-int'; 
                break;
                
            case 'plexuss_international_resources_int':
                $this->template_name = 'plexuss-international-resources-int'; 
                break;
                
            case '15_how_can_i_apply_to_colleges_through_plexuss_version_1_int':
                $this->template_name = '15-how-can-i-apply-to-colleges-through-plexuss-version-1-int'; 
                break;

            case 'ncsa_scholorship':
                $this->template_name = 'ncsa-scholorship';
                break;

            case 'springboard_direct_v2':
                $this->template_name = 'springboard-direct-v2';
                break;

            case 'holidays_2018':
                $this->template_name = 'holidays-2018';
                break;

            case 'holidays_2018_premium_offer':
                $this->template_name = 'holidays-2018-premium-offer';
                break;
                
            // Congratulations for premium users
            case 'premium_email':
                $this->template_name = 'premium-email';
                break;

            default:
                # code...
                break;
        }
    }

    public function isSupressed($email, $uid = null, $uiid = null, $secondary_key = null){

        $url = "https://api.sparkpost.com/api/v1/suppression-list/".$email;
        $client = new Client(['base_uri' => 'http://httpbin.org']);
        // $esl    = new EmailSuppressionList;
        (isset($secondary_key)) ? $key = env('SPARKPOST_DIRECT_KEY') : $key = env('SPARKPOST_KEY');
        try {
            $response = $client->request('GET', $url, [
                        'headers' => [
                            'Authorization' => $key,
                            'Accept'        => 'application/json'
                            ]
                        ]);
            $result = json_decode($response->getBody()->getContents(), true);
            if (!isset($result['results']) && !isset($secondary_key)) {
                $this->isSupressed($email, $uid, $uiid, true);
            }
            if (isset($result['results'])) {
                if (isset($result['results'][0])) {
                    $tmp = $result['results'][0];

                    $attr = array('recipient' => $tmp['recipient']);
                    $val  = array('recipient' => $tmp['recipient'], 'description' => $tmp['description'], 'source' => $tmp['source'],
                                  'type' => 'transactional', 'non_transactional' => 1);

                    isset($uid) ? $val['uid'] = $uid : null;
                    isset($uiid) ? $val['uiid'] = $uiid : null;
                    isset($uid) ? $attr['uid'] = $uid : null;
                    isset($uiid) ? $attr['uiid'] = $uiid : null;
                    $esl = EmailSuppressionList::updateOrCreate($attr, $val);

                    if (isset($uid) || isset($uiid)) {
                        $qry = EmailSuppressionList::where('recipient', $tmp['recipient'])
                                                   ->whereNull('uid')
                                                   ->whereNull('uiid')
                                                   ->delete();
                    }
                }
                return true;
            }else{
                return false;
            }
        } catch (\Exception $e) {
            return false;
            // return "something bad happened";
        }

    }

    public function sendCappedEmails($message, $email, $template_name){

        try {
            // Build your email and send it!      
            $promise = $this->sparky->transmissions->post($message);
            $response = $promise->wait();

            $ml = new MandrillLog();
            $ml->template_name = $template_name;
            $ml->response = json_encode($response->getBody());
            $ml->email = $email;
            $ml->params = json_encode($message);

            $ml->save();
            $this->template_name = '';

        } catch (\Exception $str) {

            $ml = new MandrillLog();
            $ml->template_name = $template_name;
            $ml->response = $str;
            $ml->email = $email;
            $ml->params = json_encode($message);

            $ml->save();

            $this->template_name = '';
        }
    }
}