<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyTextTemplate extends Model {

	protected $table = 'reply_text_templates';

 	protected $fillable = array('type', 'body');
}
