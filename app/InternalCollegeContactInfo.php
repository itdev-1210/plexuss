<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalCollegeContactInfo extends Model
{
    //
    protected $table = 'internal_college_contact_info';

    protected $fillable = array('unsub', 'nafsa_2018');
}
