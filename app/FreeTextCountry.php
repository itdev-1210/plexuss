<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeTextCountry extends Model {

	protected $table = 'free_text_countries';

 	protected $fillable = array('country_code', 'country_name');
}