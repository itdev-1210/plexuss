<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesApplicationsState extends Model
{
    protected $table = 'colleges_applications_states';

 	protected $fillable = array('name', 'display_name');

 	public function getAllApplicationState(){
		$eth = CollegesApplicationsState::on('rds1')->get();
		$temp = array();
		foreach ($eth as $key) {
			$temp[$key->name] = $key->display_name;
		}
		return $temp;
	}

}
