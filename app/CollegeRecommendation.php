<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class CollegeRecommendation extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'college_recommendations';


 	protected $fillable = array('agency_id', 'college_id', 'user_id', 'reason', 'active', 'type', 'aor_id', 'bucket_name');


 	public function getTodayRecommendations($college_id = null, $onDate = null, $endDate = null, $aor_id = null){

 		$now = Carbon::now();
 		$cr = CollegeRecommendation::where('college_id', $college_id);

 		if( isset($onDate) && isset($endDate) ){
 			$cr = $cr->whereBetween('created_at', array($onDate, $endDate));
 		}else{
			$cr = $cr->where('created_at', '>=', $now->today());
 		}

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$cr = $cr->where('college_recommendations.aor_id', $aor_id);
		}else{
			$cr= $cr->whereNull('college_recommendations.aor_id');
		}
		// End of AOR

 		$cr = $cr->get();

 		return $cr;

 	}

 	/**
	 * getTotalNumOfRecommendationsForColleges
	 *
	 * Total number of recommendations for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getTotalNumOfRecommendationsForColleges($college_id = null, $org_portal_id = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}
 		
 		$rec= CollegeRecommendation::on('bk')->whereIn('college_recommendations.college_id', $college_id)
						->select(DB::raw('count(DISTINCT college_recommendations.user_id) as cnt, college_recommendations.college_id'))
						->groupBy('college_recommendations.college_id');

		if (isset($org_portal_id)) {
			$rec = $rec->where('college_recommendations.org_portal_id', $org_portal_id);
		}else{
			$rec = $rec->whereNull('college_recommendations.org_portal_id');
		}

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('college_recommendations.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('college_recommendations.aor_id');
		}
		// End of AOR

		$rec = $rec->get();

 		return $rec;
 	}

 	/**
	 * getNumOfRecommendationsAcceptedPendingForColleges
	 *
	 * Total number of accepted pending recommendations for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfRecommendationsAcceptedPendingForColleges($college_id = null, $onDate = null, $endDate = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}

		$rec= CollegeRecommendation::on('bk')->whereIn('college_recommendations.college_id', $college_id)
						->where('college_recommendations.active', 0)
						->select(DB::raw('count(DISTINCT user_id) as cnt, college_recommendations.college_id'))
						->groupBy('college_recommendations.college_id');

 		if( isset($onDate) && isset($endDate) ){
 			$rec = $rec->whereBetween('college_recommendations.created_at', array($onDate, $endDate));
		}elseif( isset($onDate) ){
 			$rec = $rec->where('college_recommendations.created_at', '>', $onDate);
 		}

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('college_recommendations.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('college_recommendations.aor_id');
		}
		// End of AOR

 		$rec = $rec->get();
 		

 		return $rec;
 	}

 	/**
	 * getNumOfRecommendationsRejectedForColleges
	 *
	 * Total number of rejected recommendations for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfRecommendationsRejectedForColleges($college_id = null, $onDate = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}

		$rec= CollegeRecommendation::on('bk')->whereIn('college_id', $college_id)
					->where('active', -1)
					->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
					->groupBy('college_id');
	 	

 		if( isset($onDate) ){
 			$rec = $rec->where('created_at', '>', $onDate);
 		}

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('college_recommendations.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('college_recommendations.aor_id');
		}
		// End of AOR

 		$rec = $rec->get();

 		return $rec;
 	}

 	/**
	 * getNumOfRecommendationsIdleForColleges
	 *
	 * Total number of Idle recommendations for a particular college
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfRecommendationsIdleForColleges($college_id = null, $onDate = null, $aor_id = null){

 		if ($college_id == null) {
 			return 0;
 		}

		$rec= CollegeRecommendation::on('bk')->whereIn('college_id', $college_id)
						->where('active', 1)
						->select(DB::raw('count(DISTINCT user_id) as cnt, college_id'))
						->groupBy('college_id');

 		if( isset($onDate) ){
 			$rec = $rec->where('created_at', '>', $onDate);
 		}

		// AOR : if the user is AOR, we only want to show the AOR students	
		if (isset($aor_id)) {
			$rec = $rec->where('college_recommendations.aor_id', $aor_id);
		}else{
			$rec = $rec->whereNull('college_recommendations.aor_id');
		}
		// End of AOR

 		$rec = $rec->get();
 		

 		return $rec;
 	}
}
