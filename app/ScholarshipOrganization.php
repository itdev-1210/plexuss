<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScholarshipOrganization extends Model
{
    protected $table = "scholarship_organizations";

    protected $fillable = array( 'name' );
}
