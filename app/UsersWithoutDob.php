<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersWithoutDob extends Model
{
    protected $table = 'users_without_dob';

 	protected $fillable = array( 'user_id', 'dob', 'address', 'response', 'accurateappend', 'ebureau', 'ebureau_response',
 								 'whitepages', 'whitepages_response' );
}
