<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeOverview extends Model
{
    protected $table = 'college_overview';
}
