<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SimilarMajor extends Model{

    protected $table = 'similar_majors';
    
    protected $fillable = array('major_id', 'similar_major_id', 'similarity_rank', 'co_ocurrence');


    public function findSimilarMajors($major_id = null){

        if (is_null($major_id )) {
            return;
        }

        $majors = DB::connection('rds1')
            ->table('similar_majors as sm')
            ->join('majors as m', 'sm.similar_major_id', '=', 'm.id')
            ->select('m.*')
            ->where('major_id', $major_id)
            ->orderBy('similarity_rank')
            ->take(3)
            ->get();

        return $majors;
    }
}
