<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingUrlFragmentId extends Model
{
    protected $table = 'tracking_url_fragment_ids';

 	protected $fillable = array('tpu_id', 'fragment_id', 'college_id');
}
