<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentVerifiedApp extends Model {

	protected $table = 'recruitment_verified_apps';

 	protected $fillable = array('prev_state', 'rec_id', 'user_id', 'college_id', 'aor_id', 'org_portal_id', 'aor_portal_id');
}