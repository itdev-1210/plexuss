<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmAutoReporting extends Model
{
    //
    protected $table = 'crm_auto_reporting';

    protected $fillable = array('user_id', 'date', 'time', 'last_sent', 'created_at', 'updated_at');

    // Data must contain user_id, date, and time
    public static function insertOrUpdate($data) {
        $potentialKeys = ['user_id', 'date', 'time'];

        foreach ($potentialKeys as $key) {
            if (!isset($data[$key])) {
                return 'Failed. ' . $key . ' is missing';
            }
        }

        $attributes = [
            'user_id' => $data['user_id'],
        ];

        $values = [
            'user_id' => $data['user_id'],
            'date' => $data['date'],
            'time' => $data['time'],
        ];

        CrmAutoReporting::updateOrCreate($attributes, $values);

        return 'success';
    }
}
