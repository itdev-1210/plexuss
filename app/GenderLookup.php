<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenderLookup extends Model
{
    protected $table = 'gender_lookup';
	protected $fillable = array( 'fname', 'gender' );
}
