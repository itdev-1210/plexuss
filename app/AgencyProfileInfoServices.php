<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyProfileInfoServices extends Model {
    protected $table = 'agency_profile_info_services';

    protected $fillable = array('agency_id', 'agency_profile_info_id', 'service_name', 'updated_at', 'created_at');

	public $timestamps = true;


	// $services must be of type string array, use this function only for signup!!!!
	public static function insertServices($agency_profile_info_id, $services) {
		foreach ($services as $key => $service) {
			$values = [ 'agency_profile_info_id' => $agency_profile_info_id, 'service_name' => $service ];

			AgencyProfileInfoServices::firstOrCreate($values);
		}

		return 'success';
	}


	// Edit agency profile function, remove all services and add new ones.
	public static function removeOldAndAddNewServices($agency_id, $services) {

		// Delete all previous services. (Essentially removing the previous selected services that were left unchecked in the frontend during editing)
		AgencyProfileInfoServices::where('agency_id', '=', $agency_id)
								 ->delete();

		foreach ($services as $key => $service) {
			$values = [ 'agency_id' => $agency_id, 'service_name' => $service ];

			AgencyProfileInfoServices::firstOrCreate($values);
		}

		return 'success';

	}

	public static function getServicesByAgencyId($agency_id) {
		$query = AgencyProfileInfoServices::on('rds1')
		 						 		  ->where('agency_id', '=', $agency_id)
		 						 		  ->pluck('service_name');

		if (isset($query)) {
	 		return $query->toArray();
	 	}

	 	return [];	 						 
	}

	// Return array list of all distinct services, only grab services where a active agent exists
	public static function getAllServices() {
		$query = AgencyProfileInfoServices::on('rds1')
										  ->distinct()
										  ->join('agency as a', 'agency_id', '=', 'a.id')
										  ->where('a.active', '1')
 					   					  ->whereNotNull('service_name')
										  ->pluck('service_name');

	 	if (isset($query)) {
	 		return $query->toArray();
	 	}

	 	return [];
	}
}
