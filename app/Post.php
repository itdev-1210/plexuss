<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

  protected $table = 'sn_posts';
  protected $fillable = array('title', 'user_id', 'post_text', 'shared_link', 'privacy', 'original_post_id', 'is_shared', 'share_count', 'share_type', 'share_post_id', 'share_post_type' ,'post_status', 'posted_by_plexuss', 'share_with_id', 'created_at', 'updated_at');

  /*
  * Model Relationships
  */

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function comments()
  {
    return $this->hasMany('App\PostComment');
  }

  public function likes()
  {
    return $this->hasMany('App\Like');
  }

  public function images()
  {
    return $this->hasMany('App\PostImage')
                ->whereNull('post_comment_id')
                ->whereNull('social_article_id');
  }

  public function views()
  {
    return $this->hasMany('App\SocialView');
  }

  public function userAccountSettings()
  {
    return $this->hasOne('App\UserAccountSettings', 'user_id', 'user_id');
  }
}
