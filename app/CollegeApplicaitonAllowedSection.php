<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeApplicaitonAllowedSection extends Model
{
    protected $table = 'colleges_application_allowed_sections';

 	protected $fillable = array('college_id', 'page', 'sub_section', 'define_program', 'required');
}
