<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model {

	protected $table = 'educations';

	protected $fillable = array( 'school_id', 'school_type','graduation_date', 'latest' );

	public function users()
	{
		return $this->belongsTo('App\User');
	}

}