<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextSuppressionList extends Model {

	protected $table = 'text_suppression_list';

 	protected $fillable = array('user_id', 'phone', 'is_list_user', 'org_branch_id');
}
