<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PixelTrackedTesting extends Model
{
    //
    protected $table = 'pixel_tracked_testing';
    protected $fillable = ['user_id', 'cid', 'company', 'pixel_tracked', 'paid_client'];
    public $timestamps = true;
}
