<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Department extends Model
{
    protected $table = 'departments';
	
	protected $fillable = array('name');


	public function getAllDepartments($useIds = null, $noDefaultOption = null, $needBothNameAndId = null){
		$dep = Department::orderby('name', 'ASC')->get();
		$temp = array();

		if( !isset($noDefaultOption) ){
			$temp[''] = 'Select...';
		}

		foreach ($dep as $key) {
			if( isset($needBothNameAndId) ){
				$tmp = array();
				$tmp['name'] = $key->name;
				$tmp['id'] = (int)$key->id;
				$temp[] = $tmp;
			}elseif( !isset($useIds) ){
				$temp[$key->name] = $key->name;
			}else{
				$temp[$key->id] = $key->name;
			}
		}
		return $temp;
	}

	public function getConcatAllDepAndMajor(){
		$dep = DB::connection('rds1')->table('majors as m')
									 ->join('departments as d', 'd.id', '=', 'm.department_id')
									 ->selectRaw('CONCAT(d.id, ",", m.id) as depMajor')
									 ->get();

		return $dep;
	}
}
