<?php

namespace App;

use App\SocialAbuseReportType, App\SocialAbuseReportExplanation;
use Illuminate\Database\Eloquent\Model;

class SocialAbuseReport extends Model
{
    protected $table = 'sn_abuse_reports';

	protected $fillable = array( 'user_id', 'post_id', 'share_article_id', 'abuse_type_id', 'abuse_explanation_id');

	public function add($input){

		$attr = array('name' => $input['type']);
		$type = SocialAbuseReportType::updateOrCreate($attr, $attr);

		$attr = array('content' => $input['explanation']);

		$explanation = SocialAbuseReportExplanation::updateOrCreate($attr, $attr);

		$val = array( 'user_id' => $input['user_id'], 'post_id' => $input['post_id'], 'share_article_id' => 
						$input['share_article_id'], 'abuse_type_id' => $type->id, 'abuse_explanation_id' => $explanation->id);

		SocialAbuseReport::updateOrCreate($val, $val);

	}
}
