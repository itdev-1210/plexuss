<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Crypt;

class College extends Model
{
    protected $fillable = ['school_name', 'verified', 'user_id_submitted'];

    public $timestamps = false;

	public function collegeOverviewImages(){
		return $this->hasMany('CollegeOverviewImages');
	}

	public function Colleges($name=null)
	{
		if($name=='#')
		{
			$result = DB::connection('rds1')->table('colleges')->whereRaw("school_name regexp '^[0-9]'")->where('verified', '=', '1')->orderBy('school_name', 'asc')->get();
		}
		else
		{
			$result = DB::connection('rds1')->table('colleges')->select('id', 'slug', 'ipeds_id','school_name','city','state','zip')
			->where('verified', '=', '1')
			->where('school_name', 'LIKE', $name.'%')
			->orderBy('school_name', 'asc')
			->get();
		}
		return $result;
	}


	public function collegeInfo($schoolSlugArray=null)
	{
		$query=DB::connection('rds1')->table('colleges')
		->join('colleges_tuition', 'colleges_tuition.college_id', '=', 'colleges.id')
		->join('colleges_admissions', 'colleges_admissions.college_id', '=', 'colleges.id')
		->leftJoin('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges.id')
		->leftJoin('colleges_financial_aid as cfa', 'cfa.college_id', '=', 'colleges.id')

		->select('colleges.*','colleges_admissions.sat_percent','colleges_admissions.act_percent','colleges_admissions.deadline',
				 'colleges_admissions.sat_read_75','colleges_admissions.sat_math_75','colleges_admissions.sat_write_75','colleges_admissions.sat_read_25',
				 'colleges_admissions.sat_math_25','colleges_admissions.sat_write_25','colleges_admissions.act_composite_25','colleges_admissions.act_composite_75','colleges_admissions.application_fee_undergrad',
				 'colleges_tuition.tuition_avg_in_state_ftug','colleges_tuition.tuition_avg_out_state_ftug', 'colleges_tuition.books_supplies_1213','colleges_tuition.room_board_on_campus_1213','colleges_tuition.other_expenses_on_campus_1213','colleges_ranking.plexuss', 'cfa.undergrad_grant_pct',
				 'cfa.undergrad_grant_avg_amt'

				 );

		if($schoolSlugArray!='')
		{
			$query = $query->whereIn('slug', $schoolSlugArray);
		}
		$returnData = $query->get();
		return $returnData;
	}


	public function CollegeData($collegeId=null){

		$query=DB::connection('rds1')->table('colleges')
		->join('colleges_tuition', 'colleges_tuition.college_id', '=', 'colleges.id')
		->join('colleges_admissions', 'colleges_admissions.college_id', '=', 'colleges.id')
		->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges.id')
		->leftJoin( 'college_overview_images', 'college_overview_images.college_id', '=', 'colleges.id' )
		->leftJoin( 'colleges_stats', 'colleges.id', '=', 'colleges_stats.college_id' )
		->select('colleges.*','colleges_admissions.sat_percent','colleges_admissions.act_percent','colleges_admissions.deadline', 'colleges_admissions.sat_read_75','colleges_admissions.sat_math_75','colleges_admissions.sat_write_75','colleges_admissions.sat_read_25','colleges_admissions.sat_math_25','colleges_admissions.sat_write_25','colleges_admissions.act_composite_25','colleges_admissions.act_composite_75','colleges_admissions.application_fee_undergrad', 'colleges_tuition.tuition_avg_in_state_ftug','colleges_tuition.books_supplies_1213','colleges_tuition.room_board_on_campus_1213','colleges_tuition.other_expenses_on_campus_1213','colleges_ranking.plexuss as plexuss_ranking','colleges_stats.page_title', 'college_overview_images.url as overview_image'
				 );

		if($collegeId != '') {
			$query = $query->where('colleges.id', '=', $collegeId);
		}

		$returnData = $query->first();

		return $returnData;
	}

	public function CollegesAdmission($collegeId=null)
	{
		$result = DB::connection('rds1')->table('colleges_admissions')
		->join('colleges','colleges_admissions.college_id','=','colleges.id')
		->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges.id')
		->leftJoin( 'college_overview_images', 'colleges_admissions.college_id', '=', 'college_overview_images.college_id' )
		->select('colleges_admissions.*', 'colleges.application_url', 'colleges.logo_url', 'colleges_ranking.plexuss as plexuss_ranking', 'college_overview_images.url as overview_image')
		->where('colleges_admissions.college_id', '=',$collegeId)
		->orderBy('school_name', 'asc')
		->first();
		return $result;
	}

	public function CollegesTuition($collegeId=null){
		$result = DB::connection('rds1')->table('colleges_tuition')
										->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges_tuition.college_id')
										->leftJoin( 'colleges', 'colleges.id', '=', 'colleges_tuition.college_id' )
										->leftJoin( 'college_overview_images', 'college_overview_images.college_id', '=', 'colleges_tuition.college_id' )
										->leftjoin('colleges_custom_tuitions as cct', 'cct.college_id', '=', 'colleges.id')

										->select('colleges_tuition.*','colleges_ranking.plexuss as plexuss_ranking', 'colleges.logo_url', 'college_overview_images.url as overview_image', 'cct.id as cct_id')
										->where('colleges_tuition.college_id', '=',$collegeId)
										->orderBy('school_name', 'asc')
										->first();
		return $result;
	}

	public function CollegeOverview($collegeId=null){
		$result = DB::connection('rds1')->table('college_overview')
			->join('colleges', 'colleges.id', '=', 'college_overview.college_id')
			->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'college_overview.college_id')
			->leftJoin( 'college_overview_images', 'college_overview_images.college_id', '=', 'college_overview.college_id' )
			->select('college_overview.content', 'college_overview.page_title', 'college_overview.meta_description', 'college_overview.meta_keywords', 'colleges.*','colleges_ranking.plexuss as plexuss_ranking', 'colleges.logo_url', 'college_overview_images.url as overview_image')
			->where('college_overview.college_id', '=',$collegeId)
			->first();
		return $result;
		/* Was going to use this for comments. Using the DB class for now since
		 * it returns a stdclass object. This returns an eloquent object and
		 * the typecasting and array merge that goes on in
		 * CollegeController->view method breaks the object.
		$result = CollegeOverview::where( 'college_overview.college_id', $collegeId )
			->join('colleges', 'colleges.id', '=', 'college_overview.college_id')
			->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'college_overview.college_id')
			->leftJoin( 'college_overview_images', 'college_overview_images.college_id', '=', 'college_overview.college_id' )
			->select('college_overview.content', 'college_overview.page_title', 'college_overview.meta_description', 'college_overview.meta_keywords', 'colleges.*','colleges_ranking.plexuss as plexuss_ranking', 'colleges.logo_url', 'college_overview_images.url as overview_image', 'college_overview.comment_thread_id')
			->first();
		return $result;
		 */
	}

	public function CollegesFinancial($collegeId=null)
	{
		$result = DB::connection('rds1')->table('colleges_financial_aid')
		->join('colleges_tuition','colleges_tuition.college_id','=','colleges_financial_aid.college_id')

		->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges_financial_aid.college_id')
		->leftJoin( 'colleges', 'colleges_financial_aid.college_id', '=', 'colleges.id' )
		->leftJoin( 'college_overview_images', 'colleges_financial_aid.college_id', '=', 'college_overview_images.college_id' )
		->select('colleges_tuition.*','colleges_financial_aid.*','colleges_ranking.plexuss as plexuss_ranking', 'colleges.logo_url', 'college_overview_images.url as overview_image')

		->where('colleges_financial_aid.college_id', '=',$collegeId)
		->orderBy('colleges_financial_aid.school_name', 'asc')
		->first();

		return $result;
	}

	public function CollegesEnrollment($collegeId=null)
	{
		$result = DB::connection('rds1')->table('colleges_enrollment')
		->join('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges_enrollment.college_id')
		->leftJoin( 'colleges', 'colleges.id', '=', 'colleges_enrollment.college_id' )
		->leftJoin( 'college_overview_images', 'college_overview_images.college_id', '=', 'colleges_enrollment.college_id' )
		->select('colleges_enrollment.*','colleges_ranking.plexuss as plexuss_ranking', 'colleges.logo_url', 'college_overview_images.url as overview_image')
		->where('colleges_enrollment.college_id', '=',$collegeId)
		->orderBy('school_name', 'asc')
		->first();
		return $result;
	}

	public function CollegesAthletics($collegeId=null)
	{
		$result = DB::connection('rds1')->table('colleges_athletics')->select()
		->where('college_id', '=',$collegeId)
		->orderBy('school_name', 'asc')
		->get();
		return $result;
	}

	public function CollegesRanking($collegeId=null){
		$result = DB::connection('rds1')->table('colleges_ranking')
			->leftJoin( 'colleges', 'colleges.id', '=', 'colleges_ranking.college_id')
			->leftJoin( 'college_overview_images', 'college_overview_images.college_id', '=', 'colleges_ranking.college_id' )
			->leftJoin( 'aor_colleges as ac', 'ac.college_id', '=', 'colleges_ranking.college_id' )
			->select( 'colleges_ranking.*', 'colleges.logo_url', 'college_overview_images.url as overview_image', 'ac.aor_id' )
			->where( 'colleges_ranking.college_id', '=', $collegeId )
			->first();
		return $result;
	}

	public function getLists(){
		$result = DB::connection('rds1')->table('lists')->select()
		->get();
		return $result;
	}

	public function getCollegeLists()
	{
		$qwerty=DB::connection('rds1')->table('lists')
		->select()
		->orderBy('lists.id', 'asc')
		->where('type','=','interesting')
		->get();

		return $qwerty;
	}
	public function getInterestingList()
	{
		$qwerty=DB::connection('rds1')->table('lists')
		->select()
		->orderBy('lists.id', 'asc')
		->where('type','=','interesting')
		->get();
		$ResultData=array();
		foreach($qwerty as $key=>$data)
		{
			$dataList=College::getCollegeListSchools($data->id);
			$ResultData["list"][]=$data;
			$ResultData["listing"][]=$dataList;
		}
		return $ResultData;
	}
	public function getConferenceLists()
	{
		/*$query = DB::connection('rds1')->table('lists')
		->select('*')
		->where('type','=','conference')
		->orderBy('lists.id', 'asc')
		->get();
		return $query;*/
		$qwerty=DB::connection('rds1')->table('lists')
		->select()
		->orderBy('lists.id', 'asc')
		->where('type','=','conference')
		->get();
		$ResultData=array();
		foreach($qwerty as $key=>$data)
		{
			$dataList=College::getCollegeListSchools($data->id);
			$ResultData["list"][]=$data;
			$ResultData["listing"][]=$dataList;
		}
		return $ResultData;
	}


	public function getCollegeListSchools($listId = false){
		$qwerty = DB::connection('rds1')->table('list_schools')
		->select('list_schools.*','lists.id','colleges.id','colleges.school_name as schoolName','colleges.city','colleges.long_state','lists.title','colleges_ranking.plexuss as plexuss_rank','colleges.slug as collegeURI')
		->join('lists','lists.id','=','list_schools.lists_id')
		->join('colleges','colleges.id','=','list_schools.colleges_id')
		->leftjoin('colleges_ranking','colleges_ranking.college_id','=','list_schools.colleges_id')
		->where('lists.id','=',$listId)
		->orderBy('lists.id', 'asc')
		->get();
		return $qwerty;
	}

	public function get_stats($id){
		$stats = DB::connection('rds1')->table('colleges')
			->select(
				'colleges.id',
				'colleges.school_name',
				'colleges.address',
				'colleges.general_phone',
				'colleges.logo_url',
				'colleges.slug',
				'colleges.page_title',
				'colleges.meta_keywords',
				'colleges.meta_description',
				'colleges_admissions.deadline',
				'colleges_admissions.percent_admitted',
				'colleges.student_body_total',
				'colleges.undergrad_enroll_1112',
				'colleges_tuition.tuition_avg_in_state_ftug',
				'colleges_tuition.tuition_avg_out_state_ftug',
				'colleges.graduation_rate_4_year',
				'colleges.school_sector',
				'colleges.locale',
				'colleges.campus_housing',
				'colleges.religious_affiliation',
				'colleges.calendar_system',
				'colleges.school_url',
				'colleges.admission_url',
				'colleges.financial_aid_url',
				'colleges.application_url',
				'colleges.calculator_url',
				'colleges.mission_url',
				'colleges_admissions.sat_read_25',
				'colleges_admissions.sat_write_25',
				'colleges_admissions.sat_math_25',
				'colleges_admissions.sat_read_75',
				'colleges_admissions.sat_write_75',
				'colleges_admissions.sat_math_75',
				'colleges_admissions.sat_percent',
				'colleges_admissions.act_composite_75',
				'colleges_admissions.act_composite_25',
				'colleges_admissions.act_percent',
				'colleges.control_of_school',
				'colleges.public_endowment_end_fy_12',
				'colleges.private_endowment_end_fy_12',
				'colleges.student_faculty_ratio',
				'colleges.accred_agency',
				'colleges.accred_period',
				'colleges.accred_status',
				'colleges.bachelors_degree',
				'colleges.masters_degree',
				'colleges.post_masters_degree',
				'colleges.doctors_degree_research',
				'colleges.doctors_degree_professional',
				'colleges.rotc_army',
				'colleges.rotc_navy',
				'colleges.rotc_air',
				'who_qa',
				'who_live'
			)
			->leftJoin('colleges_admissions', 'colleges_admissions.college_id', '=', 'colleges.id')
			->leftJoin('colleges_tuition','colleges_tuition.college_id', '=', 'colleges.id')
			->where('colleges.id', '=', $id)
			->get();
		return $stats;
	}

	public function findColleges($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.id', 'c.school_name', 'c.logo_url', 'c.city', 'c.state')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
									->orWhere('c.alias', 'like', '%'.$input.'%');
					})
					->where('verified', 1)
					->limit(10)
					->get();

		return $result;
	}

	/**
	 * findCollegesForThisUser
	 * This method returns the colleges that this user doesn't have
	 * in their recruitment or prescreened
	 *
	 * @param(input)
	 * @param(user_id)
	 * @return obj
	 */
	public function findCollegesForThisUser($input = null, $user_id){
		if( !isset($input) ){
			return 'no results';
		}

		$input = trim($input);

		if (strpos($input, ' ') !== FALSE){
			$input = explode(" ", $input);
		}

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.id', 'c.school_name', 'r.id as rec_id', 'pu.id as pr_id')
					->where(function($qry) use ($input){
						if (is_array($input)) {
							foreach ($input as $key => $val) {
								$qry = $qry->orWhere('c.school_name', 'like', '%'.$val.'%')
									->orWhere('c.alias', 'like', '%'.$val.'%');
							}
						}else{
							$qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
									->orWhere('c.alias', 'like', '%'.$input.'%');
						}
					})
					->leftjoin('recruitment as r', function($qry) use ($user_id){
							    $qry->on('r.college_id', '=', 'c.id');
							    $qry->on('r.user_id', '=', DB::raw($user_id));
								$qry->on('r.user_applied', '=', DB::raw(0));
					})
					->leftjoin('prescreened_users as pu', function($qry) use ($user_id){
								$qry->on('pu.college_id', '=', 'c.id');
							    $qry->on('pu.user_id', '=', DB::raw($user_id));
							    $qry->on('pu.user_applied', '=', DB::raw(0));
					})
					->where('c.verified', 1)
					->limit(10)
					->get();

		$ret = array();
		foreach ($result as $key) {
			if (isset($key->pr_id)) {
				$key->pr_id = Crypt::encrypt($key->pr_id);
			}
			if (isset($key->rec_id)) {
				$key->rec_id = Crypt::encrypt($key->rec_id);
			}
			$ret[] = $key;
		}

		return $ret;
	}


	public function searchCollegesForSales($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.id', 'c.school_name', 'c.city', 'c.state',
							DB::raw('CASE WHEN `c`.`logo_url` IS NULL
									THEN "https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/default-missing-college-logo.png"
									ELSE CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/", c.logo_url) END as logo_url'))
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
									->orWhere('c.alias', 'like', '%'.$input.'%');
					})
					->leftJoin('colleges_ranking as cr', 'cr.college_id', '=', 'c.id')
					->orderby(DB::raw('`cr`.plexuss IS NULL,`cr`.`plexuss`'))
					
					->where('verified', 1)
					->limit(100)
					->get();

		return $result;
	}

	public function getCollegeName( $school_id = null ){
		if( !isset($school_id) ){
			return null;
		}

		$result = College::on('rds1')
						->where('id', '=', $school_id)
						->select('school_name')
						->first();

		return $result;
	}

	public function getCollegesAttended($id = null){
		if( !isset($id) ){
			return 'error - no id passed';
		}

		$schools_attended = DB::connection('rds1')->table( 'educations' )
				->select(
					'educations.user_id',
					'educations.school_id as id',
					'educations.school_type',
					'colleges.school_name as name',
					'colleges.slug',
					'colleges.city',
					'colleges.state'
				)
				->join( 'colleges', 'educations.school_id', '=', 'colleges.id' )
				->where( 'educations.school_type', 'college' )
				->where( 'user_id', $id )
				->get();

		foreach ($schools_attended as $key) {
			$key->id = (int)$key->id;
		}

		return $schools_attended;
	}

	public function findCollegesCustom($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.id', 'c.school_name as name')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
									->orWhere('c.alias', 'like', '%'.$input.'%');
					})
					->where('verified', 1)
					->orderBy('verified')
					->limit(10)
					->get();

		return $result;
	}

	public function findCollegesWithType($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.id', 'c.school_name as name')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
									->orWhere('c.alias', 'like', '%'.$input.'%');
					})
					->where('verified', 1)
					->limit(10)
					->get();

		foreach ($result as $key) {
			$key->school_type = 'college';
		}

		return $result;
	}

	public function findCollegesCustomVerifiedNotVerified($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.id', 'c.school_name as name')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
									->orWhere('c.alias', 'like', '%'.$input.'%');
					})
					->orderBy('verified', 'DESC')
					->limit(10)
					->get();

		foreach ($result as $key) {
			$key->school_type = 'college';
		}

		return $result;
	}

	/*
     * Chat bot method
     * this method return the result for what user wants to know about this college
     */
	public function getChatBotCollegeStats($college_name, $tab){

		$tab = strtolower($tab);

		switch ($tab) {
			case 'ranking':
				$rank = DB::connection('rds1')->table('colleges as c')
											  ->join('colleges_ranking as cr', 'c.id', '=', 'cr.college_id')
											  ->where('school_name', $college_name)
											  ->select('c.id as college_id', 'cr.*')
											  ->first();
				$msg = "Sorry :(, I couldn't find any ranking for ". $college_name;

				if (isset($rank)) {

					if (isset($rank->plexuss)) {
						$msg = $college_name." overall ranking on Plexuss is #".$rank->plexuss;
					}

					$check = false;
					if (isset($rank->us_news)) {
						if ($check === false) {
							$msg .= "  This is reflected by ranking from US News #".$rank->us_news;
							$check = true;
						}else{
							$msg .= " ,US News #".$rank->us_news;
						}
					}
					if (isset($rank->reuters)) {
						if ($check === false) {
							$msg .= "  This is reflected by ranking from Reuters #".$rank->reuters;
							$check = true;
						}else{
							$msg .= " ,Reuters #".$rank->reuters;
						}
					}
					if (isset($rank->forbes)) {
						if ($check === false) {
							$msg .= "  This is reflected by ranking from Forbes #".$rank->forbes;
							$check = true;
						}else{
							$msg .= " ,Forbes #".$rank->forbes;
						}
					}
					if (isset($rank->qs)) {
						if ($check === false) {
							$msg .= "  This is reflected by ranking from QS #".$rank->qs;
							$check = true;
						}else{
							$msg .= " ,QS #".$rank->qs;
						}
					}

					if (isset($rank->shanghai_academic)) {
						if ($check === false) {
							$msg .= "  .This is reflected by ranking from Shanghahi #".$rank->shanghai_academic .". ";
							$check = true;
						}else{
							$msg .= " ,and Shanghahi #".$rank->shanghai_academic;
						}
					}

					$cnt = DB::connection('rds1')->table('lists')->where('custom_college', $rank->college_id)->count();

					if (isset($cnt) && $cnt > 0 ) {
						$msg .= ".<br/><br/>There are ".$cnt." other rankings for ".$college_name.".  Would like to see them all.";
					}
				}
				break;

			case 'admission':
				$college = College::on('rds1')->where('school_name', $college_name)->first();

				$admissions = $this->CollegesAdmission($college->id);

				$msg = $msg = "Sorry :(, I couldn't find any admission info for ". $college_name;

				if (isset($admissions)) {
					$msg = "Here are some more admission info for this college: <br/><br/>";

					isset($admissions->deadline) ? $msg .= "Application Deadline: ". $admissions->deadline ."<br/>" : null;
					isset($admissions->applicants_total) ? $msg .= "# OF Applicants: ". number_format($admissions->applicants_total) ."<br/>" : null;
					isset($admissions->admissions_total) ? $msg .= "# Admitted: ". $admissions->admissions_total ."<br/>" : null;
					if (isset($admissions->applicants_total) && isset($admissions->admissions_total) && $admissions->applicants_total != 0
						&& $admissions->admissions_total != 0) {
						$msg .= "% Admitted: ". round(($admissions->admissions_total/$admissions->applicants_total) * 100) . "%<br/>";
					}
					isset($admissions->enrolled_total) ? $msg .= "# Admitted & Enrolled: ". $admissions->enrolled_total ."<br/>" : null;
					isset($admissions->open_admissions) ? $msg .= "Open Admissions: ". $admissions->open_admissions ."<br/>" : null;
					isset($admissions->common_app) ? $msg .= "Common Application: Yes" ."<br/>" : $msg .= "COMMON APPLICATION: No" ."<br/>" ;
					isset($admissions->application_fee_undergrad) ? $msg .= "Application Fee: $". $admissions->application_fee_undergrad ."<br/>" : null;
					isset($admissions->application_url) ? $msg .= "<a target='_blank' href ='". $admissions->application_url ."'>Application Link</a><br/>" : null;
					isset($admissions->admissions_men) ? $msg .= "Number of Male Admitted: ". number_format($admissions->admissions_men) ."<br/>" : null;
					isset($admissions->admissions_women) ? $msg .= "Number of Female Admitted: ". number_format($admissions->admissions_women) ."<br/>" : null;

					$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";
				}
				break;

			case 'tuition':
				$college = College::on('rds1')->where('school_name', $college_name)->first();

				$tuition = $this->CollegesTuition($college->id);

				$msg = $msg = "Sorry :(, I couldn't find any tuition info for ". $college_name;

				if (isset($tuition)) {
					$msg = "Here are some more tuition info for this college: <br/><br/>";

					isset($tuition->tuition_avg_in_state_ftug) ? $msg .= "In State Tuition: $". number_format($tuition->tuition_avg_in_state_ftug) ."<br/>" : null;
					(isset($tuition->tuition_avg_in_state_ftug) && isset( $tuition->books_supplies_1213) && isset( $tuition->room_board_on_campus_1213) && isset( $tuition->other_expenses_on_campus_1213)) ? $msg .= "In State Full Expense: $". number_format(round($tuition->tuition_avg_in_state_ftug + $tuition->books_supplies_1213 + $tuition->room_board_on_campus_1213 + $tuition->other_expenses_on_campus_1213)) ."<br/>" : null;
					isset($tuition->tuition_avg_out_state_ftug) ? $msg .= "Out of State Tuition: $". number_format($tuition->tuition_avg_out_state_ftug) ."<br/>" : null;
					(isset($tuition->tuition_avg_out_state_ftug) && isset( $tuition->books_supplies_1213) && isset( $tuition->room_board_on_campus_1213) && isset( $tuition->other_expenses_on_campus_1213)) ? $msg .= "Out of State Full Expense: $". number_format(round($tuition->tuition_avg_out_state_ftug + $tuition->books_supplies_1213 + $tuition->room_board_on_campus_1213 + $tuition->other_expenses_on_campus_1213)) ."<br/>" : null;

					$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";
				}

				break;

			case 'enrollment':
				$college = College::on('rds1')->where('school_name', $college_name)->first();

				$enrollment = $this->CollegesEnrollment($college->id);

				$msg = $msg = "Sorry :(, I couldn't find any enrollment info for ". $college_name;

				if (isset($enrollment)) {
					$msg = "Here are some more enrollment info for this college: <br/><br/>";

					(isset($enrollment->undergrad_full_time_total) && isset($enrollment->undergrad_part_time_total)) ? $msg .= "Total Enrollment: ". number_format(round($enrollment->undergrad_full_time_total + $enrollment->undergrad_part_time_total)) ."<br/>" : null;
					isset($enrollment->undergrad_transfers_total) ? $msg .= "Transfer Enrollment: ". number_format($enrollment->undergrad_transfers_total) ."<br/>" : null;
					isset($enrollment->undergrad_full_time_total) ? $msg .= "Attendance Status: ". number_format($enrollment->undergrad_full_time_total) ."<br/>" : null;
					isset($enrollment->undergrad_men_total) ? $msg .= "Number of undergrad male enrolled: ". number_format($enrollment->undergrad_men_total) ."<br/>" : null;
					isset($enrollment->undergrad_women_total) ? $msg .= "Number of undergrad female enrolled: ". number_format($enrollment->undergrad_women_total) ."<br/>" : null;

					$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";
				}


				break;

			case 'financial':
				$college = College::on('rds1')->where('school_name', $college_name)->first();

				$financial = $this->CollegesFinancial($college->id);

				$msg = $msg = "Sorry :(, I couldn't find any financial info for ". $college_name;

				if (isset($financial)) {
					$msg = "Here are some more financial aid info for this college: <br/><br/>";
					$msg .= "<b>GRANT OR SCHOLARSHIP AID</b><br/>";

					isset($financial->undergrad_grant_pct) ? $msg .= "Students who received aid: ". number_format($financial->undergrad_grant_pct) ."%<br/>" : null;
					isset($financial->undergrad_grant_avg_amt) ? $msg .= "Avg. Financial aid given: $". number_format($financial->undergrad_grant_avg_amt) ."<br/>" : null;
					$msg .= "<br/><b>FEDERAL STUDENT LOANS</b><br/>";

					isset($financial->undergrad_loan_pct) ? $msg .= "Students who received aid: ". number_format($financial->undergrad_loan_pct) ."%<br/>" : null;

					isset($financial->undergrad_loan_avg_amt) ? $msg .= "Avg. Financial aid given: $". number_format($financial->undergrad_loan_avg_amt) ."<br/>" : null;

					$msg .= "<br/><b>Out of State Tuition</b><br/>";

					$financial->undergrad_aid_avg_amt = round(($financial->undergrad_grant_avg_amt + $financial->undergrad_loan_avg_amt) / 2);

					(isset($financial->undergrad_loan_avg_amt) && isset($financial->undergrad_loan_avg_amt)) ? $msg .= "Avg. Financial aid given: $". number_format($financial->undergrad_aid_avg_amt + $financial->undergrad_loan_avg_amt ) ."<br/>" : null;

					$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";
				}
				break;

			case 'stats':
				$college = College::on('rds1')->where('school_name', $college_name)->first();

				$stats = $this->CollegeData($college->id);

				$msg = $msg = "Sorry :(, I couldn't find any stats info for ". $college_name;

				if (isset($stats)) {
					$msg = "Here are some more stats info for this college: <br/><br/>";

					isset($stats->deadline) ? $msg .= "Admission Deadline: ". $stats->deadline ."<br/>" : null;
					isset($stats->acceptance_rate) ? $msg .= "Acceptance Rate: ". $stats->acceptance_rate ."%<br/>" : null;
					isset($stats->student_body_total) ? $msg .= "Total Student Body Size: ". $stats->student_body_total ."<br/>" : null;
					isset($stats->undergrad_enroll_1112) ? $msg .= "Undergrad Student Body Size: ". $stats->undergrad_enroll_1112 ."<br/>" : null;
					isset($stats->graduation_rate_4_year) ? $msg .= "Graduation Rate: ". $stats->graduation_rate_4_year ."%<br/>" : null;
					isset($stats->average_freshman_gpa) ? $msg .= "Average Incoming Freshmen GPA: ". $stats->average_freshman_gpa ."<br/>" : null;

					$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";
				}
				break;

			case 'history':
				$college = College::on('rds1')->where('school_name', $college_name)->first();

				$msg = $msg = "Sorry :(, I couldn't find any stats info for ". $college_name;

				if (isset($college)) {
					$msg = "To read the history of ".$college_name. " ,Please click <a target='_blank' href='".env('CURRENT_URL')."college/".$college->slug."'>here</a><br>";

					$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";
				}
				break;
			default:
				# code...
				break;
		}

		return $msg;
	}

	/*
     * Chat bot method
     * this method return the list of other ranking for this particular college
     */
	public function getChatBotCollegeOtherRankings($college_name, $tab){

		$qry = DB::connection('rds1')->table('lists as l')
									 ->join('colleges as c', 'c.id', '=', 'l.custom_college')
									 ->where('c.school_name', $college_name)
									 ->select('l.*')
									 ->get();

		$msg = "Sorry :(, I couldn't find any ranking for ". $college_name;
		if (isset($qry)) {
			$msg = "Here are some more ranking for this college: <br/><br/>";
			foreach ($qry as $key) {
				$msg .= $key->title . " #". $key->rank_num. "<br />";
			}
		}

		$msg .= "<br/><br/>Would you like any other information about ".$college_name.", such as  <b>history</b>, <b>admission information</b>, <b>ranking</b>, <b>financial aid</b>, <b>stats</b>, <b>tuition</b> or <b>enrollment</b>.  Or you can ask about any other university. If you are done, just say (I'm done) <br/>";

		return $msg;
	}


	public function getDepartmentCats(){

		$result = DB::connection('rds1')->table('department_categories as dc')
					->select('dc.id', 'dc.name', 'dc.url_slug as slug')
					->orderBy('dc.name', 'asc')
					->get();

		return $result;
	}



	public function getAllColleges(){

		$result = DB::connection('rds1')->table('colleges as c')
					->select('c.school_name','c.logo_url')
					->get();

		return $result;
	}

	public function AlumniStudent($collegeId = null, $is_api = NULL){
	    $result = DB::connection('rds1')
	                ->table('users as u')
	                // ->join('colleges as c', 'u.current_school_id', '=', 'c.id')
	              	->join('sn_users_educations as sue', function($qry){
		            		$qry->on('u.id', '=', 'sue.user_id')
		            			->on('sue.edu_level', '=', DB::raw(1));
		            })
		            ->leftjoin('sn_users_education_majors as suem', 'suem.sue_id', '=', 'sue.id')
	                ->join('colleges as c',  'c.id', '=', 'sue.school_id')
		            ->join('countries as country', 'country.id', '=', 'u.country_id')

	                ->leftjoin('users_ip_locations as uil', function($q){
		            	$q->on('uil.user_id', '=', 'u.id')
		            	  ->on('uil.ip_country_id', '=', 'c.country_id');

		            })
		            // ->join('sn_users_educations as sue', 'u.id', '=', 'sue.user_id')


		            // ->leftjoin('plexuss_logging.unique_logged_in_users_per_days as uliupd', function($q){
		            // 	$q->on('uliupd.user_id', '=', 'u.id')
		            // 	  ->on('uliupd.countryName', '=', 'country.country_name');

		            // })
		            // ->leftjoin('high_schools as hs', 'hs.id', '=', 'u.current_school_id')
		            // ->leftjoin('objectives as ob', 'ob.user_id', '=', 'u.id')
		            ->leftjoin('majors as mj', 'suem.major_id', '=', 'mj.id')
		            ->leftjoin('sn_users_account_settings as snas', 'snas.user_id', '=', 'u.id')
		            ->leftjoin('college_message_thread_members as cmtm',  'cmtm.user_id', '=', 'u.id')

	                ->select( 'c.school_name','c.slug', 'c.id as college_id', 'c.state', 
	                			"c.school_name as student_school",
	                		  'u.id as user_id', 
	                		DB::raw("CONCAT(UCASE(SUBSTRING(`u`.`fname`, 1, 1)),LOWER(SUBSTRING(`u`.`fname`, 2))) as fname"),
	                        DB::raw("CONCAT(UCASE(SUBSTRING(`u`.`lname`, 1, 1)),LOWER(SUBSTRING(`u`.`lname`, 2))) as lname"), 
	                        'email','u.profile_img_loc as student_profile_photo','country.country_name', 
	                        'country.country_code', 'u.created_at as date_joined', 
	                        DB::raw('IF(LENGTH(u.profile_img_loc), CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/users/images/", u.profile_img_loc) ,"https://s3-us-west-2.amazonaws.com/asset.plexuss.com/users/images/default.png") as student_profile_photo'),
	                 	 // DB::raw("IF(u.in_college =0,  u.hs_grad_year, u.college_grad_year) as grad_year"),
	                 	 // DB::raw("IF(u.in_college =0, hs.school_name , student_college.school_name) as student_school"),
						'u.is_student',
						'u.is_alumni',
						'u.is_parent',
						'u.is_counselor',
						'u.is_organization', 'mj.name as major_name', 'sue.grad_year',
						DB::raw("IF(SUM(cmtm.num_of_sent_msg) is null ,0,SUM(cmtm.num_of_sent_msg)) as sum_sent_msg"))

	                // don't include incognito users
					->where(function ($qry) {
						$qry->where('snas.is_incognito', '=', 0)
						  ->orWhereNull('snas.is_incognito');
						})
					->where(function ($qry) {
						$qry->where('snas.appear_in_suggestions', '=', 1)
						  ->orWhereNull('snas.appear_in_suggestions');
						})

					->where('sue.is_verified', 1)
	                ->where( 'c.id', '=', $collegeId )
	                // ->where( 'in_college', '=', 1 )
	                // ->where( 'is_plexuss', '=', 0 )
	                // ->where( 'is_counselor', '=', 0 )
	                ->where( 'is_aor', '=', 0 )
	                ->where( 'is_ldy', '=', 0 )
	                // ->where( 'is_agency', '=', 0 )
	                // ->where( 'is_university_rep', '=', 0 )
	                // ->where( 'is_organization', '=', 0 )
	                ->orderBy(DB::raw('ISNULL(profile_img_loc), profile_img_loc'), 'ASC')
	                ->where('email', 'NOT LIKE', '%test%' )
	                // ->where('email', 'NOT LIKE', '%plexuss%' )
	                ->where( 'is_alumni', '=', 1 )
	                ->groupBy( 'u.id' )
		            ->orderBy(DB::raw('u.is_plexuss = 1'), ' DESC')
		            ->orderby('sum_sent_msg', 'DESC')
		            ->orderBy(DB::raw('ISNULL(profile_img_loc), profile_img_loc'), 'ASC');

        if (isset($is_api)) {
        	$result = $result->get();
        }else{
        	$result = $result->paginate(6);
        }        

        return $result;

    }

    public function CurrentStudent($collegeId = null, $is_api  = NULL){
        $result = DB::connection('rds1')
		            ->table('users as  u')
		            ->join('colleges as c', 'u.current_school_id', '=', 'c.id')
		            ->join('countries as country', 'country.id', '=', 'u.country_id')
		            ->leftjoin('users_ip_locations as uil', function($q){
		            	$q->on('uil.user_id', '=', 'u.id')
		            	  ->on('uil.ip_country_id', '=', 'c.country_id');

		            })
		            ->join('sn_users_educations as sue', 'u.id', '=', 'sue.user_id')
		            
		            // ->leftjoin('plexuss_logging.unique_logged_in_users_per_days as uliupd', function($q){
		            // 	$q->on('uliupd.user_id', '=', 'u.id')
		            // 	  ->on('uliupd.countryName', '=', 'country.country_name');

		            // })
		            ->leftjoin('high_schools as hs', 'hs.id', '=', 'u.current_school_id')
		            ->leftjoin('colleges as student_college',  'student_college.id', '=', 'u.current_school_id')
		            ->leftjoin('objectives as ob', 'ob.user_id', '=', 'u.id')
		            ->leftjoin('majors as mj', 'ob.major_id', '=', 'mj.id')
		            ->leftjoin('sn_users_account_settings as snas', 'snas.user_id', '=', 'u.id')
		            ->leftjoin('college_message_thread_members as cmtm',  'cmtm.user_id', '=', 'u.id')

		            ->select( 'c.school_name', 'c.slug', 'c.id as college_id', 'c.state', 'u.id as user_id',
		            		 DB::raw("CONCAT(UCASE(SUBSTRING(`u`.`fname`, 1, 1)),LOWER(SUBSTRING(`u`.`fname`, 2))) as fname"),
		                     DB::raw("CONCAT(UCASE(SUBSTRING(`u`.`lname`, 1, 1)),LOWER(SUBSTRING(`u`.`lname`, 2))) as lname"), 
		                     'email','country.country_name', 'c.country_id as college_country_id', 'country.country_code', 
		                     'u.created_at as date_joined', 
		                 	 DB::raw('IF(LENGTH(u.profile_img_loc), CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/users/images/", u.profile_img_loc) ,"https://s3-us-west-2.amazonaws.com/asset.plexuss.com/users/images/default.png") as student_profile_photo'),
		                 	 DB::raw("IF(u.in_college =0,  u.hs_grad_year, u.college_grad_year) as grad_year"),
		                 	 DB::raw("IF(u.in_college =0, hs.school_name , student_college.school_name) as student_school"),
							'u.is_student',
							'u.is_alumni',
							'u.is_parent',
							'u.is_counselor',
							'u.is_organization', 'mj.name as major_name',
							DB::raw("IF(SUM(cmtm.num_of_sent_msg) is null ,0,SUM(cmtm.num_of_sent_msg)) as sum_sent_msg"))

		            // don't include incognito users
					->where(function ($qry) {
					      $qry->where('snas.is_incognito', '=', 0)
					          ->orWhereNull('snas.is_incognito');
					      })
					->where(function ($qry) {
					      $qry->where('snas.appear_in_suggestions', '=', 1)
					          ->orWhereNull('snas.appear_in_suggestions');
					      })

					->where('sue.is_verified', 1)
		            ->where( 'c.id', '=', $collegeId )
		            ->where( 'in_college', 1 )
		            ->where( 'is_student', 1 )
		            // ->where( 'is_plexuss', '=', 0 )
		            // ->where( 'is_counselor', '=', 0 )
		            // ->where( 'is_aor', '=', 0 )
		            ->where( 'is_ldy', '=', 0 )
		            // ->where( 'is_agency', '=', 0 )
		            // ->where( 'is_university_rep', '=', 0 )
		            // ->where( 'is_organization', '=', 0 )
		            ->where('email', 'NOT LIKE', '%test%' )
		            // ->where('email', 'NOT LIKE', '%plexuss%' )
		            ->where( 'u.is_alumni', '=', 0 )
		            ->groupBy( 'u.id' )
		            ->orderBy(DB::raw('u.is_plexuss = 1'), ' DESC')
		            ->orderby('sum_sent_msg', 'DESC')
		            ->orderBy(DB::raw('ISNULL(profile_img_loc), profile_img_loc'), 'ASC');


            if (isset($is_api)) {
            	$result = $result->get();
            }else{
            	$result = $result->paginate(6);
            }
            
        return $result;
    }

    public function currentStudentAjaxData($skipAmount = 0, $collegeId=NULL){
        $result = DB::connection('rds1')
            ->table('users as  u')
            ->join('colleges as c', 'u.current_school_id', '=', 'c.id')
            ->leftJoin('countries as country', 'country.id', '=', 'c.country_id')
            ->select( 'school_name', 'c.slug', 'c.id as college_id', 'c.state', 'u.id as user_id', 'fname', 'lname', 'email','u.profile_img_loc as student_profile_photo','country.country_name')
            ->where( 'c.id', '=', $collegeId )
            ->where( 'in_college', '=', 1 )
            ->where( 'is_plexuss', '=', 0 )
            ->where( 'is_counselor', '=', 0 )
            ->where( 'is_aor', '=', 0 )
            ->where( 'is_ldy', '=', 0 )
            ->where( 'is_agency', '=', 0 )
            ->where( 'is_university_rep', '=', 0 )
            ->where( 'is_organization', '=', 0 )
            ->orderBy(DB::raw('ISNULL(profile_img_loc), profile_img_loc'), 'ASC')
            ->where('email', 'NOT LIKE', '%test%' )
            ->where('email', 'NOT LIKE', '%plexuss%' )
            ->where( 'is_alumni', '=', 0 )
            ->skip($skipAmount) // Skip for pagination
            ->take(6)
            ->get();

        return $result;
    }

    public function alumniAjaxData($skipAmount = 0, $collegeId=NULL){
        $result = DB::connection('rds1')
            ->table('users as  u')
            ->join('colleges as c', 'u.current_school_id', '=', 'c.id')
            ->leftJoin('countries as country', 'country.id', '=', 'c.country_id')
            ->select( 'school_name','c.slug', 'c.id as college_id', 'c.state', 'u.id as user_id', 'fname', 'lname', 'email','u.profile_img_loc as student_profile_photo','country.country_name')
            ->where( 'c.id', '=', $collegeId )
            ->where( 'in_college', '=', 1 )
            ->where( 'is_plexuss', '=', 0 )
            ->where( 'is_counselor', '=', 0 )
            ->where( 'is_aor', '=', 0 )
            ->where( 'is_ldy', '=', 0 )
            ->where( 'is_agency', '=', 0 )
            ->where( 'is_university_rep', '=', 0 )
            ->where( 'is_organization', '=', 0 )
            ->orderBy(DB::raw('ISNULL(profile_img_loc), profile_img_loc'), 'ASC')
            ->where('email', 'NOT LIKE', '%test%' )
            ->where('email', 'NOT LIKE', '%plexuss%' )
            ->where( 'is_alumni', '=', 1 )
            ->skip($skipAmount) // Skip for pagination
            ->take(6)
            ->get();

        return $result;

    }

    public function countCurrentStudent($collegeId=NULL){
        $result = DB::connection('rds1')
            ->table('users as  u')
            ->join('colleges as c', 'u.current_school_id', '=', 'c.id')
            ->leftJoin('countries as country', 'country.id', '=', 'c.country_id')
            ->select( 'school_name', 'c.slug', 'c.id as college_id', 'c.address','c.state', 'u.id as user_id', 'fname', 'lname', 'email','u.profile_img_loc as student_profile_photo','country.country_name')
            ->where( 'c.id', '=', $collegeId )
            ->where( 'c.verified', '=', 1 )
            ->where( 'in_college', '=', 1 )
            ->where( 'is_plexuss', '=', 0 )
            ->where( 'is_counselor', '=', 0 )
            ->where( 'is_aor', '=', 0 )
            ->where( 'is_ldy', '=', 0 )
            ->where( 'is_agency', '=', 0 )
            ->where( 'is_university_rep', '=', 0 )
            ->where( 'is_organization', '=', 0 )
            ->where('email', 'NOT LIKE', '%test%' )
            ->where('email', 'NOT LIKE', '%plexuss%' )
            ->where( 'is_alumni', '=', 0 )
            ->count();

        return $result;
    }

    public function countAlumni($collegeId=NULL){
        $result = DB::connection('rds1')
            ->table('users as  u')
            ->join('colleges as c', 'u.current_school_id', '=', 'c.id')
            ->leftJoin('countries as country', 'country.id', '=', 'c.country_id')
            ->select( 'school_name', 'c.slug', 'c.id as college_id', 'c.address','c.state', 'u.id as user_id', 'fname', 'lname', 'email','u.profile_img_loc as student_profile_photo','country.country_name')
            ->where( 'c.id', '=', $collegeId )
            ->where( 'c.verified', '=', 1 )
            ->where( 'in_college', '=', 1 )
            ->where( 'is_plexuss', '=', 0 )
            ->where( 'is_counselor', '=', 0 )
            ->where( 'is_aor', '=', 0 )
            ->where( 'is_ldy', '=', 0 )
            ->where( 'is_agency', '=', 0 )
            ->where( 'is_university_rep', '=', 0 )
            ->where( 'is_organization', '=', 0 )
            ->where('email', 'NOT LIKE', '%test%' )
            ->where('email', 'NOT LIKE', '%plexuss%' )
            ->where( 'is_alumni', '=', 1 )
            ->count();

        return $result;
    }
}
