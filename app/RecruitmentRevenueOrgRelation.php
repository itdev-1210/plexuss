<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentRevenueOrgRelation extends Model
{
    protected $table = 'recruitment_revenue_org_relations';
 	protected $fillable = array( 'rec_id', 'related_id', 'ro_id' );
}
