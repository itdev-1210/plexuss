<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateGrouping extends Model
{
    protected $table = 'email_template_grouping';

 	protected $fillable = array('email_description', 'template_id', 'category_id', 'etsp_id');
}
