<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesInternationalAdditionalNote extends Model
{
    protected $table = 'colleges_international_additional_notes';
	protected $fillable = array('cit_id', 'college_id', 'title', 'description', 'view_type', 'attachment_url', 'content');
}
