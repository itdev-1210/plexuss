<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AjaxToken extends Model
{
    protected $table = 'ajax_tokens';

	protected $fillable = array( 'token' );


	public function user() {
		return $this->belongsTo( 'User' );
	}
}
