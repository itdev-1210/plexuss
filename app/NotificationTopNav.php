<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\CollegeMessageController, App\Http\Controllers\UserMessageController, App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use DB;
use Carbon\Carbon;

class NotificationTopNav extends Model {


	protected $table = 'notification_topnavs';
	protected $fillable = array( 'type', 'type_id', 'submited_id', 'name',
								 'msg', 'link', 'img', 'command', 'is_read',
								 'email_sent', 'created_at', 'updated_at', 'icon_img_route' );


	/**
	 * get my notifications
	 * @param $data
	 * @return Eloquent object
	 */
	public function getMyNotifications($data, $limitBool = null, $is_api = null){

		$user_id = $data['user_id'];
		$org_school_id = $data['org_school_id'];

		if(isset($data['is_organization']) && $data['is_organization'] == 1){

			$ntn = NotificationTopNav::on('rds1')->orWhere(function ($query) use ($user_id) {
					    $query->where('type_id', $user_id)
					          ->where('type', 'user');
					})->orWhere(function ($query) use ($org_school_id)  {
					    $query->where('type', 'college')
					          ->where('type_id', $org_school_id);
					});

		}elseif (isset($data['agency_collection'])) {
			$agency = $data['agency_collection'];
			$agency_id = $agency->agency_id;

			$ntn = NotificationTopNav::on('rds1')->orWhere(function ($query) use ($user_id) {
					    $query->where('type_id', $user_id)
					          ->where('type', 'user');
					})->orWhere(function ($query) use ($agency_id)  {
					    $query->where('type', 'agency')
					          ->where('type_id', $agency_id);
					});
		}else{
			$ntn = NotificationTopNav::on('rds1')->where('type_id', $data['user_id'])
									->where('type', 'user');
		}

		if($limitBool){
			$ntn = $ntn->take(5);
		} else {
            $ntn = $ntn->limit(100);
        }

        if (isset($is_api)) {
        	$ntn = $ntn->orderBy('created_at', 'DESC')->paginate(10);
        }else{
        	$ntn = $ntn->orderBy('created_at', 'DESC')->get();
        }


		//print_r($ntn);
		//exit();

		$notifications = array();
		$notifications['data'] = array();

		$unread_cnt = 0;
		$cr = new Controller;
		foreach ($ntn as $key) {

			$tmp = array();
			$tmp['id'] = $cr->hashIdForSocial($key->id);
			$tmp['name'] = $key->name;
			$tmp['msg'] = $key->msg;
			$tmp['img'] = $key->img;
			$tmp['created_at'] = $cr->convertTimeZone($key->created_at, 'America/Los_Angeles', 'UTC');

			/// this is in case we have post or social article id that has not been hashed, we will hash them right here.
			// tags post_id, share_article_id
			if ((strpos($key->link, '/post/') !== FALSE) || (strpos($key->link, '/social/article/') !== FALSE)){
				$id = substr($key->link, strrpos($key->link, '/') + 1);
				if (is_numeric($id)) {
					$key->link = str_replace($id, "", $key->link);
					$key->link .= $cr->hashIdForSocial($id);
				}
			}
			$tmp['link'] = $key->link;

			$tmp['date'] = $key->updated_at->diffForHumans();
			$tmp['noteId'] = Crypt::encrypt($key->id);
			$tmp['command'] = $key->command;
			$tmp['type'] = $key->type;
			$tmp['type_id'] = $cr->hashIdForSocial($key->type_id);
            $tmp['is_read'] = $key->is_read;
            $tmp['icon_img_route'] = $key->icon_img_route;

            if ($tmp['icon_img_route']  == "users_image") {
				$student_profile_photo = null;
				if (isset($key->submited_id)) {
					$user = DB::connection('rds1')->table('users as u')
												  ->select(DB::raw('IF(LENGTH(u.profile_img_loc), CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/users/images/", u.profile_img_loc) , NULL ) as student_profile_photo'))
												  ->where('id', $key->submited_id)
												  ->first();

					if (isset($user->student_profile_photo)) {
						$student_profile_photo = $user->student_profile_photo;
					}
				}

				$tmp['icon_img_route'] = $student_profile_photo;
				$tmp['user_img_route'] = $student_profile_photo;
            }
			$notifications['data'][] = $tmp;

			if($key->is_read == 0){
				$unread_cnt++;
			}
		}

		//$notifications['data'] = array_reverse($notifications['data']);
		$notifications['unread_cnt'] = $unread_cnt;

		return $notifications;
	}

	public function getTopNavMessages($data){

		if(isset($data['is_organization']) && $data['is_organization'] == 1){
			$cmc = new CollegeMessageController;
			$tmp = $cmc->getAllThreads( $data, null, 'college', true );

			$cnt = 0;
			$ret= array();

			$ret['data'] = array();
			$ret['unread_cnt'] = 0;
			foreach ($tmp as $key) {
				# we want to show only 6 results, and not anymore
				if($cnt == 6){
					break;
				}
				$tmpArray = array();
				$tmpArray = $key;
				$tmpArray['date'] = $this->xTimeAgo($key['date'], date("Y-m-d H:i:s"));
				$tmpArray['link'] = '/admin/messages/'. $key['thread_type_id'] .'/inquiry-msg';
				$ret['data'][] = $tmpArray;

				if (isset($key['num_unread_msg'])) {
					$ret['unread_cnt'] += $key['num_unread_msg'];
				}else{
					$ret['unread_cnt'] += 0;
				}

				$cnt++;
			}
		}elseif(isset($data['is_agency']) && $data['is_agency'] == 1){

			$cmc = new CollegeMessageController;

			$tmp = $cmc->getAllThreadsAgency( $data, null, 'agency', true );

			$cnt = 0;
			$ret= array();

			$ret['data'] = array();
			$ret['unread_cnt'] = 0;

			foreach ($tmp as $key) {
				# we want to show only 6 results, and not anymore
				if($cnt == 6){
					break;
				}
				$tmpArray = array();
				$tmpArray = $key;
				$tmpArray['date'] = $this->xTimeAgo($key['date'], date("Y-m-d H:i:s"));
				$tmpArray['link'] = '/agency/messages/'. $key['thread_type_id'];
				$ret['data'][] = $tmpArray;

				if (isset($key['num_unread_msg'])) {
					$ret['unread_cnt'] += $key['num_unread_msg'];
				}else{
					$ret['unread_cnt'] += 0;
				}

				$cnt++;
			}
		}else{

			$umc = new UserMessageController;
			$tmp = $umc->getAllThreads( $data, null, 'user', true );

			$cnt = 0;
			$ret= array();

			$ret['data'] = array();
			$ret['unread_cnt'] = 0;
			foreach ($tmp as $key) {
				# we want to show only 6 results, and not anymore
				if($cnt == 6){
					break;
				}
				$tmpArray = array();
				$tmpArray = $key;
				$tmpArray['date'] = $this->xTimeAgo($key['date'], date("Y-m-d H:i:s"));
				$tmpArray['link'] = '/portal/messages/'. $key['thread_type_id'] .'/college';
				$ret['data'][] = $tmpArray;
				$ret['unread_cnt'] += $key['num_unread_msg'];
				$cnt++;
			}

		}

		return $ret;
	}

	/**
	 * getNumProfileViews
	 *
	 * Total number of profile views by college admin
	 *
	 * @param (numeric) (user_id) user id
	 * @return (int) (count)
	 */
	public function getNumProfileViews($user_id = null, $onDate = null, $endDate = null){

		if ($user_id == null) {
			return 0;
		}
		$ntn = NotificationTopNav::on('bk')->whereIn('submited_id', $user_id)
									->where('command', 1)
									->select(DB::raw('count(DISTINCT type_id) as cnt, submited_id'))
 									->groupBy('submited_id');

		if( isset($onDate) && isset($endDate) ){
 			$ntn = $ntn->whereBetween('created_at', array($onDate, $endDate));
		}elseif( isset($onDate) ){
			$ntn = $ntn->where('created_at', '>', $onDate);
		}

		$ntn = $ntn->get();

		return $ntn;
	}

	/**
	 * getNumProfileViewsByCollegeId
	 *
	 * Total number of profile views by college ids
	 *
	 * @param (numeric) (user_id) user id
	 * @return (int) (count)
	 */
	public function getNumProfileViewsByCollegeId($school_name = null, $onDate = null, $endDate = null){

		if ($school_name == null) {
			return 0;
		}
		$ntn = NotificationTopNav::on('bk')->whereIn('name', $school_name)
									->where('command', 1)
									->where('type', 'user')
									->select(DB::raw('count(DISTINCT id) as cnt, name'))
 									->groupBy('name');

		if( isset($onDate) && isset($endDate) ){
 			$ntn = $ntn->whereBetween('created_at', array($onDate, $endDate));
		}elseif( isset($onDate) ){
			$ntn = $ntn->where('created_at', '>', $onDate);
		}

		$ntn = $ntn->get();

		return $ntn;
	}

	private function xTimeAgo( $oldTime, $newTime ) {
		$timeCalc = strtotime( $newTime ) - strtotime( $oldTime );
		if ( $timeCalc > ( 60*60*24 ) ) {$timeCalc = round( $timeCalc/60/60/24 ) . " days";}
		else if ( $timeCalc > ( 60*60 ) ) {$timeCalc = round( $timeCalc/60/60 ) . " hrs";}
		else if ( $timeCalc > 60 ) {$timeCalc = round( $timeCalc/60 ) . " mins";}
		else if ( $timeCalc > 0 ) {$timeCalc .= " secs";}
		return $timeCalc;
	}

	public function updateTopNavNotification($id){
        return NotificationTopNav::where('id', $id)->update(array('is_read' => 1));
	}
}
