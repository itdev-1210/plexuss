<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvancedSearchNote extends Model
{
    protected $table = 'advanced_search_notes';

 	protected $fillable = array('user_id', 'note');


 	public function getAdvancedSearchNote($user_id){
 		$asn = AdvancedSearchNote::on('rds1')->where('user_id', $user_id)->first();

 		return $asn;
 	}
}
