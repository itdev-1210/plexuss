<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
	protected $fillable = array('user_id', 'school_id','school_type','school_year', 'semester', 'class_type', 'custom_class_name', 'class_name', 'class_level', 'units', 'course_grade_type', 'course_grade' );

	
	public function users()
    {
        return $this->belongsTo('User');
    }
}
