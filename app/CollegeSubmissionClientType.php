<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeSubmissionClientType extends Model
{
    protected $table = 'college_submission_client_types';

 	protected $fillable = array('college_submission_id', 'domestic_recruitment',  'intl_recruitment', 'consulting',
 								'advertising', 'other', 'retention_student_success');
}
