<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdAffiliate extends Model
{
   	protected $table = 'ad_affiliates';

 	protected $fillable = array('end_point', 'company', 'active', 'allowed_countries');

 	public function getAdCopies($user_country = null){
 		$qry = DB::connection('rds1')->table('ad_affiliates as aa')
 									->join('ad_copies as ac', 'aa.id', '=', 'ac.ad_affiliate_id') 									
 									->where('aa.active', 1)
 									->select('aa.company', 'aa.end_point', 
 											 'ac.url', 'ac.id as ad_copy_id');

 		if (isset($user_country)) {
 			$qry = $qry->where('allowed_countries', 'LIKE', '%'.$user_country.'%');
 		}else{
 			$qry = $qry->whereNull('aa.allowed_countries');
 		}

 		$qry = $qry->orderByRaw("RAND()")->first();

 		$ret = array();

 		if (!isset($qry)) {
 			return $ret;
 		}
 		$ret['company'] = $qry->company;
 		$ret['end_point'] = $qry->end_point;
 		if (strpos($qry->url, 'http') !== FALSE){
 			$ret['url'] = $qry->url;
 		}else{
 			$ret['url'] = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/ad_copies/'.$qry->url;
 		}
 		
 		$ret['ad_copy_id'] = $qry->ad_copy_id;

 		return $ret;

 	}
}
