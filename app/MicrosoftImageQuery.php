<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MicrosoftImageQuery extends Model{

	protected $table = 'microsoft_image_queries';

 	protected $fillable = array('query');
}
