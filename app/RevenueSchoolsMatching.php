<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevenueSchoolsMatching extends Model
{
    protected $table = 'revenue_schools_matching';

 	protected $fillable = array('user_id', 'college_id', 'inquiry_date', 'is_uploaded', 'email_sent');
}
