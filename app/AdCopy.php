<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCopy extends Model
{
    protected $table = 'ad_copies';

 	protected $fillable = array('ad_affiliate_id', 'url');

}
