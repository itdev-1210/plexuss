<?php
namespace App;


use Illuminate\Database\Eloquent\Model;

class NewsCollegeMapping extends Model
{
    protected $table = 'news_colleges_mapping';

    protected $fillable = array('news_id', 'college_id');

}