<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrlParam extends Model
{
    protected $table = 'tracking_page_url_params';

    protected $fillable = array( 'tpu_id', 'params' );
}