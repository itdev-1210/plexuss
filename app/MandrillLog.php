<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MandrillLog extends Model {

	protected $table = 'mandrill_logs';
	
	protected $fillable = array( 'email', 'template_name', 'params','response');

}