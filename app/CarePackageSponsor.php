<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarePackageSponsor extends Model
{
    protected $table = 'carepackage_sponsors';
	protected $fillable = array( 'name','email','phone', 'howmanystudents' , 'package_name', 'message');
}
