<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
class PlexussConference extends Model {


	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'plexuss_conferences';


 	protected $fillable = array('date', 'name', 'location', 'booth_num');

 	public function getConferences(){
 		$today = Carbon::today();
 		$pc = PlexussConference::where('date', '>=', $today)->orderBy('date')->get();

 		return $pc;
 	}

}