<?php

namespace App;

use Session;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller, App\Http\Controllers\ViewDataController;
use Illuminate\Support\Facades\DB;

class Search extends Model
{
    // protected $table = 'college';

    public function SearchData( $data, $take = NULL, $skip = NULL, $is_api = NULL ) {
        isset($data['term']) ? $keyword=$data['term'] : $keyword=NULL;

        if ( $data['type']=='college' ) {

            if(isset($data['school_name']) && $data['school_name'] != ''){
                $keyword = $data['school_name'];
            }

            if ( isset( $data['zipcode'] ) && $data['zipcode']!='' ) {
                // code for validation success!
                $LatLang  = $this->getUserLocationByZip( $data['zipcode'] );

            } else {
                // code for validation failure
                $LatLang  = $this->getUserLocationByIp();
            }

            $distanceQry = "( 3959 * acos( cos( radians(".$LatLang[0].") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$LatLang[1].") ) + sin( radians(".$LatLang[0].") ) * sin(radians(latitude)) ) ) as distance";

            $result=DB::connection('rds1')->table( 'colleges' )
            ->leftJoin( 'colleges_admissions', 'colleges_admissions.college_id', '=', 'colleges.id' )
            ->leftJoin( 'colleges_enrollment', 'colleges_enrollment.college_id', '=', 'colleges.id' )
            ->leftJoin( 'colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges.id' )
            ->leftJoin( 'colleges_tuition', 'colleges_tuition.college_id', '=', 'colleges.id' )
            ->leftJoin( 'countries', 'colleges.country_id', '=', 'countries.id' );

            if (isset($data['department']) && $data['department'] == "study-trades") {
                
                $viewDataController = new ViewDataController();
                $dt = $viewDataController->buildData();
                
                $result = $result->where('colleges.school_level', 3);

                if (isset($dt['ip'])) {
                    $base = new Controller();
                    $locationInfo = $base->iplookup();

                    if ($locationInfo['countryAbbr'] == "US" && isset($locationInfo['stateAbbr'])) {
                        $result = $result->where('colleges.state', $locationInfo['stateAbbr']);
                    }
                }

            }

            if(isset($data['department'] ) && !isset($data['majors_department_slug'])){
                // $result = $result->leftJoin('department_categories as dc', 'dc.id', '=', 'dc.id')
                //                     ->join('departments as d', 'dc.id', '=', 'd.category_id')
                //                     ->join('majors as m', 'd.id', '=', 'm.department_id')
                //                     ->join( 'college_programs as cp', 'cp.major_id', '=', 'm.id' );
                $result = $result->join('college_programs as cp', 'cp.college_id', '=', 'colleges.id')
                                 ->join('departments as d', 'd.id', '=', 'cp.department_id')
                                 ->join('department_categories as dc', 'dc.id', '=', 'd.category_id')
                                 ->join('majors as m', 'm.id', '=', 'cp.major_id');
            }
            if (isset($data['majors_department_slug'])) {
                $result = $result->join('college_programs as cp', 'cp.college_id', '=', 'colleges.id')
                                 ->join('departments as d', 'd.id', '=', 'cp.department_id')
                                 ->join('department_categories as dc', 'dc.id', '=', 'd.category_id')
                                 ->join('majors as m', 'm.id', '=', 'cp.major_id')
                                 ->join('major_mapping_simple as mms',  'mms.major_id', '=', 'm.id')
                                 ->join('meta_data_majors as mdm', 'mdm.id', '=', DB::raw('mms.simplified_major_id and
                                        if(`mdm`.`degree_type_specific` is not null
                                                    , `mdm`.`degree_type_specific` = `cp`.`degree_type`
                                                    , `cp`.`degree_type` = `cp`.`degree_type`)'))
                                 ->where('mdm.slug', '=', $data['majors_department_slug'])
                                 ->where('countries.id', 1);
            }

            $result = $result->where( 'verified', '=', 1 );

            if ( isset( $keyword ) && $keyword!='') {
                $result = $result->where( function( $query ) use ( $keyword ) {
                        $query->where( 'colleges.school_name', 'like', '%'.$keyword.'%' )->orWhere( 'colleges.alias', 'like', '%'.$keyword.'%' );
                    } );
            }


            if(isset($data['department'])){
                 $result = $result->where('dc.url_slug', '=', $data['department']);
            }

            if(isset($input['imajor']) && $input['imajor'] != NULL && $input['imajor'] != "null")
            {
                $result = $result->where('m.id', '=', $inputs['imajor']);
            }

            if(isset($data['imajor']) && $data['imajor'] != NULL && $data['imajor'] != "null")
            {
                $result = $result->where('m.id', '=', $data['imajor']);
            }

            isset($inputs['rmajor']) ? $r_major = $inputs['rmajor'] : $r_major = null;
            if(isset($r_major) && count($r_major) > 0){
                $result = $result->whereNotIn('m.id', $r_major);
            }


            isset($data['school_name']) ? $s_name = $data['school_name'] : $s_name = null;
            if(isset($s_name) && $s_name != ''){
                $result = $result->where( function( $query ) use ( $s_name ) {
                            $query->where( 'colleges.school_name', 'like', '%'.$s_name.'%' )->orWhere( 'colleges.alias', 'like', '%'.$s_name.'%' );
                } );
            }

            if ( isset( $data['city'] ) && $data['city']>'0' ) {
                $result = $result->where( 'colleges.city', '=', ''.$data['city'].'' );
            }
            if ( isset( $data['country'] ) && $data['country'] != '' ) {
                $result = $result->where( 'countries.country_code', '=', ''.$data['country'].'' );
            }
            if ( isset( $data['state'] ) && $data['state']!='' ) {
                $result = $result->where( 'colleges.long_state', '=', $data['state'] );
                //$result = $result->where('colleges.state', 'like', '%'.$data['state'].'%');
            }
            if ( isset( $data['zipcode'] ) && $data['zipcode']!='' ) {
                if ( ( isset( $data['miles_range_min_val'] ) && $data['miles_range_min_val']!='' ) && ( isset( $data['miles_range_max_val'] ) && $data['miles_range_min_val']!='' ) ) {
                    $result = $result->having( 'distance', '>', $data['miles_range_min_val'] );
                    $result = $result->having( 'distance', '<', $data['miles_range_max_val'] );
                }else{
                    $result = $result->having( 'distance', '<', 250);
                }
                $result = $result -> orderBy(DB::raw('`colleges_ranking`.plexuss IS NULL, - `colleges_ranking`.plexuss'), 'desc')
                                  -> orderBy('distance', 'asc')
                                  -> orderBy('colleges.slug', 'asc');
            }else{
                if (isset($data['department']) && $data['department'] == "study-trades") {
                    $result = $result->orderBy('distance', "ASC");
                }else{
                    $result = $result ->orderBy(DB::raw('`colleges_ranking`.plexuss IS NULL, - `colleges_ranking`.plexuss'), 'desc')
                                  ->orderBy('colleges.slug', 'asc');
                }
                
            }



            if ( isset( $data['degree'] ) && $data['degree']!='' ) {


                if ( $data['degree']=='bachelors_degree' ) {$result = $result->where( 'colleges.bachelors_degree', '=', 'Yes' );}
                elseif ( $data['degree']=='masters_degree' ) {$result = $result->where( 'colleges.masters_degree', '=', 'Yes' );}
                elseif ( $data['degree']=='post_masters_degree' ) {$result = $result->where( 'colleges.post_masters_degree', '=', 'Yes' );}
                elseif ( $data['degree']=='doctors_degree_research' ) {$result = $result->where( 'colleges.doctors_degree_research', '=', 'Yes' );}
                elseif ( $data['degree']=='doctors_degree_professional' ) {$result = $result->where( 'colleges.doctors_degree_professional', '=', 'Yes' );}

            }



            if ( ( isset( $data['campus_housing'] ) && $data['campus_housing']!='' ) ) {
                $result = $result->where( 'colleges.campus_housing', '=', 'Yes' );
            }

            if ( isset( $data['locale'] ) && $data['locale']!='' ) {
                $result = $result->where( 'colleges.locale', '=', $data['locale'] );
            }

            if ( isset( $data['religious_affiliation'] ) && $data['religious_affiliation']!='' ) {
                $result = $result->where( 'colleges.religious_affiliation', '=', $data['religious_affiliation'] );
            }

            if ( ( isset( $data['min_reading'] ) && $data['min_reading']!='' ) && ( isset( $data['max_reading'] ) && $data['max_reading']!='' ) ) {
                //$result = $result->whereRaw("colleges_admissions.sat_read_25 between ".$data['min_reading']." and ".$data['max_reading']."");
                $result = $result->whereBetween( 'colleges_admissions.sat_read_25', array( $data['min_reading'], $data['max_reading'] ) );
            }

            if ( ( isset( $data['min_sat_math'] ) && $data['min_sat_math']!='' ) && ( isset( $data['max_sat_math'] ) && $data['max_sat_math']!='' ) ) {
                $result = $result->whereBetween( 'colleges_admissions.sat_math_25', array( $data['min_sat_math'], $data['max_sat_math'] ) );
            }

            if ( ( isset( $data['min_act_composite'] ) && $data['min_act_composite']!='' ) && ( isset( $data['max_act_composite'] ) && $data['max_act_composite']!='' ) ) {
                $result = $result->whereBetween( 'colleges_admissions.act_composite_25', array( $data['min_act_composite'], $data['max_act_composite'] ) );
            }

            if ( isset( $data['tuition_max_val'] ) && $data['tuition_max_val']!='' ) {
                //$result = $result->whereBetween('colleges.tuition_fees_1213', array(0,$data['tuition_max_val']));
                $result = $result->where( 'colleges_tuition.tuition_avg_in_state_ftug', '<=', $data['tuition_max_val'] );
            }
            // begin test when move colleges_enrollment
            if (isset($data['enrollment_min_val']) && !($data['enrollment_min_val'] == 0 && $data['enrollment_max_val'] == 0)) {
                if ( ( isset( $data['enrollment_min_val'] ) && $data['enrollment_min_val'] >= 0 ) && ( isset( $data['enrollment_max_val'] ) && $data['enrollment_max_val']!='' ) ) {
                    $result = $result->whereBetween( 'undergrad_total', array( $data['enrollment_min_val'], $data['enrollment_max_val'] ) );
                    // dd($result->get());
                    // exit();
                }
            }

            if (isset($data['applicants_min_val']) && !($data['applicants_min_val'] == 0 && $data['applicants_max_val'] == 0)) {
                if ( ( isset( $data['applicants_min_val'] ) && $data['applicants_min_val'] >= 0 ) && ( isset( $data['applicants_max_val'] ) && $data['applicants_max_val']!='' ) ) {
                    $result = $result->whereBetween( 'colleges_admissions.percent_admitted', array( $data['applicants_min_val'], $data['applicants_max_val'] ) );
                }
            }

            if (isset($take) && isset($skip)) {
                $result = $result->select( DB::raw(
                    'colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,LOWER(countries.country_code) as country_code,
                colleges.long_state,colleges.zip,colleges.chief_title,colleges.chief_name,colleges.alias,
                colleges.admissions_total,colleges.applicants_total,colleges.tuition_fees_1213,colleges.campus_housing,
                colleges_admissions.sat_percent,colleges_admissions.act_percent,colleges_admissions.sat_read_75,
                colleges_admissions.sat_math_75,colleges_admissions.sat_write_75,colleges_admissions.sat_read_25,
                colleges_admissions.sat_math_25,colleges_admissions.sat_write_25,colleges_admissions.act_composite_25,
                colleges_admissions.act_composite_75,colleges_admissions.application_fee_undergrad,colleges_admissions.percent_admitted,
                colleges_enrollment.undergrad_total,colleges_tuition.tuition_avg_in_state_ftug'
                ) );

                if (!isset($is_api)) {
                    $result = $result->addSelect(DB::raw('CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/", colleges.logo_url) as logo_url'), 'colleges_ranking.plexuss as rank');
                }else{
                    $result = $result->addSelect('colleges.logo_url', 'colleges_ranking.plexuss');
                }

                return $result;
            }else{
                $result = $result->select( DB::raw(
                    'colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,LOWER(countries.country_code) as country_code,
                colleges.long_state,colleges.zip,colleges.chief_title,colleges.chief_name,colleges.logo_url,colleges.alias,
                colleges.admissions_total,colleges.applicants_total,colleges.tuition_fees_1213,colleges.campus_housing,
                colleges_admissions.sat_percent,colleges_admissions.act_percent,colleges_admissions.sat_read_75,
                colleges_admissions.sat_math_75,colleges_admissions.sat_write_75,colleges_admissions.sat_read_25,
                colleges_admissions.sat_math_25,colleges_admissions.sat_write_25,colleges_admissions.act_composite_25,
                colleges_admissions.act_composite_75,colleges_admissions.application_fee_undergrad,colleges_admissions.percent_admitted,
                colleges_enrollment.undergrad_total,colleges_ranking.plexuss,colleges_tuition.tuition_avg_in_state_ftug,'.$distanceQry
                ) );
                $result = $result->groupBy('colleges.id');
                $retArr = array();
                $retArr[] = count($result->get());
                $retArr[] = $result->simplePaginate(20);

                $result = $retArr;
            }


            //dd('here');
            /*
            print_r($result->toSql());
            exit();
            //$result = $result-> orderByRaw('colleges_ranking.plexuss desc');
            $result = $result->paginate(20);
            */
        }
        elseif ( $data['type']=='ranking' ) {
            /*------------- ranking data come here ----------------*/
        }
        elseif ( $data['type']=='news' ) {

            /*$result=DB::connection('rds1')->table('news_articles')
            ->select('news_articles.id','news_articles.external_name','news_articles.content','news_articles.img_sm');
            $result = $result->where('external_name', 'like', '%'.$keyword.'%');
            $result = $result->where('content', 'like', '%'.$keyword.'%', 'OR');
            $result = $result->orderBy('external_name', 'asc');
            $result = $result->LIMIT(30)->get();*/

            $result = DB::connection('rds1')->table( 'news_articles' )
            ->select( 'slug', 'news_articles.id', 'news_articles.external_name', 'news_articles.content', 'news_articles.img_sm' );
            $result = $result->where( 'external_name', 'like', '%'.$keyword.'%' );
            $result = $result->where( 'content', 'like', '%'.$keyword.'%', 'OR' );
            $result = $result->orderBy( 'external_name', 'asc' );
            $result = $result->paginate( 10 );

        }
        else {
            /************************* college **************************/

            $query = 'colleges.id,colleges.slug ,colleges.school_name,colleges.city,colleges.state,colleges.logo_url,colleges.admissions_total,
                      colleges.tuition_fees_1213,colleges.applicants_total,colleges_enrollment.undergrad_total,colleges_ranking.plexuss,
                      colleges_tuition.tuition_avg_in_state_ftug,"college" as category';
            $orderBy = 'colleges.school_name';
            $orderType = 'asc';

            $college_data = DB::connection('rds1')->table( 'colleges' )
            ->leftJoin( 'colleges_enrollment', 'colleges_enrollment.ipeds_id', '=', 'colleges.ipeds_id' )
            ->leftJoin( 'colleges_ranking', 'colleges_ranking.ipeds_id', '=', 'colleges.ipeds_id' )
            ->leftJoin( 'colleges_tuition', 'colleges_tuition.ipeds_id', '=', 'colleges.ipeds_id' )
            ->select( DB::raw( $query ) )
            ->where(function($query) use($keyword){
                $query->where( 'colleges.school_name', 'like', '%'.$keyword.'%' )
                      ->where( 'colleges.alias', 'like', '%'.$keyword.'%', 'OR' );
            })

            ->where('colleges.verified', 1)
            ->orderby( $orderBy, $orderType )
            ->LIMIT( 3 )
            ->get();

            /************************* college **************************/

            /************************* news **************************/

            $query2 = 'id, external_name, content,img_sm, "news" as category';
            $orderBy2 = 'external_name';
            $orderType2 = 'asc';

            $news_data= DB::connection('rds1')->table( 'news_articles' )
            ->select( DB::raw( $query2 ) )
            ->where( 'external_name', 'like', '%'.$keyword.'%' )
            ->where( 'content', 'like', '%'.$keyword.'%', 'OR' )
            ->orderby( $orderBy2, $orderType2 )
            ->LIMIT( 3 )
            ->get();
            /************************* news **************************/


            // $result=array_merge_recursive( $college_data, $news_data );
            $result = $college_data->merge($news_data);

        }

        return $result;
    }


    public function localuser() {
        $variable1 = 5;
        $variable2 =10;
        $total=$variable1+$variable2;
        return $total;
    }

    public function getUserLocationByZip( $zipcode ) {


        $ret = array();
        $data = DB::connection('rds1')->table( 'zip_codes' )->where( 'ZipCode', '=', $zipcode )->first();

        //We need to check again to see if the zip code was in our database! If not we return back to IP look up.
        if ( !$data ) {
            return $this->getUserLocationByIp();
        }

        $ret[] = $data->Latitude;
        $ret[] = $data->Longitude;

        return $ret;
    }

    public function getUserLocationByIp() {
        $ret = array();
        // $ip_address = $_SERVER['REMOTE_ADDR'];

        // $privateIP = $this->checkForPrivateIP( $ip_address );

        // //If remote IP fails we default to office IP.
        // if ( $ip_address == '::1' || $privateIP ) {
        //     $ip_address = '50.0.50.17';
        // }

        // //The better ip locator API
        // // Emergency FIX!! this freegeoip server blocked us!!!!
        // //$url = "http://freegeoip.net/json/".$ip_address;
        // $headers = array( 'Accept' => 'application/json' );
        // //$request = Requests::get($url, $headers);
        // //$dec = json_decode($request->body);


        // //if freegeoip failed, then go to this sucky API
        // //if ($dec->latitude =="" || $dec->longitude =="") {
        // $url ='http://api.ipinfodb.com/v3/ip-city/?key=ca7f2dc6b3e06662e0359d2c378dc1cc9153afeea793c30fd1084fa85e9ea509&ip='.$ip_address.'&format=json';
        // $request = Requests::get( $url, $headers );
        // $dec = json_decode( $request->body );
        // if the sucky API failed, then just set lat, and long to walnut creek



        $base = new Controller();

        $locationInfo = $base->iplookup();

        if (isset($locationInfo) && !empty($locationInfo)) {

            $ret[] = $locationInfo['latitude'];
            $ret[] = $locationInfo['longitude'];
        }else{
            $ret[] = "37.8916";
            $ret[] = "-122.0381";
        }
        //}



        return $ret;
    }

    public function checkForPrivateIP( $ip ) {
        $reserved_ips = array( // not an exhaustive list
            '167772160'  => 184549375,  /*    10.0.0.0 -  10.255.255.255 */
            '3232235520' => 3232301055, /* 192.168.0.0 - 192.168.255.255 */
            '2130706432' => 2147483647, /*   127.0.0.0 - 127.255.255.255 */
            '2851995648' => 2852061183, /* 169.254.0.0 - 169.254.255.255 */
            '2886729728' => 2887778303, /*  172.16.0.0 -  172.31.255.255 */
            '3758096384' => 4026531839, /*   224.0.0.0 - 239.255.255.255 */
        );

        $ip_long = sprintf( '%u', ip2long( $ip ) );

        foreach ( $reserved_ips as $ip_start => $ip_end ) {
            if ( ( $ip_long >= $ip_start ) && ( $ip_long <= $ip_end ) ) {
                return TRUE;
            }
        }

        return FALSE;
    }



    ////////////////////////////////////////////////////////////
    //  get colleges by department
    //  used on load for majors search
    //  with and without additional filters
    public function getCollegesWithDept($inputs){

        //if term is empty then return all colleges
        if(isset($inputs['department'])){
            $keyword=$inputs['department'];
        }
        else if(isset($inputs['term'])){
            $keyword=$inputs['term'];
        }

        else{
            $keyword = null;
        }

        // dd($keyword);

        if ( isset( $inputs['zipcode'] ) && $inputs['zipcode']!='' ) {
            // code for validation success!
            $LatLang  = $this->getUserLocationByZip( $inputs['zipcode'] );

        } else {
            // code for validation failure
            $LatLang  = $this->getUserLocationByIp();
        }

        // select c.slug, c.school_name, c.id as cid, dc.url_slug, dc.id as dc_id, d.name
        // #, m.id, m.name
        // from department_categories dc
        // join departments d on d.category_id =  dc.id
        // join majors m on d.id = m.department_id
        // join college_programs cp on cp.major_id = m.id
        // join colleges c on cp.college_id = c.id
        // left join table cols for info
        // where dc.url_slug = $slug

        // add filters
        // #ex. and cp.degree_type = 3 and c.id = 2
        //

        // and m.promote = 1
        // group by m.id, c.id
        $distanceQry = "( 3959 * acos( cos( radians(".$LatLang[0].") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$LatLang[1].") ) + sin( radians(".$LatLang[0].") ) * sin(radians(latitude)) ) ) as distance";

        $result=DB::connection('rds1')->table( 'department_categories AS dc' )

        ->join('departments as d', 'dc.id', '=', 'd.category_id')
        ->join('majors as m', 'd.id', '=', 'm.department_id')
        ->join( 'college_programs as cp', 'cp.major_id', '=', 'm.id' )
        ->join( 'colleges', 'cp.college_id', '=', 'colleges.id')
        ->leftJoin( 'colleges_admissions', 'colleges_admissions.college_id', '=', 'colleges.id' )
        ->leftJoin( 'colleges_enrollment', 'colleges_enrollment.college_id', '=', 'colleges.id' )
        ->leftJoin( 'colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges.id' )
        ->leftJoin( 'colleges_tuition', 'colleges_tuition.college_id', '=', 'colleges.id' )

        ->selectRaw(
                'dc.name as searchterm, colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,
            colleges.long_state,colleges.zip,colleges.chief_title,colleges.chief_name,colleges.logo_url,colleges.alias,
            colleges.admissions_total,colleges.applicants_total,colleges.tuition_fees_1213,colleges.campus_housing,
            colleges_admissions.sat_percent,colleges_admissions.act_percent,colleges_admissions.sat_read_75,
            colleges_admissions.sat_math_75,colleges_admissions.sat_write_75,colleges_admissions.sat_read_25,
            colleges_admissions.sat_math_25,colleges_admissions.sat_write_25,colleges_admissions.act_composite_25,
            colleges_admissions.act_composite_75,colleges_admissions.application_fee_undergrad,colleges_admissions.percent_admitted,
            colleges_enrollment.undergrad_total,colleges_ranking.plexuss,colleges_tuition.tuition_avg_in_state_ftug,'.$distanceQry
            );

        $result = $result->where( 'verified', '=', 1 );


        if ( isset( $inputs['degree'] ) && $inputs['degree']!='' ) {
            if ( $inputs['degree']=='bachelors_degree' ) {$result = $result->where( 'colleges.bachelors_degree', '=', 'Yes' );}
            elseif ( $inputs['degree']=='masters_degree' ) {$result = $result->where( 'colleges.masters_degree', '=', 'Yes' );}
            elseif ( $inputs['degree']=='post_masters_degree' ) {$result = $result->where( 'colleges.post_masters_degree', '=', 'Yes' );}
            elseif ( $inputs['degree']=='doctors_degree_research' ) {$result = $result->where( 'colleges.doctors_degree_research', '=', 'Yes' );}
            elseif ( $inputs['degree']=='doctors_degree_professional' ) {$result = $result->where( 'colleges.doctors_degree_professional', '=', 'Yes' );}

        }

        if( isset($inputs['degree_type']) && $inputs['degree_type'] != -1 ){
            $result = $result->where('cp.degree_type', '=', $inputs['degree_type']);
        }

        if ( isset( $keyword ) && $keyword!='' ) {
            $result = $result->where('dc.url_slug', '=', $keyword);
        }
        $result = $result->where('m.promote', '=', 1);

        if( !isset($inputs['department']) && !isset($inputs['term'])){

            isset($inputs['majors']) ? $majors_l = $inputs['majors'] : $majors_l = null;
            if(isset($majors_l)  && count($majors_l) > 0){
                $result = $result->where(function ($q) use ($majors_l){

                    foreach($majors_l as $major){

                        $q = $q->orWhere('m.id', '=', DB::raw($major->id));
                    }

                });

            }
        }



        isset($inputs['rmajor']) ? $r_major = $inputs['rmajor'] : $r_major = null;
        if(isset($r_major) && count($r_major) > 0){
            $result = $result->whereNotIn('m.id', $r_major);
        }

        if(isset($inputs['imajor']) && $inputs['imajor'] != ''){
            $result = $result->where('m.id', '=', $inputs['imajor']);
        }

        isset($inputs['school_name']) ? $s_name = $inputs['school_name'] : $s_name = null;
        if(isset($s_name) && $s_name != '' && $s_name != null){
            $result = $result->where( function( $query ) use ( $s_name ) {
                        $query->where( 'colleges.school_name', 'like', '%'.$s_name.'%' )->orWhere( 'colleges.alias', 'like', '%'.$s_name.'%' );
            } );
        }







        if ( isset( $inputs['city'] ) && $inputs['city']>'0' ) {
            $result = $result->where( 'colleges.city', '=', ''.$inputs['city'].'' );
        }


        if ( isset( $inputs['state'] ) && $inputs['state']!=''  && $inputs['state'] != '0' ) {
            $result = $result->where( 'colleges.long_state', '=', $inputs['state'] );
            //$result = $result->where('colleges.state', 'like', '%'.$inputs['state'].'%');
        }

        if ( ( isset( $inputs['campus_housing'] ) && $inputs['campus_housing']!='' ) ) {
            $result = $result->where( 'colleges.campus_housing', '=', 'Yes' );
        }

        if ( isset( $inputs['locale'] ) && $inputs['locale']!='' ) {
            $result = $result->where( 'colleges.locale', '=', $inputs['locale'] );
        }

        if ( isset( $inputs['religious_affiliation'] ) && $inputs['religious_affiliation']!='' ) {
            $result = $result->where( 'colleges.religious_affiliation', '=', $inputs['religious_affiliation'] );
        }

        if ( ( isset( $inputs['min_reading'] ) && $inputs['min_reading']!='' ) && ( isset( $inputs['max_reading'] ) && $inputs['max_reading']!='' ) ) {
            //$result = $result->whereRaw("colleges_admissions.sat_read_25 between ".$inputs['min_reading']." and ".$inputs['max_reading']."");
            $result = $result->whereBetween( 'colleges_admissions.sat_read_25', array( $inputs['min_reading'], $inputs['max_reading'] ) );
        }

        if ( ( isset( $inputs['min_sat_math'] ) && $inputs['min_sat_math']!='' ) && ( isset( $inputs['max_sat_math'] ) && $inputs['max_sat_math']!='' ) ) {
            $result = $result->whereBetween( 'colleges_admissions.sat_math_25', array( $inputs['min_sat_math'], $inputs['max_sat_math'] ) );
        }

        if ( ( isset( $inputs['min_act_composite'] ) && $inputs['min_act_composite']!='' ) && ( isset( $inputs['max_act_composite'] ) && $inputs['max_act_composite']!='' ) ) {
            $result = $result->whereBetween( 'colleges_admissions.act_composite_25', array( $inputs['min_act_composite'], $inputs['max_act_composite'] ) );
        }

        if ( isset( $inputs['tuition_max_val'] ) && $inputs['tuition_max_val']!='' ) {
            //$result = $result->whereBetween('colleges.tuition_fees_1213', array(0,$inputs['tuition_max_val']));
            $result = $result->where( 'colleges_tuition.tuition_avg_in_state_ftug', '<=', $inputs['tuition_max_val'] );
        }
        // begin test when move colleges_enrollment
        if ( ( isset( $inputs['enrollment_min_val'] ) && $inputs['enrollment_min_val'] > 0 ) && ( isset( $inputs['enrollment_max_val'] ) && $inputs['enrollment_max_val']!='' ) ) {
            $result = $result->whereBetween( 'undergrad_total', array( $inputs['enrollment_min_val'], $inputs['enrollment_max_val'] ) );
            // dd($result->get());
            // exit();
        }

        if ( ( isset( $inputs['applicants_min_val'] ) && $inputs['applicants_min_val'] > 0 ) && ( isset( $inputs['applicants_max_val'] ) && $inputs['applicants_max_val']!='' ) ) {
            $result = $result->whereBetween( 'colleges_admissions.percent_admitted', array( $inputs['applicants_min_val'], $inputs['applicants_max_val'] ) );
        }


        if ( isset( $inputs['zipcode'] ) && $inputs['zipcode']!='' ) {
            if ( ( isset( $inputs['miles_range_min_val'] ) && $inputs['miles_range_min_val']!='' ) && ( isset( $inputs['miles_range_max_val'] ) && $inputs['miles_range_min_val']!='' ) ) {
                $result = $result->having( 'distance', '>', $inputs['miles_range_min_val'] );
                $result = $result->having( 'distance', '<', $inputs['miles_range_max_val'] );
            }else{
                $result = $result->having( 'distance', '<', 250);
            }
            $result = $result -> orderBy(DB::raw('`colleges_ranking`.plexuss IS NULL, - `colleges_ranking`.plexuss'), 'desc')
                              -> orderBy('distance', 'asc')
                              -> orderBy('colleges.slug', 'asc');
        }else{
            $result = $result-> orderBy(DB::raw('`colleges_ranking`.plexuss IS NULL, - `colleges_ranking`.plexuss'), 'desc')
                             -> orderBy('colleges.slug', 'asc');

        }
        $result = $result->groupBy('colleges.id');


        $res = $result->get();
        // dd($res);
        $retArr = array();
        $retArr[] = count($res);
        $retArr[] = $result->simplePaginate(20);
        if(isset($res) && count($res) > 0  && ( isset($inputs['term']) || isset($inputs['department']) ) ){
            $retArr[] = $res[0]->searchterm;
        }
        // else if(isset($inputs['department']) && $inputs['type']=='majors'){
        //      $retArr[] = $inputs['department'];
        // }


        $results = $retArr;

        return $results;
    }




    ////////////////////////////////////////////////////////
    public function getMajorsFromCat($cat = '', $imajor = null){
        // select m.name as 'major'
        // from department_categories dc
        // join departments d on dc.id = d.category_id
        // join majors m on d.id = m.department_id
        // where dc.name = 'Liberal Arts & Humanities'
        //to get majors, given category name

        // dd($imajor);
        //if category not set return false
        if(!isset($cat) || empty($cat))
            return false;

        $result = DB::connection('rds1')->table('department_categories as dc')
                        ->join('departments as d', 'dc.id', '=', 'd.category_id')
                        ->join('majors as m', 'd.id', '=', 'm.department_id');



        if($cat != ''){
            $result = $result->where('dc.url_slug', '=', $cat);
        }

        if($imajor != '' && $imajor != "null"){
            $result = $result->where('m.id', '=', $imajor);
        }

        $result = $result
                ->where('m.promote', '=', 1)
                ->selectRaw('m.id, m.name')
                ->orderBy('m.name', 'asc')
                ->get()->toArray();
        // dd($result);

        return $result;

    }


    ////////////////////////////////////////////////////////
    public function getDepts(){

        $results = DB::connection('rds1')->table('department_categories as dc')
                        ->select('dc.id', 'dc.name', 'dc.url_slug')
                        ->get();
        return $results;
    }

    public function getDeptMetaInfo($slug) {
          $results = DB::connection('rds1')->table('meta_data_departments as mdd')
                                          ->join('department_categories as dc', 'dc.id', '=', 'mdd.dc_id')
                                          ->select('mdd.*')
                                          ->where('dc.url_slug', $slug)
                                          ->first();
        return $results;
    }

    public function getMajorMetaInfo($dep_slug, $major_slug){
        $results  = DB::connection('rds1')->table('meta_data_departments as mdd')
                                          ->join('department_categories as dc', 'dc.id', '=', 'mdd.dc_id')
                                          ->join('meta_data_majors as mdm', 'mdm.mdd_id', '=', 'mdd.id')
                                          ->select('mdm.*', 'mdd.department_category')
                                          ->where('dc.url_slug', $dep_slug)
                                          ->where('mdm.slug',    $major_slug)
                                          ->first();
        
        return $results;    
    }

    public function getMajorsByDepartment($id){

        $results  = DB::connection('rds1')->table('meta_data_departments as mdd')
                                          ->join('meta_data_majors as mdm', 'mdm.mdd_id', '=', 'mdd.id')
                                          ->where('mdm.mdd_id', $id)
                                          ->select('mdm.*', 'mdd.slug as mdd_slug')
                                          ->get();

        return $results;  
    }
}
