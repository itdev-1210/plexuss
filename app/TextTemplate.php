<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextTemplate extends Model {

	protected $table = 'text_templates';

 	protected $fillable = array('type', 'body');
}
