<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetaUser extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'beta_users';

	public static $rules = array(
		'name' => 'required',
		'email' => 'required|email',
		'school' => 'required'
	);

}
