<?php
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model{

	// use SluggableTrait;

	// protected $sluggable = array(
 //        'build_from' => 'name',
 //        'save_to'    => 'slug',
 //    );

	protected $table = 'news_categories';

	protected $fillable = array('name');

	public function newssubcategories(){
		return $this->hasMany('NewsSubcategory');
	}

}
