<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class FriendRelation extends Model
{
  protected $table = 'sn_friend_relations';
  protected $fillable = array('user_one_id', 'user_two_id', 'relation_status', 'action_user');

  public function getMyConnections($user_id) {
	  $my_connection = FriendRelation::on('rds1')->where('relation_status', 'Accepted')
                                               ->where(function($q) use ($user_id){
                                                        $q->orWhere('user_one_id', '=', $user_id)
                                                          ->orWhere('user_two_id', '=', $user_id);
                                               })
                                               ->select(DB::raw("IF(
                                                    user_one_id != ".$user_id." ,
                                                    user_one_id ,
                                                    user_two_id
                                                  ) as friend_uid"))
                                               ->get();

    $arr = array();
    foreach ($my_connection as $key) {
      $arr[] = $key->friend_uid;
    }

    return $arr;
  }

  public function getFriendStatus($user1, $user2){
    $qry = FriendRelation::on('rds1')->orWhere(function($q) use($user1, $user2){
                                                $q->where('user_one_id', '=', $user1)
                                                  ->where('user_two_id', '=', $user2);
                                                })
                                      ->orWhere(function($q) use($user1, $user2){
                                                $q->where('user_one_id', '=', $user2)
                                                  ->where('user_two_id', '=', $user1);
                                                })

                                      ->select('relation_status')
                                      ->first();
                                     
    return $qry;
  }
}
