<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailPlatformToggle extends Model {

	protected $table = 'email_platform_toggles';
	protected $fillable = array( 'platform', 'active' );

}