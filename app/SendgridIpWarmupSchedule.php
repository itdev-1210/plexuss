<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendgridIpWarmupSchedule extends Model
{
    protected $table = 'sendgrid_ip_warmup_schedules';

 	protected $fillable = array('day', 'volume', 'sent');
}
