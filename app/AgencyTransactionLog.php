<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyTransactionLog extends Model
{
    protected $table = 'agency_transaction_log';

 	protected $fillable = array( 'user_id', 'agency_id', 'cost', 'paid' );
}
