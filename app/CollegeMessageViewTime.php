<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeMessageViewTime extends Model
{
	protected $table = 'college_message_view_times';

 	protected $fillable = array('msg_id', 'read_user_id', 'read_time');
}
