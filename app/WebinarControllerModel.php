<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WebinarControllerModel extends Model
{
    //
    protected $table = 'webinar_controller';

 	protected $fillable = array('event_id', 'video', 'start', 'end', 'end_showing_webinar');


 	public function isWebinarLive($event_id){

 		$now = Carbon::now();

 		$wc = WebinarControllerModel::where('event_id', $event_id)
 							   ->where('start', '<=', $now)
 							   ->where('end', '>=', $now)
 						       ->first();

 		return $wc;

 	}

 	public function canShowWebinarFrontPage($event_id){

 		$now = Carbon::now();

 		$wc = WebinarControllerModel::where('event_id', $event_id)
 							   ->where('start', '<=', $now)
 							   ->where('end_showing_webinar', '>=', $now)
 						       ->first();

 		return $wc;

 	}
}
