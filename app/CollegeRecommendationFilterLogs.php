<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeRecommendationFilterLogs extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	
    public $timestamps = false;

    protected $table = 'college_recommendation_filter_logs';

 	protected $fillable = array( 'rec_filter_id', 'val' );

 	public function clearFilter($agency_id, $category = null, $college_id = null, $org_branch_id = null, $org_portal_id = null, $aor_id = null, $post_id = NULL, $share_article_id = NULL){

 		if ($category == null && $college_id == null && $org_branch_id == null && $agency_id == null && 
 			$post_id == NULL && $share_article_id == NULL) {
 			return;
 		}

 		$filter_log = CollegeRecommendationFilterLogs::join('college_recommendation_filters as crf', 'crf.id', '=', 'college_recommendation_filter_logs.rec_filter_id')
 					->where('category', $category);

 		if (isset($agency_id)) {
 			
 			$filter_log = $filter_log->where('agency_id', $agency_id);

 		}elseif(isset($college_id) && isset($org_branch_id)){
 			$filter_log = $filter_log->where('college_id', $college_id)
								 	 ->where('org_branch_id', $org_branch_id);
 		}

 		if (isset($post_id)) {
 			$filter_log = $filter_log->where('crf.post_id', $post_id);
 		}else{
 			$filter_log = $filter_log->whereNull('crf.post_id');
 		}

 		if (isset($share_article_id)) {
 			$filter_log = $filter_log->where('crf.share_article_id', $share_article_id);
 		}else{
 			$filter_log = $filter_log->whereNull('crf.share_article_id');
 		}

 		if (isset($org_portal_id)) {
 			$filter_log = $filter_log->where('crf.org_portal_id', $org_portal_id);
 		}else{
 			$filter_log = $filter_log->whereNull('crf.org_portal_id');
 		}

 		if (isset($aor_id)) {
 			$filter_log = $filter_log->where('crf.aor_id', $aor_id);
 		}else{
 			$filter_log = $filter_log->whereNull('crf.aor_id');
 		}
 		
 		$filter_log->delete();
 	}

 	public function saveFilterLog($arr = null, $rec_filter_id = null){

 		if ($arr == null || $rec_filter_id == null) {
 			return;
 		}
		$crfl = CollegeRecommendationFilterLogs::insert($arr);
 	}
	
	public function clearFilter_scholarship($category = null, $scholarship_id){
		if ($category == null) {
 			return;
 		}
		$filter_log = CollegeRecommendationFilterLogs::join('college_recommendation_filters as crf', 'crf.id', '=', 'college_recommendation_filter_logs.rec_filter_id');
 		$filter_log = $filter_log->where('scholarship_id', $scholarship_id);
		$filter_log = $filter_log->where('category', $category);			
		$filter_log->delete();
 	}
}
