<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaUser extends Model
{
    protected $table = 'nrccua_users';
	protected $fillable = array( 'fname', 'lname', 'address', 'city', 'state', 'zip', 'gender', 'user_id', 'birth_date', 'is_manual', 'phone' );
}
