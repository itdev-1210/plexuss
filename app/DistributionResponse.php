<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionResponse extends Model {

	protected $table = 'distribution_responses';

 	protected $fillable = array('url', 'params', 'submitted_by_user_id', 'user_id', 'response', 'dc_id', 'ro_id', 'success', 'error_msg', 'manual');
}
