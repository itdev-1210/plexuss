<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AgencyRecruitment;

class AgencyUserBucketLogs extends Model {
	protected $table = 'agency_user_bucket_logs';

	protected $fillable = array('agency_recruitment_id', 'agency_bucket_id', 'active', 'updated_at', 'created_at');

	public $timestamps = true;


	// Uses the agency_id and user_id to lookup the agency recruitment table to log current bucket.
	public static function logCurrent($agency_id, $user_id) {
		$values = [];

		$query = AgencyRecruitment::on('rds1')
								  ->select('id as agency_recruitment_id', 'agency_bucket_id')
								  ->where('agency_id', $agency_id)
								  ->where('user_id', $user_id)
								  ->first();

		if (!isset($query)) {
			return 'fail';
		}

		AgencyUserBucketLogs::where('agency_recruitment_id', $query->agency_recruitment_id)
					     	->update(['active' => 0]);
		

		$values['agency_recruitment_id'] = $query->agency_recruitment_id;
		$values['agency_bucket_id'] = $query->agency_bucket_id;
		$values['active'] = 1;

		AgencyUserBucketLogs::create($values);

		return 'success';
	}
}
