<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTransaction extends Model {
	
	protected $table = 'product_transactions';

 	protected $fillable = array( 'ccp_id', 'user_ccp_info_id', 'state', 'payment_id', 'price', 'payer_id', 'token');
}