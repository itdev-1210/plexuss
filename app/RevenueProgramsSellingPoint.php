<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevenueProgramsSellingPoint extends Model
{
    protected $table = 'revenue_pograms_selling_point';
    protected $fillable = [
        'rp_id',
        'selling_point',
        'created_at',
        'updated_at',
    ];
}
