<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSuppressionList extends Model
{
    protected $table = 'email_suppression_lists';

 	protected $fillable = array('recipient', 'description', 'source', 'type', 'non_transactional', 'uid', 'uiid');

 	public function add($arr){
 		$attr = $arr;
 		$val  = $arr;

 		$update = EmailSuppressionList::updateOrCreate($attr, $val);
 	}
}
