<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;

class UserAccountSettings extends Model
{
    protected $table = 'sn_users_account_settings';

    protected $fillable = ['user_id', 'is_incognito', 'receive_messages', 'receive_requests', 'appear_in_search', 'appear_in_suggestions', 'show_lname', 'show_profile_pic', 'show_school', 'created_at', 'updated_at'];

    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getUserAccountSettings($user_id) {
        $account_settings = DB::connection('rds1')->table('sn_users_account_settings as snas')
                                                    ->where('snas.user_id', $user_id)
                                                    ->select('snas.user_id', 'snas.is_incognito', 'snas.receive_messages', 'snas.receive_requests', 'snas.appear_in_search', 'snas.appear_in_suggestions', 'snas.show_lname', 'snas.show_profile_pic', 'snas.show_school')
                                                    ->first();
        if (isset($account_settings->user_id)) {
            $cr = new Controller;                                            
            $account_settings->user_id = $cr->hashIdForSocial($account_settings->user_id);
        }                                            
        
        return $account_settings;
    }
    public function getSingleAccountSetting($user_id, $setting) {
        $setting_str = 'snas.' . $setting;
        $account_settings = DB::connection('rds1')->table('sn_users_account_settings as snas')
                                                    ->where('snas.user_id', $user_id)
                                                    ->select($setting_str)
                                                    ->first();

        return $account_settings;
    }

    public static function insertOrUpdate($data) {
        if (!isset($data['user_id'])) {
            return 'Missing user_id';
        }

        $attributes = [
            'user_id' => $data['user_id'],
        ];

        $values = [
            'user_id' => $data['user_id'],
            'is_incognito' => $data['is_incognito'],
            'receive_messages' => $data['receive_messages'],
            'receive_requests' => $data['receive_requests'],
            'appear_in_search' => $data['appear_in_search'],
            'appear_in_suggestions' => $data['appear_in_suggestions'],
            'show_lname' => $data['show_lname'],
            'show_profile_pic' => $data['show_profile_pic'],
            'show_school' => $data['show_school'],
        ];

        UserAccountSettings::updateOrCreate($attributes, $values);

        return 'success';
    }
}
