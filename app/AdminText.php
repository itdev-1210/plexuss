<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminText extends Model
{
    protected $table = 'admin_texts';

 	protected $fillable = array('org_branch_id', 'num_of_free_texts', 'num_of_eligble_texts', 'tier', 'expires_at');
}
