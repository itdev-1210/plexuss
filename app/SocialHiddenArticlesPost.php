<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialHiddenArticlesPost extends Model
{
    protected $table = 'sn_hidden_articles_posts';

 	protected $fillable = array( 'user_id', 'post_id', 'social_article_id');
}
