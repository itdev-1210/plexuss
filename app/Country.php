<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Cache;

class Country extends Model
{
    protected $table = 'countries';

	public function getAllCountries(){
		return Country::on('rds1')->get();
	}

	public function getUsersCountryName( $user_id = null ){
		if( !isset($user_id) ){
			return null;
		}

		$country = User::on('rds1')->where('users.id', $user_id)
						->join('countries as c', 'c.id', '=', 'users.country_id')
						->select('c.country_name')
						->first();

		return $country;
	}

	public function getAllCountriesAndAreaCodes(){
		$ctry = Country::on('rds1')
					   ->orderBy('id','ASC')
					   ->get();

		$temp = array();

		foreach ($ctry as $key) {
			$tmp = array();
			$tmp['country_id'] = $key->id;
			$tmp['country_phone_code'] = $key->country_phone_code;
			$tmp['country_code'] = $key->country_code;
			$tmp['country_name'] = $key->country_name;

			$temp[] = $tmp;
		}

		return $temp;
	}

	public function getAllCountriesAndIds(){

		if (Cache::has(env('ENVIRONMENT') .'_'.'getAllCountriesAndIds')) {
			$temp = Cache::get(env('ENVIRONMENT') .'_'.'getAllCountriesAndIds');
		}
		$ctry = Country::on('rds1')
					   ->orderBy('id','ASC')
					   ->get();

		$temp = array();

		foreach ($ctry as $key) {
			$temp[$key->id] = $key->country_name;
		}

		Cache::put(env('ENVIRONMENT') .'_'.'getAllCountriesAndIds', $temp, 640);
		
		return $temp;
	}

	public function getAllCountriesAndIdsWithCountryCode(){

		if (Cache::has(env('ENVIRONMENT') .'_'.'getAllCountriesAndIdsWithCountryCode')) {
			$temp = Cache::get(env('ENVIRONMENT') .'_'.'getAllCountriesAndIdsWithCountryCode');
		}
		$ctry = Country::on('rds1')
					   ->orderBy('id','ASC')
					   ->get();

		$temp = array();

		foreach ($ctry as $key) {
			$temp[$key->id] = $key->country_name ." (+".$key->country_phone_code.")" ;
		}

		Cache::put(env('ENVIRONMENT') .'_'.'getAllCountriesAndIdsWithCountryCode', $temp, 640);
		
		return $temp;
	}

	public function getUniqueAreaCodes(){
		$ctry = Country::on('rds1')
					   	->orderBy('id','ASC')
					   	->groupBy('country_phone_code')
					   	->get();

		$temp = array();

		foreach ($ctry as $key) {
			$tmp = array();
			$tmp['country_id'] = $key->id;
			$tmp['country_phone_code'] = $key->country_phone_code;
			$tmp['country_code'] = $key->country_code;
			$tmp['country_name'] = $key->country_name;

			$temp[] = $tmp;
		}

		return $temp;
	}

	public function getCountriesWithNameId(){
		$ctry = Country::on('rds1')
					   ->orderBy('id','ASC')
					   ->get();

		$temp = array();

		foreach ($ctry as $key) {
			$tmp = array();
			$tmp['id'] = (int)$key->id;
			$tmp['name'] = $key->country_name;
			$tmp['abbr'] = $key->country_code;
			$tmp['code'] = $key->country_phone_code;

			$temp[] = $tmp;
		}

		return $temp;
	}

        public function getAvailableCollegeCountriesWithNameId(){
        $ctry = Country::on('rds1')
                       ->select('countries.id', 'countries.country_name', 'countries.country_code', 'countries.country_phone_code')
                       ->whereRaw('countries.id in (SELECT distinct country_id from colleges where verified = 1)')
                       ->orderBy('id','ASC')
                       ->get();

        $temp = array();

        foreach ($ctry as $key) {
            $tmp = array();
            $tmp['id'] = (int)$key->id;
            $tmp['name'] = $key->country_name;
            $tmp['abbr'] = $key->country_code;
            $tmp['code'] = $key->country_phone_code;

            $temp[] = $tmp;
        }

        return $temp;
    }

    public function getCountriesWithNameIdAsKeys(){
        $countries = Country::on('rds1')
               ->orderBy('id','ASC')
               ->get();

        $response = array();

        foreach ($countries as $country) {
            $response[$country->country_code] = $country->country_name;
        }

        return $response;
    }
}
