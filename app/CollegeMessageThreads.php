<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CollegeMessageThreads extends Model
{
    protected $table = 'college_message_threads';

	protected $fillable = ['name', 'is_chat', 'plexuss_note', 'campaign_id', 'updated_at', 'has_text'];

	private $cmt;

	public function __construct($thread_id = null){


		if ($thread_id != null) {
			$this->cmt = CollegeMessageThreads::find($thread_id);
		}
	}

	public function updatePlexussNote($note){

		$cmt = $this->cmt;

		$cmt->plexuss_note = $note;

		$cmt->save();

		return $cmt->updated_at;
	}

	public function getPlexussNote(){

		$cmt = $this->cmt;

		$data['note'] = $cmt->plexuss_note;
		$data['note_date'] = $cmt->updated_at;
		
		return $data;
	}

	public function getTotalNumOfCampaignMessages($campaign_id_arr, $this_org_user_ids){

		$cmt = DB::connection('rds1')->table('college_message_threads as cmt')
									 ->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id')
									 ->join('users as u', 'u.id', '=', 'cmtm.user_id')
									 ->whereIn('cmt.campaign_id', $campaign_id_arr)
									 ->whereNotIn('cmtm.user_id', $this_org_user_ids)
									 ->count();

		return $cmt;							 
	}

	public function getTotalNumOfCampaignReadMessages($campaign_id_arr, $this_org_user_ids){

		$cmt = DB::connection('rds1')->table('college_message_threads as cmt')
									 ->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id')
									 ->join('users as u', 'u.id', '=', 'cmtm.user_id')
									 ->whereIn('cmt.campaign_id', $campaign_id_arr)
									 ->whereNotIn('cmtm.user_id', $this_org_user_ids)
									 ->where('cmtm.num_unread_msg', 0)
									 ->count();

		return $cmt;							 
	}

	public function getTotalNumOfCampaignRepliedMessages($campaign_id_arr, $this_org_user_ids){

		$cmt = DB::connection('rds1')->table('college_message_threads as cmt')
									 ->join('college_message_thread_members as cmtm', 'cmt.id', '=', 'cmtm.thread_id')
									 ->join('users as u', 'u.id', '=', 'cmtm.user_id')
									 ->whereIn('cmt.campaign_id', $campaign_id_arr)
									 ->whereNotIn('cmtm.user_id', $this_org_user_ids)
									 ->where('cmtm.num_of_sent_msg', '!=', 0)
									 ->count();

		return $cmt;							 
	}

	public function getThisCollegeChatThread($org_branch_id = null){

		if ($org_branch_id == null) {
			return;
		}

		$cmt = DB::connection('rds1')
			      ->table('college_message_threads AS cmt')
				  ->join('college_message_thread_members AS cmtm', 'cmt.id', '=', 'cmtm.thread_id')
			      ->join('organization_branch_permissions as obp', 'obp.user_id', '=', 'cmtm.user_id')
			      ->where('cmtm.org_branch_id', $org_branch_id)
			      ->where('cmt.is_chat', 1)
			      ->groupBy('cmt.id')
			      ->select('cmt.id as thread_id')
			      ->first();
			      
		return $cmt;
	}



	/*********************************************
	*  gets thread between a specific college and student user
	*
	*********************************************/
	public function getMessageThreadId($uid, $cid){

		$results = null;

		$qryStr = "Select cmtm2.thread_id from
						(select cmtm.thread_id from college_message_thread_members as cmtm
						where user_id = ".$uid.") as f1
					join college_message_thread_members as cmtm2 
					on cmtm2.thread_id = f1.thread_id
					and cmtm2.user_id = ".$cid. "
					group by cmtm2.thread_id
					";


		$results = DB::connection('rds1')
		->select(DB::raw( $qryStr));

		// dd($results);

		if(isset($results[0])){

			$threadId = $results[0]->thread_id;

			return $threadId;
		}
		else{

			return 'error occured';
		}


	}
}
