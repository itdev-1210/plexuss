<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaQueue extends Model
{
    protected $table = 'nrccua_queues';

    protected $fillable = array('ro_id', 'user_id', 'college_id',  'created_at', 'updated_at', 'manual');
}
