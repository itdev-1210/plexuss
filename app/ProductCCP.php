<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCCP extends Model {
	
	protected $table = 'product_ccp';

 	protected $fillable = array( 'name', 'price', 'stock');
}