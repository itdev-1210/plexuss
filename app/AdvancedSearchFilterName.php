<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use DB;
use App\AdvancedSearchFilterLog, App\AdvancedSearchFilter;
use App\Http\Controllers\ViewDataController;

class AdvancedSearchFilterName extends Model
{
    	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'advanced_search_filter_names';


 	protected $fillable = array( 'college_id', 'agency_id', 'org_branch_id', 'name' );

 	public function AdvancedSearchFilters(){
        return $this->hasMany('AdvancedSearchFilter');
    }

 	public function saveAdvancedSearchFilter($data, $input){

 		// echo "<pre>";
 		// print_r($input);
 		// echo "</pre>";
 		// exit();

 		$this->getAdvancedSearchFilter(9);

 		$this->clearAdvancedSearchFilter($data, $input);

 		$asfn = new AdvancedSearchFilterName;

 		if (isset($data['agency_collection'])) {
 			$asfn->agency_id = $data['agency_collection']->agency_id;
 		}else{
 			$asfn->college_id    = $data['org_school_id'];
 			$asfn->org_branch_id = $data['org_branch_id'];

 		}

 		$asfn->name = $input['save_template_name_val'];
 		$asfn->save();
 		$asfn_id = $asfn->id;

 		unset($input['save_template_name_val']);
 		unset($input['_token']);
 		unset($input['savedFilters']);

 		foreach ($input as $key => $value) {
 			$asf = new AdvancedSearchFilter;
 			$asf->asfn_id = $asfn_id;
 			$asf->name = $key;

 		    $asf->save();
 			$as_filter_id = $asf->id;
 			
 			if (is_array($value)) {
 				foreach ($value as $k => $v) {
 					$asfl = new AdvancedSearchFilterLog;
 					$asfl->as_filter_id = $as_filter_id;
 					$asfl->val = $v;
 					$asfl->save();
 				}
 			}else{
 				$asfl = new AdvancedSearchFilterLog;
 				$asfl->as_filter_id = $as_filter_id;
				$asfl->val = $value;
				$asfl->save();
 			}	
 		}

 		return Crypt::encrypt($asfn_id);
 	}

 	public function clearAdvancedSearchFilter($data, $input){

 		$asfn = AdvancedSearchFilterName::where('name', $input['save_template_name_val']);

 		if (isset($data['agency_collection'])) {
 			$asfn = $asfn->where('agency_id', $data['agency_collection']->agency_id);
 		}else{
 			$asfn = $asfn->where('org_branch_id', $data['org_branch_id'])
 						 ->where('college_id', $data['org_school_id']);
 		}

 		$asfn = $asfn->where('name', $input['save_template_name_val'])->first();

 		if (isset($asfn)) {
 			$asfn->delete();
 		}
 	}

 	public function getAdvancedSearchFilter($id){

 		$tmp = DB::connection('rds1')->table('advanced_search_filter_names as asfn')
 									 ->join('advanced_search_filters as asf', 'asfn.id', '=', 'asf.asfn_id')
 									 ->join('advanced_search_filter_logs as asfl', 'asf.id', '=', 'asfl.as_filter_id')
 									 ->select('asf.name as filter', 'asfl.val')
 									 ->where('asfn.id', $id)
 									 ->get();
 		$ret = array();
 		foreach ($tmp as $key) {
 			if (isset($ret[$key->filter])) {
 				$tmp = $ret[$key->filter];
 				if (is_array($tmp)) {
 					$tmp[] = $key->val;
 					$ret[$key->filter] = $tmp;
 				}else{
 					$arr = array();
 					$arr[] = $ret[$key->filter];
 					$arr[] = $key->val;

 					$ret[$key->filter] = $arr;
 				}
 			}else{
 				if ($key->filter == 'country' || $key->filter == 'state' || 
 					$key->filter == 'city'    || $key->filter == 'department' ||
 					$key->filter == 'major'   || $key->filter == 'religion' || 
 					$key->filter == 'ethnicity'){
 					$ret[$key->filter][] = $key->val;
 				}else{
 					$ret[$key->filter] = $key->val;
 				}
 				
 			}
 		}

 		return $ret;
 	}

 	public function getAllAdvancedSearchFilter(){
 		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData();

		if (isset($data['agency_collection'])) {
			$asfn = AdvancedSearchFilterName::where('agency_id', $data['agency_collection']->agency_id)
											->get();
		}else{
			$asfn = AdvancedSearchFilterName::where('org_branch_id', $data['org_branch_id'])
											->where('college_id', $data['org_school_id'])
											->get();
		}
		
		$temp = array();
		$temp[''] = 'Load Filter...';
		foreach ($asfn as $key) {
			$temp[Crypt::encrypt($key->id)] = $key->name;
		}
		return $temp;
	}
}
