<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schoolClass extends Model{
	
	protected $table = 'classes';

	public function subjects(){
		return $this->belongsTo('Subject');
	}
}
