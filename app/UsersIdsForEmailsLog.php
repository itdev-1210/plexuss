<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersIdsForEmailsLog extends Model
{
    protected $table = 'users_ids_for_emails_logs';

    protected $fillable = array('uife_id');
}
