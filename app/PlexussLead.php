<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlexussLead extends Model {

	protected $connection = 'ldy';
	protected $table = 'plexuss_leads';	

	protected $fillable = array( 'tt_id' );
}