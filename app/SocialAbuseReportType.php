<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAbuseReportType extends Model
{
    protected $table = 'sn_abuse_report_types';

	protected $fillable = array( 'name' );
}
