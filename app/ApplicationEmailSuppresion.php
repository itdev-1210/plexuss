<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationEmailSuppresion extends Model
{
    protected $table = 'application_email_suppressions';

 	protected $fillable = array('no_more_email', 'is_supressed', 'user_id', 'template_name');
}
