<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrescreenedUser extends Model {

	protected $table = 'prescreened_users';

 	protected $fillable = array('user_id', 'college_id', 'aor_id', 'org_portal_id', 'aor_portal_id', 'active', 'prev_state', 'rec_id', 'note', 'applied', 'applied_at', 'enrolled', 'enrolled_at');

 	public function getTotalNumOfPrescreened($input){
 		$pu = PrescreenedUser::on('bk')->where('college_id', $input['college_id'])
 									   ->where('active', 1);

 		if (isset($input['user_id'])) {
 			$pu = $pu->where('user_id', $input['user_id']);
 		}

 		if (isset($input['aor_id'])) {
 			$pu = $pu->where('aor_id', $input['aor_id']);
 		}else{
 			$pu = $pu->whereNull('aor_id');
 		}

 		$org_portal_id = NULL;
		if (isset($input['default_organization_portal'])) {
			$org_portal_id = $input['default_organization_portal']->id;
		}

		if (isset($org_portal_id)) {
 			$pu = $pu->where('org_portal_id', $org_portal_id);
 		}

 		if (isset($input['aor_portal_id'])) {
 			$pu = $pu->where('aor_portal_id', $input['aor_portal_id']);
 		}

 		$pu = $pu->whereNotIn('user_id', function($query) use ($input) {
			$query = $query->select('rva.user_id')
						   ->from('recruitment_verified_apps as rva')
						   ->where('rva.college_id', '=', $input['college_id']);

			if (isset($input['default_organization_portal']) && !empty($input['default_organization_portal'])) {
				$query->where('rva.org_portal_id', $input['default_organization_portal']->id);
			}

			$query->get();
		});

 		$cnt = $pu->count(DB::raw("DISTINCT user_id"));

 		return $cnt;
 	}
}