<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesByAgency extends Model {


	protected $table = 'services_by_agencies';

 	protected $fillable = array( 'agency_id', 'name' );

}