<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionPlexussFieldName extends Model {

	protected $table = 'distribution_plexuss_field_names';

 	protected $fillable = array('raw_field_name', 'field_name', 'table_name');
}
