<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use DateTime;

class AgencyUserReview extends Model
{
    protected $table = 'agency_user_reviews';

 	protected $fillable = array( 'user_id', 'agency_id', 'comment', 'rating' );

 	public function add($data){

 		$attr = array('user_id' => $data['user_id'], 'agency_id' => $data['agency_id']);
 		$val  = array('user_id' => $data['user_id'], 'agency_id' => $data['agency_id'], 'comment' => $data['comment'], 'rating' => $data['rating']);

 		AgencyUserReview::updateOrCreate($attr, $val);
 		return "success";
 	}

 	public function getReviews($agency_id){
 		$qry = DB::connection('rds1')->table('agency_user_reviews as apr')
 									 ->join('users as u', 'u.id', '=', 'apr.user_id')
 									 ->leftjoin('countries as co', 'co.id', '=', 'u.country_id')
 									 ->where('apr.agency_id', $agency_id)
 									 ->select('u.fname', 'u.lname', 'u.city', 'u.state', 'u.profile_img_loc', 'co.country_name', 'apr.comment', 'apr.rating', 'apr.created_at')
 									 ->orderBy('apr.created_at', 'DESC')
 									 ->get();

 		$ret = array();

 		foreach ($qry as $key) {
 			$tmp = array();
 			$tmp['name'] = ucwords(strtolower($key->fname . ' ' . $key->lname));
 			$tmp['location'] = '';
 			$tmp['profile_img_url'] = $key->profile_img_loc;
 			isset($key->city) ? $tmp['location'] .= $key->city.', ' : null;
 			isset($key->state) ? $tmp['location'] .= ' '.$key->state.', ' : null;
 			isset($key->country_name) ? $tmp['location'] .= $key->country_name : null;
 			$tmp['comment'] = $key->comment;
 			$tmp['rating']  = $key->rating;
 			$date = Carbon::parse($key->created_at);
 			$dateObj   = DateTime::createFromFormat('!m', $date->month);
			$monthName = $dateObj->format('F'); 
 			$tmp['date'] = $monthName . ' ' . $date->day;

 			$ret[] = $tmp;
 		}

 		return $ret;
 	}

 	public static function getAverage($agency_id) {
		$review = AgencyUserReview::on('rds1')->where('agency_id', $agency_id);

		return $review->avg('rating');
 	}
}
