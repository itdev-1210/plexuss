<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScholarshipAdminUser extends Model
{
    protected $table = 'scholarship_admin_users';

    protected $fillable = array( 'user_id', 'only_scholarship', 'active' );
}
