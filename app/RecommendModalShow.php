<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecommendModalShow extends Model
{
  protected $table = 'recommend_modal';

 	protected $fillable = array( 'user_id', 'recommend_modal_show' , 'home_modal_show' );
}
