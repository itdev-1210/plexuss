<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalCollegeContactInfoLog extends Model
{
    //
    protected $table = 'internal_college_contact_info_logs';

    protected $fillable = array('sent_email_id', 'icci_id', 'template_id');
}
