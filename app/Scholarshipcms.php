<?php

namespace App;
use DB;
use App\ScholarshipsUserApplied;
use Illuminate\Database\Eloquent\Model;

class Scholarshipcms extends Model
{
    protected $table = "scholarship_verified";

    protected $fillable = array( 'id', 'scholarship_title', 'deadline', 'num_of_awards', 'max_amount', 'website', 'description', 'submission_id', 'provider_id', 'recurring', 'active','college_id','scholarshipsub_title');

 
    /***************************************
    *  just get all scholarships in verified scholarship table that are active
    *  used in sales
    *****************************************/
 	public function getAllScholarships($user_id){

 		$results = DB::connection('rds1')->table('scholarship_admin_users as sau')
 										->join('scholarship_providers as sp', 'sp.id', '=', 'sau.scholarship_org_id')
 		                                ->join('scholarship_verified as sv', 'sv.provider_id', '=', 'sp.id')
 										->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
												  'sv.scholarshipsub_title as scholarshipsub_name', 
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description');
 										 $results = $results->where('sv.active', '=', 1);
										 $results = $results->where('sau.user_id', '=',$user_id);
										 
 										 $results = $results->get();
										 $finalRst  = array();
										 foreach($results as $key => $value){
											$finalRst[$key] = $value;
											$results2 = DB::connection('rds1')->table('college_recommendation_filters as crf')
										 		->leftjoin('college_recommendation_filter_logs as crfl', 'crfl.rec_filter_id', '=', 'crf.id');
												$results2 = $results2->select('crf.type', 'crf.category', 'crf.name', 'crfl.val');
												$results2 = $results2->where('crf.scholarship_id', $finalRst[$key]->id);
												
												$departments_as_majors = clone $results2;
												$departments_as_majors = $departments_as_majors
												->where('category', "majorDeptDegree")
												->where('crfl.val', 'NOT LIKE', "%,,%")
												->join('department as d', 'd.id', '=', DB::raw("substring_index(val,',',1)"))
												->join('majors as m', 'm.name', '=', 'd.name')
												->select('crf.type', 'crf.category', 'crf.name',
														 DB::raw("concat(d.id, ',', m.id, ',', substring_index(val,',',-1)) as val"));
												$results2 = $results2->union($departments_as_majors);
												$results2 = $results2->get();
												
												//---------------------------------------------
												$ret = array();
												$name = '';
												$key1 = $key;
												$key ='';
												if (isset($results2)) {
													$temp = array();
													foreach ($results2 as $key) {
														if ($name == '') {
															$name = $key->name;
										
															$temp = array();
															$temp['type'] = $key->type;
															$temp['category'] = $key->category;
															$temp['filter'] = $key->name;
															if (isset($key->val)) {
																$temp[$key->name] = array();
																$temp[$key->name][] = $key->val;
															}
														}elseif($name == $key->name){
										
															if (isset($key->val)) {
																$temp[$key->name][] = $key->val;
															}
															$name = $key->name;
														}else{
															$ret[] = $temp;
															$name = $key->name;
															$temp = array();
															$temp['type'] = $key->type;
															$temp['category'] = $key->category;
															$temp['filter'] = $key->name;
															if (isset($key->val)) {
																$temp[$key->name] = array();
																$temp[$key->name][] = $key->val;
															}
														}
													}
													$ret[] = $temp;
												}
												//---------------------------------------------
												$finalRst[$key1]->filter = $ret;
										 }
										 
		return $finalRst;
 	} 
	
	
	/*******************************
	* only gets scholarships with finish status
	********************************/
 	public function getUserSubmitScholarships($uid){

 		if(!isset($uid)) return 'error';

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->join('scholarship_providers as sp', 
 										 	'sv.provider_id', '=','sp.id')
 										 ->leftJoin('scholarships_user_applied as sua', function($qry) use ($uid){
 										 	   $qry->on('sua.scholarship_id', '=', 'sv.id')
 										 	   ->where('sua.user_id', '=', $uid);
 										 })
 										 	
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
 										 		  'sv.website',
 										 		  'sv.num_of_awards as numberof',
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description',
 										 		  'sp.company_name as provider_name',
 										 		  'sp.id as provider_id',
 										 		  'sp.contact_fname',
 										 		  'sp.contact_lname',
 										 		  'sp.phone as provider_phone',
 										 		  'sp.email as provider_email',
 										 		  'sp.address as provider_address',
 										 		  'sp.city as provider_city',
 										 		  'sp.state as provider_state',
 										 		  'sp.zip as provider_zip',
 										 		  'sp.country_id as provider_country',
 										 		  'sua.status')
 										 ->where('sua.status' , '=', 'finish')
 										 ->get();
 		return $results;
 	}


 	/*******************************
 	* get all schoalrships with status information for user and filters applied
 	* includes removed, closed,..
 	*****************************************/
 	public function getAllScholarshipsWithStatus($uid, $filters){

 		if(!isset($uid)) return 'error';

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->join('scholarship_providers as sp', 
 										 	'sv.provider_id', '=','sp.id')
 										 ->leftJoin('scholarships_user_applied as sua', function($qry) use ($uid){
 										 	   $qry->on('sua.scholarship_id', '=', 'sv.id')
 										 	   ->where('sua.user_id', '=', $uid);
 										 })
 										 	
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
 										 		  'sv.website',
 										 		  'sv.num_of_awards as numberof',
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description',
 										 		  'sp.company_name as provider_name',
 										 		  'sp.id as provider_id',
 										 		  'sp.contact_fname',
 										 		  'sp.contact_lname',
 										 		  'sp.phone as provider_phone',
 										 		  'sp.email as provider_email',
 										 		  'sp.address as provider_address',
 										 		  'sp.city as provider_city',
 										 		  'sp.state as provider_state',
 										 		  'sp.zip as provider_zip',
 										 		  'sp.country_id as provider_country',
 										 		  'sua.status');

 		if(isset($filters['rangeF']) ){
 			$range = explode(",", $filters['rangeF']);
	 		$results = $results->where('sv.max_amount', '>', $range[0]);

	 		if($range[1] != -1){
		 		$results = $results->where('sv.max_amount', '<', $range[1]);
	 		}

 		}


 		$results = $results->groupBy('sv.id')->get();
 										
 		$num = 0;
 		
 		foreach($results as $r ){
 			if($r->status == 'finish'){
 				$num++;
 			}
 		}								 

 		$res = array();

 		$res[] = $num;
 		$res[] = $results;
 		return $res;
 	}

 	/********************************
 	* get only scholarships that a user has not interacted with -- not applied to, added,...
 	* used on "search" page
 	*********************************/
 	public function getAllScholarshipsNotApplied($uid, $filters){

 		if(!isset($uid)) return 'error';

 		$res = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->join('scholarship_providers as sp', 
 										 	'sv.provider_id', '=','sp.id')
 										 ->leftJoin('scholarships_user_applied as sua', function($qry) use ($uid){
 										 	   $qry->on('sua.scholarship_id', '=', 'sv.id')
 										 	   ->where('sua.user_id', '=', $uid);
 										 })
 										 	
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
 										 		  'sv.website',
 										 		  'sv.num_of_awards as numberof',
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description',
 										 		  'sp.company_name as provider_name',
 										 		  'sp.id as provider_id',
 										 		  'sp.contact_fname',
 										 		  'sp.contact_lname',
 										 		  'sp.phone as provider_phone',
 										 		  'sp.email as provider_email',
 										 		  'sp.address as provider_address',
 										 		  'sp.city as provider_city',
 										 		  'sp.state as provider_state',
 										 		  'sp.zip as provider_zip',
 										 		  'sp.country_id as provider_country',
 										 		  'sua.status')
 										 ->where('sv.active', '=', 1)
 										 ->where('sua.status', '=', null)
                                         ->orderBy('sv.max_amount', 'desc');

 		if(isset($filters['rangeF']) ){
 			$range = explode(",", $filters['rangeF']);
			$res = $res->where('sv.max_amount', '>=', $range[0]);
			
			if($range[0]==0){
				$res = $res->where('sv.max_amount', '<', $range[1]);
			}else{
				if($range[1] != -1){
		 			$res = $res->where('sv.max_amount', '<=', $range[1]);
	 			}
			}
	 	}


 		$res = $res->get();
 		
 		return $res;
 	}

    /********************************
    * get all scholarships that a user has not submitted yet
    * used on "search" page
    *********************************/
    public function getAllScholarshipsNotSubmitted($uid, $filters){

        if(!isset($uid)) return 'error';

        $res = DB::connection('rds1')->table('scholarship_verified as sv')
                                         ->join('scholarship_providers as sp', 
                                            'sv.provider_id', '=','sp.id')
                                         ->leftJoin('scholarships_user_applied as sua', function($qry) use ($uid){
                                               $qry->on('sua.scholarship_id', '=', 'sv.id')
                                               ->where('sua.user_id', '=', $uid);
                                         })
                                         ->select('sv.id', 
                                                  'sv.scholarship_title as scholarship_name', 
                                                  'sv.website',
                                                  'sv.num_of_awards as numberof',
                                                  'sv.deadline', 
                                                  'sv.created_at',
                                                  'sv.max_amount as amount',
                                                  'sv.description',
                                                  'sp.company_name as provider_name',
                                                  'sp.id as provider_id',
                                                  'sp.contact_fname',
                                                  'sp.contact_lname',
                                                  'sp.phone as provider_phone',
                                                  'sp.email as provider_email',
                                                  'sp.address as provider_address',
                                                  'sp.city as provider_city',
                                                  'sp.state as provider_state',
                                                  'sp.zip as provider_zip',
                                                  'sp.country_id as provider_country',
                                                  'sua.status')
                                         ->where('sv.active', '=', 1)
                                         ->where(function($q) {
                                            $q->orWhereNull('sua.status')
                                              ->orWhere('sua.status', '=', 'finish');
                                         })
                                         ->orderBy('sua.status', 'desc')
                                         ->orderBy('sv.max_amount', 'desc');

        if(isset($filters['rangeF']) ){
            $range = explode(",", $filters['rangeF']);
            $res = $res->where('sv.max_amount', '>=', $range[0]);
            
            if($range[0]==0){
                $res = $res->where('sv.max_amount', '<', $range[1]);
            }else{
                if($range[1] != -1){
                    $res = $res->where('sv.max_amount', '<=', $range[1]);
                }
            }
        }


        $res = $res->get();
        
        return $res;
    }


 	/***************************************
 	*  get all scholarship with filters applied, no user information
 	*  used on search page when not logged on
 	*****************************************/
 	public function getAllScholarshipsFilters($filters){

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->join('scholarship_providers as sp', 'sv.provider_id', '=','sp.id')
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
 										 		  'sv.website',
 										 		  'sv.num_of_awards as numberof',
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description',
 										 		  'sp.company_name as provider_name',
 										 		  'sp.id as provider_id',
 										 		  'sp.contact_fname',
 										 		  'sp.contact_lname',
 										 		  'sp.phone as provider_phone',
 										 		  'sp.email as provider_email',
 										 		  'sp.address as provider_address',
 										 		  'sp.city as provider_city',
 										 		  'sp.state as provider_state',
 										 		  'sp.zip as provider_zip',
 										 		  'sp.country_id as provider_country')
 										 ->where('sv.active', '=', 1)
                                         ->orderBy('sv.max_amount', 'desc');

 		/*if(isset($filters['rangeF']) ){
 			$range = explode(",", $filters['rangeF']);
	 		$results = $results->where('sv.max_amount', '>', $range[0]);

	 		if($range[1] != -1){
		 		$results = $results->where('sv.max_amount', '<', $range[1]);
	 		}

 		}*/
		
		if(isset($filters['rangeF']) ){
 			$range = explode(",", $filters['rangeF']);
			$results = $results->where('sv.max_amount', '>=', $range[0]);
			
			if($range[0]==0){
				$results = $results->where('sv.max_amount', '<', $range[1]);
			}else{
				if($range[1] != -1){
		 			$results = $results->where('sv.max_amount', '<=', $range[1]);
	 			}
			}
	 	}


 		$results = $results->get();

 		return $results;
 	} 

 	/********************************************/
 	public function searchScholarships($term){

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->join('scholarship_providers as sp', 'sv.provider_id', '=','sp.id')
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name', 
 										 		  'sv.website',
 										 		  'sv.num_of_awards as numberof',
 										 		  'sv.deadline', 
 										 		  'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description',
 										 		  'sp.company_name as provider_name',
 										 		  'sp.id as provider_id',
 										 		  'sp.contact_fname',
 										 		  'sp.contact_lname',
 										 		  'sp.phone as provider_phone',
 										 		  'sp.email as provider_email',
 										 		  'sp.address as provider_address',
 										 		  'sp.city as provider_city',
 										 		  'sp.state as provider_state',
 										 		  'sp.zip as provider_zip',
 										 		  'sp.country_id as provider_country');

 										

	 	if(isset($term))
	 		$results = $results->where('sv.scholarship_title', 'LIKE', '%'.$term.'%');
 										
 		$results = $results->get();

 		return $results;
 	}


 	/***************************************/
 	public function deleteScholarship($id){

 		//$update = ScholarshipsUserApplied::where('scholarship_id', $id)->update(array('status' => 'closed'));
 		$res    = Scholarship::where('scholarship_verified.id', '=', $id)->update(array('active' => 0));

 		return $res;
 	}

	public function checkScholarshipExist($college_d){
		return $check = DB::table("scholarship_verified")
		->where("college_id", "=", $college_d)
		->count();
	}
	
	public function checkScholarship($college_d){
		return $check = DB::table("scholarship_verified")
		->where("college_id", "=", $college_d)
		->first();
	}
	
	public function checkScholarshipAdminExist($user_id){
		return $check = DB::table("scholarship_verified")
		->where("user_id", "=", $user_id)
		->count();
	}
	
	public function checkScholarshipAdmin($scholarship_id){
		return $check = DB::table("scholarship_verified")
		->where("id", "=", $scholarship_id)
		->first();
	}
	
	public function getScholarshipsCms($college_id = 0){

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 										 ->select('sv.id', 
 										 		  'sv.scholarship_title as scholarship_name',
												  'sv.scholarshipsub_title as scholarshipsub_name', 
 										 		  'sv.deadline', 
 										 		   'sv.created_at',
 										 		  'sv.max_amount as amount',
 										 		  'sv.description'
 										 		  );
 										 $results = $results->where('sv.active', '=', 1);
										 $results = $results->where('sv.college_id', '=',$college_id);
										 
 										 $results = $results->get();
										 
										 $finalRst  = array();
										 foreach($results as $key => $value){
											$finalRst[$key] = $value;
											$results2 = DB::connection('rds1')->table('college_recommendation_filters as crf')
										 		->leftjoin('college_recommendation_filter_logs as crfl', 'crfl.rec_filter_id', '=', 'crf.id');
												$results2 = $results2->select('crf.type', 'crf.category', 'crf.name', 'crfl.val');
												$results2 = $results2->where('crf.scholarship_id', $finalRst[$key]->id);
												
												$departments_as_majors = clone $results2;
												$departments_as_majors = $departments_as_majors
												->where('category', "majorDeptDegree")
												->where('crfl.val', 'NOT LIKE', "%,,%")
												->join('department as d', 'd.id', '=', DB::raw("substring_index(val,',',1)"))
												->join('majors as m', 'm.name', '=', 'd.name')
												->select('crf.type', 'crf.category', 'crf.name',
														 DB::raw("concat(d.id, ',', m.id, ',', substring_index(val,',',-1)) as val"));
												$results2 = $results2->union($departments_as_majors);
												$results2 = $results2->get();
												
												//---------------------------------------------
												$ret = array();
												$name = '';
												$key1 = $key;
												$key ='';
												if (isset($results2)) {
													$temp = array();
													foreach ($results2 as $key) {
														if ($name == '') {
															$name = $key->name;
										
															$temp = array();
															$temp['type'] = $key->type;
															$temp['category'] = $key->category;
															$temp['filter'] = $key->name;
															if (isset($key->val)) {
																$temp[$key->name] = array();
																$temp[$key->name][] = $key->val;
															}
														}elseif($name == $key->name){
										
															if (isset($key->val)) {
																$temp[$key->name][] = $key->val;
															}
															$name = $key->name;
														}else{
															$ret[] = $temp;
															$name = $key->name;
															$temp = array();
															$temp['type'] = $key->type;
															$temp['category'] = $key->category;
															$temp['filter'] = $key->name;
															if (isset($key->val)) {
																$temp[$key->name] = array();
																$temp[$key->name][] = $key->val;
															}
														}
													}
													$ret[] = $temp;
												}
												//---------------------------------------------
												$finalRst[$key1]->filter = $ret;
										 }
										 
		return $finalRst;
 	} 
	
	public function getAllScholarshipsadmin($scholarship_ids){

 		$results = DB::connection('rds1')->table('scholarship_verified as sv')
 				->select('sv.id', 
 				'sv.scholarship_title as scholarship_name', 
				'sv.scholarshipsub_title as scholarshipsub_name', 
 				'sv.deadline', 
 				'sv.max_amount as amount'
			);
 		 $results = $results->whereIn('sv.id',$scholarship_ids);
		 $results = $results->where('sv.active',1);
 		 $results = $results->get();
		 return $results;
 	} 
	
	public function deleteRec($data){
		$res = DB::table('scholarship_verified')
		->where($data)
		->update(array('active' => 0));
		return $res;
 	}
	
	public function checkScholarshipcmsExist($user_id,$scholarship_id){
		return $check = DB::connection('rds1')->table('scholarship_admin_users as sau')
 		                                      ->join('scholarship_verified as sv', 'sv.provider_id', '=', 'sau.scholarship_org_id')
										      ->where("sau.user_id", "=", $user_id)
										      ->where("sv.id", "=", $scholarship_id)
											  ->count();
	}
	
}
