<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB, Session;
use App\User;

class Major extends Model{

	protected $table = 'majors';
	
	protected $fillable = array('department_id', 'name');


	public function getMajorByDepartment($name = null, $id = null, $noDefaultOption = null){
		if ( $name == null && $id == null ) {
			return;
		}

		$major = DB::table('department as d')
					->join('majors as m', 'd.id', '=', 'm.department_id');

		if( !isset($id) ){
			$major = $major->where('d.name', $name);
		}else{
			$major = $major->where('d.id', $id);
		}

		$major = $major->orderby('m.name', 'ASC')
			  		   ->get();

		$temp = array();

		if( !isset($noDefaultOption) ){
			$temp[''] = 'Select...';
		}

		foreach ($major as $key) {
			if( !isset($id) ){
				$temp[$key->name] = $key->name;
			}else{
				$temp[$key->id] = $key->name;
			}
		}
		return $temp;
	}
	
	public function getAllMajorByDept(){
		$major = DB::table('department as d')
					->join('majors as m', 'd.id', '=', 'm.department_id');

		$major = $major->orderby('m.name', 'ASC')
			  		   ->get();
		$temp = array();

		foreach ($major as $key) {
			$tmp = array();
			$tmp['name'] = $key->name;
			$tmp['id'] = (int)$key->id;
			$temp[] = $tmp;
			//$temp[$key->id] = $key->name;
		}
		return $temp;
	}

	public function getMajorNameAndIdByDepartment($id = null){
		if ( $id == null ) {
			return;
		}

		$major = DB::table('department as d')
					->join('majors as m', 'd.id', '=', 'm.department_id');

		
		$major = $major->where('d.id', $id);

		$major = $major->orderby('m.name', 'ASC')
			  		   ->get();

		$temp = array();

		foreach ($major as $key) {
			$maj = array();
			$maj['id'] = (int)$key->id;
			$maj['name'] = $key->name;
			$temp[] = $maj;
		}
		
		return $temp;
	}
	public function findMajor($input = null, $user_id = null){
		if( !isset($input) ){
			return 'no results';
		}
		$result = array();

		$result['other_majors'] = DB::connection('rds1')->table('majors as m')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('m.name', 'like', '%'.$input.'%');
					})
					// ->limit(20)
					->get();
		
		return $result;
	}

	//this finds majors that are 'like' $input, so could return multiple results
	public function findSimilarMajor($input = null, $user_id = null){

		if( !isset($input) ){
			return 'no results';
		}

		$user = array();

		if (isset($user_id)) {
			$data['user_id'] = $user_id;
		}else{
			$data['user_id'] = Session::get('userinfo.id');	
		}

		$for_higher_ed  = false;

		if (isset($data['user_id'])) {
			$user = User::on('rds1')->where('id',$data['user_id'])
									->select(DB::raw("TIMESTAMPDIFF(YEAR, date(birth_date), CURDATE()) as age"), 'country_id')
									->first();
			if ($user->country_id == 1 && $user->age >= 24) {
				$for_higher_ed = true;
			}
		}
		
		$result = array();

		$splinput = explode(' ', $input);

		$dept = DB::connection('rds1')
			->table('majors as m')
			->join('department as d', 'm.department_id', '=', 'd.id')
			->join('majors as m2', 'm2.name', '=', 'd.name');

		foreach($splinput as $value){
			$dept = $dept->where('m.name', 'like', '%'.$value.'%');
		}
		$dept = $dept
			->groupBy('d.id')
			->orderBy(DB::raw('count(*)'), 'DESC');

		$dept2 = clone $dept;

		$result['department'] = $dept2->select('m2.*')->first();

		$dept = $dept->pluck('d.id');

		if(!empty($dept) && isset($result['department']->name)){
			$pop = DB::connection('rds1')
					->table('college_recommendation_filters as crf')
					->select('m.*')
					->join('college_recommendation_filter_logs as crfl', 'crf.id', '=', 'crfl.rec_filter_id')
					->join('priority', 'priority.college_id', '=', 'crf.college_id')
					->join('majors as m', 'm.id', '=', DB::raw("SUBSTRING_INDEX(SUBSTRING_INDEX(val, ',', - 2), ',', 1)"))
					->where('crf.name', '=', 'majorDeptDegree')
					->where('m.name', '!=', $result['department']->name)
					->where(DB::raw("SUBSTRING_INDEX(val, ',', 1)"), $dept)
					->groupBy('m.id')
					->orderBy(DB::raw('count(*)'), 'DESC')
					->take(5);

			if ($for_higher_ed) {
				$pop =  $pop->where('m.for_higher_ed', 1);
			}

			$pop_ids = clone $pop;
			$pop_ids = $pop_ids->pluck('id');
					
			$result['popular_majors'] = $pop->limit(10)->get();

			$tmp_qry = DB::connection('rds1')
					->table('majors as m')
					->whereNotIn('m.id', $pop_ids);

			foreach($splinput as $value){
				$tmp_qry = $tmp_qry->where('m.name', 'like', '%'.$value.'%');
			}

			if ($for_higher_ed) {
				$tmp_qry =  $tmp_qry->where('m.for_higher_ed', 1);
			}

			$result['other_majors'] = $tmp_qry->limit(10)->get();

		}else{

			// if no dept match was found, check if input contains char in the word undecided
			// if undecided, then set other_majors to that undecided major
			if( strpos( 'undecided', strtolower($input) ) !== false ){
				$result['other_majors'] = DB::connection('rds1')
								->table('majors')
								->where('id', 1473) // undecided major id
								->limit(10)
								->get();

			}else{
				$result['other_majors'] = array();
			}	
			
			$result['popular_majors'] = array();
		}

		return $result;
	}

	//different from above findMajor - this looks for the exact major searched and returns that one object
	public function findMajorByName($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('majors as m')
					->where('name', '=', $input)
					->first();
		
		return $result;
	}

	public function getAllMajors(){
		$result = DB::connection('rds1')
					->table('majors as m')
                    ->where('m.promote', '=', 1)
					->orderBy('m.name', 'ASC')
					->get();

		foreach ($result as $key) {
			$key->id = (int)$key->id;
			$key->department_id = (int)$key->department_id;
		}
		
		return $result;
	}

	public function getUsersMajors($major_ids){


		$m = Major::whereIn('id', $major_ids)->get();

		return $m;
	}

}
