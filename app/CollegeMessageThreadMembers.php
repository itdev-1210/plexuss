<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\CollegeRecommendationFilters, App\CollegeMessageThreadMembers;
use App\Http\Controllers\ViewDataController;
use App\Http\Controllers\Controller;

class CollegeMessageThreadMembers extends Model
{
    protected $table = 'college_message_thread_members';

	protected $fillable = ['agency_id', 'thread_id', 'user_id', 'org_branch_id',
						   'email_sent', 'num_unread_msg', 'is_list_user'];

	private $org_branch_id;

	public function getAdminThreads($org_branch_id = null, $user_id = null){

		if ($org_branch_id == null || $user_id == null) {
			return null;
		}

		$this->org_branch_id = $org_branch_id;

		$cmtm = DB::connection('bk')->table('college_message_thread_members as cmtm')
									->join('college_message_threads as cmt', 'cmt.id', '=', 'cmtm.thread_id')
									->join('organization_branches as ob', 'ob.id', '=', 'cmtm.org_branch_id')
									->join('colleges as c', 'c.id', '=','ob.school_id')
									->join('users as u', 'u.id', '=', 'cmtm.user_id')

									->join('college_message_logs as cml', 'cml.thread_id', '=', 'cmtm.thread_id')
									->leftjoin('college_message_logs as cml2', function($join)
									{
									    $join->on('cml2.thread_id', '=', 'cmtm.thread_id');
									    $join->on('cml.id', '<', 'cml2.id');
									})

									->whereNotIn( 'cmtm.user_id', function($query)
									{
									    $query->distinct()->select('user_id')
									    				->from('organization_branch_permissions')
									    				->where('organization_branch_id', $this->org_branch_id);
									})

									->join('users as u2', 'u2.id', '=', 'cml.user_id')
									->select('cmt.id as thread_id', 'cmt.is_chat', 'cmt.plexuss_note', 'cmt.updated_at as note_date',
											 'cmtm.user_id', 'u.fname', 'u.lname',
											 'c.school_name',
											 'cml.user_id as cmlUserId', 'cml.msg',
											 DB::raw('IF (`u2`.`is_organization` = 1, "false", "true") as idle')
											 )
									->where('u.is_organization', 0)
									->where('cmtm.org_branch_id', $org_branch_id)
									->whereRaw('cml2.id IS NULL')
									->orderBy('cml.updated_at', 'DESC')
									->groupBy('cmt.id')
									->get();

		return $cmtm;
	}

	public function getUsersAlreadyMessaged(){

		$agency_id = null;
		$org_branch_id = null;

		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData(true);

		DB::connection('rds1')->statement('SET SESSION group_concat_max_len = 1000000;');

		if(isset($data['agency_collection']->agency_id)){
			$agency_id = $data['agency_collection']->agency_id;
		}else{
			$org_branch_id = $data['org_branch_id'];
		}

		$cmtm = CollegeMessageThreadMembers::on('rds1')->select(DB::raw('GROUP_CONCAT(DISTINCT user_id SEPARATOR ",") as users'));

		if (isset($org_branch_id)) {
			$cmtm = $cmtm->where('org_branch_id', $org_branch_id);
		}

		if (isset($agency_id)) {
			$cmtm = $cmtm->where('agency_id', $agency_id);
		}

		// If user has a department set, show results based on the filters they have set
		$crf = new CollegeRecommendationFilters;
		$filter_qry = $crf->generateFilterQry($data);

		if (isset($filter_qry) && isset($data['default_organization_portal'])) {
			$filter_qry = $filter_qry->select('userFilter.id as filterUserId');
			$bc = new Controller;
			$tmp_qry = $bc->getRawSqlWithBindings($filter_qry);
			$cmtm = $cmtm->join(DB::raw('('.$tmp_qry.')  as t2'), 't2.filterUserId' , '=', 'college_message_thread_members.user_id');
		}
		// End of department query set

		return $cmtm->first();
	}

	public function getUsersWhoHaveReceivedThisMessage($data){

		DB::connection('rds1')->statement('SET SESSION group_concat_max_len = 1000000;');

		$cmtm = DB::connection('rds1')->table('college_message_thread_members as cmtm')
									  ->join('college_message_logs as cml', 'cmtm.thread_id', '=', 'cml.thread_id')
									  ->select(DB::raw('GROUP_CONCAT(DISTINCT cmtm.user_id SEPARATOR ",") as users'))
				   					  ->where('cmtm.org_branch_id', $data['org_branch_id'])
									  ->where('cml.msg', 'LIKE', DB::RAW('"<b>Subject: </b>'.$data['subject'].'<br>%"'));

		return $cmtm->first();
		
	}

	public function getThreadidWithThisUserIdAndThisCollegeId($user_id, $college_id){

		$qry = DB::connection('rds1')->table('organization_branches as ob')
									 ->join('college_message_thread_members as cmtm', 'cmtm.org_branch_id', '=', 'ob.id')
									 ->join('college_message_threads as cmt', 'cmt.id', '=', 'cmtm.thread_id')
									 ->where('ob.school_id', $college_id)
									 ->where('cmtm.user_id', $user_id)
									 ->where('cmt.is_chat', 0)
									 ->orderBy('cmtm.updated_at', 'DESC')
									 ->select('cmtm.thread_id')
									 ->first();

		if (isset($qry)) {
		 	return $qry->thread_id;
		}

		return null;									 
	}
}
