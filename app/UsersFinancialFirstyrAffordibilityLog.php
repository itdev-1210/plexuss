<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersFinancialFirstyrAffordibilityLog extends Model {

	protected $table = 'users_financial_firstyr_affordibility_logs';

 	protected $fillable = array('user_id', 'financial_firstyr_affordibility', 'submitted_user_id', 'where_submitted');

 	public function add($user_id, $financial_firstyr_affordibility, $submitted_user_id, $where_submitted){
 		if (!isset($user_id) || !isset($financial_firstyr_affordibility)) {
 			return;
 		}

 		$uffal = new UsersFinancialFirstyrAffordibilityLog;
 		$uffal->user_id = $user_id;
 		$uffal->financial_firstyr_affordibility = $financial_firstyr_affordibility;
 		$uffal->submitted_user_id = $submitted_user_id;
 		$uffal->where_submitted = $where_submitted;

 		$uffal->save();


 	}
}

