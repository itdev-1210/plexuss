<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGood extends Model {
	
	protected $table = 'product_goods';

 	protected $fillable = array( 'name', 'manufacturer', 'description', 'stock', 'price');
}