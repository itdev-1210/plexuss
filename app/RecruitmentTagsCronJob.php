<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentTagsCronJob extends Model {

	protected $table = 'recruitment_tags_cron_jobs';

 	protected $fillable = array('user_id', 'checkGeneralRecruitmentTag_status', 'checkTaggedRecruitmentTag_status');

 	public function add($user_id, $checkGeneralRecruitmentTag_status = null, $checkTaggedRecruitmentTag_status = null){

 		$attr = array('user_id' => $user_id);
 		$val  = array('user_id' => $user_id);

 		if (isset($checkGeneralRecruitmentTag_status)) {
 			$val['checkGeneralRecruitmentTag_status'] = $checkGeneralRecruitmentTag_status;
 		}

 		if (isset($checkTaggedRecruitmentTag_status)) {
 			$val['checkTaggedRecruitmentTag_status'] = $checkTaggedRecruitmentTag_status;
 		}

 		$update = RecruitmentTagsCronJob::updateOrCreate($attr, $val);

 		return "success";
 	}
}
