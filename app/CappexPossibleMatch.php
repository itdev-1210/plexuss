<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CappexPossibleMatch extends Model
{
    protected $table = 'cappex_possible_matches';
	protected $fillable = array( 'user_id', 'college_id', 'error_log', 'user_selected', 'tried_to_fix' );
}
