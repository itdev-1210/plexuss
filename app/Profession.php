<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Profession extends Model{

	protected $table = 'professions';
	protected $fillable = array( 'profession_name' );

	public function findProfession($input = null){
		if( !isset($input) ){
			return 'no results';
		}

		$result = DB::connection('rds1')->table('professions as p')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('p.profession_name', 'like', '%'.$input.'%');
					})
					->limit(10)
					->get();
		
		return $result;
	}

	public function findProfessionByName( $input = null ){
		if( !isset($input) ){
			return false;
		}

		$result = DB::connection('rds1')->table('professions as p')
					->where('profession_name', '=', $input)
					->first();

		return $result;
	}
}
