<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyClientRemindersToSendNumbers extends Model
{
    protected $table = 'weekly_client_reminders_to_send_numbers';

    protected $fillable = array('fname', 'email', 'last_sent', 'updated_at', 'created_at');
}
