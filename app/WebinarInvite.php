<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebinarInvite extends Model{

	//saving db table name for webinar
	protected $table = 'webinar_invite';

	//table columns to be filled
	protected $fillable = array( 'user_id', 'email_sent' );

}