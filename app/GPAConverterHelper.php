<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GPAConverter;

class GPAConverterHelper extends Model
{
    protected $table = 'gpa_converter_helper';

	public static function getCountryGradingScales( $country_id = null ) {
		if ( !isset($country_id) ) {
			return [];
		}

		$scales = [];

		$query = GPAConverterHelper::on('rds1')
				  ->select('id', 'grading_scale as name', 'conversion_type', 'grading_scale_min', 'grading_scale_max', 'is_solo')
				  ->where('country_id', $country_id)
				  ->get();

		foreach ($query as $key) {
			$row = (object) [];

			// Setting non-null values to $row object.
			isset($key->id) ? $row->id = $key->id : null;
			isset($key->name) ? $row->name = $key->name : null;
			isset($key->conversion_type) ? $row->conversion_type = $key->conversion_type : null;
			isset($key->grading_scale_min) ? $row->grading_scale_min = $key->grading_scale_min : null;
			isset($key->grading_scale_max) ? $row->grading_scale_max = $key->grading_scale_max : null;
			isset($key->is_solo) ? $row->is_solo = $key->is_solo : null;


			// If conversion_type is direct (equal to 0), we need to get options from gpa_converter table
			if ($key->conversion_type == 0) {
				$row->options = [];
				$options = GPAConverter::on('rds1')->select('grade as name')->where('gch_id', $key->id)->get();
				foreach ($options as $option) {
					$tmp = [ 'id' => strtolower($option->name), 'name' => $option->name ];
					$row->options[] = $tmp;
				}
			}

			$scales[] = $row;
		}

		return $scales;
	}
}
