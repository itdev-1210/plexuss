<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersInvite extends Model {


	protected $table = 'users_invites';


 	protected $fillable = array( 'user_id', 'invite_name', 'invite_email', 'invite_phone', 'source', 'sent', 'last_sent_on', 'count', 'is_dup');
}
