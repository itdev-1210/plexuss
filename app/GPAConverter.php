<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GPAConverter extends Model
{
    protected $table = 'gpa_converter';

	// To convert to a United States GPA, we will need the grade conversion 
	// helper id, the user value to be converted, and the conversion_type.
	public static function convertToUnitedStatesGPA($gch_id, $old_value, $conversion_type) {
		if (!isset($gch_id) || !isset($old_value) || !isset($conversion_type)) { return 'No conversion available'; }

		switch($conversion_type) {
			case 0: // 0 = direct conversion
				return GPAConverter::directGPAConversion($gch_id, $old_value);
			case 1: // 1 = computational conversion
				return GPAConverter::computationalGPAConversion($gch_id, $old_value);

		}
	}

	private static function computationalGPAConversion($gch_id, $old_value) {
		$query = GPAConverter::on('rds1')
				 ->select('old_min', 'old_max', 'new_max', 'new_min')
				 ->where('gch_id', $gch_id)
				 ->where('old_min', '<=', $old_value)
				 ->where('old_max', '>=', $old_value)
				 ->first();

		foreach ($query->getAttributes() as $key) {
			if (!isset($key)) {
				return 'No conversion available';
			}
		}

		$old_min = $query->old_min;
		$old_max = $query->old_max;
		$new_max = $query->new_max;
		$new_min = $query->new_min;

		if ($old_min == $old_max) { // Exception, if true, set new_value to new_max to avoid divide by zero.
			$new_value = $new_max;
		} else {
			$new_value = ( (($old_value - $old_min) * ($new_max - $new_min)) / ($old_max - $old_min) ) + $new_min;
		}

		return round($new_value, 2);
	}

	private static function directGPAConversion($gch_id, $old_value) {
		$query = GPAConverter::on('rds1')
				 ->select('new_direct')
				 ->where('gch_id', $gch_id)
				 ->where('grade', $old_value)
				 ->first();

		foreach ($query->getAttributes() as $key) {
			if (!isset($key)) {
				return 'No conversion available';
			}
		}

		return round($query->new_direct, 2);
	}

}
