<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPage extends Model
{
    protected $table = 'ad_pages';

 	protected $fillable = array('company', 'slug');

 	public function findEddyAd($slug = null){
		if( !isset($slug) ){
			return false;
		}

		$result = AdPage::on('rds1')
						->where('slug', $slug)
						->where('company', 'eddy')
						->first();

		return $result;
	}
}
