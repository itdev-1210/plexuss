<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UtmTracking extends Model
{
    protected $table = 'utm_trackings';

 	protected $fillable = array( 'base_url', 'utm_source', 'utm_medium', 'utm_campaign', 
 								 'utm_term', 'utm_content', 'user_id', 'date', 'ip' );
}
