<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;

class CollegeCampaign extends Model
{
    protected $table = 'college_campaigns';

 	protected $fillable = array( 'name', 'subject', 'body', 'college_id', 'num_of_student_user_ids', 'sender_user_id', 
 								 'is_list_user','agency_id', 'student_user_ids', 'scheduled_at', 'last_sent_on',
 								 'ready_to_send', 'is_text_msg');


 	public function cleanUpCampaignUsers($college_id){

 		$cc = CollegeCampaign::where('college_id', $college_id)->get();
 		foreach ($cc as $key) {
 			try {
	 			$student_user_ids = explode(",", $key->student_user_ids);
	 		} catch (Exception $e) {
	 			continue;
	 		}
	 		
	 		DB::connection('rds1')->statement('SET SESSION group_concat_max_len = 1000000;');
	 		$users = User::on('rds1')->where(function($query) use ($student_user_ids){
			                           		foreach ($student_user_ids as $key => $value) {
			                           			if (!empty($value)) {
			                           				$query = $query->orWhere('id', '=', $value);
			                           			}
											}
			                           	})
	 								 ->select(DB::raw('GROUP_CONCAT(id SEPARATOR ",") as student_user_ids, count(*) as cnt'))
	 					  			 ->first();

	 		$key->student_user_ids 		  = $users->student_user_ids;
	 		$key->num_of_student_user_ids = $users->cnt;

	 		$key->save();

	 		$arr['student_user_ids']        = $users->student_user_ids;
	 		$arr['num_of_student_user_ids'] = $users->cnt;
 		}
 		
 		
 		return true; 
 	}
}
