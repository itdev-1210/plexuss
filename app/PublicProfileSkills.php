<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicProfileSkills extends Model
{
    protected $table = 'public_profile_skills';

    protected $fillable = ['name', 'user_id', 'group', 'position', 'awards', 'active', 'updated_at', 'created_at'];

    // For use with inserting new skills
    public static function insertOrUpdate($data) {
        $requiredKeys = ['user_id', 'name'];

        foreach ($requiredKeys as $key) {
            if (!isset($data[$key])) {
                return 'Missing ' . $key;
            }
        }

        $attributes = [
            'user_id' => $data['user_id'],
            'name' => ucwords($data['name']),
        ];

        $values = [
            'user_id' => $data['user_id'],
            'name' => ucwords($data['name']),
            'group' => !empty($data['group']) ? $data['group'] : '',
            'position' => !empty($data['position']) ? $data['position'] : '',
            'awards' => !empty($data['awards']) ? $data['awards'] : '',
            'active' => 1,
        ];

        PublicProfileSkills::updateOrCreate($attributes, $values);

        return 'success';
    }

    public static function updateSkill($data) {
        $requiredKeys = ['user_id', 'id'];

        foreach ($requiredKeys as $key) {
            if (!isset($data[$key])) {
                return 'Missing ' . $key;
            }
        }

        $pps = PublicProfileSkills::where('id', $data['id'])->where('user_id', '=', $data['user_id'])->first();

        $pps->name = !empty($data['name']) ? $data['name'] : '';

        $pps->group = !empty($data['group']) ? $data['group'] : '';

        $pps->position = !empty($data['position']) ? $data['position'] : '';

        $pps->awards = !empty($data['awards']) ? $data['awards'] : '';

        $pps->save();

        return 'success';
    }

    public static function removeSkill($data) {
        $requiredKeys = ['id', 'user_id'];

        foreach ($requiredKeys as $key) {
            if (!isset($data[$key])) {
                return 'Missing ' . $key;
            }
        }

        $pps = PublicProfileSkills::where('id', '=', $data['id'])
                                  ->where('user_id', '=', $data['user_id']) // Extra integrity by verifying user_id as well.
                                  ->first();

        if (empty($pps)) {
            return 'failed';
        }
        
        $pps->active = 0;

        $pps->save();                                

        return 'success';
    }
}
