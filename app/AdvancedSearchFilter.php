<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AdvancedSearchFilter extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'advanced_search_filters';


 	protected $fillable = array('asfn_id', 'name');

 	public function AdvancedSearchFilterLog(){

        return $this->hasMany('AdvancedSearchFilterLog');

    }
}
