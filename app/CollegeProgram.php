<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class CollegeProgram extends Model
{
    protected $table = 'college_programs';

 	protected $fillable = array( 'college_id', 'department_id', 'major_id', 'degree_type');

 	public function getDepAndMajorWithCollegeId($college_id){
 		$qry = DB::connection('rds1')->table('college_programs as cp')
 									 ->join('degree_type as dt', 'cp.degree_type', '=', 'dt.id')
 									 ->join('majors as m', 'cp.major_id', '=', 'm.id')
 									 ->join('departments as d', 'cp.department_id', '=', 'd.id')
 									 ->join('department_categories as dc', 'd.category_id', '=', 'dc.id')
 									 ->where('cp.college_id', $college_id)
 									 ->orderBy('d.name', 'asc')
 									 ->orderBy('cp.major_id', 'asc')
 									 ->select("cp.college_id", "cp.degree_type", "dt.display_name", "cp.department_id", "d.name as department_name", "cp.major_id", "m.name as major", "dc.url_slug as department_slug")
 									 ->groupBy('major_id', 'degree_type')
 									 ->get();

 		$d_id_arr = array();
 		$d_major_arr = array();
 		$ret = array();
 		foreach ($qry as $key) {
 			if (!in_array($key->department_id, $d_id_arr) && !in_array($key->major_id, $d_major_arr)) {
 				$tmp = array();
	 			$tmp['department_id'] 	= $key->department_id;
	 			$tmp['department_name'] = $key->department_name;
	 			$tmp['department_slug'] = $key->department_slug;
	 			$tmp['majors'] = array();
	 			$inner_tmp = array();
	 			$inner_tmp['major_name'] = $key->major;
	 			$inner_tmp['major_id']   = $key->major_id;
	 			$inner_tmp['degrees'] = array();

	 			$tmp_degree = array();
	 			$tmp_degree['degree_name']= $key->display_name;
	 			$tmp_degree['degree_id']  = $key->degree_type;

	 			$inner_tmp['degrees'][] = $tmp_degree;
	 			$tmp['majors'][] = $inner_tmp;
	 			$ret[] = $tmp;

	 			$d_id_arr[]    = $key->department_id;
	 			$d_major_arr[] = $key->major_id;

 			}elseif (in_array($key->department_id, $d_id_arr) && !in_array($key->major_id, $d_major_arr)) {
 				$dep_cnt   = count($ret);
 				$inner_tmp = array();
	 			$inner_tmp['major_name'] = $key->major;
	 			$inner_tmp['major_id']   = $key->major_id;
	 			$inner_tmp['degrees'] = array();

	 			$tmp_degree = array();
	 			$tmp_degree['degree_name']= $key->display_name;
	 			$tmp_degree['degree_id']  = $key->degree_type;

	 			$inner_tmp['degrees'][] = $tmp_degree;

 				$ret[$dep_cnt - 1]['majors'][] = $inner_tmp;
 				$d_major_arr[] = $key->major_id;

 			}elseif (in_array($key->major_id, $d_major_arr)) {
 				$dep_cnt   = count($ret);
 				$major_cnt = count($ret[$dep_cnt - 1]['majors']);
 	
 				$tmp = array();
 				$tmp['degree_name']= $key->display_name;
	 			$tmp['degree_id']  = $key->degree_type;
 				
 				$ret[$dep_cnt - 1]['majors'][$major_cnt - 1]['degrees'][] = $tmp;			
 			}
 		}

 		return $ret;
 	}
}
