<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUserCCPInfo extends Model {

	protected $table = 'product_user_ccp_info';

	protected $fillable = array( 'user_id', 'expected_arrival_date', 'billing_firstname', 'billing_lastname', 'billing_address', 'billing_apt', 'billing_city', 'billing_state', 'billing_zip', 'billing_email', 'shipping_firstname', 'shipping_lastname', 'shipping_address', 'shipping_apt', 'shipping_city', 'shipping_state', 'shipping_zip', 'shipping_email' );

	public function orders()
    {
        return $this->hasMany('ProductOrder');
    }
}
