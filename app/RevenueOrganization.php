<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RevenueOrganization extends Model
{
    protected $table = 'revenue_organizations';
	protected $fillable = array( 'name', 'active', 'type', 'conversion_on' );


	public function getRevenueOrganizationCap($ro, $user_id){

		$cap = 0;
		switch ($ro->type) {
			case 'post':
				$cnt = DB::connection('rds1')->table('distribution_clients as dc')
											 ->join('distribution_responses as dr', 'dc.id', '=', 'dr.dc_id')
											 ->where('dc.ro_id', $ro->id)
											 ->where('dr.user_id', $user_id)
											 ->where('dr.success', 1)
											 ->count(DB::raw("distinct(dr.dc_id)"));

				$cap = $ro->cap - $cnt;
				break;
			
			case 'linkout':
				$cnt = DB::connection('rds1')->table('revenue_organizations as ro')
											 ->join('ad_clicks as ac', 'ro.name', '=', 'ac.company')
											 ->where('ro.id', $ro->id)
											 ->where('ac.user_id', $user_id)
											 ->count('ac.id');

				$cap = $ro->cap - $cnt;

				break;

			case 'click':
				$cnt = DB::connection('rds1')->table('ad_redirect_campaigns as arc')
											 ->join('ad_clicks as ac', 'arc.company', '=', 'ac.company')
											 ->where('arc.ro_id', $ro->id)
											 ->where('ac.user_id', $user_id)
											 ->count('ac.id');

				$cap = $ro->cap - $cnt;
				break;
		}

		return $cap;
	}
}
