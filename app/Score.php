<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model {


	protected $table = 'scores';

	protected $fillable = array( 'user_id', 'whocansee', 'hs_gpa', 'weighted_gpa', 'max_weighted_gpa',
								 'act_english', 'act_math', 'act_composite', 'is_pre_2016_psat', 'psat_reading', 'psat_math', 'psat_reading_writing',
								 'psat_writing', 'psat_total', 'is_pre_2016_sat', 'sat_reading', 'sat_math', 'sat_writing', 'sat_reading_writing',
								 'sat_total', 'gedfp', 'ged_score', 'overall_gpa', 'other_values', 'other_exam', 
								 'lsat_total', 'gmat_total', 'gre_verbal', 'gre_quantitative', 'gre_analytical',
								 'toefl_total', 'toefl_reading', 'toefl_listening', 'toefl_speaking', 'toefl_writing',
								 'ielts_total', 'ielts_reading', 'ielts_listening', 'ielts_speaking', 'ielts_writing',
								 'english_institute_name', 'pte_total', 'itep_total', 'native_english', 'ap_overall',
								 'toefl_ibt_total', 'toefl_ibt_reading', 'toefl_ibt_listening','toefl_ibt_speaking',
								 'toefl_ibt_writing', 'toefl_pbt_total', 'toefl_pbt_reading', 'toefl_pbt_listening',
								 'toefl_pbt_written');
	
	public function user()
    {
        return $this->belongsTo('User');
    }

}