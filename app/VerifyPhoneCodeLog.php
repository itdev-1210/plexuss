<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyPhoneCodeLog extends Model {

	protected $table = 'verify_phone_code_logs';

 	protected $fillable = array('code', 'user_id');
}
