<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CollegeList extends Model
{
    protected $table = 'college_list';

	protected $fillable = array( 'school_id' );
	
	public $timestamps = false;

	public function users()
	{
		return $this->belongsTo('User');
	}	
	// your list college
	public function yourListData($userid=false,$id=false,$menu=false)
	{
		
		$query = 'college_list.id as list_id,college_list.status,colleges.id,colleges.slug,colleges.ipeds_id,colleges.school_name,colleges.address,colleges.city,colleges.state,colleges.long_state,colleges.zip,colleges.logo_url,colleges.chief_title,colleges.chief_name,colleges.logo_url,colleges.alias,colleges.student_faculty_ratio,colleges.student_body_total,colleges.admissions_total,colleges.applicants_total,colleges_ranking.plexuss,colleges_tuition.tuition_avg_in_state_ftug,colleges_tuition.tuition_avg_out_state_ftug,colleges_athletics.class_name,colleges_admissions.sat_read_75,colleges_admissions.sat_write_75,colleges_admissions.sat_math_75,colleges_admissions.deadline,colleges_admissions.act_composite_75,"college_list" as tablename';
		
		$result=DB::connection('rds1')->table('college_list')
		->join('colleges', 'colleges.id', '=', 'college_list.school_id')		
		->leftJoin('colleges_ranking', 'colleges_ranking.college_id', '=', 'colleges.id')		
		->leftJoin('colleges_tuition', 'colleges_tuition.college_id', '=', 'colleges.id')	
		->leftJoin('colleges_athletics', 'colleges_athletics.college_id', '=', 'colleges.id')	
		->leftJoin('colleges_admissions', 'colleges_admissions.college_id', '=', 'colleges.id')		
		->select( DB::raw($query));
		
		if(isset($userid) && $userid!='')
		{		
			$result = $result->where('college_list.user_id', '=',$userid);			
		}
		if(isset($id) && $id!='')
		{		
			$result = $result->where('college_list.id', '=',$id);			
		}
		if(isset($menu) && $menu=='menu5')
		{
			$result = $result->whereNotNull('college_list.trash');
		}
		else
		{
			$result = $result->whereNull('college_list.trash');
		}
		
		$result = $result->orderBy('college_list.sortorder','asc');
		$result = $result->orderBy('colleges.school_name', 'asc');	
		$result = $result->groupBy('college_list.school_id');
		
		//$result = $result->toSql();	
		$result = $result->get();
		return $result;
	}
}
