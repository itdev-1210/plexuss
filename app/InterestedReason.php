<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestedReason extends Model {

	protected $table = 'interested_reason';

	//protected $fillable = array('school_id' );
	public $timestamps = false;

	public function users()
	{
		return $this->belongsTo('User');
	}	
	
	
}