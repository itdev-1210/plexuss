<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialView extends Model
{
  protected $table = 'sn_views';
  protected $fillable = array('post_id', 'social_article_id', 'view_type');

  /*
  * Model Relationships
  */

  public function post()
  {
    return $this->belongsTo('App/Post');
  }

  public function article()
  {
    return $this->belongsTo('App/SocialArticle');
  }
}
