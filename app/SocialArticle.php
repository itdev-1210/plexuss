<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Http\Controllers\Controller;

class SocialArticle extends Model
{

  protected $table = 'sn_articles';
  protected $fillable = array('user_id', 'article_text', 'article_title', 'status', 'project_and_publication', 'original_article_id', 'is_shared', 'share_count', 'posted_by_plexuss', 'share_with_id', 'created_at', 'updated_at');

  public function getSocialArticles($user_id, $projects = false) {
    $profile_settings = DB::connection('rds1')->table('sn_articles as sna')
                                              ->leftjoin('sn_post_images as sni', 'sni.social_article_id', '=', 'sna.id')

                                              ->where('sna.user_id', $user_id)
                                              ->where('sna.status', 1)

                                              ->select('sna.id', 'sna.article_title', 'sna.created_at', 'sni.image_link');

    if($projects == true){
      $profile_settings = $profile_settings->where('sna.project_and_publication', 1);
    }
    $profile_settings = $profile_settings->get();

    $cr = new Controller;

    foreach ($profile_settings as $k) {
      $k->id = $cr->hashIdForSocial($k->id);
    }
    return $profile_settings;
  }

  /*
  * Model Relationships
  */

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function userAccountSettings()
  {
    return $this->hasOne('App\UserAccountSettings', 'user_id', 'user_id');
  }

  public function comments()
  {
    return $this->hasMany('App\PostComment');
  }

  public function likes()
  {
    return $this->hasMany('App\Like');
  }

  public function images()
  {
    return $this->hasMany('App\PostImage');
  }

  public function views()
  {
    return $this->hasMany('App\SocialView');
  }

  public function tags()
  {
    return $this->hasMany('App\ArticleTag');
  }
}
