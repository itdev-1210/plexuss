<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingNotificationLog extends Model {

	protected $table = 'setting_notification_logs';

 	protected $fillable = array('user_id', 'snn_id', 'type');
 	
}