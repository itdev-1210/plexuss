<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrlCount extends Model
{
    protected $table = 'tracking_page_url_counts';

    protected $fillable = array('date', 'count', 'tpu_id', 'college_id');
}
