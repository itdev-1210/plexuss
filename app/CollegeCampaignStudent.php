<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeCampaignStudent extends Model
{
    protected $table = 'college_campaign_students';

 	protected $fillable = array( 'campaign_id', 'user_id', 'sent');
}
