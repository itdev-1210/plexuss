<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneLog extends Model {

	protected $table = 'phone_log';
	protected $fillable = array( 'campaign_id', 'sender_user_id', 'receiver_user_id', 'is_list_user', 'sid', 'from', 'to',
								 'phoneBy', 'status', 'direction', 'error_url', 'price', 'price_unit', 'error_code', 'error_message',
								 'num_media', 'num_segments', 'thread_id', 'college_id', 'recording_url', 'recording_duration', 'org_branch_id');

}
