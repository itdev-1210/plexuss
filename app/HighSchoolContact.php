<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighSchoolContact extends Model {

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'high_school_contacts';

	protected $fillable = array( 'user_id', 'sent' );
}