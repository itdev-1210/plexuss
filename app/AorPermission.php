<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AorPermission extends Model
{
    protected $table = 'aor_permissions';

 	protected $fillable = array( 'aor_id', 'user_id');

}
