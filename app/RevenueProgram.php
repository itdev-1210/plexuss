<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevenueProgram extends Model
{
    protected $table = 'revenue_programs';
    protected $fillable = [
        'ro_id', 
        'college_id', 
        'program_name', 
        'quota', 
        'quota_type', 
        'program_type', 
        'degree_type', 
        'simple_major_name', 
        'url',
        'created_at', 
        'updated_at',
    ];
}
