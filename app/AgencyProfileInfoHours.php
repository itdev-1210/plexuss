<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AgencyProfileInfo;

class AgencyProfileInfoHours extends Model {
	protected $table = 'agency_profile_info_hours';

	protected $fillable = array('agency_id', 'agency_profile_info_id', 'day', 'start', 'end', 'updated_at', 'created_at');

	public $timestamps = true;

	/**
	* This function is used for inserting or updating agency business days
	*
	* @param $type contains 'signup' for agency signups OR 'profile' for existing profile changes
	* @param $id contains either agency_profile_info_id for signups OR agency_id for existing agency.
	* @return success if everything went fine else fail
	*/
	public static function insertOrUpdate($type, $id, $days_of_operation) {
		$attributes = [];

		switch ($type) {
			case 'signup':
				$attributes['agency_profile_info_id'] = $id;
				break;
			case 'profile':
				$attributes['agency_id'] = $id;
				break;
			default: // Invalid $type
				return 'fail';
		}

		foreach ($days_of_operation as $day => $hours) {
			$attributes['day'] = $day;
			$values = [ 'start' => $hours->start, 'end' => $hours->end ];

			AgencyProfileInfoHours::updateOrCreate($attributes, $values);
		}

		return 'success';
	}

	// Grabs the business days and hours based on $agency_id.
	public static function getBusinessDays($agency_id) {
		$response = [];

		$query = AgencyProfileInfoHours::on('rds1')
									   ->where('agency_id', $agency_id)
									   ->get();

		if (isset($query)) {
			foreach ($query as $hours) {
				$response[$hours->day]['start'] = $hours->start;
				$response[$hours->day]['end'] = $hours->end;
			}
		}

		return $response;
	}

}
