<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RightHandsideCat extends Model {

	protected $table = 'right_handside_cat';

	protected $fillable = array( 'category');

}