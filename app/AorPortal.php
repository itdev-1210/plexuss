<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AorPortal extends Model
{
    protected $table = 'aor_portals';

 	protected $fillable = array('aor_id', 'name');
}
