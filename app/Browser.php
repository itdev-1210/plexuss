<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Browser extends Model
{
    protected $table = 'browsers';
    
    protected $fillable = array( 'name' );

}
