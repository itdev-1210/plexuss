<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalizationPageSectionContent extends Model {

	protected $table = 'localization_page_section_contents';

 	protected $fillable = array('lang', 'content');
}