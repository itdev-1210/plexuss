<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyPermission extends Model
{
    protected $table = 'agency_permissions';

 	protected $fillable = array( 'name' );
}
