<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $table = 'occupations';
	
	protected $fillable = array( 'user_id', 'profession_id' );
}
