<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    //
  protected $table = 'sn_post_comments';
  protected $fillable = array('parent_id','comment_text','user_id', 'post_id', 'social_article_id', 'shared_link', 'created_at', 'updated_at');

  /*
  * Model Relationships
  */

  public function post()
  {
    return $this->belongsTo('App\Post');
  }

  public function article()
  {
    return $this->belongsTo('App\SocialArticle');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function likes()
  {
    return $this->hasMany('App\Like');
  }

  public function images()
  {
    return $this->hasMany('App\PostImage');
  }
  public function userAccountSettings()
  {
    return $this->hasOne('App\UserAccountSettings', 'user_id', 'user_id');
  }
}
