<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaDataCollegesByState extends Model
{
    protected $table = 'meta_data_colleges_by_state';
    
    protected $fillables = ['state_name', 'state_abbr', 'meta_title', 'meta_description', 'headline', 'content', 'background_img_name', 'background_img_alt', 'flag_img_name', 'flag_img_alt', 'slug'];
}
