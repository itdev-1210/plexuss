<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesInternationalAlum extends Model
{
    protected $table = 'colleges_international_alums';
	protected $fillable = array('cit_id', 'college_id', 'alumni_name', 'dep_id', 'grad_year', 'location',
								'linkedin', 'photo_url');

	public function removeInternationalAlumni($id){
		$cit = CollegesInternationalAlum::find($id);
		$cit->delete();

		return "success";
	}
}
