<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionCustomQuestionUserAnswer extends Model
{
    protected $table = 'distribution_custom_question_user_answers';

 	protected $fillable = array('user_id', 'ro_id', 'field_name', 'value', 'college_id');
}
