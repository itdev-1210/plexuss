<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPremiumEssay extends Model {

	protected $table = 'users_premium_essays';

 	protected $fillable = array('user_id', 'news_id');
}