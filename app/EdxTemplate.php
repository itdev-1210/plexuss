<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EdxTemplate extends Model
{
    protected $table = 'edx_templates';

 	protected $fillable = array( 'template_name' );

}
