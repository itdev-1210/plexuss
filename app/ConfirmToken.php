<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmToken extends Model
{
    protected $table = 'confirm_token';

	protected $fillable = array('token');

	public function user()
    {
        return $this->belongsTo('User');
    }
}
