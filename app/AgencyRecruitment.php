<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\AgencyController;

class AgencyRecruitment extends Model
{
    protected $table = 'agency_recruitment';

 	protected $fillable = array( 'user_id', 'agency_id', 'user_recruit', 'agency_recruit', 
 								 'active', 'note', 'email_sent', 'token', 'paid', 'type', 
 								 'applied', 'enrolled', 'applied_at', 'enrolled_at', 'agency_bucket_id' );

 	/**
	 * getNumOfApprovedForColleges
	 *
	 * Total number of approved for a particular agency
	 *
	 * @param (numeric) (college_id) college id 
	 * @return (int) (count)
	 */
 	public function getNumOfApprovedForAgency($agency_id = null, $onDate = null){
 		if ($agency_id == null) {
 			return 0;
 		}
 		$rec= AgencyRecruitment::on('bk')->whereIn('agency_id', $agency_id)
 							->where('user_recruit', 1)
 							->where('agency_recruit', 1)
 							->select(DB::raw('count(DISTINCT user_id) as cnt, agency_id'))
 							->groupBy('agency_id');

 		if( isset($onDate) ){
 			$rec = $rec->where('created_at', '>', $onDate);
 		}
 		
 		$rec = $rec->get();
 		
 		return $rec;
 	}


 	public function moveUserToCompletedApplicationBucket($user_id){

 		$qry = AgencyRecruitment::where('user_id', $user_id)
 								->where('agency_bucket_id', '!=', 3) // give me agencies who have not gone to completed application
 								->get();

		$ac = new AgencyController;

 		if (isset($qry)) {
 			foreach ($qry as $key) {
 				$input = array();
 				$data  = array();

	 			$input['hashed_id']   = Crypt::encrypt($key->user_id);
	 			$input['bucket_name'] = 'applications';

	 			$data['agency_collection'] = new \stdClass;
	 			$data['agency_collection']->agency_id = $key->agency_id;

	 			$ac->changeStudentAgencyBucket($input, $data);
 			}
 			
 		}
 	}
}
