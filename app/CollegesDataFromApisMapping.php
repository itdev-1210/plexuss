<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesDataFromApisMapping extends Model
{
    protected $table = 'colleges_data_from_apis_mappings';

 	protected $fillable = array('plex_column', 'api_column');
}
