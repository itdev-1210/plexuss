<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class PlexussAnnouncement extends Model {

	protected $table = 'plexuss_announcements';

 	protected $fillable = array('text', 'start', 'end');

 	public function getPlexussAnnouncement($user_id){
 		
 		// if (Cache::has(env('ENVIRONMENT') . '_plexuss_announcements')) {
 		// 	$pa = Cache::get(env('ENVIRONMENT') . '_plexuss_announcements');

 		// 	return $pa;
 		// }

 		$now = Carbon::now();
 		$now = $now->toDateString();
 		$pa  = PlexussAnnouncement::on('rds1')->where('start', '<=', $now)
 											  ->where('end',   '>=', $now)
 											  ->select('text', 'id');

 		if (Cache::has(env('ENVIRONMENT') .'_dismissPlexussAnnouncement_'.$user_id)) {
 			$arr = Cache::get(env('ENVIRONMENT') .'_dismissPlexussAnnouncement_'.$user_id);

 			$pa = $pa->whereNotIn('id', $arr);
 		}

 		$pa = $pa->get();


 		if (isset($pa)) {
 			Cache::put(env('ENVIRONMENT') . '_plexuss_announcements', $pa, 460);
 		}
 		
 		return $pa;
 	}
}
