<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerEmailLogsDateId extends Model
{
    protected $table = 'partner_email_logs_date_ids';

 	protected $fillable = array( 'date', 'timestamp', 'pel_id' );
}
