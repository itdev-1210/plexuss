<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Cache;

class UsersIpLocation extends Model
{
    protected $table = 'users_ip_locations';

 	protected $fillable = array( 'user_id', 'ip', 'ip_city', 'ip_state', 'ip_state_id', 'ip_country_id', 
 							     'ip_zip', 'created_at', 'updated_at');

 	public function getUsersCountryCode($user_id){

 		if (Cache::has( env('ENVIRONMENT') .'_'. 'users_ip_locations_getUsersCountryCode'.$user_id)) {
    		
    		$cron = Cache::get( env('ENVIRONMENT') .'_'. 'users_ip_locations_getUsersCountryCode'.$user_id);

    		return $cron;
    	}


 		$qry = DB::connection('rds1')->table('users as u')
 									 ->leftjoin('users_ip_locations as uil', 'u.id', '=', 'uil.ip_country_id')
 									 ->leftjoin('countries as c1', 'uil.ip_country_id', '=', 'c1.id')
 									 ->leftjoin('countries as c2', 'u.country_id', '=', 'c2.id')
 									 ->where('u.id', $user_id)
 									 ->select('c1.country_code as c1_code', 'c2.country_code as c2_code')
 									 ->first();

 		$country_code = null;

 		if (isset($qry->c1_code)) {
 			$country_code = $qry->c1_code;
 		}elseif (isset($qry->c2_code)) {
 			$country_code = $qry->c2_code;
 		}

 		Cache::put( env('ENVIRONMENT') .'_'. 'users_ip_locations_getUsersCountryCode'.$user_id, $country_code, 1440);

 		return $country_code;
 	}
}
