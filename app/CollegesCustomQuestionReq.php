<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesCustomQuestionReq extends Model
{
    protected $table = 'colleges_custom_question_reqs';

 	protected $fillable = array('cq_id', 'college_id', 'aor_id');
}
