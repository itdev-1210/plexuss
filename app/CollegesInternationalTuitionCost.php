<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesInternationalTuitionCost extends Model
{
    protected $table = 'colleges_international_tuition_costs';

 	protected $fillable = array('college_id', 'undergrad_application_fee',	'undergrad_avg_tuition', 'undergrad_other_cost',	
								'undergrad_avg_scholarship', 'undergrad_avg_work_study', 'undergrad_other_financial', 'grad_application_fee',	
								'grad_avg_tuition',	'grad_other_cost',	'grad_avg_scholarship',	'grad_avg_work_study',	'grad_other_financial',
								'epp_application_fee', 'epp_avg_tuition',	'epp_other_cost',	'epp_avg_scholarship',	'epp_avg_work_study',	
								'epp_other_financial', 'quick_tip', 'company_logo_file');
}
