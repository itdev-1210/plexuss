<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerEmailTemplate extends Model
{
    protected $table = 'partner_email_templates';

 	protected $fillable = array( 'template_name', 'take', 'college_id', 'contact_method', 'start_time', 'end_time' );
}
