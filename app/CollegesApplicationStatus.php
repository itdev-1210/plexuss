<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesApplicationStatus extends Model {
    const PLEXUSS_COLLEGE_ID = 7916;

    protected $table = 'colleges_application_status';

    protected $fillable = array('user_id', 'college_id', 'status');

	public $timestamps = true;

    public static function getPlexussStatus($user_id) {
    	return CollegesApplicationStatus::getStatus($user_id, CollegesApplicationStatus::PLEXUSS_COLLEGE_ID);
    }

    public static function getStatus($user_id, $college_id) {
    	if (!isset($user_id) || !isset($college_id)) {
    		return 'pending';
    	}

		$query = CollegesApplicationStatus::on('rds1')
					->where('user_id', $user_id)
					->where('college_id', $college_id)
					->first();

		if (!$query) {
			return 'pending';
		}

		return $query->status;
    }

    public static function updateOrCreateStatus($user_id, $college_id, $status) {
    	if (!isset($user_id) || !isset($college_id)) {
    		return 'pending';
    	}

    	$attributes = ['user_id' => $user_id, 'college_id' => $college_id];
    	$values = ['status' => $status];

    	CollegesApplicationStatus::updateOrCreate($attributes, $values);

    	return $status;
    }

    /**   
    *   This function will look at all college application statuses for this 'user_id'.
    *    
    *   If the user has at least one 'accepted', return 'accepted'.
    *   If the user has at least one 'rejected' with none 'accepted', return 'rejected'.
    *   Else will return 'pending'.
    *      
    *   The objective of this function is to help a Plexuss admin determine if any colleges have accepted 
    *   the student.
    *
    *   @param $user_id
    *
    *   @return $status of user applications
    */       
    public static function determineUserStatus($user_id) {
    	if (!isset($user_id)) {
    		return 'pending';
    	}

    	$status = 'pending';

    	$colleges = CollegesApplicationStatus::on('rds1')
					  ->select('status')
					  ->where('college_id', '!=', CollegesApplicationStatus::PLEXUSS_COLLEGE_ID)
					  ->where('user_id', $user_id)
					  ->where(function($qry) {
					  		$qry->orWhere('status', '=', 'accepted')
					  			->orWhere('status', '=', 'rejected');
					  })
					  ->orderBy('status', 'ASC')
					  ->first();

		if (isset($colleges)) {
			$status = $colleges->status;
		}

		return $status;

    }
}
