<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeRemoveStudentsFeedback extends Model
{
    protected $table = 'college_remove_students_feedbacks';

 	protected $fillable = array('aor_id', 'user_id', 'agency_id', 'org_branch_id', 'reason');
}
