<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CovetedUser extends Model
{
    protected $table = 'coveted_users';
    protected $fillable = array('user_id');
}
