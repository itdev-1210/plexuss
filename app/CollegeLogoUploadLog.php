<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class CollegeLogoUploadLog extends Model
{
	protected $table = 'college_logo_upload_logs';
	
    protected $fillable = ['college_id', 'logo_url', 'type'];

    public $timestamps = false;

}
