<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageId extends Model
{
    protected $table = 'tracking_page_ids';

	protected $fillable = array( 'tp_id', 'date', 'timestamp');
}
