<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewCollegesFromSelfSignup extends Model
{
    protected $table = 'new_colleges_from_self_signup';

    protected $fillable = array( 'college_self_signup_applications_id', 'school_name', 'country_code', 'state', 'city');

    public $timestamps = true;

    public static function insertOrUpdate($college_self_signup_applicatons_id, $input) {
        if (!isset($college_self_signup_applicatons_id) || !isset($input['school_name']) || !isset($input['country_code']) || !isset($input['state']) || !isset($input['city']))
            return 'fail';

        $attributes = ['college_self_signup_applications_id' => $college_self_signup_applicatons_id];

        $values = [
            'college_self_signup_applications_id' => $college_self_signup_applicatons_id,
            'school_name' => $input['school_name'],
            'country_code' => $input['country_code'], 
            'state' => $input['state'], 
            'city' => $input['city'],
        ];

        NewCollegesFromSelfSignup::updateOrCreate($attributes, $values);

        return 'success';
    }
}
