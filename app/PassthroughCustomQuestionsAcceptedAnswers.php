<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassthroughCustomQuestionsAcceptedAnswers extends Model
{
    //
    protected $table = 'passthrough_custom_questions_accepted_answers';
    protected $fillable = ['pcq_id', 'value'];
}
