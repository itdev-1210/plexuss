<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class LocalizationPage extends Model {

	protected $table = 'localization_pages';

 	protected $fillable = array('page', 'url');

 	public function getPageContent($page, $lang){

 		$qry = DB::connection('rds1')->table('localization_pages as lp')
 									 ->join('localization_page_sections as lps', 'lp.id', '=', 'lps.page_id')
 									 ->leftjoin('localization_page_section_contents as lpsc', function($q) use ($lang) {
 									 			$q->on('lps.id', '=', 'lpsc.section_id');
 									 			$q->on('lpsc.lang', '=', DB::raw("'".$lang."'"));
 									 })
 									 ->where('lp.page', $page)
 									 ->get();


 		$ret = array();

 		foreach ($qry as $key) {
 			if ($lang == "en") {
 				$ret[$key->section_name] = $key->english_content;
 			}else{
 				if(!isset($key->content))
 					break;
 				$ret[$key->section_name] = $key->content;
 			}
 		}

 		return $ret;
 	}
}