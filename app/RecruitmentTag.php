<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentTag extends Model {

	protected $table = 'recruitment_tags';

 	protected $fillable = array('user_id', 'college_id', 'aor_id', 'org_portal_id', 'aor_portal_id');
}
