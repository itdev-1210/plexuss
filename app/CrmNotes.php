<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmNotes extends Model
{
    //
    protected $table = 'crm_notes';

    protected $fillable = array('org_branch_id', 'creator_user_id', 'student_user_id', 'note', 'updated_at', 'created_at');

    public $timestamps = true;

}
