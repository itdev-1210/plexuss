<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileDeviceToken extends Model
{
    protected $table = 'mobile_device_tokens';

 	protected $fillable = array('user_id', 'device_token', 'platform', 'active');


 	public function saveToken($arr){

 		$attr = array('user_id' => $arr['user_id'], 'active' => 1);
 		$val  = array('user_id' => $arr['user_id'], 'platform' => $arr['platform'], 'device_token' => $arr['device_token'], 'active' => 1);

 		$update = MobileDeviceToken::updateOrCreate($attr, $val);

 		return $update;
 	}

 	public function getToken($user_id){

 		$qry = MobileDeviceToken::on('rds1')->where('user_id', $user_id)
 											->where('active', 1)
 										    ->first();

 		if (isset($qry->device_token)) {
 			return $qry;
 		}
 		
 		return null;
 	}

}
