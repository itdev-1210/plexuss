<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCodes extends Model
{
    protected $table = 'zip_codes';

	public function getAllUsState(){
		$states = ZipCodes::on('rds1')->select('StateName', 'StateAbbr')->orderBy('StateName')->groupby('StateName')->get();
		$temp = array();
		$temp[''] = 'Select...';
		foreach ($states as $key) {
			$temp[$key->StateName] = $key->StateName;
		}
		return $temp;
	}

	public function getAllUsStateAbbreviation(){
		$states = ZipCodes::on('rds1')->select('StateName', 'StateAbbr')->orderBy('StateName')->groupby('StateName')->get();
		$temp = array();
		$temp[''] = 'Select...';
		foreach ($states as $key) {
			$temp[$key->StateAbbr] = $key->StateName;
		}
		return $temp;
	}


	public function getCityByState($stateName = null){

		$temp = array();
		
		if(isset($stateName)){
			$cities = ZipCodes::on('rds1')->select('CityName')->orderBy('CityName')
					->where('stateName', $stateName)->groupby('CityName')->get();
			
			foreach ($cities as $key) {
				$temp[] = $key->CityName;
			}
		}
		return $temp;

	}

	public function getCityStateByZip($zip){

		$cityState = ZipCodes::on('rds1')->select('CityName', 'StateAbbr')
							 ->where('ZIPCode', $zip)
							 ->first();

		return $cityState;
	}

	public function getLatLongByZip($zip){
		
		$latLong = ZipCodes::on('rds1')->select('Latitude', 'Longitude')
							 ->where('ZIPCode', $zip)
							 ->first();

		return $latLong;
	}
}
