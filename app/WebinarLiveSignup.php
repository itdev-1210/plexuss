<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebinarLiveSignup extends Model {

	protected $table = 'webinar_live_signups';

 	protected $fillable = array('event_id', 'name', 'ip', 'email', 'user_id');

 	public function hasWebinarSignedup($data){

 		if (isset($data['user_id']) && $data['user_id'] != -1) {
 			$wls = WebinarLiveSignup::on('rds1')
 									->where('event_id', $data['event_id'])
 									->where('user_id', $data['user_id'])
 									->first();
 			
 		}else{
 			$wls = WebinarLiveSignup::on('rds1')
 									->where('event_id', $data['event_id'])
 									->where('ip', $data['ip'])
 									->first();
 		}

 		return $wls;
 	}
}
