<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webinar extends Model{

	//saving db table name for webinar
	protected $table = 'webinar_rsvp';

	//table columns to be filled
	protected $fillable = array( 'user_id', 'event_id' );

	//
	public function user()
    {
        return $this->belongsTo('User');
    }

}