<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeEnrollment extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'colleges_enrollment';


 	protected $fillable = array( 'college_id', 'page_title', 'meta_keywords', 'meta_description' );
}
