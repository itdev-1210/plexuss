<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicProfileProjectsAndPublications extends Model
{
    protected $table = 'public_profile_projects_and_publications';

    protected $fillable = ['user_id', 'title', 'url', 'active', 'updated_at', 'created_at'];

    public static function insertOrUpdate($data) {
        $potentialKeys = ['user_id', 'title', 'url'];

        foreach ($potentialKeys as $key) {
            if (!isset($data[$key])) {
                return 'Missing ' . $key;
            }
        }

        $attributes = [
            'user_id' => $data['user_id'],
            'title' => $data['title'],
            'url' => $data['url'],
        ];

        $values = [
            'user_id' => $data['user_id'],
            'title' => $data['title'],
            'url' => $data['url'],
            'active' => 1,
        ];

        $query = PublicProfileProjectsAndPublications::updateOrCreate($attributes, $values);

        return [
            'id' => $query->id,
            'title' => $query->title,
            'url' => $query->url,
        ];
    }

    public static function removePublication($data) {
        $id = $data['publication_id'];
        $user_id = $data['user_id']; // Ensure the user_id matches the entry

        $query = PublicProfileProjectsAndPublications::where('id', '=', $id)
                                                     ->where('user_id', '=', $user_id)
                                                     ->first();

        if (empty($query)) {
            return 'failed';
        }

        $query->active = 0;

        $query->save();

        return 'success';
    }
}
