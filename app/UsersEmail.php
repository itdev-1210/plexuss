<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersEmail extends Model {


	protected $table = 'users_emails';


 	protected $fillable = array( 'user_id', 'email_template', 'type', 'type_id');
}
