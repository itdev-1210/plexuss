<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerEmailLog extends Model
{
    protected $table = 'partner_email_logs';

 	protected $fillable = array( 'user_id', 'template_id', 'user_invite_id' );
}
