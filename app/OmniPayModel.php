<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OmniPayModel extends Model {

	protected $table = 'omni_pay';
	
	protected $fillable = array( 'user_id', 'customer_id', 'type', 'address_city', 'address_country',
								 'address_line1', 'address_state', 'apt', 'business_name', 'exp_month',
								 'exp_year', 'last4', 'name', 'phone', 'card_type', 'card_reference', 'zip_code',
								 'plan', 'payment_type', 'transactionReference');


	public function getInvoiceForUsers($data){

		$qry = DB::connection('rds1')->table('omni_pay as op')
							 ->join('omni_purchase_history as oph', 'op.user_id', '=', 'oph.user_id')
							 ->join('premium_users as pu', 'pu.user_id', '=', 'op.user_id')
							 ->where('op.user_id', $data['user_id'])
							 ->select('oph.id', 'oph.amount', 'oph.currency', 'pu.level', 'oph.created_at', 'pu.type')
							 ->orderBy('oph.id', 'DESC')
							 ->get();


		$arr = array();

		foreach ($qry as $key) {
			$tmp = array();
			$tmp['type']       = $key->type;
			$tmp['invoice_id'] = $key->id * 1000;
			$tmp['level']      = 'Premium User Level '. $key->level;
			$tmp['amount']	   = $key->amount;
			$tmp['currency']   = $key->currency;
			$tmp['created_at'] = $key->created_at;
			$tmp['created_at'] = date("m/d/Y", strtotime($tmp['created_at']));

			$arr[] = $tmp;
		}

		return $arr; 
	}

	public function getInvoiceForAdmin($data) {
		$qry = DB::connection('rds1')->table('omni_pay as op')
					->join('omni_purchase_history as oph', 'op.user_id', '=', 'oph.user_id')
					->where('op.user_id', $data['user_id'])
					->select('op.type', 'oph.id', 'oph.amount', 'oph.currency', 'oph.created_at')
					->orderBy('oph.id', 'DESC')
					->get();

		$arr = array();

		foreach ($qry as $key) {
			$tmp = array();
			$tmp['type'] = $key->type;
			$tmp['invoice_id'] = $key->id * 1000;
			$tmp['amount']	   = $key->amount;
			$tmp['currency']   = $key->currency;
			$tmp['created_at'] = $key->created_at;
			$tmp['created_at'] = date("m/d/Y", strtotime($tmp['created_at']));

			$arr[] = $tmp;
		}

		return $arr;
	}
}