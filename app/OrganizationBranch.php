<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrganizationBranch extends Model
{
    
    protected $table = 'organization_branches';
	protected $fillable = array( 'organization_id','slug', 'school_id', 'aor_only', 'num_of_filtered_rec', 'premier_trial_end_date',
                'bachelor_plan', 'is_auto_approve_rec', 'cost_per_handshake', 'balance',
                'set_goal_reminder', 'existing_client', 'num_of_applications', 'num_of_enrollments', 'trigger_set',
                'trigger_frequency', 'trigger_notify_emails', 'trigger_emergency_set', 'trigger_emergency_percentage',
                'trigger_sent_on', 'requested_upgrade', 'pending_auto_campaign', 'handshake_auto_campaign');

	public function orgs()
    {
        return $this->belongsTo('Organization');
    }

	public function organization()
    {
        return $this->hasOne('Organization');
    }

    public function getCollegeMemberSince($school_id){

        $ob = OrganizationBranch::on('rds1')->where('school_id', $school_id)->first();
        
        return $ob;
    }

    public function searchCollegesInOrgBranchesByKeyword($input){
        if (!isset($input)) {
            return null;
        }
        $input = trim($input);

        if (strpos($input, ' ') !== FALSE){
            $input = explode(" ", $input);
        }
        
        $ob = DB::connection('rds1')->table('organization_branches as ob')
                            ->join('colleges as c', 'c.id', '=', 'ob.school_id')
                            ->where(function($qry) use ($input){
                                if (is_array($input)) {
                                    foreach ($input as $key => $val) {
                                        $qry = $qry->orWhere('c.school_name', 'like', '%'.$val.'%')
                                            ->orWhere('c.alias', 'like', '%'.$val.'%');
                                    }
                                }else{
                                    $qry = $qry->orWhere('c.school_name', 'like', '%'.$input.'%')
                                            ->orWhere('c.alias', 'like', '%'.$input.'%');
                                }                       
                            })
                            ->select('c.id as college_id', 'c.school_name', 'c.slug', 'c.logo_url',
                                     'c.city', 'c.state')
                            ->take(100)
                            ->get();

        return $ob;
    }
}
