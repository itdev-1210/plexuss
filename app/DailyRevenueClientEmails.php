<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyRevenueClientEmails extends Model
{
    protected $table = 'daily_revenue_client_emails';

    protected $fillable = array('fname', 'email', 'client_name', 'last_sent');

    public static function updateLastSent($id) {
        $right_now = date('Y-m-d H:i:s');

        DailyRevenueClientEmails::where('id', '=', $id)->update(['last_sent' => $right_now]);

        return 'success';
    }
}
