<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfilawSurveyUser extends Model {


	protected $table = 'infilaw_survey_users';

 	protected $fillable = array( 'ip', 'school_name', 'name', 'email', 'phone', 'address', 'experience_satisfy', 
 			'career_satisfy', 'networking_alum', 'in_legal', 'income', 'interested_in_stipend', 'pass_bar',
 			'bar_state', 'licensed_attorney', 'jurisdiction_state', 'practicing_attorney', 'current_employer', 
 			'practicing_attorney_experience', 'is_finished', 'amazon_code_status');
}