<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';

	//protected $fillable = array( 'school_id' );
	public $timestamps = false;
}
