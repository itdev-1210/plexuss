<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EdxEmailLog extends Model
{
    protected $table = 'edx_email_logs';

 	protected $fillable = array( 'user_id', 'template_id', 'user_invite_id' );
}
