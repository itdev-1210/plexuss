<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlexussOnboardingSignupApplication extends Model
{
    protected $table = 'plexuss_onboarding_signup_applications';

    protected $fillable = array('user_id', 'fname', 'lname', 'email','password','birth_date','company','title','working_since','phone', 'skype','blurb', 'profile_photo', 'created_at', 'updated_at');

    public static $rules = array(
        'fname'=>'required|regex:/^([a-zA-Z]+\s*)+$/',
        'lname' => 'required|regex:/^([a-zA-Z]+\s*)+$/',
        'email' => 'required|unique:plexuss_onboarding_signup_applications',
        'password' => array('required', 'unique:plexuss_onboarding_signup_applications', 'regex:/^(?=[^\d_].*?\d)\w(\w|[!@#$%*]){7,20}/'),
        'company' =>  array('unique:plexuss_onboarding_signup_applications', 'regex:/^([a-zA-Z]+\s*)+$/')
    );
}