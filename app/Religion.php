<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $table = 'religions';

	public function getAllUsReligions(){
		$eth = Religion::orderBy('religion')->get();
		$temp = array();
		$temp[''] = 'Select...';
		foreach ($eth as $key) {
			$temp[$key->religion] = $key->religion;
		}
		return $temp;
	}

	public function getAllUsReligionsById(){
		$eth = Religion::orderBy('religion')->get();
		$temp = array();
		$temp[''] = 'Select...';
		foreach ($eth as $key) {
			$temp[$key->id] = $key->religion;
		}
		return $temp;
	}

	public function getAllReligionsCustom(){
		$rel = Religion::orderBy('religion')->get();

		$all = array();

		foreach ($rel as $key) {
			$tmp = array();
			$tmp['id'] = (int)$key->id;
			$tmp['name'] = $key->religion;
			$all[] = $tmp;
		}

		return $all;
	}
}
