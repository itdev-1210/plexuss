<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSalesControl extends Model {


	protected $table = 'users_sales_control';

 	protected $fillable = array( 'user_id' );


 	public function user(){
        return $this->belongsTo('User');
    }


}