<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeAdmission extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'colleges_admissions';
}
