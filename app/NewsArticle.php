<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use DB;
class NewsArticle extends Model
{
	use Sluggable;

    /**
     * Sluggable configuration.
     *
     * @var array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source'         => 'title',
                'separator'      => '-',
                'includeTrashed' => true,
            ]
        ];
    }

	protected $table = 'news_articles';

	protected $fillable = array('news_subcategory_id', 'author', 'title', 'content', 'img_sm', 'img_lg','created_at', 'mobile_height');

	protected $input;

	public function subcategory(){
		return $this->hasOne('NewsSubcategory', 'id', 'news_subcategory_id');
	}
 	public function getNewsIdbyName($name)
	{
		$getNews=DB::table('news_articles')
		->where('slug',$name)
		->where('news_articles.live_status', '1')
		->first();
		return $getNews->id;
	}

	public function getCatSubCatIdbyName($name,$table)
	{
		$getCats=DB::table($table)->where('slug',$name)->first();

		return isset($getCats->id) ? $getCats->id : null;
	}

	public function listAll(){
		$list = DB::table('news_articles')
			->leftJoin('news_subcategories', 'news_subcategories.id', '=', 'news_articles.news_subcategory_id')
			->leftJoin('news_categories', 'news_categories.id', '=', 'news_subcategories.news_category_id')
			->leftJoin('users', 'users.id', '=', 'news_articles.author')
			->select('news_articles.id as id', 'news_articles.updated_at as updated', 'news_categories.name as category', 'news_subcategories.name as subcategory', 'news_articles.slug as slug', 'news_articles.title as title', 'users.fname as author_f', 'users.lname as author_l', 'news_articles.who_qa as who_qa', 'news_articles.who_live as who_live', 'news_articles.live_status as live_status', 'news_articles.page_title as page_title', 'news_articles.meta_keywords as meta_keywords', 'news_articles.meta_description as meta_description')
			//->orderBy('news_articles.updated_at', 'DESC')
			//->orderBy('news_articles.live_status', 'ASC')
			->get();

		return $list;
	}

	public function getCategoryId($article){
		$category_id = DB::table('news_subcategories')->where('id', $article['news_subcategory_id'])->pluck('news_category_id');

		$category_id = $category_id[0];
		return $category_id;
	}

	public function setInput($input){
		$this->input = $input;
	}

	/* Adds a new revision (row) to a live article as a pending revision (not live)
	 * This method uses the setInput() method in order to work with the user's
	 * input. This method also calls handleImages() which handles uploading
	 * and deleting of images on S3. After saving values to the DB, calls the
	 * newAdminActivity() method, which updates the admin_activity table which
	 * logs all updates from all admin-backend pages.
	 *
	 * **For the function that adds a new pending revision for an entirely new
	 * article, see newNewsArticle(). 
	 *
	 */
	public function addNewsRevision($type){
		echo "addNewsRevision function! <br>";
		$new_pending = new NewsArticle;
		if($type == 'existing'){
			$existing = NewsArticle::find($this->input['id']);
			$new_pending->author = $existing->author;
			$new_pending->prev_id = $this->input['id'];
		}
		else{
			$new_pending->author = $this->input['user_id'];
		}
		$new_pending->title = $this->input['title'];
		$new_pending->slug = $this->input['slug'];
		$new_pending->news_subcategory_id = $this->input['news_subcategory_id'];
		$new_pending->content = $this->input['content'];
		$new_pending->source = $this->input['source'];
		$new_pending->page_title = $this->input['page_title'];
		$new_pending->meta_keywords = $this->input['meta_keywords'];
		$new_pending->meta_description = $this->input['meta_description'];
		if($new_pending->source === 'external'){
			$new_pending->external_name = $this->input['external_name'];
			$new_pending->external_url = $this->input['external_url'];
			$new_pending->external_author = $this->input['external_author'];
		}
		
		// Set DE/QA/LIVE
		/* oldArticle is instantiated to 'retire' the previous article if the current
		 * pending revision is going live
		 */
		$new_pending->who_de = isset($this->input['who_de']) ? $this->input['who_de'] : $new_pending->who_de;
		$new_pending->who_qa = $this->input['who_qa'];
		$new_pending->who_live = $this->input['who_live'];
		if(!is_null($new_pending->who_live)){
			$new_pending->live_status = 1;
			if(!is_null($this->input['prev_id'])){
				$oldArticle = NewsArticle::find($this->input['prev_id']);
				$oldArticle->live_status = -1;
			}
		}
		else{
			$new_pending->live_status = 0;
		}

		// Set images/upload to AWS
		$this->handleImages($new_pending);
		echo "after handle images: <br>";
		//Set image filenames to same as old if none added
		if(is_null($new_pending->img_lg)){
			$new_pending->img_lg = $existing->img_lg;
		}
		else{
			echo "new large image set: " . $new_pending->img_lg . "<br>";
		}
		if(is_null($new_pending->img_sm)){
			$new_pending->img_sm = $existing->img_sm;
		}
		else{
			echo "new small image set: " . $new_pending->img_sm . "<br>";
		}

		// DATABASE INTERACTION
		// sets two arrays to compare original and modified changes between
		// two articles, so that they can be set after a successful insert
		$modified_rev = $new_pending->attributes;
		// We set the original revision to the existing article
		if($type == 'existing'){
			$original_rev = $existing->original;
		}
		// if it's a NEW ARTICLE then everything's changed, so we set all to null
		// so that newAdminActivity() can recognize the changes
		else{
			$original_rev = $modified_rev;
			foreach($original_rev as $key => $val){
				$original_rev[$key] = null;
			}
		}

		// update article row
		$insert = $new_pending->save();
		if($insert){
			// if we're updating a pending for for an EXISTING article
			if($type == 'existing'){
				// gets the id of the new revision in order to use the value in the log
				$new_rev_id = NewsArticle::where('prev_id', '=', $this->input['id'])->pluck('id');

				$new_rev_id = $new_rev_id[0];
			}
			// if we're adding a new row for a NEW article
			else{
				// gets the id of the new revision in order to use the value in the log
				$new_rev_id = NewsArticle::where('slug', '=', $this->input['slug'])->pluck('id');

				$new_rev_id = $new_rev_id[0];
			}
			// Calls a function which logs changes in the admin_activity table
			$this->newAdminActivity($original_rev, $modified_rev, $new_rev_id);
		
			// set previous live article to 'old' (-1) if pushing article live
			if(isset($oldArticle)){
				$unsetLive = $oldArticle->save();
			}
		}
	}

	/* AWS S3 image logic
	 * Receives an array of images, checks if they're different from what
	 * the article was already pointing to. Also updates the article's
	 * columns that point to the filename
	 * param		$existing			obj		the news_article object
	 * */
	private function handleImages($existing){
		// IMAGE UPLOAD
		// Image path on DB
		$lg = $this->input['img_lg'];
		$sm = $this->input['img_sm'];
		$images = array($lg, $sm);
		$i = 0;
		$uid = $this->input['user_id'];;
		$timestamp = date('Y-m-d_His');
		echo "large: " . $existing->img_lg . "<br>";
		echo "small: " . $existing->img_sm . "<br>";
		foreach($images as $image){
			echo "foreach run! <br>";

			if(is_null($image)){
				$i++;
				continue;
			}
			$filePath = $image->getRealPath();
			$extension = $image->getClientOriginalExtension();
			$path = $filePath;

			switch($i){
				case 0:
					$size = "_lg";
					$filename = $uid . "_" . $timestamp . $size . "." . $extension;
					break;

				case 1:
					$size = "_sm";
					$filename = $uid . "_" . $timestamp . $size . "." . $extension;
					break;
			}

			$image_size = ($i == 0) ? 'img_lg' : 'img_sm';
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mime_type = finfo_file($finfo, $image);
			$aws = AWS::get('s3');
			/* Adding this exception here for when ADDING a NEW ARTICLE and the images
			 * are not yet set. FIRST DELETE THE IMAGE WITH THE OBJECT'S FILENAME
			 */
			if(!is_null($existing->$image_size)){
				$aws->deleteObject(array(
					'Bucket'	=> 'asset.plexuss.com/news/images',
					'Key'		=> $existing->$image_size,
				));
			}
			//THEN ADD THE FILENAME FOR THE NEW IMAGE
			$existing->$image_size = $filename;
			echo "i: " . $i . " image_size: " . $image_size . " filename: " . $filename . "<br>";
			$aws->putObject(array(
				'ACL'        => 'public-read',
				'Bucket'     => 'asset.plexuss.com/news/images',
				'contentType'=> $mime_type,
				'Key'        => $filename,
				'SourceFile' => $path, 
			));
			$i++;
		}

	}

	/* Edits a pending (live_status = 0) news article row
	 * This function interacts with a property 'input' which is set
	 * by the helper function setInput(). It modifies the pending row
	 * in news_articles, sets the de/qa/live status, and calls the
	 * functions which uploads images and removes old ones on AWS S3.
	 * Also calls the functions which logs changes in the
	 * admin_activity table
	 * */
	public function editNewsRevision(){
		echo "NewsArticle function: editRevision running! <br>";
		//set variables to be added to new row
		$existing = NewsArticle::find($this->input['id']);
		$existing->title = $this->input['title'];
		$existing->slug = $this->input['slug'];
		$existing->news_subcategory_id = $this->input['news_subcategory_id'];
		$existing->content = $this->input['content'];
		$existing->source = $this->input['source'];
		$existing->page_title = $this->input['page_title'];
		$existing->meta_keywords = $this->input['meta_keywords'];
		$existing->meta_description = $this->input['meta_description'];
		if($existing->source === 'external'){
			$existing->external_name = $this->input['external_name'];
			$existing->external_url = $this->input['external_url'];
			$existing->external_author = $this->input['external_author'];
		}

		// Set DE/QA/LIVE
		$existing->who_de = isset($this->input['who_de']) ? $this->input['who_de'] : $existing->who_de;
		$existing->who_qa = $this->input['who_qa'];
		$existing->who_live = $this->input['who_live'];
		// Set live status and unset old article here
		if(!is_null($existing->who_live)){
			$existing->live_status = 1;
			//Get old article - if it exists - to unset it
			if(!is_null($this->input['prev_id'])){
				$oldArticle = NewsArticle::find($this->input['prev_id']);
				$oldArticle->live_status = -1;
			}
		}
		else{
			$existing->live_status = 0;
		}

		// IMAGE UPLOAD
		// Image path on DB
		$this->handleImages($existing);

		// DATABASE INTERACTION
		// sets two arrays to compare original and modified changes between
		// two articles, so that they can be set after a successful insert
		$original_rev = $existing->original;
		$modified_rev = $existing->attributes;

		// update article row
		$insert = $existing->save();
		if($insert){
			// Calls a function which logs changes in the admin_activity table
			$this->newAdminActivity($original_rev, $modified_rev, $original_rev['id']);
		
			// set previous live article to 'old' (-1) if pushing article live
			if(isset($oldArticle)){
				$unsetLive = $oldArticle->save();
			}
		}
	}

	/* Receives a newsArticle object. Loops through all changes and
	 * adds a new row in the admin_activity table to log the changes made
	 * param		$existing		object			an object containing both new changes (->attributes)
	 * 												and old changes (->original)
	 * param		$id				int				the id that will show up in the
	 * 												admin_activity table `item_id` column
	 */
	public function newAdminActivity($original, $modified, $id){
		foreach($modified as $key => $val){
			if($original[$key] != $val){
				echo $key . " is MODIFIED! " . $val . "<br>";
				DB::table('admin_activity')
					->insert(array(
						'table' => 'news_articles',
						'column' => $key,
						'item_id' => $id,
						'old_val' => $original[$key],
						'new_val' => $val,
						'user_id' => $this->input['user_id'],
					));
			}
			else{
				echo $key . " is the same as the original! <br>";
			}
		}

	}

	public function getArticle( $id, $is_api = null ){

		$article = NewsArticle::where( 'news_articles.id', '=', $id )
			->join('news_subcategories', 'news_subcategories.id', '=', 'news_articles.news_subcategory_id')
			->join('news_categories', 'news_categories.id', '=', 'news_subcategories.news_category_id')
			->select(
				'news_articles.source',
				'news_articles.external_name',
				'news_articles.external_author',
				'news_articles.author',
				'news_articles.id',
				'news_articles.title',
				'news_articles.content',
				'news_articles.highlighted',
				'news_articles.basic_content',
				'news_articles.premium_content',
				
				'news_articles.created_at',
				'news_articles.slug',
				'news_articles.news_subcategory_id',
				'news_articles.comment_thread_id',
				
				'news_articles.authors_description',
				'news_articles.authors_profile_link',
				'news_categories.name as cat',
				'news_categories.id as cat_id',
				'news_categories.slug as catSlug',
				'news_subcategories.name as subcat',
				'news_subcategories.slug as subcatSlug'
			)
			->where('news_articles.live_status', '1');

		if (isset($is_api)) {
			$article = $article->addSelect('news_articles.img_sm', 'news_articles.img_lg', DB::raw('CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/news/images/", news_articles.authors_img) as authors_img'));
		}else{
			$article = $article->addSelect('news_articles.img_sm', 'news_articles.img_lg','news_articles.authors_img');
		}			

		$article =	$article->first();

		return $article;
	}

	public function newsDetails( $id=false, $catid = false, $sort = "desc", $flagCat = false, $admin = false, $user_id = null, $is_api = false ){
		// dd($flagCat);
		// ALLOW FETCHING OF NON-LIVE ARTICLES OF USER IS AN ADMIN
		// SHOW ONLY LIVE ARTICLES TO NON-ADMIN USERS
		/*
		if($admin){
			if($admin['news'] == 'r' || $admin['news'] == 'w'){
			$query=DB::table('news_articles')
			->join('news_subcategories', 'news_subcategories.id', '=', 'news_articles.news_subcategory_id')
			->join('news_categories', 'news_categories.id', '=', 'news_subcategories.news_category_id')
			->select('news_articles.source', 'news_articles.external_name', 'news_articles.external_author', 'news_articles.author', 'news_articles.id', 'news_articles.title', 'news_articles.content','news_articles.img_sm','news_articles.img_lg','news_articles.created_at','news_articles.slug','news_articles.news_subcategory_id','news_subcategories.name as subcat','news_categories.name as cat','news_categories.id as cat_id','news_categories.slug as catSlug','news_subcategories.slug as subcatSlug', 'news_articles.comment_thread_id');
			}
		} else {
			$query=DB::table('news_articles')
			->join('news_subcategories', 'news_subcategories.id', '=', 'news_articles.news_subcategory_id')
			->join('news_categories', 'news_categories.id', '=', 'news_subcategories.news_category_id')
			->select('news_articles.source', 'news_articles.external_name', 'news_articles.external_author', 'news_articles.author', 'news_articles.id', 'news_articles.title', 'news_articles.content','news_articles.img_sm','news_articles.img_lg','news_articles.created_at','news_articles.slug','news_articles.news_subcategory_id','news_subcategories.name as subcat','news_categories.name as cat','news_categories.id as cat_id','news_categories.slug as catSlug','news_subcategories.slug as subcatSlug', 'news_articles.comment_thread_id')
			->where('news_articles.live_status', '1');
		}
		 */


		$query=DB::table('news_articles')
		->join('news_subcategories', 'news_subcategories.id', '=', 'news_articles.news_subcategory_id')
		->join('news_categories', 'news_categories.id', '=', 'news_subcategories.news_category_id')
		->select('news_articles.source', 'news_articles.external_name', 'news_articles.external_author', 'news_articles.author', 'news_articles.id', 
				'news_articles.title', 'news_articles.content', 'news_articles.basic_content', 'news_articles.premium_content', 'news_articles.authors_description', 'news_articles.img_sm','news_articles.img_lg',
				'news_articles.created_at','news_articles.slug', 'news_articles.authors_img',
				'news_articles.news_subcategory_id','news_subcategories.name as subcat','news_categories.name as cat','news_categories.id as cat_id',
				'news_categories.slug as catSlug','news_subcategories.slug as subcatSlug', 'news_articles.comment_thread_id', 'news_articles.has_video')
		->where('news_articles.live_status', '1')
		->where('news_articles.news_subcategory_id', '!=', 22)
		->where('news_articles.news_subcategory_id', '!=', 12)
		->where('news_articles.news_subcategory_id', '!=', 11);
		
		// ->whereNotNull('news_articles.content')
		// ->whereNull('news_articles.basic_content')
		// ->whereNull('news_articles.premium_content');

		// If user is not an admin
		if( !$admin ){
			if( $admin['news'] != 'r' && $admin['news'] != 'w' ){
				$query = $query->where('news_articles.live_status', '1');
			}
		}

		if (isset($user_id)) {
			$query = $query->leftJoin('users_premium_essays as upe', function($q) use ($user_id){
									  $q->on('upe.news_id', '=', 'news_articles.id');
									  $q->on('upe.user_id', '=', DB::raw($user_id));
									})
						   ->addSelect('upe.id as hasViewed');
		}

		// If cat is not college essays
		if($catid != 5){	
			$query = $query->whereNotNull('news_articles.content')
							->whereNull('news_articles.basic_content')
							->whereNull('news_articles.premium_content');
		}

		
		if($id!='') {	
			$query = $query->where('news_articles.id', '=',$id);
		}
		// dd($query->get());
		if($catid!='' && $flagCat=='1') {	
			
			$query = $query->where('news_categories.id', '=',$catid);
		}
		// dd($query->get());
		if($catid!='' && $flagCat=='2') {	
			//echo 'b';
			$query = $query->where('news_articles.news_subcategory_id', '=', $catid);
		}
		$query = $query->orderBy('news_articles.id', $sort);

        if ($is_api && $catid == 5) {
            $query = $query->addSelect('news_articles.premium_content')
                           ->get();
        }else{
            $query = $query->paginate(6);
        }

		foreach($query as $news_val){
			if($news_val->source == 'external'){
				if($news_val->external_author != ''){
					$author = 'By ' . $news_val->external_author;
					$news_val->visible_author = $author;
				}
				else{
					$author = 'Via ' . $news_val->external_name;
					$news_val->visible_author = $author;
				}
			}
			else{
				'By' . $author = $news_val->author;
				$news_val->visible_author = $author;
			}

			if( $is_api ){
				$news_val->img_sm = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/news/images/'.$news_val->img_sm;
				$news_val->img_lg = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/news/images/'.$news_val->img_lg;
				$news_val->authors_img = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/news/images/'.$news_val->authors_img;
			}
		}

		return $query;


	}	
	
	public function HomeNewsDetails($skipAmount = 0){

		$query = DB::table('news_articles')
		->join('news_subcategories', 'news_subcategories.id', '=', 'news_articles.news_subcategory_id')
		->join('news_categories', 'news_categories.id', '=', 'news_subcategories.news_category_id')
		->select('news_articles.source', 'news_articles.external_name', 'news_articles.external_author', 'news_articles.author','news_articles.id', 'news_articles.authors_img',
			'news_articles.slug', 'news_articles.title', 'news_articles.content','news_articles.img_sm','news_articles.img_lg','news_articles.created_at',
			'news_articles.news_subcategory_id','news_subcategories.name as subcat','news_categories.name as cat','news_categories.id as cat_id',
			'news_articles.has_video')
		->orderBy('news_articles.id', 'desc')
		->where('news_articles.live_status', '1')
		->where('news_subcategory_id', '!=', 22)
		->where('news_subcategory_id', '!=', 12)
		->where('news_subcategory_id', '!=', 11)
		->skip($skipAmount) // Skip for pagination
		->take(6);

		$newsdata = $query->get();

		//echo '<pre>';
		//var_dump($newsdata);
		//exit;
		foreach($newsdata as $news_val){
			if($news_val->source == 'external'){
				if($news_val->external_author != ''){
					$author = 'By ' . $news_val->external_author;
					$news_val->visible_author = $author;
				}
				else{
					$author = 'Via ' . $news_val->external_name;
					$news_val->visible_author = $author;
				}
			}
			else{
				'By' . $author = $news_val->author;
				$news_val->visible_author = $author;
			}
		}

		return $newsdata;
	}
	
	
	
	
	public function relatedNews($catid=false,$newsid=false)
	{
		$result=DB::table('news_articles')->where('news_subcategory_id', '=',$catid)->whereNotIn('id', array($newsid))->get();
		return $result;
	}
	
	public function FeaturedArticles($catid=false){
		$result = DB::table('news_articles')
		->orderByRaw("RAND()")
		->where('news_articles.live_status', '1')
		->where('news_articles.news_subcategory_id', '!=', '11') //blog press
		->where('news_articles.news_subcategory_id', '!=', '12') //blog new features
		->where('news_articles.news_subcategory_id', '!=', '22') //blog 
		->take(3)
		->get();
		return $result;
	}


	public function featuredBlog($catId){
		$result = DB::table('news_articles as na')
		->orderByRaw("RAND()")
		->where('na.live_status', '1')
		->where('na.news_subcategory_id', '=', $catId)
		->take(3)
		->get();
		return $result;
	}

	public function getVideoArticles(){
		$result = DB::connection('rds1')->table('news_articles')
					->orderByRaw("RAND()")
					->where('news_articles.live_status', '1')
					->where('news_articles.has_video', '1')
					->take(10)
					->get();
		
		return $result;
	}

	public function searchArticles($input = null){
		if( !isset($input) ){
			return 'fail';
		}

		$result = DB::connection('rds1')->table('news_articles as na')
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('na.title', 'like', '%'.$input.'%')
								   ->orWhere('na.content', 'like', '%'.$input.'%')
								   ->orWhere('na.premium_content', 'like', '%'.$input.'%');
					})
					->orderBy('na.created_at', 'desc')
					->get();
		
		return $result;
	}


	public function searchBlog($input = null, $type = ''){

		$catId = 22;
		
		if( !isset($input) ){
			return false;
		}



		if(strtolower($type) == 'press releases'){
			$catId = 11;
		}
	


		$result = DB::connection('rds1')->table('news_articles as na')
					->where('na.news_subcategory_id', $catId)
					->where(function($qry) use ($input){
						$qry = $qry->orWhere('na.title', 'like', '%'.$input.'%')
									->orWhere('na.content', 'like', '%'.$input.'%');
					})
					->orderBy('na.updated_at', 'desc')
					->get();
		
		return $result;


	}


	public function getBlogArticles($subCat, $page){

		$subCat = $subCat ? $subCat : 'blog';

		$catID = null;
		$flag = 1;  //used in other methods to see if getting all or subcat only

		switch($subCat){
			case 'blog': 
				$catID = $this->getCatSubCatIdbyName('blog','news_subcategories');  
				break;
			case 'press':
				$catID = $this->getCatSubCatIdbyName('b2b-press','news_subcategories'); 
				break;
			case 'features':
				$catID = $this->getCatSubCatIdbyName('plexuss-new-features','news_subcategories'); 
				break;
			default:
				$catID = $this->getCatSubCatIdbyName('blog','news_subcategories');  
				break;

		}


		$result = DB::connection('bk')->table('news_articles as na')
					->where('news_subcategory_id', $catID );

		$flag = 2;
						

		$result = $result->orderBy('na.created_at', 'desc')->paginate(8);

		$result->cat_id = 7;
		$result->subcat_id = $subCat;
		$result->flag_cat = $flag;
		$result->page = $page;
		$data['currentPage'] = $page;

		return $result;

	}


	public function getNewFeatures($offset){

		// $month = sprintf( '%02d',  $month+1);

		$result = DB::connection('bk')->table('news_articles as na')
					->where('news_subcategory_id', 12 )
				    //->whereRaw('na.updated_at >=  DATE_SUB(NOW(),INTERVAL 1 YEAR) or na.created_at >=  DATE_SUB(NOW(),INTERVAL 1 YEAR)')
					->orderBy('na.updated_at', 'desc')
					->skip($offset)
					->limit(1)
					->get();

		return $result;


	}

	public function getNewsByCategoryName($category_slug, $offset){
		
		$qry = DB::connection('rds1')->table('news_articles as na')
									 ->join('news_subcategories as ns', 'ns.id', '=', 'na.news_subcategory_id')
									 ->join('news_categories as nc', 'nc.id', '=', 'ns.news_category_id')
									 ->where('na.live_status', 1);

		if ($category_slug == "college-essays") {

			$qry = $qry->whereNull('na.content')
					   ->whereNotNull('na.basic_content')
					   ->whereNotNull('na.premium_content');
		}else{
			$qry = $qry->whereNotNull('na.content')
					   ->whereNull('na.basic_content')
					   ->whereNull('na.premium_content');
		}
									 
		$qry = $qry->where('nc.slug', $category_slug)
				   ->take(10)
				   ->skip($offset)
				   ->groupBy('na.id')
				   ->selectRaw('na.*, CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/news/images/",na.img_sm) as full_sm, CONCAT("https://s3-us-west-2.amazonaws.com/asset.plexuss.com/news/images/",na.img_lg) as full_lg')
				   ->get();

		return $qry;
	}
	
	// public function fullNewsDetails($id){
		
	// 	$result = DB::table('news_articles')
	// 	->join('news_subcategories as ns', 'na.news_subcategories.id', '=', 'ns.id')
	// 	->join('news_categories as nc', 'nc.id', '=', 'ns.news_category_id')
	// 	->select('na.*');
		
		
				 
		
	// 			/*SELECT na.* from `news_articles` as na 
	// 	INNER JOIN `news_subcategories` as ns 
	// 	ON na.`news_subcategory_id` = ns.`id` 
	// 	INNER JOIN `news_categories` as nc 
	// 	ON nc.`id` = ns.`news_category_id` WHERE na.'id' = $id*/
	// }
}
