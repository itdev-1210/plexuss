<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use DB;
use App\CollegesApplicationDeclaration;
use App\Http\Controllers\Controller;

class UsersAppliedColleges extends Model {

	protected $table = 'users_applied_colleges';

 	protected $fillable = array('user_id', 'college_id', 'submitted', 'auto_prescreened');

 	// This method adds a user applied college with submitted status
 	public static function addUserAppliedCollege($user_id, $college_id) {
 		$attributes = [ 'user_id' => $user_id, 'college_id' => $college_id ];
 		
 		$values = [ 'submitted' => 1 ];

 		UsersAppliedColleges::updateOrCreate($attributes, $values);
 	}

 	public static function removeUserAppliedCollege($user_id, $college_id) {
 		UsersAppliedColleges::where('user_id', $user_id)
 							->where('college_id', $college_id)
 							->delete();
 	}

 	public function getAppliedColleges($user_id){

 		$applyTo_schools = array();
 		$bc = new Controller;

 		$qry = DB::connection('rds1')->table('users as u')
 									 ->leftjoin('users_applied_colleges as uac', function($join) {
				                        $join->on('uac.user_id', '=', 'u.id');
				                        $join->where('uac.college_id', '!=', 7916);
				                    })
				                    ->leftjoin('users_applied_colleges_declarations as uacd', 'uacd.user_id', '=', 'u.id')

				                    ->leftjoin('colleges as c2', 'c2.id', '=', 'uac.college_id')
				                    ->leftJoin('colleges_ranking as cr2', 'cr2.college_id', '=', 'c2.id')
				                    ->leftJoin('colleges_admissions as ca2', 'ca2.college_id', '=', 'c2.id')
				                    ->leftjoin('colleges_international_tuition_costs as citc', 'citc.college_id', '=', 'c2.id')

				                    ->where('u.id', $user_id)
				                    ->select('uac.college_id as applied_college_id', 'uac.submitted as applied_submitted',
                        					 'uacd.declaration_id', 'c2.school_name as applied_college_school_name', 
                        					 'c2.logo_url as applied_college_logo_url','c2.city as city2', 'c2.state as state2',
                        					 'ca2.application_fee_undergrad as application_fee_undergrad2', 
                        					 DB::raw('IF(c2.paid_app_url IS NOT NULL, c2.paid_app_url, c2.application_url) as app_url'),
                        					 DB::raw('IF(cr2.plexuss IS NOT NULL, cr2.plexuss, "N/A") as rank2'),
                        					 'citc.undergrad_avg_tuition', 'citc.grad_avg_tuition', 'citc.undergrad_other_cost', 
                        					 'citc.grad_other_cost')

				                    ->orderBy('uac.updated_at', 'desc')
				                    ->get();

		$user = User::on('rds1')->find($user_id);

		foreach ($qry as $key) {

			if (isset($key->applied_college_id) && !$bc->get_index_multidimensional_boolean($applyTo_schools, 'college_id', (int)$key->applied_college_id) ) {
                $tmp = array();

                if (isset($user_id)) {

                    $query = DB::connection('rds1')->table('colleges_application_allowed_sections')
                       ->where('college_id', $key->applied_college_id)
                       ->select('page', 'sub_section', 'required')
                       ->groupBy('page', 'sub_section');


                    if (isset($user)) {
                        if (isset($user->degree_type)) {
                            if ($user->degree_type == 1 || $user->degree_type == 2 || $user->degree_type == 3 ||
                                $user->degree_type == 6 || $user->degree_type == 7 || $user_obj->degree_type == 8) {

                                $query = $query->where(function($q){
                                                        $q->orWhere('define_program', '=', 'undergrad')
                                                          ->orWhere('define_program', '=', 'epp');
                                });
                            }else{
                                $query = $query->where(function($q){
                                                        $q->orWhere('define_program', '=', 'grad')
                                                          ->orWhere('define_program', '=', 'epp');
                                });
                            }
                        }
                    }

                    $query = $query->get();

                    foreach ($query as $k) {

                        if ($k->page == 'uploads') {
                            if (isset($tmp['allowed_uploads'])) {
                                $tmp['allowed_uploads'][] = $k->sub_section;
                            }else{
                                $tmp['allowed_uploads'] = array();
                                $tmp['allowed_uploads'][] = $k->sub_section;

                                $tmp['allowed_sections'][] = $k->page;
                            }
                        }elseif ($k->page == 'custom' || $k->page == 'additional') {
                           if (isset($tmp['custom_questions'])) {
                                $tmp['custom_questions'][$k->sub_section] = (isset($k->required) && $k->required == 1) ? true : false;
                           }else{
                                $tmp['custom_questions']   = array();
                                $tmp['custom_questions'][$k->sub_section] = (isset($k->required) && $k->required == 1) ? true : false;
                           }
                        }else{
                            $tmp['allowed_sections'][] = $k->page;
                        }
                    }
                }

                if (isset($user)) {
                    $cap = CollegesApplicationDeclaration::on("rds1")->where('college_id', $key->applied_college_id);
                    if (isset($user->degree_type) && ( $user->degree_type == 1 || $user->degree_type == 2 || $user->degree_type == 3 ||
                        $user->degree_type == 6 || $user->degree_type == 7 || $user->degree_type == 8 ))  {

                        $cap = $cap->where(function($q){
                                           $q->orWhere('type', '=', DB::raw("'undergrad'"));
                                           $q->orWhere('type', '=', DB::raw("'both'"));
                        });
                    }
                    else{
                        $cap = $cap->where(function($q){
                                           $q->orWhere('type', '=', DB::raw("'grad'"));
                                           $q->orWhere('type', '=', DB::raw("'both'"));
                        });
                    }

                    $cap = $cap->get();

                    foreach ($cap as $k) {
                        $new = array();

                        $new['id']       = $k->id;
                        $new['language'] = $k->language;

                        $tmp['declarations'][] = $new;
                    }
                }

                $tmp['city'] = $key->city2;
                $tmp['state'] = $key->state2;                
                $tmp['rank']        = $key->rank2;
                $tmp['application_fee'] = $key->application_fee_undergrad2;

                if (isset($key->app_url) && !empty($key->app_url) && substr( $key->app_url, 0, 4 ) !== "http") {
                	$tmp['app_url']         = 'http://'.$key->app_url;
                }

                $tmp['college_id'] = (int)$key->applied_college_id;
                $tmp['submitted']  = (int)$key->applied_submitted;
                $tmp['logo_url']   = 'https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/'.$key->applied_college_logo_url;
                $tmp['school_name']			  = $key->applied_college_school_name;
                
                $tmp['avg_tuition']     	  = isset($key->undergrad_avg_tuition) ? (double)$key->undergrad_avg_tuition : 0;
                
                $tmp['undergrad_column_cost'] = ((double)$key->undergrad_avg_tuition + (double)$key->undergrad_other_cost) != 0 ? 
                                                  (double)$key->undergrad_avg_tuition + (double)$key->undergrad_other_cost : 0;

                $tmp['grad_column_cost']      = ((double)$key->grad_avg_tuition + (double)$key->grad_other_cost) != 0 ? 
                                                      (double)$key->grad_avg_tuition + (double)$key->grad_other_cost : 0;

                $applyTo_schools[] = $tmp;
            }
		}

		return $applyTo_schools;	                    
 	}
}
