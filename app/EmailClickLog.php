<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailClickLog extends Model
{
    protected $table = 'email_click_logs';
    
 	protected $fillable = array('email', 'user_id', 'template_id', 'geo_city', 'geo_country', 'geo_latitude', 'geo_longitude', 'click_date', 'url', 'user_agent', 'response', 'valid', 'company', 'ro_id', 'college_id', 'utm_source', 'utm_medium', 'utm_content');

}