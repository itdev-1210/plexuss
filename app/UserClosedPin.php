<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClosedPin extends Model{

	protected $table = 'user_closed_pins';
}
