<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TwSmsLog  extends Model {

	protected $connection = 'ldy';
    protected $table = 'tw_sms_log';
    
    public $timestamps = false;
    
    protected $fillable = array(
                    'url', 
                    'leadid', 
                    'time', 
                    'date', 
                    'fromPhone', 
                    'toPhone', 
                    'smsBy', 
                    'smsMessageSid', 
                    'smsSid', 
                    'body',
                    'price',
					'price_unit',
					'error_code',
					'error_message',
					'smsStatus'
					);
	
	public function getNewMessages($params){
		
		$lastmessageid = $params->lastmessageid;
		$leadid = $params->leadid;

		$newmessages = DB::table('tw_sms_log')
							->where('id','>',$lastmessageid)
							->Where('leadid','=',$leadid)
							->get();
		 
		return $newmessages;				
	}
		
}