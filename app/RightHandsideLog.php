<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RightHandsideLog extends Model
{
    protected $table = 'right_handside_log';

	protected $fillable = array( 'cat_id','type','type_id', 'user_id', 'ip', 'updated_at');

}
