<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyProfileInfo extends Model {
	protected $table = 'agency_profile_info';

    protected $fillable = array('user_id', 'company_name', 'representative_name', 'website_url', 'about_company_text', 'agreement_name', 'agreement_email', 'agreement_date', 'profile_photo_url', 'created_at', 'updated_at', 'skype_id', 'whatsapp_id');

	public $timestamps = true;

	public static function insertOrUpdate($user_id, $input) {
		// Validate all required inputs are here.
		if (!isset($user_id) || !isset($input['company_name']) || !isset($input['representative_name']) || !isset($input['agreement_name']) || !isset($input['agreement_email']) || !isset($input['agreement_date'])) {

			return 'fail';
		}

		$attributes = ['user_id' => $user_id];

		$values = [ 'company_name' => $input['company_name'], 'representative_name' => $input['representative_name'],
				    'agreement_name' => $input['agreement_name'], 'agreement_email' => $input['agreement_email'],
				    'agreement_date' => $input['agreement_date']];

		// Loop through potential values and if set, store to values array.		
		$other_potential_values = ['website_url', 'about_company_text', 'profile_photo_url', 'skype_id', 'whatsapp_id'];

		foreach($other_potential_values as $key => $value) {
			if (isset($input[$value])) {
				$values = array_merge($values, [$value => $input[$value]]);
			}
		}

		AgencyProfileInfo::updateOrCreate($attributes, $values);

		return 'success';
	}


}
