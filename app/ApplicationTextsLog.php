<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationTextsLog extends Model
{
    protected $table = 'application_texts_logs';

 	protected $fillable = array('user_id', 'template_name');

}
