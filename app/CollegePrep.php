<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegePrep extends Model
{
    /**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'college_prep';


 	protected $fillable = array( 'company', 'contact', 'title', 'email', 'phone',
 	 'notes', 'college_counseling', 'tutoring_center', 'test_preparation', 'international_student_assistance' );
}
