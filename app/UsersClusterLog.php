<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersClusterLog extends Model
{
    protected $table = 'users_cluster_logs';

 	protected $fillable = array( 'cluster_id', 'user_id', 'user_updated' );

 	public function updateCluster($user_id, $user_updated = null){
 		
 		$user = User::on('rds1')->where('id', $user_id)->first();

 		$cluster_num = -1;
 		if (isset($user)) {

 			if (!isset($user->birth_date) && !isset($user->in_college)) {
 				if ($user->country_id == 1) {
 					$cluster_num = 5;
 				}else{
 					$cluster_num = 3;
 				}
 			}elseif (isset($user->birth_date)) {
 				// Cluster 1 (US Undergad)
 				$today = date("Y-m-d");
				$diff = date_diff(date_create($user->birth_date), date_create($today));
				$age = $diff->format('%y');

				if (isset($user->in_college)) {
					if ($user->in_college == 0 && $user->country_id == 1 && $age <=18) {
		 				$cluster_num = 1;
		 			}elseif ($user->in_college == 1 && $user->country_id == 1 && $age <= 25) {
		 				$cluster_num = 2;
		 			}elseif ($user->in_college == 0 && $user->country_id != 1  && $age <= 21) {
		 				$cluster_num = 3;
		 			}elseif ($user->in_college == 1 && $user->country_id != 1  && $age <= 28) {
		 				$cluster_num = 4;
		 			}elseif ($user->in_college == 0 && ( ($user->country_id == 1 && $age > 18) || ($user->country_id != 1 && $age > 21) ) ) {
		 				$cluster_num = 5;
		 			}elseif ($user->in_college == 1 && ( ($user->country_id == 1 && $age > 25) || ($user->country_id != 1 && $age > 28) ) ) {
		 				$cluster_num = 6;
		 			}
				}else{
					if ($user->country_id == 1 ) {
						$cluster_num = 5;
					}else{
						if ($age <= 25) {
							$cluster_num = 5;
						}else{
							$cluster_num = 6;
						}
					}
				}
 			}elseif (!isset($user->birth_date) && isset($user->in_college)){
 				if($user->in_college == 1){
 					$cluster_num = 6;
 				}else{
 					$cluster_num = 5;
 				}
 			}
 			
 		}
 		
 		if (isset($user_updated)) {
 			$user_updated = 1;
 		}else{
 			$user_updated = 0;
 		}
 		
 		if ($cluster_num != -1) {

 			$attr = array('user_id' => $user_id);
 			$val  = array('cluster_id' => $cluster_num, 'user_id' => $user_id, 'user_updated' => $user_updated);

 			$update = UsersClusterLog::updateOrCreate($attr, $val);
 			return true;
 		}else{
 			return false;
 		}
 	}
}
