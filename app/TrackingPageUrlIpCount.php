<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrlIpCount extends Model
{
    protected $table = 'tracking_page_url_ip_counts';

    protected $fillable = array( 'ip', 'date', 'count', 'tpu_id', 'device_id', 'browser_id', 'platform_id', 'college_id');
}
