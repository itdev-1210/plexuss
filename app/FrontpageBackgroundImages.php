<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontpageBackgroundImages extends Model
{
    //
    protected $table = 'frontpage_background_images';
}
