<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaDataMajor extends Model
{
     protected $table = 'meta_data_majors';

 	protected $fillable = array('cron_parsed', 'json_data');
}
