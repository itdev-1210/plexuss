<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    private $take = 10;				// Int: how many records to take
	private $thread_id;				// Id of the comment thread to look for
	private $latest_id;				// Id of the last comment the user has seen
									// So we know to fetch comment ids greater than
									// the latest id
	private $earliest_id;			// ID of the earliest comment. Used to
									// Fetch an earlier set of comments
	private $comments_array;		// Universal container to store comments array
	private $parent_ids;			// ID of the parent comment, if any
	private $get_earlier = false;	// boolean. used in switch to get earlier or
									// later comments
	private $get_later = true;		// boolean, used in switch to get later
	private $limit_children = true;	// Limits findChildren() results to only children
									// of the specified set of $this->parent_ids (an arr)

	public function setTake( $take ){
		$this->take = $take;
	}

	public function setThreadId( $id ){
		$this->thread_id = $id;
	}

	public function setLatestId( $id ){
		$this->latest_id = $id;
	}

	public function setEarliestId( $id ){
		$this->earliest_id = $id;
	}

	/* Receives an ARRAY of ids for a WHERE IN statement */
	public function setParentIds( $ids ){
		$this->parent_ids = $ids;
	}

	public function getArray(){
		return $this->comments_array;
	}

	public function setResultsEarlier(){
		$this->get_earlier = true;
		$this->get_later = false;
	}

	public function setResultsLater(){
		$this->get_later = true;
		$this->get_earlier = false;
	}

	public function limitChildren( $limit ){
		$this->limit_children = $limit;
	}

	public function findTopLevel(){
		// Get new comments since time of last heartbeat and time of submit
		$new_comments = Comment::where( 'comments.comment_thread_id', $this->thread_id );
			// gets earlier or later
			if( $this->get_earlier ){
				$new_comments = $new_comments->where( 'comments.id', '<', $this->earliest_id );
			}
			else{
				$new_comments = $new_comments->where( 'comments.id', '>', $this->latest_id );
			}
			$new_comments = $new_comments->where( 'comments.parent_id', null )
			->leftJoin( 'users', 'users.id', '=', 'comments.user_id_submitted' )
			->select( 
				'comments.*',
				'users.profile_img_loc as profile_image',
				'users.id as user_id',
				'users.fname',
				'users.lname',
				'users.is_student',
				'users.is_intl_student',
				'users.is_alumni',
				'users.is_parent',
				'users.is_counselor',
				'users.is_organization', 'users.comment_anon AS anon' 
			)
			->take( $this->take )
			->orderBy( 'comments.id', 'desc' )
			->get()
			->toArray();

		// Reverse results, so oldest items are first, and newer items push older ones down
		//$new_comments = array_reverse( $new_comments );

		$this->comments_array = $new_comments;
	}

	// finds all children of a specified set of parents
	public function findChildren(){
		// we get an error if the whereIn clause is used with an empty parent_ids array
		if( empty( $this->parent_ids ) ){
			$this->limit_children = false;
		}
		// get child comments
		$new_child_comments = Comment::where( 'comment_thread_id', $this->thread_id );
			// this allows us to pickup replies in the heartbeat
			if( $this->limit_children ){
				$new_child_comments = $new_child_comments->whereIn( 'parent_id', $this->parent_ids );
			}
			// we need this in order to show correct replies
			if( !$this->get_earlier ){
				$new_child_comments = $new_child_comments->where( 'comments.id', '>', $this->latest_id );
			}
			$new_child_comments = $new_child_comments
				->whereNotNull( 'parent_id' )
				->leftJoin( 'users AS u', 'u.id', '=', 'comments.user_id_submitted' )
				->leftJoin( 'users AS r', 'r.id', '=', 'comments.reference_user_id' )
				->select( 
					'comments.*',
					'u.profile_img_loc as profile_image',
					'u.id as user_id',
					'u.fname',
					'u.lname',
					'u.is_student',
					'u.is_intl_student',
					'u.is_alumni',
					'u.is_parent',
					'u.is_counselor',
					'u.is_organization',
					'u.comment_anon AS anon',
					'r.fname AS ref_fname',
					'r.lname AS ref_lname',
					'r.comment_anon AS ref_anon' 
				)
			->orderBy( 'comments.id', 'asc' )
			->get()
			->toArray();

		$this->comments_array = $new_child_comments;
	}
}
