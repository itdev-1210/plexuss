<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesApplicationDeclaration extends Model
{
    protected $table = 'colleges_application_declarations';

 	protected $fillable = array('college_id', 'language', 'type');
}
