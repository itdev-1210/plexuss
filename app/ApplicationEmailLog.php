<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationEmailLog extends Model
{
    protected $table = 'application_email_logs';

 	protected $fillable = array('user_id', 'template_name');
}
