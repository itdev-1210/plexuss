<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizzeResults extends Model {

	protected $table = 'quizzes_1_results';

	protected $fillable = array( 'quizzes_1_id', 'user_id', 'is_correct' );

}
