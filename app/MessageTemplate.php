<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageTemplate extends Model {


	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'message_templates';


 	protected $fillable = array( 'college_id', 'name', 'content' );
}