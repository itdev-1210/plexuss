<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLinkPreview extends Model
{
    protected $table = 'sn_link_previews';

 	protected $fillable = array('description', 'image', 'real_url', 'title', 'url', 'embed_code', 'video_id');

 	public function pictures()
 	{
    	return $this->hasMany('App\SocialLinkPreviewPic', 'sn_link_preview_id');
    }
    
    public function pic()
    {
        return $this->belongsTo('App\SocialLinkPreviewPic');
    }
}
