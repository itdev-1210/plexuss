<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickACollegeView extends Model {

	protected $table = 'pick_a_college_views';

 	protected $fillable = array('user_id', 'college_id', 'targetted');

 	public function getNumOfPickACollegeViews($college_id, $start_date = null, $end_date = null){

 		$cnt = PickACollegeView::on('rds1')->where('college_id', $college_id)
 										   ->where('targetted', 1);

 		if (isset($start_date)) {
 			$cnt = $cnt->where('created_at', '>=', $start_date);
 		}
 		if (isset($end_date)) {
 			$cnt = $cnt->where('created_at', '<=', $end_date);
 		}
 		
 		$cnt = $cnt->count();

 		return $cnt;
 	}
}
