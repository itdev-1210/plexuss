<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{

  protected $table = 'sn_post_images';
  protected $fillable = array('post_id', 'post_comment_id', 'social_article_id', 'image_link', 'is_gif', 'gif_link');

  /*
  * Model Relationships
  */

  public function post()
  {
    return $this->belongsTo('App/Post');
  }

  public function article()
  {
    return $this->belongsTo('App/SocialArticle');
  }

  public function user()
  {
    return $this->belongsTo('App/User');
  }

  public function comment()
  {
    return $this->belongsTo('App/PostComment');
  }

}
