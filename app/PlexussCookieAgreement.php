<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlexussCookieAgreement extends Model
{
    protected $table = 'plexuss_cookie_agreements';
    protected $fillable = ['user_id', 'device', 'browser', 'platform', 'countryName', 'stateName', 'cityName', 'updated_at', 'created_at'];
}
