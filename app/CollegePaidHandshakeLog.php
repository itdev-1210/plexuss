<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegePaidHandshakeLog extends Model
{
    protected $table = 'college_paid_handshake_logs';

 	protected $fillable = array('org_branch_id', 'aor_id', 'user_id', 'recruitment_id');
}
