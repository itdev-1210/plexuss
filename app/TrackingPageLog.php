<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageLog extends Model
{
    protected $table = 'tracking_page_logs';

	protected $fillable = array( 'ip', 'user_id', 'city', 'state', 'zip', 'country', 'cnt', 'tpi_id', 'tpt_id' );
}


