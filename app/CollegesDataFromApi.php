<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegesDataFromApi extends Model
{
    protected $table = 'colleges_data_from_apis';

 	protected $fillable = array('ipeds_id', 'school_name', 'city', 'state', 'long_state', 'zip', 'school_url', 'calculator_url', 'locale', 'school_level', 'control_of_school', 'alias', 'longitude', 'latitude', 'undergrad_enroll_1112', 'religious_affiliation', 'graduation_rate_4_year_150', 'accred_agency', 'tuition_avg_in_state_ftug', 'tuition_avg_out_state_ftug', 'private_net_prc_avg_faid_030k', 'public_net_prc_avg_faid_030k', 'private_net_prc_avg_faid_30k48k', 'public_net_prc_avg_faid_30k48k', 'private_net_prc_avg_faid_48k75k', 'public_net_prc_avg_faid_48k75k', 'private_net_prc_avg_faid_75k110k', 'public_net_prc_avg_faid_75k110k', 'private_net_prc_avg_faid_110kover', 'public_net_prc_avg_faid_110kover', 'open_admissions', 'percent_admitted', 'sat_read_75', 'sat_read_25', 'sat_write_75', 'sat_write_25', 'sat_math_75', 'sat_math_25', 'act_composite_75', 'act_composite_25', 'act_english_75', 'act_english_25', 'act_math_75', 'act_math_25', 'act_write_75', 'act_write_25', 'undergrad_grant_pct', 'undergrad_loan_pct');
}
