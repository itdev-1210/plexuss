<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeConversions extends Model {

	protected $table = 'grade_conversions';

	public function getUniqueConversionCountries(){
		$tmp = array();
		$countries = GradeConversions::on('rds1')->select('country')->groupby('country')->get();

		foreach ($countries as $key) {
			$tmp[] = $key->country;
		}

		return $tmp;
	}

	public function getConversionsFor( $country = null ){
		if( !isset($country) ){
			return 'no results';
		}

		$tmp = array();
		$grades = GradeConversions::on('rds1')
					->select('grading_scale', 'scale', 'description', 'us_grade')
					->where('country', $country)
					->get();

		foreach ($grades as $key) {
			$tmp[] = $key;
		}

		return $tmp;
	}

}
