<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\AjaxController;

class RoleBaseEmail extends Model
{
    protected $table = 'role_based_emails';
    protected $fillable = array('prefix');


    public function isRoleBase($email){
    	$tmp = explode("@", $email, 2);

        // This is probably a bad email return!
        if (!isset($tmp[1])) {
            return true;
        }
        
    	//check if plexuss
    	if ($tmp[1] == "plexuss.com") {
    		return false;
    	}

    	$str = $tmp[0]."@";

    	$qry= RoleBaseEmail::on('rds1')->where('prefix', 'LIKE', "%". $str."%")->first();

    	if (isset($qry) && !empty($qry)) {

    		$ac = new AjaxController();
    		$ac->unsubscribeThisEmail($email);
    		
    		return true;
    	}
    	
    	return false;
    }
}
