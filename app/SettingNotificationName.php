<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ViewDataController;
use DB;

class SettingNotificationName extends Model {

	protected $table = 'setting_notification_names';

 	protected $fillable = array('type', 'name');
 	
 	public function getSettingNotifications(){

 		$viewDataController = new ViewDataController();
		$data = $viewDataController->buildData(true);


		$qry = DB::connection('rds1')->table('setting_notification_names as snn')
								   ->join('setting_notification_logs as snl', 'snl.snn_id', '=', 'snn.id')
								   ->where('user_id', $data['user_id'])
								   ->select('snn.type', 'snn.name')
								   ->orderBy('snn.id')
								   ->get();
		$arr = array();
		$arr['text'] = array();
		$arr['email']= array();

		foreach ($qry as $key) {
			$arr[$key->type][$key->name] = false;
	    }						   

	    if (!isset($arr['email']['college_stats_updates']) && !isset($arr['email']['college_news_updates'])
			&& !isset($arr['email']['other_plexuss_notifications']) ){
	    	$arr['email']['all_plexuss_notifications'] = true;
	    }

	    if (!isset($arr['text']['college_stats_updates']) && !isset($arr['text']['college_news_updates'])
			&& !isset($arr['text']['other_plexuss_notifications']) ){
	    	$arr['text']['all_plexuss_notifications'] = true;
	    }

	    if (!isset($arr['email']['wants_to_recruit_you']) && !isset($arr['email']['viewed_your_profile']) &&
	    	!isset($arr['email']['sent_you_a_message']) && !isset($arr['email']['handshakes_with_colleges'])) {
	    	$arr['email']['all_school_notifications'] = true;
	    }

	    if (!isset($arr['text']['wants_to_recruit_you']) && !isset($arr['text']['viewed_your_profile']) &&
	    	!isset($arr['text']['sent_you_a_message']) && !isset($arr['text']['handshakes_with_colleges'])) {
	    	$arr['text']['all_school_notifications'] = true;
	    }

	    
	    return $arr;
 	}
}