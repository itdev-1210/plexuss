<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use DB;

class CollegesInternationalMajor extends Model
{
    protected $table = 'colleges_international_majors';
	protected $fillable = array('cit_id', 'college_id', 'type', 'val');

	public function getMajors($college_id){

		if (Cache::has(env('ENVIRONMENT') .'_getMajors_for_colleges_college_id_'. $college_id)) {
			$ret = Cache::get(env('ENVIRONMENT') .'_getMajors_for_colleges_college_id_'. $college_id);
			return $ret;
		}

		$ret = array();

		$qry = CollegesInternationalMajor::on('rds1')->where('college_id', $college_id)->get();


		foreach ($qry as $key) {
			$val_arr   = explode(",", $key->val);
			$dept_id   = $val_arr[0];
			$major_id  = $val_arr[1];
			$degree_id = $val_arr[2];

			if (isset($ret[$degree_id])) {
				if (isset($major_id) && !empty($major_id)) {
					$ret[$degree_id][$major_id] = (int)$major_id;
				}else{
					$tmp = DB::connection('rds1')->table('majors')
												 ->where('department_id', $dept_id)
												 ->pluck('id');

					// $ret[$degree_id] = array_merge($ret[$degree_id], $tmp);
					if( isset($tmp) && !empty($tmp) ){
						foreach ($tmp as $key) {
							$ret[$degree_id][$key] = $key;
						}
					}
					
				}
			}else{
				$ret[$degree_id] = array();

				if (isset($major_id) && !empty($major_id)) {
					$ret[$degree_id][$major_id] = (int)$major_id;
				}else{
					$tmp = DB::connection('rds1')->table('majors')
												 ->where('department_id', $dept_id)
												 ->pluck('id');

					// $ret[$degree_id] = array_merge($ret[$degree_id], $tmp);

					if( isset($tmp) && !empty($tmp) ){
						foreach ($tmp as $key) {
							$ret[$degree_id][$key] = $key;
						}
					}
					
				}

			}
		}

		Cache::put(env('ENVIRONMENT') .'_getMajors_for_colleges_college_id_'. $college_id, $ret, 1440);

		return $ret;
	}
}
