<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingDataPreferenceLog extends Model
{
    protected $table = 'setting_data_preference_logs';

 	protected $fillable = array('user_id', 'optin', 'lcca', 'st_patrick', 'lbsf', 'gisma', 'aul', 'bsbi', 'tsom', 'tlg', 'ulaw');
}
