<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAppliedCollegesDeclaration extends Model {

	protected $table = 'users_applied_colleges_declarations';

 	protected $fillable = array('user_id', 'declaration_id');
}