<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentConvert extends Model
{
   	
   	protected $table = 'recruitment_converts';
 	protected $fillable = array( 'rec_id', 'converted_id' );
}
