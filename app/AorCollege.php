<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\AorPortal, App\OrganizationBranch, App\CollegeRecommendationFilters, App\User;
use App\Http\Controllers\Controller;

class AorCollege extends Model
{
    protected $table = 'aor_colleges';

 	protected $fillable = array('aor_id', 'college_id', 'aor_only');

 	public function addAORCondition($college_id, $user_id){

 		$aorMatches = array();

 		$aors = DB::connection('rds1')->table('aor_colleges')
 			->where('college_id',$college_id)
			->join('aor','aor_colleges.aor_id','=','aor.id')
			->select('aor.id as aor_id','aor.distinction','aor.overlap_allowed')
			->orderby('aor.cost_per_handshake', 'DESC')
			->get();

		$tmp_branch_id = OrganizationBranch::on('rds1')
						->where('school_id','=',$college_id)
						->first();


		if (!isset($tmp_branch_id->id)) {
			$aors = array();
		}

		if(!empty($aors)){
			foreach($aors as $key){

				if($key->distinction == 'none'){
					$aorMatches[] = array('aor_id' => $key->aor_id);
					return $aorMatches;
				}

				elseif($key->distinction == 'filter'){

					$portals = AorPortal::on('rds1')
						->where('aor_id', $key->aor_id)
						->select('id')
						->get();

					$fltr_data['org_branch_id'] = OrganizationBranch::on('rds1')
						->where('school_id','=',$college_id)
						->pluck('id');
					$fltr_data['org_branch_id'] = $fltr_data['org_branch_id'][0];

					$fltr_data['org_school_id'] = $college_id;
					$fltr_data['aor_id'] = $key->aor_id;

					$all_portals = array($fltr_data);

					if(isset($portals)){
						$all_portals = array();
						foreach($portals as $portal){
							$fltr_data['aor_portal_id'] = $portal->id;
							$all_portals[] = $fltr_data;
						}
					}

					foreach($all_portals as $data){

						$crf = new CollegeRecommendationFilters;

						$qry = $crf->generateFilterQry($data);

						if(isset($qry)){

							$cr  = new Controller();
							$qry = $cr->getRawSqlWithBindings($qry);
							$qry = $qry.' and userFilter.id = '.$user_id;

							$qry = DB::select($qry);

							if (!empty($qry)){
								if($key->overlap_allowed == 0 && empty($aorMatches)){
									$aorMatches[] = array('aor_id' => $key->aor_id);
									return $aorMatches;
								}
								elseif($key->overlap_allowed == 1){
									$aorMatches[] = array('aor_id' => $key->aor_id);
									break;
								}
							}
						}
					}
						
				}
				elseif($key->distinction == 'online-us'){
					$match = User::on('rds1')
						->leftjoin('country_conflicts as cc','users.id','=','cc.user_id')
						->whereIn('interested_school_type',array(1,2))
						->where('country_id','=',1)
						->where('users.id', $user_id)
						->whereNull('cc.id')
						->select('interested_school_type')
						->first();

					if(!empty($match)){
						if($match->interested_school_type == 1){
							$aorMatches[] = array('aor_id' => $key->aor_id);
						}else{
							$aorMatches[] = array('aor_id' => $key->aor_id);
							$aorMatches[] = array('aor_id' => null);
						}
					}
					else{
						$aorMatches[] = array('aor_id' => null);
					}
				}
			}
		}

		if(empty($aorMatches)){
			$aorMatches[] = array('aor_id' => null);
		}

		return $aorMatches;
	}

	public function getAORCollegeIds($aor_id){

 		$aor = DB::connection('rds1')->table('aor_colleges as ac')
 									 ->where('aor_id','=',$aor_id)
 									 ->pluck('college_id');

 		return $aor;
 	}
}
