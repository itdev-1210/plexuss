<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PressReleaseContact extends Model {

	protected $table = 'press_release_contacts';

 	protected $fillable = array('email', 'email_sent');
}

