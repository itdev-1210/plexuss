<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    //
    	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'contact_us';


 	protected $fillable = array( 'fname', 'lname', 'email', 'phone', 'company', 'tell_us_more', 'type' );
}
