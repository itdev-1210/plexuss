<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationPortalUser extends Model {


	protected $table = 'organization_portal_users';

 	protected $fillable = array( 'org_portal_id', 'user_id', 'is_default' );
 	
}

