<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPageUrlDictionaryLog extends Model
{
    protected $table = 'tracking_page_url_dictionary_logs';

    protected $fillable = array( 'tpu_id', 'tpud_id'  );
}
