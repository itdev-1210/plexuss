<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortalNotification extends Model {


	protected $table = 'portal_notifications';

	protected $fillable = array( 'user_id', 'school_id', 'is_accepted', 'is_viewing','is_viewing_trash', 'is_recommend','is_recommend_trash','is_recruiting', 'is_recruit_trash', 'is_major_recommend', 'is_department_recommend', 'is_higher_rank_recommend', 'is_lower_tuition_recommend', 'is_top_75_percentile_recommend', 'recommend_based_on_college_id');
	
}
