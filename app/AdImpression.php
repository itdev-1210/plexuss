<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdImpression extends Model
{
    protected $table = 'ad_impressions';

 	protected $fillable = array('company', 'ip', 'user_id', 'slug', 'device', 'browser', 'platform',
 								'countryName', 'stateName', 'cityName', 'pixel_tracked', 'ad_copy_id');
}
