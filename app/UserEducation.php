<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    protected $table = 'sn_users_educations';

 	protected $fillable = array('user_id', 'edu_level', 'school_id', 'school_name', 'degree_type', 'grad_year', 'is_verified');

 	public function user()
  	{
    	return $this->belongsTo('App\User');
  	}

  	public function degree()
	{
	    return $this->belongsTo('App\Degree', 'degree_type', 'id');
	}

	public function majors()
	{
	    return $this->hasMany('App\UserEducationMajor', 'sue_id');
	}

	public function highschool(){
        return $this->belongsTo('App\Highschool', 'school_id', 'id')
                    ->where('verified', 1);
    }

    public function college(){
        return $this->belongsTo('App\College', 'school_id', 'id')
                    ->where('verified', 1);
    }
}
