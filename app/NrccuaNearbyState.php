<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaNearbyState extends Model
{
    protected $table = 'nrccua_nearby_states';
}
