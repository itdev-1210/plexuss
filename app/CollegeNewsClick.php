<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeNewsClick extends Model
{
    protected $table = 'college_news_clicks';

 	protected $fillable = array('news_link', 'ip', 'user_id', 'slug', 'device', 'browser', 'platform',
 								'countryName', 'stateName', 'cityName');
}
