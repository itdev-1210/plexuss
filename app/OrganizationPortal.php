<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\College;

class OrganizationPortal extends Model {


	protected $table = 'organization_portals';

 	protected $fillable = array( 'org_branch_id', 'name', 'num_of_applications', 'num_of_enrollments', 'active', 'priority',
 								 'premier_trial_begin_date', 'premier_trial_end_date', 'num_of_filtered_rec', 'ro_id' );

 	public function getUsersOrgnizationPortals($org_branch_id, $user_id, $showActiveOnly = null){

		$op = DB::connection('rds1')->table('organization_portals as op')
									->leftjoin('organization_portal_users as opu', 'op.id', '=', 'opu.org_portal_id')
									->leftjoin('revenue_organizations as ro', 'ro.id', '=', 'op.ro_id')
											->where('opu.user_id', $user_id)
											->where('op.org_branch_id', $org_branch_id)
											->select('op.*', 'opu.is_default', 'ro.type as ro_type');
		if (isset($showActiveOnly)) {
			$op = $op->where('op.active', 1);
		}

		$op = $op->orderBy(DB::raw('ISNULL(op.priority), op.priority'), 'ASC')
				 ->get();		

		return $op;
	}

	public function getUsersOrgnizationPortalsByOrgBranchId($org_branch_id, $user_id = null){
		$op = DB::connection('rds1')->table('organization_portals as op')
									->leftjoin('organization_portal_users as opu', 'op.id', '=', 'opu.org_portal_id')
									->leftjoin('organization_branch_permissions as obp', function($join)
									{
									    $join->on('obp.user_id', '=', 'opu.user_id');
									    $join->on('obp.organization_branch_id', '=', 'op.org_branch_id');
									})
									->leftjoin('users as u', 'u.id', '=', 'opu.user_id')
									->where('op.org_branch_id', $org_branch_id)
									->select('op.*', 'opu.is_default', 'opu.user_id', 'u.email', 'obp.super_admin');

		if (isset($user_id)) {
			$op = $op->where('opu.user_id', $user_id);
		}
		
		$op = $op->orderBy(DB::raw('ISNULL(op.priority), op.priority'), 'ASC')
				 ->get();											
		return $op;
	}

	public function getOrganizationPortalsForThisCollege($college_id){

		if (!isset($college_id)) {
			return NULL;
		}
		$qry = College::on('rds1')->leftjoin('organization_branches as ob', 'ob.school_id', '=', 'colleges.id')
											 ->leftjoin('college_recommendation_filters as crf', 'crf.college_id', '=', 'colleges.id')
								 			 ->join(DB::raw('(SELECT op.id AS op_id FROM organization_portals AS op where active = 1) AS t2'),function($join)
				 				 			 {
								 		    	// $join->on(DB::raw('t2.op_id = crf.org_portal_id'), 'OR', DB::raw('crf.org_portal_id IS NULL'));
								 		    	$join->orWhere('t2.op_id', '=', 'crf.org_portal_id')
								 		    		 ->orWhereNull('crf.org_portal_id');
								 			 })
											->leftjoin('organization_portals as op', 'ob.id', '=', 'op.org_branch_id')
											->leftjoin('aor_portals as ap', 'ap.id', '=', 'crf.aor_portal_id')
								 		 	->select(  'op.id as org_portal_id', 'crf.aor_id', 'crf.aor_portal_id',
								 		 		       'colleges.school_name', 'colleges.id',
								 		 		       'op.name as org_portal_name',
								 		 		       'ap.name as aor_portal_name')
											->where('colleges.id', $college_id)
											->where(function($q){
												$q->where('op.id', '!=', DB::raw('""'))
												  ->orWhere('crf.aor_portal_id', '!=', DB::raw('""'));
											})
											->groupby('op.id', 'crf.college_id');

		$qry = $qry->orderBy(DB::raw('ISNULL(op.priority), op.priority'), 'ASC')
				   ->get();

		return $qry;

	}
}

