<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model {

	protected $table = 'product_orders';

	protected $fillable = array( 'user_ccp_info_id', 'product_transaction_id', 'ccp_id', 'quantity', 'price', 'status', 'personal_msg_to', 'personal_msg_from', 'personal_msg_body');

	public function userccpinfo()
    {
        return $this->belongsTo('ProductUserCCPInfo');
    }
}
