<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateSenderProvider extends Model
{
    protected $table = 'email_template_sender_providers';

 	protected $fillable = array('template_name', 'provider');
}
