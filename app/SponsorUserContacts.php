<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SponsorUserContacts extends Model
{
    protected $table = 'sponsor_user_contacts';

 	protected $fillable = array( 'user_id', 'option', 'fname', 'lname', 'phone', 'phone_code', 'email', 'contact_name', 'title', 'org_name', 'optin', 'relation', 'created_at', 'updated_at' );

	public $timestamps = true;

 	public static function saveUserContacts($user_id, $input) {
 		if (isset( $input['option'] ) && isset( $input['optin'] ) && isset( $input[ $input['option'] ] )) {
 		 	$option = $input['option'];
 		 	$optin = $input['optin'];
	 		$sponsors = $input[$option];

	 		// $number_of_entries represents the number of sponsors the user sees in the front end.
	 		// The sponsor indices that are higher will be deleted from the db.
	 		$number_of_entries = $input['number_of_entries'];

	 		foreach ($sponsors as $index => $sponsor) {
	 			$sponsor['user_id'] = $user_id;
	 			$sponsor['option'] = $option;
	 			$sponsor['optin'] = $optin;

	 			if (isset( $sponsor['id'] )) {
	 				if ($number_of_entries < (int) $index + 1) {
	 					SponsorUserContacts::find($sponsor['id'])->delete();
	 				} else {
	 					SponsorUserContacts::updateOrCreate(['id' => $sponsor['id']], $sponsor);
	 				}
	 			} else if ($number_of_entries >= (int) $index + 1) {
	 				SponsorUserContacts::create($sponsor);
	 			}
	 		}
 		}
 		
	 	return SponsorUserContacts::getUserContacts($user_id);
 	}

 	public static function getUserContacts($user_id) {
 		$sponsors = SponsorUserContacts::on('rds1')->where('user_id', $user_id)->get();
 		$index = 0;
 		$result = [];

 		// The sponsors contact data is in $sponsors, 
 		// the following code is to help the OneApp front end render the result.
 		foreach ($sponsors as $sponsor) {
 			if (isset($sponsor->option)) {
	 			$option = $sponsor->option;

				$result['sponsor_will_pay_option'] = $option;
				$result['sponsor_will_pay_optin'] = $sponsor->optin;

	 			$result['sponsor_will_pay_id_' . $index] = $sponsor->id;
	 			$result['sponsor_will_pay_phone_' . $index] = $sponsor->phone;
	 			$result['sponsor_will_pay_phone_' . $index . '_code'] = $sponsor->phone_code;
	 			$result['sponsor_will_pay_email_' . $index] = $sponsor->email;

	 			if ($option == 'relative') {
		 			$result['sponsor_will_pay_relation_' . $index] = $sponsor->relation;
	 			}

	 			if ($option == 'parent' || $option == 'relative') {
		 			$result['sponsor_will_pay_fname_' . $index] = $sponsor->fname;
		 			$result['sponsor_will_pay_lname_' . $index] = $sponsor->lname;
	 			}

	 			if ($option == 'sponsor') {
		 			$result['sponsor_will_pay_org_name_' . $index] = $sponsor->org_name;
		 			$result['sponsor_will_pay_contact_name_' . $index] = $sponsor->contact_name;
		 			$result['sponsor_will_pay_title_' . $index] = $sponsor->title;
	 			}

	 			$index++;
	 		}
 		}
		
		$result['sponsor_number_of_entries'] = $index; 	

 		return $result;
 	}
}
