<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionClientFieldMapping extends Model {

	protected $table = 'distribution_client_field_mappings';

 	protected $fillable = array('dc_id','client_field_name', 'plexuss_field_id', 'field_type', 'is_required');
}
