<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NrccuaMajorsMapping extends Model
{
    protected $table = 'nrccua_majors_mapping';

    protected $fillable = array( 'plexuss_majors_id', 'nrccua_majors_id', 'created_at', 'updated_at' );

    public $timestamps = true;
}
