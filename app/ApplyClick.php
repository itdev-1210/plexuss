<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplyClick extends Model
{
    protected $table = 'apply_clicks';

 	protected $fillable = array('user_id', 'slug', 'device', 'browser', 'platform', 'ip', 'pixel_fired_url', 'source',
 								'countryName', 'stateName', 'cityName', 'pixel_tracked', 'pixel_fired_at', 'org_app_url');

}
