<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersPortalEmailEffortLogsDateId extends Model
{
    protected $table = 'users_portal_email_effort_logs_date_ids';

 	protected $fillable = array( 'date', 'timestamp', 'upeel_id' );
}
