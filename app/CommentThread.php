<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentThread extends Model
{
    protected $table = 'comment_threads';

	/*
		$thread_checker = new CommentThread;
		$thread_checker->setRecord( $news_article );
		$has_thread = $thread_checker->check();
		if( !$has_thread ){
			$thread_checker->create();
			$thread_checker->store();
	 */

	// For thread checker: we check if this item's comment_thread_id is null
	private $record;
	// For thread checker: assigned once we create a new thread
	private $new_thread_id;

	public function setRecord( $record ){
		$this->record = $record;
	}

	public function getNewThreadId(){
		return $this->new_thread_id;
	}

	/***********************************************************************
	 *========================= CHECK FOR THREAD ID ========================
	 ***********************************************************************
	 * Checks for a thread id based on the users's set $record object.
	 * @return		boolean			returns true if has thread id, false if not
	 */
	public function checkHasThreadId(){
		$record = $this->record;
		$thread_id = $record->comment_thread_id;
		if( !$thread_id ){
			return false;
		}
		return true;
	}
	/***********************************************************************/

	/***********************************************************************
	 *========================== CREATE NEW THREAD =========================
	 ***********************************************************************
	 * Creates a new comment thread and assigns it to the given $record object.
	 * Also assigns $new_thread_id on success
	 * @return			boolean			true on success, false on failure
	 */
	public function createNewThread(){
		$record = $this->record;

		// make new thread
		$new_thread = new CommentThread;
		// get new id
		$thread_id = $new_thread->insertGetId( array() );

		// save new thread_id to record
		$record->comment_thread_id = $thread_id;
		$success = $record->save();

		if( $success ){
			$this->new_thread_id = $thread_id;
			return true;
		}
		return false;
	}
	/***********************************************************************/
}
