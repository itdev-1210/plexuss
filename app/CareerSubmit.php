<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerSubmit extends Model
{
    protected $table = 'careers-submit';

	protected $fillable = ['fname','lname','email','phone','zipcode','school','grade_level','counselor','gpa','position', 'camid', 'specid', 'pixel', 'referer'];
}
