<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEducationMajor extends Model
{
    protected $table = 'sn_users_education_majors';

 	protected $fillable = array('major_id', 'sue_id', 'is_minor');


 	public function userEducation()
    {
   		return $this->belongsTo('App\UserEducation');
    }

    public function mmmmm(){
    	return $this->hasMany('App\Major', 'id1');	
    }
}
