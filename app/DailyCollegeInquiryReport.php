<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyCollegeInquiryReport extends Model
{
    //
    protected $table = 'daily_college_inquiry_reports';
    protected $fillable = array('org_branch_id', 'is_pixel_set', 'active', 'fname', 'email', 'client_name', 'last_sent');

    public static function updateLastSent($id) {
        $right_now = date('Y-m-d H:i:s');

        DailyCollegeInquiryReport::where('id', '=', $id)->update(['last_sent' => $right_now]);

        return 'success';
    }
}
