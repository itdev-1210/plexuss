<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrgSavedAttachment extends Model
{
    protected $table = 'org_saved_attachments';

 	protected $fillable = array('name', 'agency_id', 'org_branch_id', 'url', 'created_at');
}
