<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedisUserList extends Model
{
    protected $table = 'redis_user_lists';

 	protected $fillable = array('is_online', 'user_id', 'client_id', 'connected_at', 'disconnected_at');
}
