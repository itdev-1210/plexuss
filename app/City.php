<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

 	protected $fillable = array( 'state_id', 'name', 'zc_id', 'zip_code' );

}
