<?php

namespace App;
use DB;
use App\User, App\ProfanityList;

use Illuminate\Database\Eloquent\Model;

class ProfanityList extends Model {

	protected $table = 'profanity_list';


	public function hasProfanity($user_id){
		$user = User::on('rds1')->find($user_id);

		$email = substr($user->email, 0, strpos($user->email, "@"));

		$qry = ProfanityList::on('rds1')->where(function($q) use ($user, $email){
										$q->orWhere('name', 'LIKE', '%'.$user->fname.'%')
										  ->orWhere('name', 'LIKE', '%'.$user->lname.'%')
										  ->orWhere('name', 'LIKE', '%'.$email.'%');
										})
										->first();


		if (isset($qry)) {
			return true;
		}
		return false;
	}
}
