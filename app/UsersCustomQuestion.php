<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class UsersCustomQuestion extends Model {

	protected $table = 'users_custom_questions';
	
 	protected $fillable = array('user_id', 'application_state', 'application_state_id', 'christian_interested', 'is_transfer', 'alternate_name', 'preferred_phone', 'alternate_preferred_phone', 'alternate_phone','address2', 'preferred_alternate_phone', 'alternate_line1', 'alternate_line2', 'alternate_city', 'alternate_state_id', 'alternate_state', 'alternate_country_id', 'alternate_zip', 'country_of_birth', 'city_of_birth', 'citizenship_status', 'languages', 'num_of_yrs_in_us', 'num_of_yrs_outside_us', 'parents_married', 'siblings', 'dual_citizenship_country', 'essay_content','application_terms_of_conditions', 'application_signature', 'application_signature_date', 'application_submitted','passport_expiration_date', 'passport_number', 'living_in_us', 'emergency_contact_name', 'emergency_phone', 'emergency_phone_code',
 		'financial_plan','have_sponsor', 'ielts_date', 'took_pearson_versant_exam', 'toefl_ibt_date', 'name_of_hs', 'city_of_hs', 'country_of_hs', 'hs_start_date', 'hs_end_date', 'gap_in_academic_record', 'attended_additional_institutions', 'academic_misconduct', 'behavior_misconduct', 'criminal_offense', 'academic_expulsion', 'misdemeanor', 'disciplinary_violation', 'guilty_of_crime', 'i20_end_date', 'i20_institution', 'i20_dependents', 'addtl__post_secondary_school_type', 'addtl__post_secondary_name', 'addtl__post_secondary_city', 'addtl__post_secondary_country', 'addtl__post_secondary_start_date', 'addtl__post_secondary_end_date', 'addtl__post_secondary_resume', 'num_of_yrs_outside_us', 'num_of_yrs_in_us', 'emergency_contact_relationship', 'home_phone', 'is_hispanic', 'have_allergies', 'have_medical_needs', 'have_dietary_restrictions', 'have_student_visa_and_will_transfer', 'need_form_i20_for_visa', 'racial_category', 'visa_type', 'visa_expiration', 'i94_expiration', 'took_pearson_versant_exam_date', 'took_pearson_versant_exam_score', 'emergency_contact_address', 'emergency_contact_email', 'devry_funding_plan',
 			'already_attended_school_of_management_or_nursing', 'graduate_of_carrington_or_chamberlain', 
 			'fathers_name', 'fathers_job', 'mothers_name', 'mothers_job', 'guardian_name', 'guardian_job', 'parents_have_degree', 'why_did_you_apply',
 			'parent_guardian_email', 'understand_health_insurance_is_required', 'have_any_of_the_following_conditions', 'have_good_physical_and_mental_health',
 			'state_of_hs', 'hs_completion_status', 'have_graduated_from_a_university', 'planning_to_take_esl_classes', 'have_attended_language_school', 
 			'academic_goal', 'have_dependents', 'have_currency_restrictions', 'lived_at_permanent_addr_more_than_6_months', 'mailing_and_permanent_addr_same',
 			'contact_preference', 
 			'was_instruction_taught_in_english', 'fathers_addr', 'fathers_city', 'fathers_district', 'fathers_country', 'mothers_addr', 'mothers_city', 
 			'mothers_district', 'mothers_country', 'guardian_addr', 'guardian_city', 'guardian_district', 'guardian_country', 
 			'academic_goal__complete_associate_or_certificate', 'academic_goal__improve_english', 'academic_goal__meet_transfer_reqs_for_bachelors_degree', 
 			'academic_goal__prep_for_graduate_school', 'illnesses', 'lang_school_completed_current_level', 'date_attended_lang_school',
 			'num_of_yrs_studied_english_after_hs', 'have_been_dismissed_from_school_for_disciplinary_reasons', 'have_used_drugs_last_12_months',
 			'applying_for_admission_school', 'plan_to_enroll_in', 'understand_I_need_to_submit_medical_examination_form', 'academic_goals_essay',
			'have_you_graduated_from_hs', 'program_you_are_interested_in', 'understand_christian_position_of_liberty', 'wish_to_study_at_christian_university',
			'liberty_housing_requirements', 'are_you_christian', 'faith_essay', 'seeking_u_of_arkansas_degree', 'who_graduated_from_u_of_arkansas',
			'previously_attended_u_of_arkansas', 'are_graduating_from_hs', 'will_have_fewer_than_24_transferrable_credits', 
			'will_have_more_than_24_transferrable_credits', 'have_earned_undergrad_grad_pro_degree',
			'applying_to_esl_program', 'previous_college_experience', 'how_did_you_hear_about_msoe', 'financial_support_provided_by',
			'num_of_yrs_planning_on_studying_at_pccd', 'liberty_housing_requirements__residence_hall', 'liberty_housing_requirements__off_campus',
			'have_you_graduated_from_hs__have_graduated_on_this_date', 'have_you_graduated_from_hs__will_graduate_on_this_date',
			'peralta_have_graduated_on_this_date', 'peralta_will_graduate_on_this_date', 'name_of_church', 'financial_support_provided_by__student',
			'financial_support_provided_by__student_parents', 'financial_support_provided_by__private_sponsor', 'financial_support_provided_by__govt_scholarship', 
			'financial_support_provided_by__athletic_scholarship', 'financial_support_provided_by__other', 'intend_to_follow_code_of_conduct',

			'will_study_english_prior_to_attending_devry', 'plan_to_enroll_in__esl_program', 'plan_to_enroll_in__academic_program', 
			'have_you_graduated_from_hs__no_will_not_graduate', 'plan_to_take_ielts', 'family_income');



		public function getUserStatus($id){

			$res = DB::table('users_custom_questions')->select('application_state')->where('user_id' , '=', $id)->first();
			if($res){
				return $res->application_state;
			}else{
				return 'basic';
			}
		}
} 
