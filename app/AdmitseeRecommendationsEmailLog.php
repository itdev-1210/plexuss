<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmitseeRecommendationsEmailLog extends Model
{
    protected $table = 'admitsee_recommendations_email_logs';

 	protected $fillable = array('user_id', 'essay_id');
}
