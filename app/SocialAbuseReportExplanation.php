<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAbuseReportExplanation extends Model
{
    protected $table = 'sn_abuse_report_explanations';

	protected $fillable = array( 'content' );
}
